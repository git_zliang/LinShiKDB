/**
 * Created by ��־�� on 2016/10/21.
 */
export var Comment = {
    objectToString(obj){
        var des = "";
        for(var i in obj){
            var pro = obj[i];
            des +=i+"="+pro+"\n";
        }
        alert(des);
    },
    getRandomString(len) {
        var charss = 'ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678'; // 默认去掉了容易混淆的字符oOLl,9gq,Vv,Uu,I1
        var maxPos = charss.length;
        var pwd = '';
        for (var i = 0; i < len; i++) {
            pwd += charss.charAt(Math.floor(Math.random() * maxPos));
        }
        return pwd;
    },
    SETPAGESIZE:{
        "num1":20,
        "num2":50,
        "num3":100
    },
    SETENCODING:{
        "num1":"GBK",
    },
    SETLANGUAGE:{
    },
    SETSEPARATE:{
        "num1":",",
    },
    SETMETHOD:{
        "num1":"FTP",
        "num2":"SFTP",
    },
    ERROR500:"role permissions error",
    LOGINSUCCESS:"loginsuccess",
    LOGOUT:"logout",
    GETUSERLIST:"getuserlist",
    GETDICTSLIST:"getdictlist",
    DELETEDICT:"deletedict",
    GETDICTDATA:"getdictdata",
    DELETEPROJECT:"deleteproject",
    GETPROJECTSLIST:"getprojectslist",
    PROJECTCLICK:"projectclick",
    DELETEUSER:"deleteuser",
    GETDICTDATALIST:"getdictdatalist",
    DELETEDICTFILE:"deletedictfile",
    GETDICTDATAFILE:"getdictdatafile",
    GETPROJECTID:"getprojectid",
    GETPROJECTDICT:"getprojectdict",
    GETSOUREBDSODS:"getsourebdsods",
    GETFEATURESdEF:"getfeaturesdef",
    GETFEATUREDETAIL:"getfeaturedetail",
    GETFEATURESdEFLIST:"getfeaturesdeflist",
    GETFEATUREDETAILLIST:"getfeaturedetaillist",
    GETFEATUREPROJECT:"getfeatureproject",
    GETSUCCESSBDSODS:"getsuccessbdsods",
    GETMETADATA:"getmetadata",
    GETTABLESMETADATA:"gettablesmetadata",
    EDITINDEXTEXT:"editindextext",
    GETIFRUNBAR:"getifrunbar",
    GETIFRUNBARID:"getifrunbarid",
    GETIFRUNBARTIME:"getifrunbartime",
    GETIFRUNBARRUN:"getifrunbarrun",
    GETIFRUNBARS:"getifrunbars",
    GETAPPROVERNUM:"getappovernum",
    TAMRLOGINSUCCESS:"tamrloginsuccess",
    GETPARTICIPLELIST:"getparticiplelist",
    GETPARTICIPLELISTZZL:"getparticiplelistzzl",
    GETPARTICIPLEDATALIST:"getparticipledatalist",
    GETPARTICIPLEDATADDLIST:"getparticipledataddlist",
    GETPARTICIPLEDATADDLISTZZL:"zzltextfile",
    GETPARTICIPLEDATADDLISTZZLURL:"zzlurl",
    GETPARTICIPLECONFLIST:"getparticipleconflist",
    GETPARTICIPLEDATALZZ:"getdatapalezz",
    GETPARTICIPLEPROJECT:"getparticipleproject",
    DATA_URL:"https://192.168.1.188:8883/",
   // TAMR_URL:"https://192.168.1.188:8883/KDB/eseasky/index.html"
    //DATA_URL:"http://localhost:8080/"
};