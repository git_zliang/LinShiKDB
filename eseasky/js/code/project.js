/**
 * Created by ��־�� on 2017/2/17.
 */
import React from 'react';
import crypto from "crypto";
import {Comment} from "../comment";
import {IfDispatcher} from "../store/dispatcher";
import Underscore from "underscore";

export var Project = React.createClass({
    contextTypes: {
        router: React.PropTypes.object.isRequired,
    },
    getInitialState(){

        return{
            projectname:null,
            describes:null,
            filtertext:"",
            iferror:false,
            ifduplicate:false,
            projectindex:null,
            ifNoChange:false,


        }
    },
    componentDidMount(){
        var loginInfo = this.props.loginInfo;
        if(!loginInfo.name || loginInfo.iffrist){

            this.context.router.push("/fronpage");
            IfDispatcher.dispatch({
                actionType:Comment.LOGOUT
            });
        }else{

            this.getprojectList();
        }
    },

    /**
     *The function name: getprojectList.
     *Function used: API for getting the list of project with userId.
     *Create time: 2017/3/9
     */
    getprojectList(){
        $("#ajaxloading").show();
        var loginInfo = this.props.loginInfo;
        if(loginInfo.userid){
            $.ajax({
                url: Comment.DATA_URL+"KDB/FindProject",
                data:$.param({
                userid:loginInfo.userid,
                }),
                type: 'GET',
                success: function(data){
                    $("#ajaxloading").hide();
                    var msg = data;
                    IfDispatcher.dispatch({
                        actionType: Comment.GETPROJECTSLIST,
                        projectslist:msg.data
                    });
                },
                error:function(){
                    $("#ajaxloading").hide();
                },
                statusCode:{
                    414:function(){
                    },
                    406:function (){
                        IfDispatcher.dispatch({
                            actionType:Comment.LOGOUT
                        });
                    }
                }
            });
        }
    },

    /**
     *The function name: handleChange.
     *Function used:Get the value of the input box's and change some state of error in real time.
     *Create time: 2017/3/9
     */
    handleChange(name,event){

        var newstate ={};
        newstate[name] = event.target.value;
        newstate.iferror = false;
        newstate.ifduplicate=false;
        newstate.ifNoChange=false;
        this.setState(newstate);

    },

    /**
     *The function name: addprojectlick.
     *Function used:Pop up a window to create a new project
     *Create time: 2017/3/9
     */
    addprojectlick(){
        $("#addproject").modal("show");
        this.setState({
            iferror:false
        })
    },

    /**
     *The function name: confirmNew.
     *Function used:A API for creating a new project
     *Create time: 2017/3/9
     */
    confirmNew(){

        if(!this.state.projectname || !this.state.describes){
            this.setState({
                iferror:true
            })

        }else{
            $("#ajaxloading").show();
            var loginInfo = this.props.loginInfo;
            var that = this;
            $.ajax({
                url: Comment.DATA_URL+"KDB/AddProject",
                data: $.param({
                    projectname:that.state.projectname,
                    projectdes:that.state.describes,
                    userid:loginInfo.userid,
                }),
                type: 'POST',
                contentType: 'application/x-www-form-urlencoded',
                success: function (data) {
                    $("#addproject").modal("hide");
                    $("#ajaxloading").hide();
                    that.getprojectList();
                },
                error:function(){
                    $("#ajaxloading").hide();
                },
                statusCode:{
                    414:function(){
                        alert("add project fail!");
                    },
                    406:function (){
                        IfDispatcher.dispatch({
                            actionType:Comment.LOGOUT
                        });
                    },
                    413:function () {
                        that.setState({
                            ifduplicate:true
                        })
                        $('#addproject').on('hide.bs.modal', function () {
                            that.setState({
                                ifduplicate:false
                            })
                        })
                    }

                }
            });
        }
    },

    /**
     *The function name: projectclick.
     *Function used:View details of a project
     *Create time: 2017/3/9
     */
    projectclick(name,id){

        var that = this;
        var inFunc = function () {
            //var btn = document.getElementById("Projectzzl");
            //btn.style.background = "#E0E0E0";
            //var btn1 = document.getElementById("Dictionaryzzl");
            //btn1.style.background = "#E0E0E0";
            //var btn2 = document.getElementById("DataFeaturezzl");
            //btn2.style.background = "#E0E0E0";
            //var btnClor = {};
            //btnClor .btn = btn.style.background;
            //btnClor .btn1 = btn1.style.background;
            //btnClor .btn2 = btn2.style.background;
            //localStorage.setItem("btnClor",JSON.stringify(btnClor));
            IfDispatcher.dispatch({
                actionType: Comment.PROJECTCLICK,
                porname: name,
                projectid :id

            });
            that.context.router.push("/dictdata")
        };
        return inFunc;
    },

    /**
     *The function name: projectdelclick.
     *Function used:Pop up a window to comfirm delete a  project
     *Create time: 2017/3/9
     */
    projectdelclick(id){
        var that = this;
        var inFunc = function(){
            that.state.projectindex = id;
            $("#deleteproject").modal("show");

        };
        return inFunc;
    },

    /**
     *The function name: deleteProjectConfirm.
     *Function used:API for sending a request to delete a project
     *Create time: 2017/3/9
     */
    deleteProjectConfirm(){
        $("#deleteproject").modal("hide");
        var that = this;
        $("#ajaxloading").show();
        $.ajax({
            url:Comment.DATA_URL+"KDB/DelProject  ",
            data: $.param({
                projectid:that.state.projectindex,
                _method:"delete"
            }),
            type:'POST',
            contentType: 'application/x-www-form-urlencoded',
            success:function(data){
                $("#ajaxloading").hide();
                that.getprojectList();
            },
            error:function () {
                $("#ajaxloading").hide();
            },
            statusCode:{
                406:function (){
                    IfDispatcher.dispatch({
                        actionType:Comment.LOGOUT
                    });
                }
             }

        });
    },

    /**
     *The function name:filterChange.
     *Function used:Get the value of the search box's in real time.
     *Create time: 2017/3/9
     */
    filterChange(event){
        this.setState({
            filtertext:event.target.value
        })
    },


    /**
     *The function name: editProject.
     *Function used:Pop up a window to re-edit some information of project
     *Create time: 2017/3/9
     */
    editProject(id,name,desc){

        var that = this;
        var inFunc = function(){
            that.setState({
                iferror:false,
                ifNoChange:false,
            })
            that.state.projectindex=id;
            that.state.projectname=name;
            that.state.describes=desc;
            var projectname=name;
            var projectdesc=desc;
            $("#editprojectname").val(projectname);
            $("#editprojectdesc").val(projectdesc);
            $("#editproject").modal("show");
        };
        return inFunc;
    },

    /**
     *The function name: confirmEdit.
     *Function used:API for sending a request to update some information of project
     *Create time: 2017/3/9
     */
    confirmEdit(){
       var name = $("#editprojectname").val();
       var desc = $("#editprojectdesc").val();
        if(!name || !desc){
            this.setState({
                iferror:true,
            });
        }else{
            $("#ajaxloading").show();
            var that = this;
            if((that.state.projectname==name) && (that.state.describes==desc)) {
                $("#ajaxloading").hide();
                this.setState({
                    ifNoChange:true,
                });
            }else if((that.state.projectname==name) && (that.state.describes!=desc)){
                $.ajax({
                    url: Comment.DATA_URL+"KDB/UpdateProject",
                    data: $.param({
                        projectid:that.state.projectindex,
                        projectdes:desc,
                    }),
                    type: 'POST',
                    contentType: 'application/x-www-form-urlencoded',
                    success: function (data) {
                        $("#editproject").modal("hide");
                        $("#ajaxloading").hide();
                        that.getprojectList();
                    },
                    error:function(){
                        $("#ajaxloading").hide();
                    },
                    statusCode:{
                        406:function (){
                            IfDispatcher.dispatch({
                                actionType:Comment.LOGOUT
                            });
                        },
                    }
                });
            }else{
                $.ajax({
                    url: Comment.DATA_URL+"KDB/UpdateProject",
                    data: $.param({
                        projectname:name,
                        projectid:that.state.projectindex,
                        projectdes:desc,
                    }),
                    type: 'POST',
                    contentType: 'application/x-www-form-urlencoded',
                    success: function (data) {
                        $("#editproject").modal("hide");
                        $("#ajaxloading").hide();
                        that.getprojectList();
                    },
                    error:function(){
                        $("#ajaxloading").hide();
                    },
                    statusCode:{
                        406:function (){
                            IfDispatcher.dispatch({
                                actionType:Comment.LOGOUT
                            });
                        },
                        413:function () {
                            that.setState({
                                ifduplicate:true
                            })
                            $('#editproject').on('hide.bs.modal', function () {
                                that.setState({
                                    ifduplicate:false
                                })
                            })
                        }
                    }
                });
            }
        }
    },
    render(){
        var pageHeight = this.props.propsheight;
        var projects = this.props.projectslist;
        var loginInfo = this.props.loginInfo;
        var adminDisableStyle = true;
        var showproowner = {marginLeft:"0px",marginRight:"0px"};
        if(!loginInfo.ifadmin){
            adminDisableStyle = "";
            showproowner={display:"none"};
        }
        var that  = this;


        if(this.state.filtertext && this.state.filtertext.length>0){
            projects = Underscore.filter(projects,function (item) {
                return item.project_name.toLowerCase().indexOf(that.state.filtertext.toLowerCase())>=0;
            });
        }
        var emptyNameStyle = {display:"none"};
        if(this.state.iferror){
            emptyNameStyle = {}
        }

        var duplicateStyle = {display:"none"};
        if(this.state.ifduplicate){
            duplicateStyle = {}
        }
        var llist = [];
        if(projects.length != 0){
            if(projects.length >1){
                for(var i=0;i <projects.length;i=i+2){
                    if((i+1) ==(projects.length)){
                        llist.push(
                            <div className="row">
                                <div className="col-sm-6 " >
                                    <div className="col-sm-8 col-sm-offset-4">
                                        <div className="panel panel-default zzlborder2">
                                            <div className="panel-body " style={{background:"#fff"}}>
                                                <div className="row">
                                                    <div className="col-sm-8">
                                                        <h3 style={{color:"#379cf8",marginTop:"0px"}}><strong>{projects[i].project_name}</strong></h3>
                                                    </div>
                                                    <div className="col-sm-4" style={{textAlign:"right"}}>
                                                        <button
                                                            role="button"
                                                            className="btn glyphicon glyphicon-trash "
                                                            style={{background:"#fff",color:"#379cf8",border:"none"}}
                                                            onClick={this.projectdelclick(projects[i].project_id)}
                                                            disabled={adminDisableStyle}
                                                            >
                                                        </button>
                                                        <button
                                                            className="btn glyphicon glyphicon-pencil "
                                                            style={{marginLeft:"5px",background:"#fff",color:"#379cf8",border:"none"}}
                                                            disabled={adminDisableStyle}
                                                            onClick={this.editProject(projects[i].project_id,projects[i].project_name,projects[i].project_des)}
                                                            >
                                                        </button>
                                                    </div>
                                                </div>
                                                <div className="row" style={{marginLeft:"0px",marginRight:"0px",marginTop:"5px"}}>
                                                    <div className="col-sm-12" style={{color:"#379cf8"}}>
                                                        <h5><strong>Project Description: </strong>{projects[i].project_des}</h5>
                                                    </div>

                                                </div>
                                                <div className="row" style={{marginLeft:"0px",marginRight:"0px",marginTop:"5px"}}>
                                                    <div className="col-sm-12" style={{color:"#379cf8"}}>
                                                        <h5><strong>Project Time: </strong>{projects[i].create_time}</h5>
                                                    </div>
                                                </div>
                                                <div className="row" style={showproowner}>
                                                    <div className="col-sm-12" style={{color:"#379cf8",marginTop:"5px"}}>
                                                        <h5><strong>Project Owner: </strong>{projects[i].user_name}</h5>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="panel-footer " style={{backgroundColor:"#F7F7F7"}}>
                                                <div className="row" style={{marginLeft:"0px",marginRight:"0px"}}>
                                                    <div className="col-sm-2 col-sm-offset-5">
                                                        <button
                                                            role="button"
                                                            className="btn btn-default"
                                                            onClick={this.projectclick(projects[i].project_name,projects[i].project_id)}
                                                            style={{height:"40px",color:"#379cf8",border:"1px solid #379cf8"}}
                                                            >
                                                            Update
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        );
                    }else{
                        llist.push(
                            <div className="row">
                                <div className="col-sm-6 " >
                                    <div className="col-sm-8 col-sm-offset-4">
                                        <div className="panel panel-default zzlborder2">
                                            <div className="panel-body " style={{background:"#fff"}}>
                                                <div className="row">
                                                    <div className="col-sm-8">
                                                        <h3 style={{color:"#379cf8",marginTop:"0px"}}><strong>{projects[i].project_name}</strong></h3>
                                                    </div>
                                                    <div className="col-sm-4" style={{textAlign:"right"}}>
                                                        <button
                                                            role="button"
                                                            className="btn glyphicon glyphicon-trash "
                                                            onClick={this.projectdelclick(projects[i].project_id)}
                                                            style={{background:"#fff",color:"#379cf8",border:"none"}}
                                                            disabled={adminDisableStyle}
                                                            >
                                                        </button>
                                                        <button
                                                            className="btn glyphicon glyphicon-pencil "
                                                            style={{background:"#fff",color:"#379cf8",border:"none",marginLeft:"5px"}}
                                                            disabled={adminDisableStyle}
                                                            onClick={this.editProject(projects[i].project_id,projects[i].project_name,projects[i].project_des)}
                                                            >
                                                        </button>
                                                    </div>
                                                </div>
                                                <div className="row" style={{marginLeft:"0px",marginRight:"0px",marginTop:"5px"}}>
                                                    <div className="col-sm-12" style={{color:"#379cf8"}}>
                                                        <h5><strong>Project Description: </strong>{projects[i].project_des}</h5>
                                                    </div>
                                                </div>
                                                <div className="row" style={{marginLeft:"0px",marginRight:"0px",marginTop:"5px"}}>
                                                    <div className="col-sm-12" style={{color:"#379cf8"}}>
                                                        <h5><strong>Project Time: </strong>{projects[i].create_time}</h5>
                                                    </div>
                                                </div>
                                                <div className="row" style={showproowner}>
                                                    <div className="col-sm-12" style={{color:"#379cf8",marginTop:"5px"}}>
                                                        <h5><strong>Project Owner: </strong>{projects[i].user_name}</h5>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="panel-footer " style={{backgroundColor:"#F7F7F7"}}>
                                                <div className="row" style={{marginLeft:"0px",marginRight:"0px"}}>
                                                    <div className="col-sm-2 col-sm-offset-5">
                                                        <button
                                                            role="button"
                                                            className="btn btn-default"
                                                            onClick={this.projectclick(projects[i].project_name,projects[i].project_id)}
                                                            style={{height:"40px",color:"#379cf8",border:"1px solid #379cf8"}}
                                                            >
                                                            Update
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-sm-6 ">
                                    <div className="col-sm-8 ">
                                        <div className="panel panel-default zzlborder2 ">
                                            <div className="panel-body " style={{background:"#fff"}}>
                                                <div className="row">
                                                    <div className="col-sm-8">
                                                        <h3 style={{color:"#379cf8",marginTop:"0px"}}><strong>{projects[i+1].project_name}</strong></h3>
                                                    </div>
                                                    <div className="col-sm-4" style={{textAlign:"right"}}>
                                                        <button
                                                            role="button"
                                                            className="btn glyphicon glyphicon-trash "
                                                            style={{background:"#fff",color:"#379cf8",border:"none"}}
                                                            onClick={this.projectdelclick(projects[i+1].project_id)}
                                                            disabled={adminDisableStyle}
                                                            >
                                                        </button>
                                                        <button
                                                            //  type="button"
                                                            className="btn glyphicon glyphicon-pencil "
                                                            disabled={adminDisableStyle}
                                                            style={{marginLeft:"5px",background:"#fff",color:"#379cf8",border:"none"}}
                                                            onClick={this.editProject(projects[i+1].project_id,projects[i+1].project_name,projects[i+1].project_des)}
                                                            >
                                                        </button>
                                                    </div>
                                                </div>
                                                <div className="row" style={{marginLeft:"0px",marginRight:"0px",marginTop:"5px"}}>
                                                    <div className="col-sm-12" style={{color:"#379cf8"}}>
                                                        <h5><strong>Project Description: </strong>{projects[i+1].project_des}</h5>
                                                        <span></span>
                                                    </div>

                                                </div>
                                                <div className="row" style={{marginLeft:"0px",marginRight:"0px",marginTop:"5px"}}>
                                                    <div className="col-sm-12" style={{color:"#379cf8"}}>
                                                        <h5><strong>Project Time: </strong>{projects[i+1].create_time}</h5>
                                                    </div>

                                                </div>
                                                <div className="row" style={showproowner}>
                                                    <div className="col-sm-12" style={{color:"#379cf8",marginTop:"5px"}}>
                                                        <h5><strong>Project Owner: </strong>{projects[i+1].user_name}</h5>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="panel-footer " style={{backgroundColor:"#F7F7F7"}}>
                                                <div className="row" style={{marginLeft:"0px",marginRight:"0px"}}>
                                                    <div className="col-sm-2 col-sm-offset-5">
                                                        <button
                                                            role="button"
                                                            className="btn btn-default"
                                                            onClick={this.projectclick(projects[i+1].project_name,projects[i+1].project_id)}
                                                            style={{height:"40px",color:"#379cf8",border:"1px solid #379cf8"}}
                                                            >
                                                            Update
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        );
                    }
                }
            }else{
                llist.push(
                    <div className="row">
                        <div className="col-sm-6 " >
                            <div className="col-sm-8 col-sm-offset-4">
                                <div className="panel panel-default zzlborder2">
                                    <div className="panel-body " style={{background:"#fff"}}>
                                        <div className="row">
                                            <div className="col-sm-8" >
                                                <h3 style={{color:"#379cf8",marginTop:"0px"}}><strong>{projects[0].project_name}</strong></h3>
                                            </div>
                                            <div className="col-sm-4" style={{textAlign:"right"}}>
                                                <button
                                                    role="button"
                                                    className="btn glyphicon glyphicon-trash "
                                                    style={{background:"#fff",color:"#379cf8",border:"none"}}
                                                    onClick={this.projectdelclick(projects[0].project_id)}
                                                    disabled={adminDisableStyle}
                                                    >
                                                </button>
                                                <button
                                                    //  type="button"
                                                    className="btn glyphicon glyphicon-pencil "
                                                    style={{marginLeft:"5px",background:"#fff",color:"#379cf8",border:"none"}}
                                                    disabled={adminDisableStyle}
                                                    onClick={this.editProject(projects[0].project_id,projects[0].project_name,projects[0].project_des)}
                                                    >
                                                </button>
                                            </div>
                                        </div>
                                        <div className="row" style={{marginLeft:"0px",marginRight:"0px",marginTop:"5px"}}>
                                            <div className="col-sm-12" style={{color:"#379cf8"}}>
                                                <h5><strong>Project Description: </strong>{projects[0].project_des}</h5>
                                            </div>

                                        </div>
                                        <div className="row" style={{marginLeft:"0px",marginRight:"0px",marginTop:"5px"}}>
                                            <div className="col-sm-12" style={{color:"#379cf8"}}>
                                                <h5><strong>Project Time: </strong>{projects[0].create_time}</h5>
                                            </div>
                                        </div>
                                        <div className="row" style={showproowner}>
                                            <div className="col-sm-12" style={{color:"#379cf8",marginTop:"5px"}}>
                                                <h5><strong>Project Owner: </strong>{projects[0].user_name}</h5>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="panel-footer " style={{backgroundColor:"#F7F7F7"}}>
                                        <div className="row" style={{marginLeft:"0px",marginRight:"0px"}}>
                                            <div className="col-sm-2 col-sm-offset-5">
                                                <button
                                                    role="button"
                                                    className="btn btn-default"
                                                    onClick={this.projectclick(projects[0].project_name,projects[0].project_id)}
                                                    style={{height:"40px",color:"#379cf8",border:"1px solid #379cf8"}}
                                                    >
                                                    Update
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                );
            }
        }

        var NoChangeStyle = this.state.ifNoChange?{}:{display:"none"};
        return(
            <div >
                <div className="row" style={{height:"70px",marginLeft:"0px",marginRight:"0px",backgroundColor:"#f0f2f5"}}>
                    <div className="col-sm-4 col-sm-offset-7" >
                        <input type="text"
                               className="form-control zzlborder zzlinputcolborder "
                               value={this.state.filtertext}
                               onChange={this.filterChange}
                               style={{height:"40px",marginTop:"15px"}}
                               placeholder="Search by Project Name"/>

                    </div>
                    <div className="col-sm-1" style={{textAlign:"left",marginTop:"10px"}}>
                        <button
                            className="btn btn-default zzlborder"
                            //role="button"
                            id="zzlbutton1"
                            disabled={adminDisableStyle}
                            onClick={this.addprojectlick}
                            style={{backgroundColor:"#379cf8",color:"#FFFFFF",width:"50px",height:"50px"}}
                            >
                            +
                        </button>
                    </div>
                </div>
                <div className="row"  style={{height:(pageHeight-130+"px"),marginLeft:"0px",marginRight:"0px"}}>
                        <div className="col-sm-12">
                            <div className="panel panel-default" style={{borderStyle:"none",marginLeft:"-15px",marginRight:"-15px"}}>
                                <div className="panel-body"  style={{overflowY:'auto',height:(pageHeight-130)+"px",backgroundColor:"#f0f2f5"}}>
                                    <div style={{marginTop:"20px"}}>
                                        {llist}
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>

                <div className="modal fade" id="addproject" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                                <h4 className="modal-title" id="newmedialable">New Project</h4>
                            </div>
                            <div className="modal-body">
                                <form>
                                    <div className="form-group">
                                        <label htmlFor="mediaNameInput">Project Name</label>
                                        <input type="text" className="form-control" id="mediaNameInput"
                                               placeholder="Project Name"
                                               onChange={this.handleChange.bind(this,"projectname")}
                                        />
                                        <label htmlFor="mediaPassInput">Description</label>
                                         <textarea
                                             rows="10"
                                             style={{height:(200+"px")}}
                                             className="form-control"
                                             placeholder="Please describe in less than 30 words of project..."
                                             onChange={this.handleChange.bind(this,"describes")}
                                             >
                                         </textarea>

                                    </div>
                                    <div className="form-group form-group-lg" >
                                        <div className="col-sm-offset-1 col-sm-10" style={duplicateStyle}>
                                            <p  style={{color:"#750000",textAlign:"center"}}>Please enter a unique name for your project</p>
                                        </div>
                                    </div>
                                    <div className="form-group form-group-lg" >
                                        <div className="col-sm-offset-1 col-sm-10" style={emptyNameStyle}>
                                            <p  style={{color:"#750000",textAlign:"center"}}>Project name or Description can't be empty</p>
                                        </div>
                                    </div>

                                </form>



                            </div>
                            <div className="modal-footer">
                                <a type="button" className="btn btn-default" data-dismiss="modal">Cancel</a>
                                <a type="button" className="btn btn-primary" onClick={this.confirmNew}>Confirm</a>
                            </div>
                        </div>
                    </div>

                </div>


                <div className="modal fade" id="editproject" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                                <h4 className="modal-title" id="newmedialable">Eidt Project</h4>
                            </div>
                            <div className="modal-body">
                                <form>
                                    <div className="form-group">
                                        <label htmlFor="mediaNameInput">Project Name</label>
                                        <input type="text" className="form-control"
                                               id="editprojectname"
                                               //placeholder="User Name"
                                              // onChange={this.handleChange.bind(this,"projectname")}
                                               //value={this.state.projectname}
                                               name="projectname"
                                        />
                                        <label htmlFor="mediaPassInput">Description</label>
                                         <textarea
                                            rows="10"
                                            style={{height:(200+"px")}}
                                            className="form-control"
                                            //placeholder="Please describe in less than 30 words of project..."
                                           // value={this.state.describes}
                                            name="projectdesc"
                                            id="editprojectdesc"
                                         >
                                         </textarea>

                                    </div>
                                    <div className="form-group form-group-lg" >
                                        <div className="col-sm-offset-1 col-sm-10" style={NoChangeStyle}>
                                            <p  style={{color:"#750000",textAlign:"center"}}>No Change!</p>
                                        </div>
                                    </div>
                                    <div className="form-group form-group-lg" >
                                        <div className="col-sm-offset-1 col-sm-10" style={emptyNameStyle}>
                                            <p  style={{color:"#750000",textAlign:"center"}}>Project name or Description can't be empty!</p>
                                        </div>
                                    </div>
                                    <div className="form-group form-group-lg" >
                                        <div className="col-sm-offset-1 col-sm-10" style={duplicateStyle}>
                                            <p  style={{color:"#750000",textAlign:"center"}}>Please enter a unique name for your project!</p>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div className="modal-footer">
                                <a type="button" className="btn btn-default" data-dismiss="modal">Cancel</a>
                                <a type="button" className="btn btn-primary" onClick={this.confirmEdit}>Confirm</a>
                            </div>
                        </div>
                    </div>

                </div>
                <div className="modal fade" id="deleteproject" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                                <h4 className="modal-title" >Confirm the delete action</h4>
                            </div>
                            <div className="modal-body">
                                <h3>
                                    Are you sure to delete this project?
                                </h3>

                            </div>
                            <div className="modal-footer">
                                <a type="button" className="btn btn-default" data-dismiss="modal">Cancel</a>
                                <a type="button"
                                   onClick={this.deleteProjectConfirm}
                                   className="btn btn-primary" >Confirm</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
});