/**
 * Created by zzl on 2017/5/12.
 */
import React from 'react';
import crypto from "crypto";
import {Comment} from "../comment";
import {IfDispatcher} from "../store/dispatcher";
import Underscore from "underscore";

export var Participle = React.createClass({
    contextTypes:{
        router:React.PropTypes.object.isRequired
    },
    getInitialState(){
        return{
            iferror:false,
            ifexist:false,
            ifbid:false,
            iflonger:false,
            newparticiple:null,
            confid:null,
            updateword:null,
            filtertext:"",
            //ifexport:false,
            //exporturl:"",

        }
    },
    componentDidMount (){
    },
    handleChange(name,event){
        var newstate = {};
        newstate[name] = event.target.value;
        newstate.ifexist = false;
        newstate.iferror = false;
        newstate.ifbid = false;
        newstate.iflonger = false;
        this.setState(newstate);
    },
    getParticipledata(){
        $("#ajaxloading").show();
        var that = this;
        $.ajax({
            url: Comment.DATA_URL+"KDB/getAllConfs",
            data:$.param({
                confDefineId:that.props.currentPartFileId,
            }),
            type: 'GET',
            contentType: 'application/x-www-form-urlencoded',
            success: function(data){
                $("#ajaxloading").hide();
                var msg = data;
                IfDispatcher.dispatch({
                    actionType: Comment.GETPARTICIPLEDATALIST,
                    getparticipledata:msg.data
                });
            },
            error:function(){
                $("#ajaxloading").hide();
            },
            statusCode:{
                414:function(){

                },
                406:function (){
                    IfDispatcher.dispatch({
                        actionType:Comment.LOGOUT
                    });
                }
            }
        });
    },
    addParticipleClick(){
        this.setState({
            iferror:false,
            ifexist:false,
            ifbid:false,
            iflonger:false,
            newparticiple:null,
        });
        $("#addParticiple").modal("show");
    },
    createParticipleLick(){
        if(!this.state.newparticiple){
            this.setState({iferror:true});
        }else if(this.state.newparticiple.length >40){
            this.setState({iflonger:true});
        }
        else{
            $("#ajaxloading").show();
            var that = this;
            $.ajax({
                url:Comment.DATA_URL+"KDB/AddConf",
                data: $.param({
                    confDefId:that.props.currentPartFileId,
                    word:that.state.newparticiple,
                }),
                type:'POST',
                contentType: 'application/x-www-form-urlencoded',
                success:function(data){
                    $("#addParticiple").modal("hide");
                    $("#ajaxloading").hide();
                    that.getParticipledata();
                },
                error:function () {
                    $("#ajaxloading").hide();
                },
                statusCode:{
                    406:function (){
                        IfDispatcher.dispatch({
                            actionType:Comment.LOGOUT
                        });
                    },
                    416:function(){
                        that.setState({
                            ifexist:true
                        })
                    },
                    422:function(){
                        that.setState({
                            ifbid:true
                        })
                    }
                }

            });
        }
    },
    removeParticipleClick(){
        $("#removeParticiple").modal("show");
    },
    removeParticipleComf(){
        $("#ajaxloading").show();
        var that=this;
        $.ajax({
            url: Comment.DATA_URL+"KDB/ClearConf",
            data: $.param({
                confDefineId:that.props.currentPartFileId,
                _method :"delete",
            }),
            type: 'POST',
            contentType: 'application/x-www-form-urlencoded',
            success: function (data) {
                $("#removeParticiple").modal("hide");
                $("#ajaxloading").hide();
                IfDispatcher.dispatch({
                    actionType: Comment.GETPARTICIPLEDATALIST,
                    getparticipledata:[]
                });
            },
            error:function(){
                $("#ajaxloading").hide();
            },
            statusCode:{
                406:function (){
                    IfDispatcher.dispatch({
                        actionType:Comment.LOGOUT
                    });
                },
                414:function(){
                },
                413:function(){
                }
            }

        });
    },
    deleteParticipleClick(id){
        var that = this;
        var inFunc = function(){
            $("#ajaxloading").show();
            $.ajax({
                url: Comment.DATA_URL+"KDB/DeleteConf",
                data: $.param({
                    confid:id,
                    _method:"delete"
                }),
                type: 'POST',
                contentType: 'application/x-www-form-urlencoded',
                success: function (data) {
                    $("#ajaxloading").hide();
                    that.getParticipledata();
                },
                error:function(){
                    $("#ajaxloading").hide();
                },
                statusCode:{
                    406:function (){
                        IfDispatcher.dispatch({
                            actionType:Comment.LOGOUT
                        });
                    },
                    414:function(){

                    },
                    413:function(){

                    },

                }

            });
        };
        return inFunc;
    },
    editParticiple(id,word){
        var that = this;
        var inFunc = function(){
            that.setState({
                updateword:word,
                confid:id,
                iferror:false,
                ifexist:false,
                ifbid:false,
                iflonger:false,
            })
            $("#editParticiple").modal("show");
        };

        return inFunc;
    },
    editParticipleLick(){
        if(!this.state.updateword ||this.state.updateword == ""){
            this.setState({iferror:true});
        }
        else if(this.state.updateword.length >40){
            this.setState({iflonger:true});
        }
        else{
            $("#ajaxloading").show();
            var that = this;
            $.ajax({
                url:Comment.DATA_URL+"KDB/UpdateConf",
                data: $.param({
                    confid:that.state.confid,
                    updateword:that.state.updateword,
                }),
                type:'POST',
                contentType: 'application/x-www-form-urlencoded',
                success:function(data){
                    $("#editParticiple").modal("hide");
                    $("#ajaxloading").hide();
                    that.getParticipledata();
                },
                error:function () {
                    $("#ajaxloading").hide();
                },
                statusCode:{
                    406:function (){
                        IfDispatcher.dispatch({
                            actionType:Comment.LOGOUT
                        });
                    },
                    416:function(){
                        that.setState({
                            ifexist:true
                        })
                    },
                    422:function(){
                        that.setState({
                            ifbid:true
                        })
                    }
                }

            });
        }
    },
    filterChange(event){
        //IfDispatcher.dispatch({
        //    actionType: Comment.GETPARTICIPLEDATADDLISTZZL,
        //    filtertext:event.target.value,
        //});
        this.setState({
            filtertext:event.target.value,
        })
    },
    importParticipleClick(){
        this.chooseFile();
    },
    chooseFile(){
        var that = this;

        $('#fileDialog').unbind('change');
        $('#fileDialog').change(function(evt) {
            that.readcsvfile($('#fileDialog'));
            $(this).val('');

        });
        $('#fileDialog').trigger('click');
    },
    readcsvfile(obj){
        var data = new FormData();
        data.append("name",$(obj)[0].files[0].name);
        data.append("file",$(obj)[0].files[0]);
        $("#ajaxloading").show();
        var that = this;
        $.ajax({
            url: Comment.DATA_URL+"KDB/UpLoadConf?confDefineId="+that.props.currentPartFileId,
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            success: function(data){
                 $("#ajaxloading").hide();
                that.getParticipledata();
            },
            error:function(jxr,scode){
                $("#ajaxloading").hide();
            },
            statusCode:{
                406:function (){
                    IfDispatcher.dispatch({
                        actionType:Comment.LOGOUT
                    });
                },
                416:function(){
                    alert("The file format is incorrect.");
                },
                413:function () {

                },
                415:function () {
                    alert("Uploading files exceeds the maximum limit.");
                }
            }
        });

    },
    exportParticipleClick(){
        var that = this;
        $.ajax({
            url: Comment.DATA_URL+"KDB/DownloadConf",
            data: $.param({
                confDefineId:that.props.currentPartFileId,
            }),
            contentType: 'application/x-www-form-urlencoded',
            type: 'GET',
            success: function (data) {
                $("#ajaxloading").hide();
                var msg = data;
                //that.setState({
                //    ifexport:true,
                //    exporturl:msg.url,
                //});
                IfDispatcher.dispatch({
                    actionType: Comment.GETPARTICIPLEDATADDLISTZZLURL,
                    exporturl:msg.url,
                });
            },
            error:function(jxr,scode){
                $("#ajaxloading").hide();

            },
            statusCode:{
                406:function (){
                    IfDispatcher.dispatch({
                        actionType:Comment.LOGOUT
                    });
                },
                414:function(){

                },

            }
        });
    },
    ClearExportClick(){
        this.setState({
            ifexport:false,
            exporturl:"",
        });
    },
    render(){
        var pageHeight = this.props.pageHeight;
        var currentParticipleData = this.props.getparticipledata;
        var PartData = [];
        var FileDataIsNull = "";
        var that = this;
        //if(this.props.getfiltertext && this.props.getfiltertext.length>0){
        //    currentParticipleData = Underscore.filter(currentParticipleData,function (item) {
        //        return item.word.toLowerCase().indexOf(that.props.getfiltertext.toLowerCase())>=0;
        //    });
        //}
        if(this.state.filtertext && this.state.filtertext.length>0){
            currentParticipleData = Underscore.filter(currentParticipleData,function (item) {
                return item.word.toLowerCase().indexOf(that.state.filtertext.toLowerCase())>=0;
            });
        }
        var elink = ""
        if(currentParticipleData.length !=0){
            if(currentParticipleData.length>1){
                var i=currentParticipleData.length -1;
                for(i;i>=0;i=i-2){
                    if((i-1) < 0){
                        PartData.push(
                            <div className="row " style={{marginLeft:"0px",marginRight:"0px"}}>
                                <div className="col-sm-6 " >
                                    <div className="row zzltrjion"  style={{marginLeft:"0px",marginRight:"0px",borderBottom:"1px solid #ccc"}}>
                                        <div className="row" style={{marginLeft:"0px",marginRight:"-15px",marginBottom:"5px",marginTop:"5px"}}>
                                            <div className="col-sm-9" >
                                                <div className="row wrap"  style={{marginLeft:"0px",marginRight:"0px"}}>
                                                    <span>{currentParticipleData[i].word}</span>
                                                </div>
                                            </div>
                                            <div className="col-sm-3" style={{textAlign:"right"}}>
                                                <div className="btn-group" role="group" >
                                                    <button
                                                        className="btn btn-default glyphicon glyphicon-trash zzlbuttoncoltrash"
                                                        onClick={this.deleteParticipleClick(currentParticipleData[i].conf_detail_id)}
                                                        style={{border:"0px none"}}
                                                        ></button>
                                                    <button
                                                        className="btn btn-default glyphicon glyphicon-edit zzlbuttoncoltrash"
                                                        onClick={this.editParticiple(currentParticipleData[i].conf_detail_id,currentParticipleData[i].word)}
                                                        style={{border:"0px none"}}
                                                        ></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        );
                    }else{
                        PartData.push(
                            <div className="row " style={{marginLeft:"0px",marginRight:"0px"}}>
                                <div className="col-sm-6 " >
                                    <div className="row zzltrjion"  style={{marginLeft:"0px",marginRight:"0px",borderBottom:"1px solid #ccc"}}>
                                        <div className="row" style={{marginLeft:"0px",marginRight:"-15px",marginBottom:"5px",marginTop:"5px"}}>
                                            <div className="col-sm-9" >
                                                <div className="row wrap"  style={{marginLeft:"0px",marginRight:"0px"}}>
                                                    <span>{currentParticipleData[i].word}</span>
                                                </div>
                                            </div>
                                            <div className="col-sm-3" style={{textAlign:"right"}}>
                                                <div className="btn-group" role="group" >
                                                    <button
                                                        className="btn btn-default glyphicon glyphicon-trash zzlbuttoncoltrash"
                                                        onClick={this.deleteParticipleClick(currentParticipleData[i].conf_detail_id)}
                                                        style={{border:"0px none"}}
                                                        ></button>
                                                    <button
                                                        className="btn btn-default glyphicon glyphicon-edit zzlbuttoncoltrash"
                                                        onClick={this.editParticiple(currentParticipleData[i].conf_detail_id,currentParticipleData[i].word)}
                                                        style={{border:"0px none"}}
                                                        ></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-sm-6">
                                    <div className="row zzltrjion"  style={{marginLeft:"0px",marginRight:"0px",borderBottom:"1px solid #ccc"}}>
                                        <div className="row" style={{marginLeft:"0px",marginRight:"-15px",marginBottom:"5px",marginTop:"5px"}}>
                                            <div className="col-sm-9" >
                                                <div className="row wrap"  style={{marginLeft:"0px",marginRight:"0px"}}>
                                                    <span>{currentParticipleData[i-1].word}</span>
                                                </div>
                                            </div>
                                            <div className="col-sm-3" style={{textAlign:"right"}}>
                                                <div className="btn-group" role="group" >
                                                    <button
                                                        className="btn btn-default glyphicon glyphicon-trash zzlbuttoncoltrash"
                                                        onClick={this.deleteParticipleClick(currentParticipleData[i-1].conf_detail_id)}
                                                        style={{border:"0px none"}}
                                                        ></button>
                                                    <button
                                                        className="btn btn-default glyphicon glyphicon-edit zzlbuttoncoltrash"
                                                        onClick={this.editParticiple(currentParticipleData[i-1].conf_detail_id,currentParticipleData[i-1].word)}
                                                        style={{border:"0px none"}}
                                                        ></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        );
                    }
                }
            }else{
                PartData.push(
                    <div className="row" style={{marginLeft:"0px",marginRight:"0px"}}>
                        <div className="col-sm-6 " >
                            <div className="row zzltrjion"  style={{marginLeft:"0px",marginRight:"0px",borderBottom:"1px solid #ccc"}}>
                                <div className="row" style={{marginLeft:"0px",marginRight:"-15px",marginBottom:"5px",marginTop:"5px"}}>
                                    <div className="col-sm-9" >
                                        <div className="row wrap"  style={{marginLeft:"0px",marginRight:"0px"}}>
                                            <span >{currentParticipleData[0].word}</span>
                                        </div>
                                    </div>
                                    <div className="col-sm-3" style={{textAlign:"right"}}>
                                        <div className="btn-group" role="group" >
                                            <button
                                                className="btn btn-default glyphicon glyphicon-trash zzlbuttoncoltrash"
                                                onClick={this.deleteParticipleClick(currentParticipleData[0].conf_detail_id)}
                                                style={{border:"0px none"}}
                                                ></button>
                                            <button
                                                className="btn btn-default glyphicon glyphicon-edit zzlbuttoncoltrash"
                                                onClick={this.editParticiple(currentParticipleData[0].conf_detail_id,currentParticipleData[0].word)}
                                                style={{border:"0px none"}}
                                                ></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                );
            }

            //if(this.state.ifexport){
            //    elink = <a
            //        className="btn btn-default zzlbuttoncolborder"
            //        href={this.state.exporturl}
            //        onClick={this.ClearExportClick}
            //        //style={{marginRight:"5px"}}
            //        >
            //        <span className="glyphicon glyphicon-floppy-save" aria-hidden="true"></span>Click To Download
            //    </a>;
            //}
            if(this.props.getexporturl != null){
                    elink = <a
                        className="btn btn-default zzlbuttoncolborder"
                        href={this.props.getexporturl}
                        >
                        <span className="glyphicon glyphicon-floppy-save" aria-hidden="true"></span>Click To Download
                    </a>;
            }
        }else{
            FileDataIsNull = true;
        }

        var longerStyle = this.state.iflonger?{}:{display:"none"};
        var emptyStyle  = this.state.iferror?{}:{display:"none"};
        var existStyle  = this.state.ifexist?{}:{display:"none"};
        var specialStyle  = this.state.ifbid?{}:{display:"none"};
        return(
            <div>
                <div className="row" style={{height:"130px",marginLeft:"0px",marginRight:"0px"}}>
                    <div className="row" style={{height:"60px",marginLeft:"0px",marginRight:"0px"}}>
                        <div className="col-sm-6" style={{marginTop:"5px"}}>
                            <h4 style={{color:"#379cf8"}}>{this.props.currentPartName}</h4>
                        </div>
                        <div className="col-sm-6" >
                            <input type="text"
                                   className="form-control zzlborder zzlbuttoncolborder"
                                   value={this.state.filtertext}
                                   onChange={this.filterChange}
                                   style={{height:"35px",marginTop:"15px"}}
                                   placeholder="Search by Token Name"/>
                        </div>

                    </div>
                    <div className="row" style={{height:"70px",marginLeft:"0px",marginRight:"0px"}}>
                        <div className="row" style={{marginLeft:"0px",marginRight:"0px",marginTop:"20px"}}>
                            <div className="col-md-6">
                                <div className="btn-group" role="group" >
                                    <button className="btn btn-default zzlbuttoncolborder" role="button"
                                         onClick={this.addParticipleClick}
                                        >
                                        Add Entry
                                    </button>
                                    <button className="btn btn-default zzlbuttoncolborder"
                                          onClick={this.removeParticipleClick}
                                          disabled={FileDataIsNull}
                                        >
                                        Remove Tokens
                                    </button>
                                </div>
                            </div>
                            <div className="col-md-6 " style={{textAlign:"right"}}>
                                {elink}
                                <div className="btn-group" role="group" >
                                    <button className="btn btn-default zzlbuttoncolborder"
                                            onClick={this.importParticipleClick}
                                            style={{marginLeft:"5px"}}
                                        >
                                        Import Tokens
                                    </button>
                                    <button className="btn btn-default zzlbuttoncolborder" role="button"
                                           onClick={this.exportParticipleClick}
                                           disabled={FileDataIsNull}
                                        >
                                        Export Tokens
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row" style={{height:(pageHeight-190+"px"),marginLeft:"0px",marginRight:"0px"}}>
                    <div className="row" style={{height:"40px",marginLeft:"0px",marginRight:"0px",marginTop:"15px",borderBottom:"1px solid #ccc"}}>

                    </div>
                    <div className="row" style={{height:(pageHeight-245+"px"),overflowY:"auto",marginTop:"0px",marginLeft:"0px",marginRight:"0px"}}>
                        {PartData}

                    </div>

                </div>

                <div className="modal fade" id="addParticiple" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                                <h4 className="modal-title">New Token Name</h4>
                            </div>
                            <div className="modal-body">
                                <form>
                                    <div className="form-group">
                                        <label htmlFor="mediaNameInput">Token Name</label>
                                        <input type="text" className="form-control"  placeholder="Token Name"
                                               onChange={this.handleChange.bind(this,"newparticiple")} value={this.state.newparticiple}/>
                                    </div>
                                </form>
                            </div>
                            <div className="modal-footer">
                                <div className="row">
                                    <div className="col-md-8">
                                        <div style={emptyStyle}>
                                            <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                Token File name  can't be empty!
                                            </div>
                                        </div>
                                        <div style={existStyle}>
                                            <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                Please enter a unique name for your Token File!
                                            </div>
                                        </div>
                                        <div style={specialStyle}>
                                            <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                Your Token File Name not allowed to enter special characters, please try again.
                                            </div>
                                        </div>
                                        <div style={longerStyle}>
                                            <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                Please enter no more than 40 characters.
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-4">
                                        <a type="button" className="btn btn-default" data-dismiss="modal">Cancel</a>
                                        <a type="button" className="btn btn-primary" onClick={this.createParticipleLick}>Confirm</a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
                <div className="modal fade" id="removeParticiple" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                                <h4 className="modal-title" >Confirm The Remove</h4>
                            </div>
                            <div className="modal-body">
                                <h3>
                                    Are you sure to remove Current Token File?
                                </h3>

                            </div>
                            <div className="modal-footer">
                                <a type="button" className="btn btn-default" data-dismiss="modal">Cancel</a>
                                <a type="button"
                                   onClick={this.removeParticipleComf}
                                   className="btn btn-primary" >Confirm</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="modal fade" id="editParticiple" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 className="modal-title" >Edit Token</h4>
                            </div>
                            <div className="modal-body">
                                <form>
                                    <div className="form-group">
                                        <label htmlFor="personnewname">New Token</label>
                                        <input type="text" className="form-control" id="updateword"
                                               onChange={this.handleChange.bind(this,"updateword")}
                                               value={this.state.updateword}
                                            />
                                    </div>
                                </form>
                            </div>
                            <div className="modal-footer">
                                <div className="row">
                                    <div className="col-md-8">
                                        <div style={emptyStyle}>
                                            <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                Token File name  can't be empty!
                                            </div>
                                        </div>
                                        <div style={existStyle}>
                                            <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                Please enter a unique name for your Token File!
                                            </div>
                                        </div>
                                        <div style={specialStyle}>
                                            <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                Your Token File Name not allowed to enter special characters, please try again.
                                            </div>
                                        </div>
                                        <div style={longerStyle}>
                                            <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                Please enter no more than 40 characters.
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-4">
                                        <a type="button" className="btn btn-default" data-dismiss="modal">Cancel</a>
                                        <a type="button" className="btn btn-default" onClick={this.editParticipleLick} style={{backgroundColor:"#46A3FF"}}>Confirm</a>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>);
    },
});