/**
 * Created by ��־�� on 2017/3/20.
 */
import React from 'react';
import crypto from "crypto";
import {Comment} from "../comment";
import {IfDispatcher} from "../store/dispatcher";
import Underscore from "underscore";
import {Adminapprove} from "./adminapprove";
import {Staffapprove} from "./staffapprove";

export var Dictapprove = React.createClass({
    contextTypes:{
        router:React.PropTypes.object.isRequired
    },
    componentDidMount(){
        var loginInfo = this.props.loginInfo;
        if(!loginInfo.name){
            this.context.router.push("/fronpage");
            IfDispatcher.dispatch({
                actionType:Comment.LOGOUT
            });
        }
    },
   render(){
       var pageHeight = this.props.propsheight;
       var propswidth = this.props.propswidth;
       var approvelist = [];
       if(this.props.loginInfo.ifadmin){
           approvelist.push(
               <Adminapprove
                   pageHeight={pageHeight}
                   propswidth={propswidth}
                   loginInfo={this.props.loginInfo}
                   approvedatalist={this.props.approvedatalist}
                   >
               </Adminapprove>);
       }else{
           approvelist.push(
               <Staffapprove
                   pageHeight={pageHeight}
                   propswidth={propswidth}
                   loginInfo={this.props.loginInfo}
                   approvedatalist={this.props.approvedatalist}
                   >
               </Staffapprove>);
       }
       return(

           <div>
               {approvelist}
           </div>)
   },

});