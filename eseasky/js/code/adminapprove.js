/**
 * The module function:the administrator management module of examination and approval.
 * create time:2017/3/6.
 */
import React from 'react';
import crypto from "crypto";
import {Comment} from "../comment";
import {IfDispatcher} from "../store/dispatcher";
import Underscore from "underscore";

export var Adminapprove = React.createClass({
    contextTypes:{
        router:React.PropTypes.object.isRequired
    },
    getInitialState(){
        return{
            currentpage:0,
            pagesize:16,
            pagenum:7,
            filtertext:null,

        }
    },
    componentDidMount (){
        this.getapprovedataList();
    },

    /**
     *The function name: getapprovedataList.
     *Function used: API for getting all information list of dict's approval by admin.
     *Create time: 2017/3/18
     */
    getapprovedataList(){
        $("#ajaxloading").show();
        var that = this;
        $.ajax({
            url: Comment.DATA_URL+"KDB/GetApproveDic",
            data:$.param({
                userId:that.props.loginInfo.userid,
            }),
            type: 'GET',
            contentType: 'application/x-www-form-urlencoded',
            success: function(data){
                $("#ajaxloading").hide();
                var msg = data;
                IfDispatcher.dispatch({
                    actionType: Comment.GETAPPROVERNUM,
                    approvedatalist:msg.data
                });
            },
            error:function(){
                $("#ajaxloading").hide();
                //alert("get GetApproveDic fail");
            },
            statusCode:{
                414:function(){
                    //alert("get GetApproveDic fail");
                },
                406:function (){
                    IfDispatcher.dispatch({
                        actionType:Comment.LOGOUT
                    });
                }
            }
        });
    },
    /**
     * The function name: PublishClick.
     * Function used: API for approving operation to create,delete or re-edit a item of global dictionary
     * Create time: 2017/3/18
     */
    PublishClick(approveid){
        var that = this;
        var inFunc = function(){
            $("#ajaxloading").show();
            $.ajax({
                url: Comment.DATA_URL+"KDB/ApproveDic",
                data: $.param({
                    approveId:approveid,
                    sign:1,
                }),
                type: 'POST',
                contentType: 'application/x-www-form-urlencoded',
                success: function (data) {
                    $("#ajaxloading").hide();
                    that.getapprovedataList();
                },
                error:function(){
                    $("#ajaxloading").hide();
                },
                statusCode:{
                    406:function (){
                        IfDispatcher.dispatch({
                            actionType:Comment.LOGOUT
                        });
                    },
                    414:function(){
                        //alert("ApproveDic fail!");
                    },
                    415:function(){
                        alert("can not ApproveDic again!");
                    },
                    416:function(){
                        $("#Publishfail").show();
                    },
                }
            });
        };
        return inFunc;
    },

    /**
     * The function name: PublishClick.
     * Function used: API for disapproving operation to create,delete or re-edit a item of global dictionary
     * Create time: 2017/3/18
     */
    NoPublishClick(approveid){
        var that = this;
        var inFunc = function(){
            $("#ajaxloading").show();
            $.ajax({
                url: Comment.DATA_URL+"KDB/ApproveDic",
                data: $.param({
                    approveId:approveid,
                    sign:0,

                }),
                type: 'POST',
                contentType: 'application/x-www-form-urlencoded',
                success: function (data) {
                    $("#ajaxloading").hide();
                    that.getapprovedataList();
                },
                error:function(){
                    $("#ajaxloading").hide();
                },
                statusCode:{
                    406:function (){
                        IfDispatcher.dispatch({
                            actionType:Comment.LOGOUT
                        });
                    },
                    414:function(){
                        //alert("ApproveDic fail!");
                    },
                    415:function(){
                        alert("can not ApproveDic again!");
                    },
                    416:function(){
                        $("#Publishfail").show();
                    },
                }
            });
        };
        return inFunc;
    },

    /**
     *The function name: firstpageclick.
     *Function used: The ability to jump to the first page.
     *Create time: 2017/3/18
     */
    firstpageclick(){
        this.setState({
            currentpage:0
        })

    },

    /**
     *The function name: leftpageclick.
     *Function used: The ability to jump to the previous page.
     *Create time: 2017/3/18
     */
    leftpageclick(){
        if(this.state.currentpage!=0){
            this.setState({
                currentpage:this.state.currentpage-1
            })
        }
    },

    /**
     *The function name: pnumclick.
     *Function used: The ability to jump to the specify page.
     *Create time: 2017/3/18
     */
    pnumclick(num){
        var that = this;
        var infun = function(){
            if(that.state.currentpage!=num){
                that.setState({
                    currentpage:num
                })
            }
        };
        return infun;
    },

    /**
     *The function name: rightpageclick.
     *Function used: The ability to jump to the next page.
     *Create time: 2017/3/6
     */
    rightpageclick(maxPageNum){
        var that = this;
        var infun = function () {
            if(that.state.currentpage!=maxPageNum-1){
                that.setState({
                    currentpage:that.state.currentpage+1
                })
            }
        };
        return infun;
    },

    /**
     *The function name: trailpageclick.
     *Function used: The ability to jump to the last page.
     *Create time: 2017/3/6
     */
    trailpageclick(maxpn){
        var that = this;
        var infun = function(){
            if(that.state.currentpage!=maxpn-1){
                that.setState({
                    currentpage:maxpn-1
                })
            }
        };
        return infun;
    },

    /**
     *The function name:filterChange.
     *Function used:Get the value of the search box's in real time.
     *Create time: 2017/3/6
     */
    filterChange(event){
        this.setState({
            filtertext:event.target.value,
        })
    },
    render(){
        var pageHeight = this.props.pageHeight;
        var propswidth = this.props.propswidth;
        var colorClass = "white";
        var approvedatalist = this.props.approvedatalist;

        var startPos = this.state.currentpage*this.state.pagesize;
         var stopPos = Math.min(startPos+this.state.pagesize,approvedatalist.length);
         var maxPageNum = Math.ceil(approvedatalist.length/this.state.pagesize);
        approvedatalist = approvedatalist.slice(startPos,stopPos);
        if(this.state.filtertext && this.state.filtertext.length>0){
            var egx = ".*" + this.state.filtertext + ".*";
            var g=0;
            var str=[];
            for (var a in approvedatalist) {
                if (approvedatalist[a].target_name.toLowerCase().match(egx.toLowerCase())) {
                    str[g] = approvedatalist[a];
                    g++;
                }else if(approvedatalist[a].source_name.toLowerCase().match(egx.toLowerCase())){
                    str[g] = approvedatalist[a];
                    g++;
                }else if(approvedatalist[a].update_source_name.toLowerCase().match(egx.toLowerCase())){
                    str[g] = approvedatalist[a];
                    g++;
                }
            }
            approvedatalist=str;
        }

        var leftButtonClass = "";
        var rightButtonClass = "";
        if(this.state.currentpage == 0) {
            leftButtonClass = "disabled";
        }
        if(maxPageNum == 0){
            rightButtonClass = "disabled";
        }
        if(this.state.currentpage == maxPageNum-1){
            rightButtonClass = "disabled";
        }
        var pageButtonGrp = [];
        pageButtonGrp.push(<li className={leftButtonClass}>
            <a
                onClick={this.firstpageclick}
                style={{color:"#000000"}}>
                <span>First</span>
            </a>
        </li>);

        if(maxPageNum <= this.state.pagenum){
            pageButtonGrp.push(<li className={leftButtonClass}>
                <a
                    onClick={this.leftpageclick}
                    >
                    <span style={{color:"#000000"}}>Previous</span>
                </a>
            </li>);

            for(var pn = 0;pn<=maxPageNum-1;pn++){
                var currentPageButtonCls = "";
                if(this.state.currentpage == pn){
                    currentPageButtonCls = "active";
                }
                pageButtonGrp.push(<li className={currentPageButtonCls}>
                    <a
                        onClick={this.pnumclick(pn)}
                        >
                        {parseInt(pn)+1}

                    </a>
                </li>);

            }
            pageButtonGrp.push(<li className={rightButtonClass}>
                <a
                    onClick={this.rightpageclick(maxPageNum)}
                    >
                    <span style={{color:"#000000"}}>Next</span>
                </a>
            </li>);
        }else{
            if(this.state.currentpage < maxPageNum-7){
                pageButtonGrp.push(<li className={leftButtonClass}>
                    <a
                        onClick={this.leftpageclick}
                        >
                        <span style={{color:"#000000"}}>Previous</span>
                    </a>
                </li>);
                for(var pn = this.state.currentpage;pn<=this.state.currentpage+2;pn++){
                    var currentPageButtonCls = "";
                    if(this.state.currentpage == pn){
                        currentPageButtonCls = "active";
                    }
                    pageButtonGrp.push(<li className={currentPageButtonCls}>
                        <a
                            onClick={this.pnumclick(pn)}

                            >
                            {parseInt(pn)+1}

                        </a>
                    </li>);
                }
                pageButtonGrp.push(<li className={"disabled"}>
                    <a>
                        {"..."}

                    </a>
                </li>);
                for(var pn =maxPageNum-3;pn<=maxPageNum-1;pn++ ){
                    var currentPageButtonCls = "";
                    if(this.state.currentpage == pn){
                        currentPageButtonCls = "active";
                    }
                    pageButtonGrp.push(<li className={currentPageButtonCls}>
                        <a
                            onClick={this.pnumclick(pn)}
                            >
                            {parseInt(pn)+1}

                        </a>
                    </li>);
                }
                pageButtonGrp.push(<li className={rightButtonClass}>
                    <a
                        onClick={this.rightpageclick(maxPageNum)}
                        >
                        <span style={{color:"#000000"}}>Next</span>
                    </a>
                </li>);
            }else{
                pageButtonGrp.push(<li className={leftButtonClass}>
                    <a
                        onClick={this.leftpageclick}
                        >
                        <span style={{color:"#000000"}}>Previous</span>
                    </a>
                </li>);
                for(var pn = maxPageNum-7;pn<=maxPageNum-1;pn++){
                    var currentPageButtonCls = "";
                    if(this.state.currentpage == pn){
                        currentPageButtonCls = "active";
                    }
                    pageButtonGrp.push(<li className={currentPageButtonCls}>
                        <a
                            onClick={this.pnumclick(pn)}
                            >
                            {parseInt(pn)+1}

                        </a>
                    </li>);
                }
                pageButtonGrp.push(<li className={rightButtonClass}>
                    <a
                        onClick={this.rightpageclick(maxPageNum)}
                        >
                        <span style={{color:"#000000"}}>Next</span>
                    </a>
                </li>);
            }
        }

        pageButtonGrp.push(<li className={rightButtonClass}>
            <a
                onClick={this.trailpageclick(maxPageNum)}
                >
                <span style={{color:"#000000"}}>Last</span>
            </a>
        </li>);

        var datalist =[];
        var approdata = [];
        if(approvedatalist.length >0){
            for(var k in approvedatalist){
                if(approvedatalist[k].state == 0){
                    datalist.push(
                        <tr  style={{display:"table",border:"0px none"}}>
                            <td  style={{width:"60px",textAlign:"center",borderTop:"0px none",borderLeft:"1px solid #ccc",borderBottom:"1px solid #ccc"}}><span className={colorClass}>{parseInt(k)+1}</span></td>
                            <td style={{width:((propswidth-114)*0.18+"px"),borderTop:"0px none",borderBottom:"1px solid #ccc",color:"#379cf8"}}><span>{approvedatalist[k].source_name}</span></td>
                            <td style={{width:((propswidth-114)*0.18+"px"),borderTop:"0px none",borderBottom:"1px solid #ccc",color:"#379cf8"}}><span>{approvedatalist[k].update_source_name}</span></td>
                            <td style={{width:((propswidth-114)*0.18+"px"),borderTop:"0px none",borderBottom:"1px solid #ccc",color:"#379cf8"}}><span>{approvedatalist[k].target_name}</span></td>
                            <td style={{width:((propswidth-114)*0.08+"px"),borderTop:"0px none",borderBottom:"1px solid #ccc",color:"#379cf8"}}><span>{approvedatalist[k].user_name}</span></td>
                            <td style={{width:((propswidth-114)*0.14+"px"),borderTop:"0px none",borderBottom:"1px solid #ccc",color:"#379cf8"}}><span>{approvedatalist[k].handletype}</span></td>
                            <td style={{width:((propswidth-114)*0.08+"px"),borderTop:"0px none",borderBottom:"1px solid #ccc",color:"#379cf8"}}><span >In The Pending</span></td>
                            <td style={{width:((propswidth-114)*0.16+"px"),borderTop:"0px none",borderRight:"1px solid #ccc",borderBottom:"1px solid #ccc",color:"#379cf8"}}>
                                <div class="btn-group" role="group" aria-label="...">
                                    <button className="btn btn-default zzlbuttoncolborder" role="button"
                                            onClick={this.PublishClick(approvedatalist[k].approve_id)}
                                            style={{width:"80px"}}
                                        >
                                        Approve
                                    </button>
                                    <button className="btn btn-default zzlbuttoncolborder" role="button"
                                            onClick={this.NoPublishClick(approvedatalist[k].approve_id)}
                                            style={{width:"80px"}}
                                        >
                                        Reject
                                    </button>
                                </div>
                            </td>
                        </tr>
                    );
                }else if(approvedatalist[k].state == 1){
                    datalist.push(
                        <tr  style={{display:"table",border:"0px none"}}>
                            <td  style={{width:"60px",textAlign:"center",borderTop:"0px none",borderLeft:"1px solid #ccc",borderBottom:"1px solid #ccc"}}><span className={colorClass}>{parseInt(k)+1}</span></td>
                            <td style={{width:((propswidth-114)*0.18+"px"),borderTop:"0px none",borderBottom:"1px solid #ccc",color:"#379cf8"}}><span>{approvedatalist[k].source_name}</span></td>
                            <td style={{width:((propswidth-114)*0.18+"px"),borderTop:"0px none",borderBottom:"1px solid #ccc",color:"#379cf8"}}><span>{approvedatalist[k].update_source_name}</span></td>
                            <td style={{width:((propswidth-114)*0.18+"px"),borderTop:"0px none",borderBottom:"1px solid #ccc",color:"#379cf8"}}><span>{approvedatalist[k].target_name}</span></td>
                            <td style={{width:((propswidth-114)*0.08+"px"),borderTop:"0px none",borderBottom:"1px solid #ccc",color:"#379cf8"}}><span>{approvedatalist[k].user_name}</span></td>
                            <td style={{width:((propswidth-114)*0.14+"px"),borderTop:"0px none",borderBottom:"1px solid #ccc",color:"#379cf8"}}><span>{approvedatalist[k].handletype}</span></td>
                            <td style={{width:((propswidth-114)*0.08+"px"),borderTop:"0px none",borderBottom:"1px solid #ccc",color:"#379cf8"}}><span style={{background:"#A5E980"}}>Publish Ok</span></td>
                            <td style={{width:((propswidth-114)*0.16+"px"),borderTop:"0px none",borderRight:"1px solid #ccc",borderBottom:"1px solid #ccc",color:"#379cf8"}}>
                                <div class="btn-group" role="group" aria-label="...">
                                    <button className="btn btn-default zzlbuttoncolborder" role="button"
                                        // onClick={this.addNewEntries}
                                            style={{width:"80px"}}
                                            disabled={"disabled"}
                                        >
                                        Approve
                                    </button>
                                    <button className="btn btn-default zzlbuttoncolborder" role="button"
                                        // onClick={this.addNewEntries}
                                            style={{width:"80px"}}
                                            disabled={"disabled"}
                                        >
                                        Reject
                                    </button>
                                </div>
                            </td>
                        </tr>
                    );
                }else if(approvedatalist[k].state == 2){
                    datalist.push(
                        <tr  style={{display:"table",border:"0px none"}}>
                            <td  style={{width:"60px",textAlign:"center",borderTop:"0px none",borderLeft:"1px solid #ccc",borderBottom:"1px solid #ccc"}}><span className={colorClass}>{parseInt(k)+1}</span></td>
                            <td style={{width:((propswidth-114)*0.18+"px"),borderTop:"0px none",borderBottom:"1px solid #ccc",color:"#379cf8"}}><span>{approvedatalist[k].source_name}</span></td>
                            <td style={{width:((propswidth-114)*0.18+"px"),borderTop:"0px none",borderBottom:"1px solid #ccc",color:"#379cf8"}}><span>{approvedatalist[k].update_source_name}</span></td>
                            <td style={{width:((propswidth-114)*0.18+"px"),borderTop:"0px none",borderBottom:"1px solid #ccc",color:"#379cf8"}}><span>{approvedatalist[k].target_name}</span></td>
                            <td style={{width:((propswidth-114)*0.08+"px"),borderTop:"0px none",borderBottom:"1px solid #ccc",color:"#379cf8"}}><span>{approvedatalist[k].user_name}</span></td>
                            <td style={{width:((propswidth-114)*0.14+"px"),borderTop:"0px none",borderBottom:"1px solid #ccc",color:"#379cf8"}}><span>{approvedatalist[k].handletype}</span></td>
                            <td style={{width:((propswidth-114)*0.08+"px"),borderTop:"0px none",borderBottom:"1px solid #ccc",color:"#379cf8"}}><span style={{background:"#F1E980"}}>Publish Fail</span></td>
                            <td style={{width:((propswidth-114)*0.16+"px"),borderTop:"0px none",borderRight:"1px solid #ccc",borderBottom:"1px solid #ccc",color:"#379cf8"}}>
                                <div class="btn-group" role="group" aria-label="...">
                                    <button className="btn btn-default zzlbuttoncolborder" role="button"
                                        // onClick={this.addNewEntries}
                                            style={{width:"80px"}}
                                            disabled={"disabled"}
                                        >
                                        Approve
                                    </button>
                                    <button className="btn btn-default zzlbuttoncolborder" role="button"
                                        // onClick={this.addNewEntries}
                                            style={{width:"80px"}}
                                            disabled={"disabled"}
                                        >
                                        Reject
                                    </button>
                                </div>
                            </td>
                        </tr>
                    );
                }

            }
            approdata.push(
                <div>
                    <div className="row" style={{height:(pageHeight-185+"px"),marginLeft:"0px",marginRight:"0px"}}>
                        <div className="row" style={{height:"30px",marginLeft:"15px",marginRight:"15px",marginTop:"15px"}}>
                            <table  className="table table-hover table-condensed" >
                                <thead  style={{display:"block",border:"0px none"}}>
                                <tr style={{display:"block", width:"100%",tableLayout:"fixed",border:"0px none"}}>
                                    <th style={{width:"60px",textAlign:"center",borderBottom:"1px solid #ccc",color:"#379cf8"}}><span>Rows</span></th>
                                    <th style={{width:((propswidth-114)*0.18+"px"),borderBottom:"1px solid #ccc",color:"#379cf8"}}><span>Source Name</span></th>
                                    <th style={{width:((propswidth-114)*0.18+"px"),borderBottom:"1px solid #ccc",color:"#379cf8"}}><span>New Source Name</span></th>
                                    <th style={{width:((propswidth-114)*0.18+"px"),borderBottom:"1px solid #ccc",color:"#379cf8"}}><span>Target Name</span></th>
                                    <th style={{width:((propswidth-114)*0.08+"px"),borderBottom:"1px solid #ccc",color:"#379cf8"}}><span>Operator</span></th>
                                    <th style={{width:((propswidth-114)*0.14+"px"),borderBottom:"1px solid #ccc",color:"#379cf8"}}><span>Operation Type</span></th>
                                    <th style={{width:((propswidth-114)*0.08+"px"),borderBottom:"1px solid #ccc",color:"#379cf8"}}><span>Status</span></th>
                                    <th style={{width:((propswidth-114)*0.16+"px"),borderBottom:"1px solid #ccc",color:"#379cf8"}}><span>Operation</span></th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                        <div className="row" style={{height:(pageHeight-232+"px"),overflowY:"auto",marginTop:"0px",marginLeft:"15px",marginRight:"0px"}}>
                            <table  className="table table-hover table-condensed" >
                                <tbody style={{display:"block",marginTop:"0px",border:"0px none"}}>
                                {datalist}
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div className="row" style={{height:"50px",backgroundColor:"#fff", marginLeft:"0px",marginRight:"0px"}}>
                        <div className="col-sm-9" style={{textAlign:"left"}}>
                            <nav aria-label="Page navigation">
                                <ul className="pagination" style={{paddingRight:"0px",marginTop:"0px"}}>
                                    {pageButtonGrp}
                                </ul>
                            </nav>
                        </div>

                    </div>
                </div>
            );
        }


      return(
          <div>
              <div className="row" style={{height:"75px",backgroundColor:"#fff",marginLeft:"0px",marginRight:"0px",borderBottom:"1px solid #ccc"}}>
                  <div className="col-sm-7" >
                      <h3 style={{color:"#379cf8"}}>Dictapprove Management</h3>
                  </div>
                  <div className="col-sm-5 " >
                      <input type="text"
                             className="form-control zzlborder zzlbuttoncolborder"
                             value={this.state.filtertext}
                             onChange={this.filterChange}
                             style={{height:"40px",marginTop:"15px"}}
                             placeholder="Search by SourceName or NewsourceName or TargetName"/>

                  </div>
              </div>
              <div>
                  {approdata}
              </div>
              <div className="modal fade" id="Publishfail" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                  <div className="modal-dialog">
                      <div className="modal-content">
                          <div className="modal-header">
                              <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                              <h4 className="modal-title">Publish Message</h4>
                          </div>
                          <div className="modal-body">
                              This dictionary has been in existence in the global dictionary, rejected Suggestions!
                          </div>
                          <div className="modal-footer">
                              <div className="row">
                                  <div className="col-md-4">
                                      <a type="button" className="btn btn-default" data-dismiss="modal">Confirm</a>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>

              </div>
          </div>);
    },
});