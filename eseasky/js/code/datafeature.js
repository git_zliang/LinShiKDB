/**
 * The module function:the module which administrator and users manage datafeature.
 * Create time:2017/3/7
 */
import React from 'react';
import crypto from "crypto";
import {Comment} from "../comment";
import {IfDispatcher} from "../store/dispatcher";
import Underscore from "underscore";

export var Datafeature = React.createClass({
    contextTypes: {
        router: React.PropTypes.object.isRequired,
    },
    getInitialState(){
        return{
            featuredefname:null,
            thisfeatureno:null,
            thisfeaturename:null,
            FDetailname:null,
            FDetailContext:null,
            FDetailType:null,
            newFdetailName:null,
            newFdetailContext:null,
            filtertext:"",
            selectItem:null,
            thisfdetailno:null,
            thisfdetailname:null,
            thisfdetailcontext:null,
            iferror:false,
            iftoolong:false,
            ifduplicate:false,
            ifadmindupdict:false,
            ifexport:false,
            exporturl:null,
            iffeatureTypeError:false,
            ifregRight:true,
            iffnameused:false,
            ifnamematchkinds:true,
            ifnamematchlength:true,
        }
    },
    componentDidMount(){
        var loginInfo = this.props.loginInfo;
        if(!loginInfo.name){
            this.context.router.push("/fronpage");
            IfDispatcher.dispatch({
                actionType:Comment.LOGOUT
            });
        }else{
            this.getDataFeature();
        }
    },

    /**
     *The function name: getDataFeature.
     *Function used: API for getting the list of datafeature with userId.
     *Create time: 2017/3/7
     */
    getDataFeature(){

        $("#ajaxloading").show();
        var loginInfo = this.props.loginInfo;
        var that = this;
        $.ajax({
            url: Comment.DATA_URL+"KDB/GetAllFeaturesDefByUserId",
            data:$.param({
                user_id:loginInfo.userid,
            }),
            type: 'GET',
            contentType: 'application/x-www-form-urlencoded',
            success: function(data){
                $("#ajaxloading").hide();
                var msg = data;
                IfDispatcher.dispatch({
                    actionType: Comment.GETFEATURESdEF,
                    featuredeflist:msg.data
                });
                if(msg.data.length==0){
                    IfDispatcher.dispatch({
                        actionType: Comment.GETFEATUREDETAIL,
                        featureDetlist:[]
                    });
                }else{
                    $("#ajaxloading").show();
                    var feature_no = msg.data[0].featuredef_no;
                    var feature_name = msg.data[0].featuredef_name;
                    that.setState({
                        thisfeatureno:feature_no,
                        thisfeaturename:feature_name,
                    });
                    $.ajax({
                        url: Comment.DATA_URL+"KDB/GetAllFeaturesDetailByFeatureNo",
                        data:$.param({
                            featuredef_no:feature_no,
                        }),
                        type: 'GET',
                        contentType: 'application/x-www-form-urlencoded',
                        success: function(data){
                            // alert("success");
                            $("#ajaxloading").hide();
                            var msg = data;
                            IfDispatcher.dispatch({
                                actionType: Comment.GETFEATUREDETAIL,
                                featureDetlist:msg.data
                            });
                        },
                        error:function(){
                            $("#ajaxloading").hide();
                            //alert("get featureDeflist fail");
                        },
                        statusCode:{
                            414:function(){
                            },
                            406:function (){
                                IfDispatcher.dispatch({
                                    actionType:Comment.LOGOUT
                                });
                            }
                        }
                    });
                }
            },
            error:function(){
                $("#ajaxloading").hide();
               // alert("get featureDetaillist fail");
            },
            statusCode:{
                414:function(){
                },
                406:function (){
                    IfDispatcher.dispatch({
                        actionType:Comment.LOGOUT
                    });
                }
            }
        });
    },

    /**
     *The function name: dictdataclick.
     *Function used: API for getting all items of datafeature with a featureNo.
     *Create time: 2017/3/7
     */
    dictdataclick(fdefno,item,fdefname){
        var that = this;
        var inFunc = function(){
            var tdef_no = fdefno;
            that.setState({
                thisfeatureno:tdef_no,
                selectItem:item,
                thisfeaturename:fdefname,
                ifexport:false,
                exporturl:null,
            })
            $.ajax({
                url: Comment.DATA_URL+"KDB/GetAllFeaturesDetailByFeatureNo",
                data:$.param({
                    featuredef_no:tdef_no,
                }),
                type: 'GET',
                success: function(data){
                    // alert("success");
                    var msg = data;
                    IfDispatcher.dispatch({
                        actionType: Comment.GETFEATUREDETAIL,
                        featureDetlist:msg.data
                    });

                },
                error:function(){
                    $("#ajaxloading").hide();
                    // alert("get FindDic fail");
                },
                statusCode:{
                    414:function(){
                    },
                    406:function (){
                        IfDispatcher.dispatch({
                            actionType:Comment.LOGOUT
                        });
                    }
                }
            });
        };
        return inFunc;
    },
    checkusername(Yname){
        var regular = /^\w+$/ ;
        if(!regular.test(Yname)){
            // alert("用户名只能包括英文字母 数字下划线！");
            return true;
        }
        return false;
    },
    checkusernames(Yname){
        var regular = /^[a-zA-Z0-9_\u4e00-\u9fa5]+$/ ;
        if(!regular.test(Yname)){
            // alert("用户名只能包括英文字母 中文数字下划线！");
            return true;
        }
        //for(var i=0;i<Yname.length;i++){
        //    var Ytext= Yname.charCodeAt(i);
        //    if(!((Ytext>=48)&&(Ytext<=57) || (Ytext >= 97 && Ytext <= 122) || (Ytext >= 65 && Ytext <= Ytext) || String.fromCharCode(Ytext)=="_")){
        //        //alert("用户名只能包括英文字母 数字下划线！");
        //        return true;
        //    }
        //}
        return false;
    },
    /**
     *The function name: handleChange.
     *Function used:Get the value of the input box's and change some state of error in real time.
     *Create time: 2017/3/7
     */
    handleChange(name,event){
        var newstate ={};
        newstate[name] = event.target.value;
        newstate.iferror = false;
        newstate.ifduplicate=false;
        newstate.iffnameused=false;
        newstate.ifadmindupdict=false;
        newstate.iftoolong=false;
        newstate.iffeatureTypeError=false;
        newstate.ifregRight=true;
        newstate.ifnamematchkinds=true;
        newstate.ifnamematchlength=true,
        this.setState(newstate);

    },

    /**
     *The function name: addFeatdefclick.
     *Function used:Pop up a window to create a new feature
     *Create time: 2017/3/7
     */
    addFeatdefclick(){
        this.setState({
            iftoolong:false,
            ifnamematchkinds:true,
            iferror:false,
        });
        $("#addFeatdef").modal("show");
    },

    /**
     *The function name: addFeatdefConf.
     *Function used:A API for creating a new feature
     *Create time: 2017/3/7
     */
    addFeatdefConf(){
        if(!this.state.featuredefname ){
            this.setState({iferror:true});
        }
        else if(this.checkusername(this.state.featuredefname)){
            this.setState({ifnamematchkinds:false});
        }
        else if(this.state.featuredefname.length>40){
            this.setState({iftoolong:true});
        }else{
            $("#ajaxloading").show();
            var loginInfo = this.props.loginInfo;
            var that = this;
            $.ajax({
                url: Comment.DATA_URL+"KDB/AddFeatureDefine",
                data: $.param({
                    featuredef_name:that.state.featuredefname,
                    user_id:loginInfo.userid,
                }),
                type: 'POST',
                contentType: 'application/x-www-form-urlencoded',
                success: function (data) {
                    $("#addFeatdef").modal("hide");
                    $("#ajaxloading").hide();
                    that.state.selectItem=0;
                    that.getDataFeature();
                },
                error:function(){
                    $("#ajaxloading").hide();
                },
                statusCode:{
                    406:function (){
                        IfDispatcher.dispatch({
                            actionType:Comment.LOGOUT
                        });
                    },
                    414:function(){
                        alert("add feature fail!");
                    },
                    413:function(){
                        if(loginInfo.ifadmin){
                            that.setState({
                                ifadmindupdict:true
                            })
                        }else{
                            that.setState({
                                ifduplicate:true
                            })
                        }

                    },
                    417:function () {
                        that.setState({
                            iffnameused:true
                        })
                    }
                }
            });
        }
    },

    /**
     *The function name: deleteFeaturedefCli.
     *Function used:Pop up a window to comfirm delete a  feature
     *Create time: 2017/3/7
     */
    deleteFeaturedefCli(){
        $("#deleteFeaturedef").modal("show");
    },
    removeFeaturesClick(){
        $("#removeFeaturedef").modal("show");
    },
    removeFeaturedefComf(){
        var that=this;
        $.ajax({
            url: Comment.DATA_URL+"KDB/ClearFeature",
            data: $.param({
                featuredef_no:that.state.thisfeatureno
            }),
            type: 'POST',
            contentType: 'application/x-www-form-urlencoded',
            success: function (data) {
                $("#removeFeaturedef").modal("hide");
                $.ajax({
                    url: Comment.DATA_URL+"KDB/GetAllFeaturesDetailByFeatureNo",
                    data:$.param({
                        featuredef_no:that.state.thisfeatureno,
                    }),
                    type: 'GET',
                    success: function(data){
                        // alert("success");
                        var msg = data;
                        IfDispatcher.dispatch({
                            actionType: Comment.GETFEATUREDETAIL,
                            featureDetlist:msg.data
                        });

                    },
                    error:function(){
                        $("#ajaxloading").hide();
                        // alert("get FindDic fail");
                    },
                    statusCode:{
                        414:function(){
                        },
                        406:function (){
                            IfDispatcher.dispatch({
                                actionType:Comment.LOGOUT
                            });
                        }
                    }
                });
            },
            error:function(){
                $("#ajaxloading").hide();
            },
            statusCode:{
                406:function (){
                    IfDispatcher.dispatch({
                        actionType:Comment.LOGOUT
                    });
                },
                414:function(){
                },
                413:function(){
                }
            }

        });
    },
    /**
     *The function name: deleteFeaturedefCli.
     *Function used:API for sending a request to delete a user
     *Create time: 2017/3/7
     */
    deleteFeaturedefComf(){
            var deflist = this.props.featuredeflist;
            var that=this;
            if(deflist.length==1){
                IfDispatcher.dispatch({
                    actionType:Comment.GETFEATUREDETAIL,
                    featureDetlist:[]
                });
            }

            $.ajax({
                url: Comment.DATA_URL+"KDB/DeleteFeatureDefine",
                data: $.param({
                    _method:"delete",
                    featuredef_no:that.state.thisfeatureno
                }),
                type: 'POST',
                contentType: 'application/x-www-form-urlencoded',
                success: function (data) {
                    $("#deleteFeaturedef").modal("hide");
                    that.getDataFeature();
                    that.state.selectItem=0;
                    that.state.thisfeaturename=null;
                    that.state.thisfeatureno=null;
                },
                error:function(){
                    $("#ajaxloading").hide();
                },
                statusCode:{
                    406:function (){
                        IfDispatcher.dispatch({
                            actionType:Comment.LOGOUT
                        });
                    },
                    414:function(){
                    },
                    413:function(){
                    }
                }

            });
    },

    /**
     *The function name: addNewEntries.
     *Function used:Pop up a window to create a new item for a feature
     *Create time: 2017/3/7
     */
    addNewEntries(){
        this.setState({
            ifnamematchkinds:true,
            ifnamematchlength:true,
            iffeatureTypeError:false,
            iferror:false,
        });
        $("#addNewEntries").modal("show");
    },

    /**
     *The function name: addNewEntriesComf.
     *Function used:API for sending a request to create a new item of feature
     *Create time: 2017/3/7
     */
    addNewEntriesComf(){
        if(!this.state.FDetailname || !this.state.FDetailContext || !this.state.FDetailType){
            this.setState({iferror:true});
        }
        else if(this.checkusernames(this.state.FDetailname)){
            this.setState({ifnamematchkinds:false});
        }
        //else if(this.checkusernames(this.state.FDetailContext)){
        //    this.setState({ifnamematchkinds:false});
        //}
        else if(this.state.FDetailname.length<0 ||this.state.FDetailname.length>40){
            this.setState({ifnamematchlength:false});
        }
        //else if(this.state.FDetailContext.length<0 ||this.state.FDetailContext.length>40){
        //    this.setState({ifnamematchlength:false});
        //}
        else if((this.state.FDetailType!=1) && (this.state.FDetailType!=0)){
            this.setState({iffeatureTypeError:true});
        }else{
            $("#ajaxloading").show();
            var that = this;
            $.ajax({
                url: Comment.DATA_URL+"KDB/AddFeaturesDetail",
                data: $.param({
                    featuredef_no:that.state.thisfeatureno,
                    featuredetail_type:that.state.FDetailType,
                    featuredetail_name:that.state.FDetailname,
                    featuredetail_context:that.state.FDetailContext,
                }),
                type: 'POST',
                contentType: 'application/x-www-form-urlencoded',
                success: function (data) {
                    $("#addNewEntries").modal("hide");
                    var thisfeature_no = that.state.thisfeatureno;
                    $.ajax({
                        url: Comment.DATA_URL+"KDB/GetAllFeaturesDetailByFeatureNo",
                        data:$.param({
                            featuredef_no:thisfeature_no,
                        }),
                        type: 'GET',
                        success: function(data){
                            // alert("success");
                            $("#ajaxloading").hide();
                            var msg = data;
                            IfDispatcher.dispatch({
                                actionType: Comment.GETFEATUREDETAIL,
                                featureDetlist:msg.data
                            });

                        },
                        error:function(){
                            $("#ajaxloading").hide();
                            //alert("get featuredetils fail");
                        },
                        statusCode:{
                            406:function (){
                                IfDispatcher.dispatch({
                                    actionType:Comment.LOGOUT
                                });
                            },
                            414:function(){
                            },
                            413:function(){
                            }
                        }
                    });
                },
                error:function(){
                    $("#ajaxloading").hide();
                },
                statusCode:{
                    406:function (){
                        IfDispatcher.dispatch({
                            actionType:Comment.LOGOUT
                        });
                    },
                    414:function(){
                        alert("add entries fail!");
                    },
                    413:function(){
                        that.setState({
                            ifduplicate:true
                        })
                    },
                    416:function () {
                        that.setState({
                            ifregRight:false
                        })
                    }
                }
            });
        }
    },


    /**
     *The function name: delFDetailCli.
     *Function used:Pop up a window to comfirm delete a item of feature
     *Create time: 2017/3/7
     */
    delFDetailCli(id){
        var that = this;
        var inFunc = function(){
            that.state.thisfdetailno = id;
            $("#deletefDetail").modal("show");
        };
        return inFunc;
    },

    /**
     *The function name: deletefDetailComf.
     *Function used:API for sending a request to delete a item of feature
     *Create time: 2017/3/7
     */
    deletefDetailComf(){
        if(true){
            $("#ajaxloading").show();
            var that = this;
            $.ajax({
                url: Comment.DATA_URL+"KDB/DeleteFeaturesDetailByDetailNo",
                data:$.param({
                    _method:"delete",
                    featuredetail_no:that.state.thisfdetailno,
                }),
                type: 'POST',
                success: function(data){
                    // alert("success");
                    $("#deletefDetail").modal("hide");
                    var thisfeature_no = that.state.thisfeatureno;
                    $.ajax({
                        url: Comment.DATA_URL+"KDB/GetAllFeaturesDetailByFeatureNo",
                        data:$.param({
                            featuredef_no:thisfeature_no,
                        }),
                        type: 'GET',
                        success: function(data){
                            // alert("success");
                            $("#ajaxloading").hide();
                            var msg = data;
                            IfDispatcher.dispatch({
                                actionType: Comment.GETFEATUREDETAIL,
                                featureDetlist:msg.data
                            });

                        },
                        error:function(){
                            $("#ajaxloading").hide();
                            //alert("get featuredetils fail");
                        },
                        statusCode:{
                            406:function (){
                                IfDispatcher.dispatch({
                                    actionType:Comment.LOGOUT
                                });
                            },
                            414:function(){

                            }
                        }
                    });

                },
                error:function(){
                    $("#ajaxloading").hide();
                    //alert("delete the entrie fail");
                },
                statusCode:{
                    406:function (){
                        IfDispatcher.dispatch({
                            actionType:Comment.LOGOUT
                        });
                    },
                }
            });
        }
    },


    /**
     *The function name: editFDetial.
     *Function used:Pop up a window to re-edit a item of feature
     *Create time: 2017/3/7
     */
    editFDetial(id,context,name){
        var that = this;
        var inFunc = function(){
            that.setState({
                iferror:false,
                ifduplicate:false,
                ifnamematchkinds:true,
                ifnamematchlength:true,
            })
            that.state.thisfdetailno = id;
            that.state.thisfdetailcontext = context;
            that.state.thisfdetailname = name;
            var featureContent=context;
            var featureName=name;
            $("#editFDetailname").val(featureName);
            $("#editFDetailcontent").val(featureContent);
            $("#editDetail").modal("show");
        };

        return inFunc;
    },

    /**
     *The function name: editDetailComf.
     *Function used:API for sending a request to update a item of feature
     *Create time: 2017/3/7
     */
    editDetailComf(){
        var that = this;
        var content = null;
        var name = null;
        var newFeatureName = $("#editFDetailname").val();
        var newFeatureContext = $("#editFDetailcontent").val();

        if(!newFeatureContext && !newFeatureName){
            that.setState({
                iferror:true
            })
        }
        else if(this.checkusernames(newFeatureName)){
            this.setState({ifnamematchkinds:false});
        }
        //else if(this.checkusernames(newFeatureContext)){
        //    this.setState({ifnamematchkinds:false});
        //}
        else if(newFeatureName.length<0 ||newFeatureName.length>40){
            this.setState({ifnamematchlength:false});
        }
        //else if(newFeatureContext.length<0 ||newFeatureContext.length>40){
        //    this.setState({ifnamematchlength:false});
        //}
        else {
            if(!newFeatureContext){
                content=newFeatureContext;
            }else{
                content=newFeatureContext;
            }

            if(!newFeatureName){
                name=that.state.thisfdetailname;
            }else{
                name=newFeatureName;
            }
            $("#ajaxloading").show();
            $.ajax({
                url: Comment.DATA_URL+"KDB/UpdateFeaturesDetail",
                data:$.param({
                    featuredetail_no:that.state.thisfdetailno,
                    featuredetail_context_result:content,
                    featuredetail_name_result:name,
                }),
                type: 'POST',
                success: function(data){
                    $("#editDetail").modal("hide");
                    var thisfeature_no = that.state.thisfeatureno;
                    $.ajax({
                        url: Comment.DATA_URL+"KDB/GetAllFeaturesDetailByFeatureNo",
                        data:$.param({
                            featuredef_no:thisfeature_no,
                        }),
                        type: 'GET',
                        success: function(data){
                            // alert("success");
                            $("#ajaxloading").hide();
                            var msg = data;
                            IfDispatcher.dispatch({
                                actionType: Comment.GETFEATUREDETAIL,
                                featureDetlist:msg.data
                            });

                        },
                        error:function(){
                            $("#ajaxloading").hide();
                            //alert("get featuredetils fail");
                        },
                        statusCode:{
                            406:function (){
                                IfDispatcher.dispatch({
                                    actionType:Comment.LOGOUT
                                });
                            },
                            414:function(){

                            },
                            413:function(){

                            }
                        }
                    });

                },
                error:function(){
                    $("#ajaxloading").hide();
                },
                statusCode:{
                    406:function (){
                        IfDispatcher.dispatch({
                            actionType:Comment.LOGOUT
                        });
                    },
                    414:function(){
                        that.setState({
                            ifduplicate:true
                        })
                    },
                    413:function(){
                        that.setState({
                            ifduplicate:true
                        })
                    }
                }
            });
        }
    },

    /**
     *The function name:filterChange.
     *Function used:Get the value of the search box's in real time.
     *Create time: 2017/3/7
     */
    filterChange(event){
        this.setState({
            filtertext:event.target.value,
        })
    },

    /**
     *The function name:importfeatureClick.
     *Function used:Pop up a window to import a csc file of feature
     *Create time: 2017/3/7
     */
    importfeatureClick(){
        this.chooseFile();
    },

    /**
     *The function name:chooseFile.
     *Function used:Pop up a window to choose a file of feature for import
     *Create time: 2017/3/7
     */
    chooseFile(){
        var that = this;
        $('#fileDialog').unbind('change');
        $('#fileDialog').change(function(evt) {
            that.readcsvfile($('#fileDialog'));
            $(this).val('');

        });

        $('#fileDialog').trigger('click');
    },


    /**
     *The function name:readcsvfile.
     *Function used:API for sending a request to read the file of feature
     *Create time: 2017/3/7
     */
    readcsvfile(obj){
        //var Ifcsvfile = $(obj)[0].files[0].name.split(".");
        var data = new FormData();
        data.append("name",$(obj)[0].files[0].name);
        data.append("file",$(obj)[0].files[0]);
        $("#ajaxloading").show();
        var that = this;
        $.ajax({
            url: Comment.DATA_URL+"KDB/UploadFeatureDetail?featuredef_no="+that.state.thisfeatureno,
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            success: function(data){
                // $("#ajaxloading").hide();
                $.ajax({
                    url: Comment.DATA_URL+"KDB/GetAllFeaturesDetailByFeatureNo",
                    data:$.param({
                        featuredef_no:that.state.thisfeatureno,
                    }),
                    type: 'GET',
                    contentType: 'application/x-www-form-urlencoded',
                    success: function(data){
                        // alert("success");
                        $("#ajaxloading").hide();
                        var msg = data;
                        IfDispatcher.dispatch({
                            actionType: Comment.GETFEATUREDETAIL,
                            featureDetlist:msg.data
                        });
                    },
                    error:function(){
                        $("#ajaxloading").hide();
                        //alert("get featureDeflist fail");
                    },
                    statusCode:{
                        406:function (){
                            IfDispatcher.dispatch({
                                actionType:Comment.LOGOUT
                            });
                        },
                        413:function(){

                        },
                        414:function () {
                        }
                    }
                });
            },
            error:function(jxr,scode){
               // alert("import fail!");
                $("#ajaxloading").hide();
            },
            statusCode:{
                406:function (){
                    IfDispatcher.dispatch({
                        actionType:Comment.LOGOUT
                    });
                },
                413:function(){
                    alert("Invalid format of the file content!");
                },
                414:function () {
                    alert("The file is empty!");
                },
                415:function () {
                    alert("Uploading files exceeds the maximum limit.");
                }
            }
        });
        //if(Ifcsvfile[Ifcsvfile.length-1] == "csv"){
        //
        //}else{
        //    alert("Please upload the CSV file!");
        //}
    },

    /**
     *The function name:exportfeaturecsv.
     *Function used:API for sending a request to export a feature
     *Create time: 2017/3/7
     */
    exportfeaturecsv(){
        var that = this;
        var featureno = that.state.thisfeatureno;
        $.ajax({
            url: Comment.DATA_URL+"KDB/DownloadFeatureDetail",
            data: $.param({
                featuredef_no:featureno,
            }),
            contentType: 'application/x-www-form-urlencoded',
            type: 'GET',
            success: function (data) {
                $("#ajaxloading").hide();
                var msg = data;
                that.setState({
                    ifexport:true,
                    exporturl:msg.url,
                });
            },
            error:function(jxr,scode){
                $("#ajaxloading").hide();
                //alert("DownloadDic fail!");
            },
            statusCode:{
                406:function (){
                    IfDispatcher.dispatch({
                        actionType:Comment.LOGOUT
                    });
                },
                409:function(){

                }
            }
        });
    },
    render(){
        var pageHeight = this.props.propsheight;
        var propswidth = this.props.propswidth;
        var colorClass = "white";

        var emptyStyle = {display:"none"};
        if(this.state.iferror){
            emptyStyle = {}
        }

        var duplicateStyle = {display:"none"};
        var admindupdictStyle = {display:"none"};
        if(this.state.ifduplicate){
            duplicateStyle = {}
        }
        if(this.state.ifadmindupdict){
            admindupdictStyle = {}
        }
        var featdefnameStyle = {display:"none"};
        if(this.state.iftoolong){
            featdefnameStyle = {}
        }

        var typeErrorType={display:"none"};
        if(this.state.iffeatureTypeError){
            typeErrorType = {}
        }

        var regStyle={display:"none"};
        if(!this.state.ifregRight){
            regStyle = {}
        }

        var fnameusedSyle={display:"none"};
        if(this.state.iffnameused){
            fnameusedSyle = {}
        }


        var fdeflist = this.props.featuredeflist;
        var fealistIsNullSty = "";
        var selectStylel=[];
        var selectColor = [];
        for(var i in fdeflist){
          //  selectStylel[i]={display:"none"};
            selectStylel[i]={float:"left",color:"#fff"};
            selectColor[i]="#000";
        }
        if(this.state.selectItem){
          //  selectStylel[0]={display:"none"}
            selectStylel[0]={float:"left",color:"#fff"}
            selectStylel[this.state.selectItem]={float:"left",color:"#0F7BB5"}
            selectColor[0]="#000";
            selectColor[this.state.selectItem]="#379cf8";
        }else{
            selectStylel[0]={float:"left",color:"#0F7BB5"}
            selectColor[0]="#379cf8";
        }
        var flist=[];
        var zzlflist=[];
        if(fdeflist.length>0){
            for(var i in fdeflist){
                flist.push(
                    <div className="row zzltr"  style={{marginLeft:"0px",marginRight:"0px",borderBottom:"1px solid #ccc"}}>
                        <div className="col-sm-11" style={{marginLeft:"-15px",marginRight:"0px"}}>
                            <a
                                role="button"
                                className="col-sm-12  btn btn-default zzlbuttoncol"
                                //className="btn btn-default glyphicon glyphicon-trash"
                                onClick={this.dictdataclick(fdeflist[i].featuredef_no,i,fdeflist[i].featuredef_name)}
                                style={{textAlign:"left",border:"0px none",marginTop:"5px",marginBottom:"5px",color:selectColor[i]}}
                                >
                                {fdeflist[i].featuredef_name}

                            </a>
                        </div>
                        <div className="col-sm-1" style={{textAlign:"left",marginTop:"10px"}}>
                            <div  style={selectStylel[i]}>
                                 <span
                                     className="zzlselectstyle"
                                     >√</span>
                            </div>

                        </div>
                    </div>
                );
            }
            zzlflist.push(
                <div>
                    <div className="row" style={{height:(pageHeight-245+"px"),overflowY:"auto",marginTop:"0px",marginLeft:"0px",marginRight:"0px",background:"#fff"}}>
                        {flist}
                    </div>
                 </div>
            );

        }else{
            fealistIsNullSty=true;
        }
        var fdetlist = this.props.featuredetlist;
        var zzlSty = "";
        if(!(fdetlist.length>0)){
           if(fealistIsNullSty ==""){
               zzlSty = true;
           } else{
               zzlSty = fealistIsNullSty
           }
        }else{}
        var featureDetailIsNullSty="";
        var that = this;
        if(this.state.filtertext && this.state.filtertext.length>0){
            fdetlist = Underscore.filter(fdetlist,function (item) {
                return item.featuredetail_name.toLowerCase().indexOf(that.state.filtertext.toLowerCase())>=0;
            });
        }
        var elink = ""
        if(this.state.ifexport){
            elink = <a
                className="btn btn-default zzlbuttoncolborder"
                href={this.state.exporturl}
                style={{marginRight:"5px"}}>
                <span className="glyphicon glyphicon-floppy-save" aria-hidden="true"></span>Click To Download
            </a>;
        }

        var tlist=[];
        var zzltlist=[];
        if(fdetlist.length>0){
            for(var j in fdetlist){
                //tlist.push(
                //    <tr style={{display:"table"}}>
                //        <td style={{width:"50px",textAlign:"center"}}><span className={colorClass}>{parseInt(j)+1}</span></td>
                //        <td style={{width:((propswidth-620)*0.30+"px")}}>{fdetlist[j].featuredetail_name}</td>
                //        <td style={{width:((propswidth-620)*0.46+"px")}}>{fdetlist[j].featuredetail_context}</td>
                //        <td style={{width:"300px"}}>{fdetlist[j].featuredetail_type==1?"regex":"others"}</td>
                //        <td className="list_btn" style={{width:((propswidth-620)*0.24+"px"),borderRight:"0px none"}}>
                //            <div className="btn-group" role="group" >
                //                <button
                //                    className="btn btn-default glyphicon glyphicon-trash"
                //                    //disabled={IfDisable}
                //                    onClick={this.delFDetailCli(fdetlist[j].featuredetail_no)}
                //                    style={{backgroundColor:"#E0E0E0",marginRight:"15px",width:"50px",borderTop:"0px none",borderLeft:"0px none",borderRight:"0px none",borderBottom:"0px none"}}
                //                ></button>
                //
                //                <button
                //                    className="btn btn-default glyphicon glyphicon-edit"
                //                    //disabled={IfDisable}
                //                    onClick={this.editFDetial(fdetlist[j].featuredetail_no,fdetlist[j].featuredetail_context,fdetlist[j].featuredetail_name)}
                //                    style={{backgroundColor:"#E0E0E0",width:"50px",borderTop:"0px none",borderLeft:"0px none",borderRight:"0px none",borderBottom:"0px none"}}
                //                ></button>
                //
                //            </div>
                //
                //        </td>
                //    </tr>
                //);
                tlist.push(
                    <div className="row zzltr"  style={{marginLeft:"0px",marginRight:"0px",borderBottom:"1px solid #ccc"}}>
                        <div className="row" style={{marginLeft:"0px",marginRight:"0px",marginBottom:"5px",marginTop:"5px"}}>
                            <div className="col-sm-1" >
                                <span className={colorClass} >{parseInt(j)+1}</span>
                            </div>
                            <div className="col-sm-3" >
                                <div className="row wrap"  style={{marginLeft:"0px",marginRight:"0px"}}>
                                    <span style={{color:"#379cf8"}}>{fdetlist[j].featuredetail_name}</span>
                                </div>

                            </div>
                            <div className="col-sm-4" >
                                <div className="row wrap"  style={{marginLeft:"0px",marginRight:"0px"}}>
                                    <span style={{color:"#379cf8"}}>{fdetlist[j].featuredetail_context}</span>
                                </div>
                            </div>
                            <div className="col-sm-2" >
                                <span style={{color:"#379cf8"}}>{fdetlist[j].featuredetail_type==1?"regex":"others"}</span>
                            </div>
                            <div className="col-sm-2" >
                                <div className="btn-group" role="group" >
                                    <button
                                        className="btn btn-default glyphicon glyphicon-trash zzlbuttoncoltrash"
                                        //disabled={IfDisable}
                                        onClick={this.delFDetailCli(fdetlist[j].featuredetail_no)}
                                        style={{width:"50px",border:"0px none",color:"#379cf8"}}
                                        ></button>
                                    <button
                                        className="btn btn-default glyphicon glyphicon-edit zzlbuttoncoltrash"
                                        //disabled={IfDisable}
                                        onClick={this.editFDetial(fdetlist[j].featuredetail_no,fdetlist[j].featuredetail_context,fdetlist[j].featuredetail_name)}
                                        style={{width:"50px",border:"0px none",color:"#379cf8"}}
                                        ></button>

                                </div>
                            </div>
                        </div>
                    </div>
                );
            }
            //zzltlist.push(
            //    <div>
            //        <div className="row" style={{height:"30px",marginLeft:"0px",marginRight:"0px"}}>
            //            <table  className="table table-bordered table-hover table-condensed" >
            //                <thead  style={{display:"block"}}>
            //                <tr style={{backgroundColor:"#E7E7E7",display:"block", width:"100%",tableLayout:"fixed"}}>
            //                    <th style={{width:"50px"}}>Rows</th>
            //                    <th style={{width:((propswidth-620)*0.30+"px")}}>Feature Name</th>
            //                    <th style={{width:((propswidth-620)*0.46+"px")}}>Feature Content</th>
            //                    <th style={{width:"300px"}}>Feature Type</th>
            //                    <th style={{width:((propswidth-620)*0.24+"px"),borderRight:"0px none"}}>Operation</th>
            //                </tr>
            //                </thead>
            //            </table>
            //        </div>
            //        <div className="row" style={{height:(pageHeight-220+"px"),overflowY:"auto",marginTop:"2px",marginLeft:"0px",marginRight:"0px",backgroundColor:"#FFFFFF"}}>
            //            <table  className="table table-bordered table-hover table-condensed">
            //                <tbody style={{display:"block",marginTop:"0px"}}>
            //                {tlist}
            //                </tbody>
            //            </table>
            //        </div>
            //    </div>
            //);
            zzltlist.push(
                <div>
                    <div className="row" style={{height:(pageHeight-245+"px"),overflowY:"auto",marginTop:"0px",marginLeft:"0px",marginRight:"0px"}}>
                        {tlist}
                    </div>
                </div>
            );
        }else{
            featureDetailIsNullSty=true;
        }
        var nameIfMatchlength = this.state.ifnamematchlength?{display:"none"}:{};
        var nameIfMatchkinds = this.state.ifnamematchkinds?{display:"none"}:{};
        return(
            <div>
                <div  style={{float:"left",height:(pageHeight-60+"px"),width:"400px"}}>
                    <div className="row" style={{height:"130px",marginLeft:"0px",marginRight:"0px"}}>
                        <div className="row" style={{height:"60px",marginLeft:"0px",marginRight:"0px"}}>
                            <div className="col-sm-12"  style={{marginTop:"5px"}}>
                                <h4 style={{color:"#379cf8"}}>Data Feature List</h4>
                            </div>
                        </div>
                        <div className="row" style={{background:"#fff",height:"70px",marginLeft:"0px",marginRight:"0px"}}>
                            <div className="col-sm-6">
                                <button className="btn btn-default zzlbuttoncolborder" role="button"
                                   style={{width:"70px",marginTop:"20px"}}
                                   onClick={this.addFeatdefclick}
                                    >
                                    Create
                                </button>
                            </div>
                            <div className="col-sm-6" style={{textAlign:"right"}}>
                                <button className="btn btn-default zzlbuttoncolborder" role="button"
                                        style={{width:"70px",marginTop:"20px"}}
                                        onClick={this.deleteFeaturedefCli}
                                        disabled={fealistIsNullSty}
                                    >
                                    Delete
                                </button>
                            </div>
                        </div>

                    </div>
                    <div className="row" style={{height:(pageHeight-190+"px"),marginLeft:"0px",marginRight:"0px"}}>
                        <div className="row" style={{height:"40px",marginLeft:"0px",marginRight:"0px",marginTop:"15px",borderBottom:"1px solid #ccc"}}>
                            <div className="col-sm-6" style={{textAlign:"left"}}>
                                <span style={{color:"#379cf8"}}>Featurefile Name</span>
                            </div>
                            <div className="col-sm-6" style={{textAlign:"right"}}>
                                <span style={{color:"#379cf8"}}>Current Logo</span>
                            </div>
                        </div>
                        {zzlflist}
                    </div>
                </div>
                <div style={{float:"left",height:(pageHeight-60+"px"),width:"5px",background:"#F1F1F1"}}>
                </div>
                <div style={{float:"left",width:(propswidth-405+"px"),height:(pageHeight-60+"px")}}>
                    <div className="row" style={{height:"130px",marginLeft:"0px",marginRight:"0px"}}>
                        <div className="row" style={{height:"60px",marginLeft:"0px",marginRight:"0px"}}>
                            <div className="col-sm-6" style={{marginTop:"5px"}}>
                                <h4 style={{color:"#379cf8"}}>{this.state.thisfeaturename}</h4>
                            </div>
                            <div className="col-sm-6" >
                                <input type="text"
                                       className="form-control zzlborder zzlbuttoncolborder"
                                      // value={this.state.filtertext}
                                       onChange={this.filterChange}
                                       style={{height:"35px",marginTop:"15px"}}
                                       placeholder="Search by FeatureName"/>
                            </div>
                        </div>
                        <div className="row" style={{height:"70px",marginLeft:"0px",marginRight:"0px"}}>
                            <div className="row" style={{marginLeft:"0px",marginRight:"0px",marginTop:"20px"}}>
                                <div className="col-md-6">
                                    <div className="btn-group" role="group" >
                                        <button className="btn btn-default zzlbuttoncolborder" role="button"
                                                onClick={this.addNewEntries}
                                                disabled={fealistIsNullSty}
                                            //style={{backgroundColor:"#0F7BB5"}}
                                            >
                                            Add Entry
                                        </button>
                                        <button className="btn btn-default zzlbuttoncolborder"
                                                onClick={this.removeFeaturesClick}
                                            //style={{marginLeft:"5px"}}
                                                disabled={zzlSty}
                                            >
                                            Remove Features
                                        </button>
                                    </div>
                                </div>
                                <div className="col-md-6 " style={{textAlign:"right"}}>
                                    {elink}
                                    <div className="btn-group" role="group" >
                                        <button className="btn btn-default zzlbuttoncolborder"
                                                onClick={this.importfeatureClick}
                                            //  disabled={adminDisableStyle}
                                                style={{marginLeft:"5px"}}
                                                disabled={fealistIsNullSty}

                                            >
                                            Import Features
                                        </button>
                                        <button className="btn btn-default zzlbuttoncolborder" role="button"
                                                onClick={this.exportfeaturecsv}
                                            //  style={{marginLeft:"5px"}}
                                                disabled={featureDetailIsNullSty}
                                            >
                                            Export Features
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div className="row" style={{height:(pageHeight-190+"px"),marginLeft:"0px",marginRight:"0px"}}>
                        <div className="row" style={{height:"40px",marginLeft:"0px",marginRight:"0px",marginTop:"15px",borderBottom:"1px solid #ccc"}}>
                            <div className="col-sm-1" >
                                <span style={{color:"#379cf8"}}>Rows</span>
                            </div>
                            <div className="col-sm-3" >
                                <span style={{color:"#379cf8"}}>Feature Name</span>
                            </div>
                            <div className="col-sm-4" >
                                <span style={{color:"#379cf8"}}>Feature Content</span>
                            </div>
                            <div className="col-sm-2" >
                                <span style={{color:"#379cf8"}}>Feature Type</span>
                            </div>
                            <div className="col-sm-2" >
                                <span style={{color:"#379cf8"}}>Operation</span>
                            </div>
                        </div>
                        {zzltlist}
                    </div>
                </div>


                <div className="modal fade" id="addFeatdef" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                                <h4 className="modal-title">New Feature</h4>
                            </div>
                            <div className="modal-body">
                                <form>
                                    <div className="form-group">
                                        <label htmlFor="mediaNameInput">Feature Name</label>
                                        <input type="text" className="form-control"  placeholder="Dict Name"
                                               onChange={this.handleChange.bind(this,"featuredefname")} value={this.state.featuredefname}/>

                                    </div>

                                </form>
                            </div>
                            <div className="modal-footer">
                                <div className="row">
                                    <div className="col-md-8">
                                        <div style={emptyStyle}>
                                            <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                Feature name  can't be empty!
                                            </div>
                                        </div>
                                        <div style={duplicateStyle}>
                                            <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                Please enter a unique name for your feature!
                                            </div>
                                        </div>
                                        <div style={nameIfMatchkinds}>
                                            <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                Your name/content not allowed to enter special characters(content support Chinese), please try again.
                                            </div>
                                        </div>
                                        <div style={featdefnameStyle}>
                                            <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                Please enter no more than 40 characters.
                                            </div>
                                        </div>
                                        <div style={fnameusedSyle}>
                                            <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                This featureName is used by others or yourself.
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-4">
                                        <a type="button" className="btn btn-default" data-dismiss="modal">Cancel</a>
                                        <a type="button" className="btn btn-primary" onClick={this.addFeatdefConf}>Confirm</a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
                <div className="modal fade" id="removeFeaturedef" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                                <h4 className="modal-title" >Confirm the remove</h4>
                            </div>
                            <div className="modal-body">
                                <h3>
                                    Are you sure to remove this Feature?
                                </h3>

                            </div>
                            <div className="modal-footer">
                                <a type="button" className="btn btn-default" data-dismiss="modal">Cancel</a>
                                <a type="button"
                                   onClick={this.removeFeaturedefComf}
                                   className="btn btn-primary" >Confirm</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="modal fade" id="deleteFeaturedef" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                                <h4 className="modal-title" >Confirm the delete</h4>
                            </div>
                            <div className="modal-body">
                                <h3>
                                    Are you sure to delete this Feature?
                                </h3>

                            </div>
                            <div className="modal-footer">
                                <a type="button" className="btn btn-default" data-dismiss="modal">Cancel</a>
                                <a type="button"
                                   onClick={this.deleteFeaturedefComf}
                                   className="btn btn-primary" >Confirm</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="modal fade" id="addNewEntries" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                                <h4 className="modal-title">Create New Entry</h4>
                            </div>
                            <div className="modal-body">
                                <form>
                                    <div className="form-group">
                                        <label htmlFor="mediaNameInput"> Name</label>
                                        <input type="text" className="form-control"  placeholder="Name"
                                               onChange={this.handleChange.bind(this,"FDetailname")} value={this.state.FDetailname}/>
                                        <label htmlFor="mediaNameInput"> Content</label>
                                        <input type="text" className="form-control"  placeholder="Content"
                                               onChange={this.handleChange.bind(this,"FDetailContext")} value={this.state.FDetailContext}/>
                                        <label htmlFor="mediaNameInput">Type(1:regex/0:others)</label>
                                        <input type="text" className="form-control"  placeholder="type"
                                               onChange={this.handleChange.bind(this,"FDetailType")} value={this.state.FDetailType}/>
                                    </div>

                                </form>

                            </div>
                            <div className="modal-footer">
                                <div className="row">
                                    <div className="col-md-8">
                                        <div style={emptyStyle}>
                                            <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                Name, Content or Type  can't be empty!
                                            </div>
                                        </div>
                                        <div style={nameIfMatchlength}>
                                            <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                Your name/content must be at least  0 characters and at most 40 characters.
                                            </div>
                                        </div>
                                        <div style={nameIfMatchkinds}>
                                            <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                Your name/content not allowed to enter special characters(content support Chinese), please try again.
                                            </div>
                                        </div>
                                        <div style={duplicateStyle}>
                                            <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                Please enter a unique feature entry!
                                            </div>
                                        </div>
                                        <div style={typeErrorType}>
                                            <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                Please enter '1' or '0' in the 'Type' field.
                                            </div>
                                        </div>
                                        <div style={regStyle}>
                                            <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                Your  regexp  is illegal.
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-4">
                                        <a type="button" className="btn btn-default" data-dismiss="modal">Cancel</a>
                                        <a type="button" className="btn btn-primary" onClick={this.addNewEntriesComf}>Confirm</a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
                <div className="modal fade" id="deletefDetail" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                                <h4 className="modal-title">Delete Entry</h4>
                            </div>
                            <div className="modal-body">
                                Are you sure to delete the entry?

                            </div>
                            <div className="modal-footer">
                                <a type="button" className="btn btn-default" data-dismiss="modal">Cancel</a>
                                <a type="button" className="btn btn-primary" onClick={this.deletefDetailComf}>Confirm</a>
                            </div>
                        </div>
                    </div>

                </div>

                <div className="modal fade" id="editDetail" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                                <h4 className="modal-title">Edit Entry</h4>
                            </div>
                            <div className="modal-body">
                                <form>
                                    <div className="form-group">
                                        <label htmlFor="mediaNameInput">New Name</label>
                                        <input type="text" className="form-control"  placeholder="New Name" id="editFDetailname"
                                               //onChange={this.handleChange.bind(this,"newFdetailName")}
                                               //value={this.state.newFdetailName}
                                        />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="mediaNameInput">New Content</label>
                                        <input type="text" className="form-control"  placeholder="New Context" id="editFDetailcontent"
                                               //onChange={this.handleChange.bind(this,"newFdetailContext")}
                                               //value={this.state.newFdetailContext}
                                        />
                                    </div>

                                </form>
                            </div>
                            <div className="modal-footer">
                                <div className="row">
                                    <div className="col-md-8">
                                        <div style={duplicateStyle}>
                                            <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                Please enter a unique feature entry!
                                            </div>
                                        </div>
                                        <div style={nameIfMatchlength}>
                                            <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                Your name/content must be at least  0 characters and at most 40 characters.
                                            </div>
                                        </div>
                                        <div style={nameIfMatchkinds}>
                                            <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                Your name/content not allowed to enter special characters(content support Chinese), please try again.
                                            </div>
                                        </div>
                                        <div style={emptyStyle}>
                                            <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                Name or Content  can't be empty.
                                            </div>
                                        </div>
                                        <div style={typeErrorType}>
                                            <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                Please enter '1' or '0'.
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-4">
                                        <a type="button" className="btn btn-default" data-dismiss="modal">Cancel</a>
                                        <a type="button" className="btn btn-primary" onClick={this.editDetailComf}>Confirm</a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
});