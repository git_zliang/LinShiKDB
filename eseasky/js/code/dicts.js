/**
 * The module file:This is the dictionary management module, mainly dictionary to create, modify, delete, and so on .
 * Create time:2017/3/6
 */
import React from 'react';
import crypto from "crypto";
import {Comment} from "../comment";
import {IfDispatcher} from "../store/dispatcher";
import Underscore from "underscore";


export var Dicts = React.createClass({
    contextTypes: {
        router: React.PropTypes.object.isRequired,
    },

    getInitialState(){
        return{
            dictname:null,
            newdictname:null,
            iferror:false,
            ifduplicate:false,
            ifadmindupdict:false,
            sourcename:null,
            sourcenamezzl:null,
            targetnamezzl:null,
            ifsourceeqltar:false,
            ifdicid:null,
            sourcenameresult:null,
            filtertext:"",
            selectItem:"",
            ifclicDic:true,
            globaldicid:null,
            ifglobal:false,
            iftoolong:false,
            ifexport:false,
            exporturl:null,
            adminid:null,
            glabolid:null,
            versionname:null,
            thisDictVName:null,
            versionlist:[],
            versionDetaillist:[],
            versionnameifDu:false,
            thisDictIsNull:false,
            globalflag:null,
            iflocked:false,
            ifinfluence:false,
            ifnameused:false,
            ifnamematchkinds:true,
            ifnamematchlength:true,
            conflictzzldata:null,
            confFiledata:null,
        }
    },
    componentDidMount(){
        var loginInfo = this.props.loginInfo;
        if(!loginInfo.name){
            this.context.router.push("/fronpage");
            //alert(loginInfo.name);
            IfDispatcher.dispatch({
                actionType:Comment.LOGOUT
            });
        }else{
            this.getdictsList();
        }
    },

    handleChange(name,event){
        var newstate ={};
        newstate[name] = event.target.value;
        newstate.iferror = false;
        newstate.ifduplicate=false;
        newstate.ifadmindupdict=false;
        newstate.iftoolong=false;
        newstate.ifsourceeqltar=false;
        newstate.ifclicDic=true;
        newstate.versionnameifDu=false;
        newstate.thisDictIsNull=false;
        newstate.ifnameused=false;
        newstate.ifnamematchkinds=true;
        newstate.ifnamematchlength=true;
        newstate.ifinfluence=false;
        newstate.conflictzzldata=null;
        this.setState(newstate);

    },
    /**
     *The function name: getdictsList.
     *Function used: To obtain a list dictionary.
     *Create time: 2017/4/5
     */
    getdictsList(){
        $("#ajaxloading").show();
        var loginInfo = this.props.loginInfo;
        var that = this;
        $.ajax({
            url: Comment.DATA_URL+"KDB/GetUserDict",
            data:$.param({
                userid:loginInfo.userid,
            }),
            type: 'GET',
            success: function(data){
                //alert("success");
                var msg = data;
                IfDispatcher.dispatch({
                    actionType: Comment.GETDICTSLIST,
                    dictslist:msg.data
                });
                var dic_id = msg.data[0].dic_id;
                var sdic_id = msg.data[1].dic_id;
                that.setState({
                    ifdicid:dic_id,
                    adminid:dic_id,
                    glabolid:sdic_id,
                    globalflag:msg.data[0].app_range,
                });
                var hash = crypto.createHash("md5");
                hash.update(dic_id.toString());
                $.ajax({
                    url: Comment.DATA_URL+"KDB/FindDic",
                    data:$.param({
                        dicid:dic_id,
                        paramcheck:hash.digest("hex"),
                    }),
                    type: 'GET',
                    success: function(data){
                        $("#ajaxloading").hide();
                        var msg = data;
                        IfDispatcher.dispatch({
                            actionType: Comment.GETDICTDATA,
                            dictdatalist:msg.data
                        });
                    },
                    error:function(){
                        $("#ajaxloading").hide();
                        //alert("get globaldata fail");
                    },
                    statusCode:{
                        414:function(){
                        },
                        406:function (){
                            IfDispatcher.dispatch({
                                actionType:Comment.LOGOUT
                            });
                        }
                    }
                });

            },
            error:function(){
                $("#ajaxloading").hide();
               // alert("get dictslist fail");
            },
            statusCode:{
                414:function(){
                },
                406:function (){
                    IfDispatcher.dispatch({
                        actionType:Comment.LOGOUT
                    });
                }
            }
        });

    },
    /**
     *The function name: adddictclick.
     *Function used: The new dictionary entry.
     *Create time: 2017/4/5
     */
    adddictclick(){
        this.setState({
            iferror:false,
            ifsourceeqltar:false,
            ifduplicate:false,
            iftoolong:false,
            ifnamematchkinds:true,
        });
        $("#adddict").modal("show");
    },
    /**
     *The function name: confirmAdddict.
     *Function used: The new dictionary entry.
     *Create time: 2017/4/5
     */
    confirmAdddict(){
        if(!this.state.dictname ){
            this.setState({iferror:true});
        }
        else if(this.checkusername(this.state.dictname)){
            this.setState({ifnamematchkinds:false});
        }
        else if(this.state.dictname.length>40){
            this.setState({iftoolong:true});
        }else{
            $("#ajaxloading").show();
            var loginInfo = this.props.loginInfo;
            var that = this;
            $.ajax({
                url: Comment.DATA_URL+"KDB/AddDictionary",
                data: $.param({
                    dicname:that.state.dictname,
                    userid:loginInfo.userid,
                }),
                type: 'POST',
                contentType: 'application/x-www-form-urlencoded',
                success: function (data) {
                    $("#adddict").modal("hide");
                    $("#ajaxloading").hide();
                    //增加时让勾勾显示在全局变量那里
                    that.state.selectItem=0;
                    that.getdictsList();
                },
                error:function(){
                    $("#ajaxloading").hide();
                },
                statusCode:{
                    406:function (){
                        IfDispatcher.dispatch({
                            actionType:Comment.LOGOUT
                        });
                    },
                    414:function(){
                        alert("add dict fail!");
                    },
                    413:function(){
                        if(loginInfo.ifadmin){
                            that.setState({
                                ifadmindupdict:true
                            })
                        }else{
                            that.setState({
                                ifduplicate:true
                            })
                        }

                    },
                    417:function () {
                        that.setState({
                            ifnameused:true
                        })
                    }

                }
            });
        }
    },
    /**
     *The function name: dictdataclick.
     *Function used: Get a dictionary entry details.
     *Create time: 2017/4/5
     */
    dictdataclick(dicid,item,global){
        var that = this;
        var inFunc = function(){
            var dic_id = dicid;
            that.setState({
                ifdicid:dicid,
                selectItem:item,
                ifglobal:false,
                ifclicDic:true,
                ifexport:false,
                exporturl:null,
                globalflag:global,
            })
            var hash = crypto.createHash("md5");
            hash.update(dic_id.toString());
            $.ajax({
                url: Comment.DATA_URL+"KDB/FindDic",
                data:$.param({
                    dicid:dic_id,
                    paramcheck:hash.digest("hex"),
                }),
                type: 'GET',
                success: function(data){
                    // alert("success");
                    var msg = data;
                    IfDispatcher.dispatch({
                        actionType: Comment.GETDICTDATA,
                        dictdatalist:msg.data
                    });
                },
                error:function(){
                    $("#ajaxloading").hide();
                   // alert("get FindDic fail");
                },
                statusCode:{
                    414:function(){
                    },
                    406:function (){
                        IfDispatcher.dispatch({
                            actionType:Comment.LOGOUT
                        });
                    }
                }
            });
        };
        return inFunc;
    },
    /**
     *The function name: addentrieclick.
     *Function used: New items in group existing entries.
     *Create time: 2017/4/5
     */
    addentrieclick(dicid,targetname){
        var that = this;
        var inFunc = function(){
            that.state.dicid = dicid;
            that.state.targetname = targetname;
            that.setState({
                iferror:false,
                ifsourceeqltar:false,
                ifduplicate:false,
                iflocked:false,
                ifinfluence:false,
                conflictzzldata:null,
                sourcename:null,
                //ifnamematchkinds:true,
                //ifnamematchlength:true,
            });
            $("#addentrie").modal("show");
        };
        return inFunc;
    },
    /**
     *The function name: confirmaddentrie.
     *Function used: New items in group existing entries.
     *Create time: 2017/4/5
     */
    confirmaddentrie(){
        if(!this.state.sourcename){
            this.setState({iferror:true});
        }
        else if(this.state.sourcename==this.state.targetname){
            this.setState({ifsourceeqltar:true});
        } else{
            $("#ajaxloading").show();
            var that = this;
            $.ajax({
                url: Comment.DATA_URL+"KDB/CheckAddDic",
                data: $.param({
                    sourcename:that.state.sourcename,
                    dicId:that.state.dicid,
                    targetname:that.state.targetname,
                    userId:that.props.loginInfo.userid,
                }),
                type: 'POST',
                contentType: 'application/x-www-form-urlencoded',
                success: function (data) {
                    var msgzzl = data;
                    if(200 == parseInt(msgzzl.status)){
                        $.ajax({
                            url: Comment.DATA_URL+"KDB/AddDic",
                            data:$.param({
                                sourcename:that.state.sourcename,
                                dicid:that.state.dicid,
                                targetname:that.state.targetname,
                                userId:that.props.loginInfo.userid,
                            }),
                            type: 'POST',
                            contentType: 'application/x-www-form-urlencoded',
                            success: function(data){
                                $("#addentrie").modal("hide");
                                var dic_id = that.state.dicid;
                                var hash = crypto.createHash("md5");
                                hash.update(dic_id.toString());
                                $.ajax({
                                    url: Comment.DATA_URL+"KDB/FindDic",
                                    data:$.param({
                                        dicid:dic_id,
                                        paramcheck:hash.digest("hex"),
                                    }),
                                    type: 'GET',
                                    success: function(data){
                                        $("#ajaxloading").hide();
                                        var msg = data;
                                        IfDispatcher.dispatch({
                                            actionType: Comment.GETDICTDATA,
                                            dictdatalist:msg.data
                                        });

                                    },
                                    error:function(){
                                        $("#ajaxloading").hide();
                                        // alert("get dictdata fail");
                                    },
                                    statusCode:{
                                        406:function (){
                                            IfDispatcher.dispatch({
                                                actionType:Comment.LOGOUT
                                            });
                                        },
                                        414:function(){
                                            //alert("add entries fail!");
                                        },
                                    }
                                });
                            },
                            error:function(){
                                $("#ajaxloading").hide();
                            },
                            statusCode:{
                                406:function (){
                                    IfDispatcher.dispatch({
                                        actionType:Comment.LOGOUT
                                    });
                                },
                                414:function(){
                                    //alert("add entries fail!");
                                },
                                415:function(){
                                    that.setState({
                                        iflocked:true
                                    })
                                }
                            }
                        });
                    }else{
                        $("#ajaxloading").hide();
                        that.setState({
                            conflictzzldata:msgzzl.data
                        });
                    }
                },
                error:function(){
                    $("#ajaxloading").hide();
                },
                statusCode:{
                    406:function (){
                        IfDispatcher.dispatch({
                            actionType:Comment.LOGOUT
                        });
                    },
                    414:function(){
                        alert("add entries fail!");
                    },
                    413:function(){
                        that.setState({
                            ifduplicate:true
                        })
                    },
                    417:function(){
                        that.setState({
                            ifinfluence:true,
                        });
                    },
                }
            });
        }
    },
    /**
     *The function name: deletallEntries.
     *Function used: Delete item group.
     *Create time: 2017/4/5
     */
    deletallEntries(dicid,targetname){
        var that = this;
        var inFunc = function(){
            that.state.dicids = dicid;
            that.state.targetnames = targetname;
            that.setState({
                iflocked:false,
            });
            $("#deleteentrie").modal("show");
        };
        return inFunc;
    },
    /**
     *The function name: confirmdeleteentries.
     *Function used: Delete item group.
     *Create time: 2017/4/5
     */
    confirmdeleteentries(){
        if(true){
            $("#ajaxloading").show();
            var that = this;
            var hashs = crypto.createHash("md5");
            var msgg = that.state.targetnames+that.state.dicids+that.props.loginInfo.userid;
            hashs.update(msgg);
            $.ajax({
                url: Comment.DATA_URL+"KDB/DelAllDic",
                data:$.param({
                    dicid:that.state.dicids,
                    userId:that.props.loginInfo.userid,
                    targetname:that.state.targetnames,
                    paramcheck:hashs.digest("hex"),
                    _method:"delete"
                }),
                type: 'POST',
                success: function(data){
                    // alert("success");
                    $("#deleteentrie").modal("hide");
                    var dic_id = that.state.dicids;
                    var hash = crypto.createHash("md5");
                    hash.update(dic_id.toString());
                    $.ajax({
                        url: Comment.DATA_URL+"KDB/FindDic",
                        data:$.param({
                            dicid:dic_id,
                            paramcheck:hash.digest("hex"),
                        }),
                        type: 'GET',
                        success: function(data){
                            // alert("success");
                            $("#ajaxloading").hide();
                            var msg = data;
                            IfDispatcher.dispatch({
                                actionType: Comment.GETDICTDATA,
                                dictdatalist:msg.data
                            });

                        },
                        error:function(){
                            $("#ajaxloading").hide();
                            //alert("get dictdata fail");
                        },
                        statusCode:{
                            414:function(){
                            },
                            406:function (){
                                IfDispatcher.dispatch({
                                    actionType:Comment.LOGOUT
                                });
                            }
                        }
                    });

                },
                error:function(){
                    $("#ajaxloading").hide();
                },
                statusCode:{
                    406:function (){
                        IfDispatcher.dispatch({
                            actionType:Comment.LOGOUT
                        });
                    },
                    414:function(){
                        alert("delete DelAllDic fail!");
                    },
                    415:function(){
                        that.setState({
                            iflocked:true
                        })
                    }
                }
            });
        }
    },
    /**
     *The function name: deletentries.
     *Function used: Delete the single entry.
     *Create time: 2017/4/5
     */
    deletentries(dicid,targetname,sourcename){
        var that = this;
        var inFunc = function(){
            that.state.dicidd = dicid;
            that.state.targetnamed = targetname;
            that.state.sourcenamed = sourcename;
            that.setState({
                iflocked:false,
            });
            $("#deletezzldd").modal("show");
        };
        return inFunc;
    },
    /**
     *The function name: confirmdeleteentrie.
     *Function used: Delete the single entry.
     *Create time: 2017/4/5
     */
    confirmdeleteentrie(){
        if(true){
            $("#ajaxloading").show();
            var that = this;
            var hashs = crypto.createHash("md5");
            var ddmsg = that.state.sourcenamed+that.state.targetnamed+that.state.dicidd+that.props.loginInfo.userid;
            hashs.update(ddmsg);
            $.ajax({
                url: Comment.DATA_URL+"KDB/DelDic",
                data:$.param({
                    dicid:that.state.dicidd,
                    userId:that.props.loginInfo.userid,
                    targetname:that.state.targetnamed,
                    sourcename:that.state.sourcenamed,
                    paramcheck:hashs.digest("hex"),
                    _method:"delete"
                }),
                type: 'POST',
                success: function(data){
                    // alert("success");
                    $("#deletezzldd").modal("hide");
                    var dic_id = that.state.dicidd;
                    var hash = crypto.createHash("md5");
                    hash.update(dic_id.toString());
                    $.ajax({
                        url: Comment.DATA_URL+"KDB/FindDic",
                        data:$.param({
                            dicid:dic_id,
                            paramcheck:hash.digest("hex"),
                        }),
                        type: 'GET',
                        success: function(data){
                            // alert("success");
                            $("#ajaxloading").hide();
                            var msg = data;
                            IfDispatcher.dispatch({
                                actionType: Comment.GETDICTDATA,
                                dictdatalist:msg.data
                            });

                        },
                        error:function(){
                            $("#ajaxloading").hide();
                           // alert("get dictdata fail");
                        },
                        statusCode:{
                            414:function(){
                            },
                            406:function (){
                                IfDispatcher.dispatch({
                                    actionType:Comment.LOGOUT
                                });
                            }
                        }
                    });

                },
                error:function(){
                    $("#ajaxloading").hide();
                },
                statusCode:{
                    406:function (){
                        IfDispatcher.dispatch({
                            actionType:Comment.LOGOUT
                        });
                    },
                    414:function(){
                        alert("delete the entry fail");
                    },
                    415:function(){
                        that.setState({
                            iflocked:true
                        })
                    }
                }
            });
        }
    },
    /**
     *The function name: amendentrie.
     *Function used: Modify the entry.
     *Create time: 2017/4/5
     */
    amendentrie(dicid,targetname,sourcename){
        var that = this;
        var inFunc = function(){
            that.state.dicidf = dicid;
            that.state.targetnamef = targetname;
            that.state.sourcenamef = sourcename;
            that.setState({
                iferror:false,
                ifsourceeqltar:false,
                ifduplicate:false,
            });
            $("#amendentrie").modal("show");
        };
        return inFunc;
    },
    /**
     *The function name: confirmamendentrie.
     *Function used: Modify the entry.
     *Create time: 2017/4/5
     */
    confirmamendentrie(){
        if(!this.state.sourcenameresult){
          this.setState({iferror:true});
        }else if(this.state.targetnamef==this.state.sourcenameresult){
            this.setState({ifsourceeqltar:true});
        }else{
            $("#ajaxloading").show();
            var that = this;
            var hashs = crypto.createHash("md5");
            var msgfg = that.state.sourcenamef+that.state.targetnamef+that.state.sourcenameresult+that.state.dicidf;
            hashs.update(msgfg);
            $.ajax({
                url: Comment.DATA_URL+"KDB/UpdateDic",
                data:$.param({
                    dicid:that.state.dicidf,
                    targetname:that.state.targetnamef,
                    sourcename:that.state.sourcenamef,
                    sourcenameresult:that.state.sourcenameresult,
                    paramcheck:hashs.digest("hex"),
                }),
                type: 'POST',
                success: function(data){
                    // alert("success");
                    $("#amendentrie").modal("hide");
                    var dic_id = that.state.dicidf;
                    var hash = crypto.createHash("md5");
                    hash.update(dic_id.toString());
                    $.ajax({
                        url: Comment.DATA_URL+"KDB/FindDic",
                        data:$.param({
                            dicid:dic_id,
                            paramcheck:hash.digest("hex"),
                        }),
                        type: 'GET',
                        success: function(data){
                            // alert("success");
                            $("#ajaxloading").hide();
                            var msg = data;
                            IfDispatcher.dispatch({
                                actionType: Comment.GETDICTDATA,
                                dictdatalist:msg.data
                            });

                        },
                        error:function(){
                            $("#ajaxloading").hide();
                            //alert("get dictdata fail");
                        },
                        statusCode:{
                            414:function(){
                            },
                            406:function (){
                                IfDispatcher.dispatch({
                                    actionType:Comment.LOGOUT
                                });
                            }
                        }
                    });

                },
                error:function(){
                    $("#ajaxloading").hide();
                },
                statusCode:{
                    406:function (){
                        IfDispatcher.dispatch({
                            actionType:Comment.LOGOUT
                        });
                    },
                    413:function(){
                        that.setState({
                            ifduplicate:true
                        })
                    }
                }
            });
        }
    },
    /**
     *The function name: addNewEntries.
     *Function used: New items in group existing entries.
     *Create time: 2017/4/5
     */
    addNewEntries(){
        this.setState({
            iferror:false,
            ifsourceeqltar:false,
            ifduplicate:false,
            iflocked:false,
            ifinfluence:false,
            conflictzzldata:null,
            targetnamezzl:null,
            sourcenamezzl:null,
            //ifnamematchkinds:true,
            //ifnamematchlength:true,
        });
        $("#addNewEntries").modal("show");
    },
    checkusername(Yname){
        var regular = /^\w+$/ ;
        if(!regular.test(Yname)){
            // alert("用户名只能包括英文字母 数字下划线！");
            return true;
        }
        return false;
    },
    /**
     *The function name: comfrimaddNewEntries.
     *Function used: New items in group existing entries.
     *Create time: 2017/4/5
     */
    comfrimaddNewEntries(){
        if(!this.state.sourcenamezzl || !this.state.targetnamezzl){
            this.setState({iferror:true});
        }
        else if(this.state.sourcenamezzl==this.state.targetnamezzl){
            this.setState({ifsourceeqltar:true});
        }else{
            $("#ajaxloading").show();
            var that = this;
            $.ajax({
                url: Comment.DATA_URL+"KDB/CheckAddDic",
                data: $.param({
                    sourcename:that.state.sourcenamezzl,
                    dicId:that.state.ifdicid,
                    targetname:that.state.targetnamezzl,
                    userId:that.props.loginInfo.userid,
                }),
                type: 'POST',
                contentType: 'application/x-www-form-urlencoded',
                success: function (data) {
                    var msgzzl = data;
                    if(200 == parseInt(msgzzl.status)){
                        $.ajax({
                            url: Comment.DATA_URL+"KDB/AddNewDic",
                            data:$.param({
                                sourcename:that.state.sourcenamezzl,
                                dicid:that.state.ifdicid,
                                targetname:that.state.targetnamezzl,
                                userId:that.props.loginInfo.userid,
                            }),
                            type: 'POST',
                            contentType: 'application/x-www-form-urlencoded',
                            success: function(data){
                                $("#addNewEntries").modal("hide");
                                var dic_id = that.state.ifdicid;
                                var hash = crypto.createHash("md5");
                                hash.update(dic_id.toString());
                                $.ajax({
                                    url: Comment.DATA_URL+"KDB/FindDic",
                                    data:$.param({
                                        dicid:dic_id,
                                        paramcheck:hash.digest("hex"),
                                    }),
                                    type: 'GET',
                                    success: function(data){
                                        $("#ajaxloading").hide();
                                        var msg = data;
                                        IfDispatcher.dispatch({
                                            actionType: Comment.GETDICTDATA,
                                            dictdatalist:msg.data
                                        });

                                    },
                                    error:function(){
                                        $("#ajaxloading").hide();
                                        // alert("get dictdata fail");
                                    },
                                    statusCode:{
                                        406:function (){
                                            IfDispatcher.dispatch({
                                                actionType:Comment.LOGOUT
                                            });
                                        },
                                        414:function(){
                                            //alert("add entries fail!");
                                        },
                                    }
                                });
                            },
                            error:function(){
                                $("#ajaxloading").hide();
                            },
                            statusCode:{
                                406:function (){
                                    IfDispatcher.dispatch({
                                        actionType:Comment.LOGOUT
                                    });
                                },
                                414:function(){
                                    //alert("add entries fail!");
                                },
                                415:function(){
                                    that.setState({
                                        iflocked:true
                                    })
                                }
                            }
                        });
                    }else{
                        $("#ajaxloading").hide();
                        that.setState({
                            conflictzzldata:msgzzl.data
                        });
                    }
                },
                error:function(){
                    $("#ajaxloading").hide();
                },
                statusCode:{
                    406:function (){
                        IfDispatcher.dispatch({
                            actionType:Comment.LOGOUT
                        });
                    },
                    414:function(){
                        alert("add entries fail!");
                    },
                    413:function(){
                        that.setState({
                            ifduplicate:true
                        })
                    },
                    417:function(){
                        that.setState({
                            ifinfluence:true,
                        });
                    },
                }
            });
        }
    },
    permitAdddict(){
        $("#ajaxloading").show();
        var hashs = crypto.createHash("md5");
        hashs.update(JSON.stringify(this.state.conflictzzldata));
        var permitdata = {
            dicId:this.state.ifdicid,
            data:this.state.conflictzzldata,
            paramcheck:hashs.digest("hex"),
        }
        var that = this;
        $.ajax({
            url: Comment.DATA_URL+"KDB/AddDicAfterCheck ",
            data:JSON.stringify(permitdata),
            dataType:"json",
            type: 'POST',
            contentType: 'application/json;charset=utf-8',
            success: function (data) {
                $("#addNewEntries").modal("hide");
                $("#addentrie").modal("hide");
                var dic_id = that.state.ifdicid;
                var hash = crypto.createHash("md5");
                hash.update(dic_id.toString());
                $.ajax({
                    url: Comment.DATA_URL+"KDB/FindDic",
                    data:$.param({
                        dicid:dic_id,
                        paramcheck:hash.digest("hex"),
                    }),
                    type: 'GET',
                    success: function(data){
                        $("#ajaxloading").hide();
                        var msg = data;
                        IfDispatcher.dispatch({
                            actionType: Comment.GETDICTDATA,
                            dictdatalist:msg.data
                        });

                    },
                    error:function(){
                        $("#ajaxloading").hide();
                        // alert("get dictdata fail");
                    },
                    statusCode:{
                        406:function (){
                            IfDispatcher.dispatch({
                                actionType:Comment.LOGOUT
                            });
                        },
                        414:function(){
                            //alert("add entries fail!");
                        },
                    }
                });
            },
            error:function(){
                $("#ajaxloading").hide();
            },
            statusCode:{
                414:function(){
                    alert("add AddDicAfterCheck fail!");
                },
                406:function (){
                    IfDispatcher.dispatch({
                        actionType:Comment.LOGOUT
                    });
                }

            }
        });
    },
    /**
     *The function name: filterChange.
     *Function used: Search some entries.
     *Create time: 2017/4/5
     */
    filterChange(event){
        this.setState({
            filtertext:event.target.value,
        })
    },
    removeDictClick(){
        $("#removeDic").modal("show");
    },
    removeDtConf(){
        var that=this;
        $.ajax({
            url: Comment.DATA_URL+"KDB/ClearDic",
            data: $.param({
                dicid:that.state.ifdicid,
            }),
            type: 'POST',
            contentType: 'application/x-www-form-urlencoded',
            success: function (data) {
                $("#removeDic").modal("hide");
                var hash = crypto.createHash("md5");
                hash.update(that.state.ifdicid.toString());
                $.ajax({
                    url: Comment.DATA_URL+"KDB/FindDic",
                    data:$.param({
                        dicid:that.state.ifdicid,
                        paramcheck:hash.digest("hex"),
                    }),
                    type: 'GET',
                    success: function(data){
                        // alert("success");
                        var msg = data;
                        IfDispatcher.dispatch({
                            actionType: Comment.GETDICTDATA,
                            dictdatalist:msg.data
                        });

                    },
                    error:function(){
                        $("#ajaxloading").hide();
                        // alert("get FindDic fail");
                    },
                    statusCode:{
                        414:function(){
                        },
                        406:function (){
                            IfDispatcher.dispatch({
                                actionType:Comment.LOGOUT
                            });
                        }
                    }
                });

            },
            error:function(){
                $("#ajaxloading").hide();
            },
            statusCode:{
                500:function(){
                    IfDispatcher.dispatch({
                        actionType:Comment.ERROR500
                    });
                },

                414:function(){
                    //that.setState({
                    //    ifremoveDic:false
                    //})
                },
                413:function(){

                },
                406:function (){
                    IfDispatcher.dispatch({
                        actionType:Comment.LOGOUT
                    });
                },
            }

        });
    },
    /**
     *The function name: deleteDicCli.
     *Function used: Delete the dictionary file.
     *Create time: 2017/4/5
     */
    deleteDicCli(){
            $("#deleteDic").modal("show");
    },
    /**
     *The function name: deleteDicConf.
     *Function used: Delete the dictionary file.
     *Create time: 2017/4/5
     */
    deleteDicConf(){

        if(this.state.globalflag=="GLOBAL"){
            this.setState({ifglobal:true});
        }else{
            var that=this;
            $.ajax({
                url: Comment.DATA_URL+"KDB/DelDictionary",
                data: $.param({
                    dicid:that.state.ifdicid,
                    _method:"delete"
                }),
                type: 'POST',
                contentType: 'application/x-www-form-urlencoded',
                success: function (data) {
                    $("#deleteDic").modal("hide");
                    that.getdictsList();
                    that.state.selectItem=0;
                },
                error:function(){
                    $("#ajaxloading").hide();
                },
                statusCode:{
                    500:function(){
                        IfDispatcher.dispatch({
                            actionType:Comment.ERROR500
                        });
                    },

                    414:function(){
                        that.setState({
                        ifclicDic:false
                        })
                    },
                    413:function(){

                    },
                    406:function (){
                        IfDispatcher.dispatch({
                            actionType:Comment.LOGOUT
                        });
                    },
                }

            });
        }

    },
    /**
     *The function name: readcsvfile.
     *Function used: Import the dictionary file.
     *Create time: 2017/4/5
     */
    readcsvfile(obj){
       // var Ifcsvfile = $(obj)[0].files[0].name.split(".");
        var data = new FormData();
        data.append("name",$(obj)[0].files[0].name);
        data.append("file",$(obj)[0].files[0]);
        // data.append("dicid",this.state.ifdicid);
        $("#ajaxloading").show();
        var that = this;
        $.ajax({
            url: Comment.DATA_URL+"KDB/Upload?dicid="+that.state.ifdicid,
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            success: function(data){
                var msgz = data;
                if(400 == msgz.status){
                    $("#ajaxloading").hide();
                    alert(msgz.data);
                }else if(201 == msgz.status){
                    $("#ajaxloading").hide();
                    that.setState({
                        confFiledata:msgz.data
                    });
                    $("#confFileEntries").modal("show");
                }else{
                    var dic_id = that.state.ifdicid;
                    var hash = crypto.createHash("md5");
                    hash.update(dic_id.toString());
                    $.ajax({
                        url: Comment.DATA_URL+"KDB/FindDic",
                        data:$.param({
                            dicid:dic_id,
                            paramcheck:hash.digest("hex"),
                        }),
                        type: 'GET',
                        success: function(data){
                            $("#ajaxloading").hide();
                            var msg = data;
                            IfDispatcher.dispatch({
                                actionType: Comment.GETDICTDATA,
                                dictdatalist:msg.data
                            });

                        },
                        error:function(){
                            $("#ajaxloading").hide();
                        },
                        statusCode:{
                            406:function (){
                                IfDispatcher.dispatch({
                                    actionType:Comment.LOGOUT
                                });
                            },
                            414:function(){

                            },
                        }
                    });
                }
            },
            error:function(jxr,scode){
                //alert("import fail!");
                $("#ajaxloading").hide();
            },
            statusCode:{
                406:function (){
                    IfDispatcher.dispatch({
                        actionType:Comment.LOGOUT
                    });
                },
                414:function(){
                    alert("Invalid format of the file content!");
                },
                413:function () {
                    alert("The file is empty!");
                },
                415:function () {
                    alert("Uploading files exceeds the maximum limit.");
                }
            }
        });
    },
    permitFilezzldict(){
        $("#ajaxloading").show();
        var permitdata = {
            dicId:this.state.ifdicid,
            data:this.state.confFiledata
        }
        var that = this;
        $.ajax({
            url: Comment.DATA_URL+"KDB/AddDicAfterUpLoad",
            data:JSON.stringify(permitdata),
            dataType:"json",
            type: 'POST',
            contentType: 'application/json;charset=utf-8',
            success: function (data) {
                $("#confFileEntries").modal("hide");
                var dic_id = that.state.ifdicid;
                var hash = crypto.createHash("md5");
                hash.update(dic_id.toString());
                $.ajax({
                    url: Comment.DATA_URL+"KDB/FindDic",
                    data:$.param({
                        dicid:dic_id,
                        paramcheck:hash.digest("hex"),
                    }),
                    type: 'GET',
                    success: function(data){
                        $("#ajaxloading").hide();
                        var msg = data;
                        IfDispatcher.dispatch({
                            actionType: Comment.GETDICTDATA,
                            dictdatalist:msg.data
                        });
                    },
                    error:function(){
                        $("#ajaxloading").hide();
                        // alert("get dictdata fail");
                    },
                    statusCode:{
                        406:function (){
                            IfDispatcher.dispatch({
                                actionType:Comment.LOGOUT
                            });
                        },
                        414:function(){
                            //alert("add entries fail!");
                        },
                    }
                });
            },
            error:function(){
                $("#ajaxloading").hide();
            },
            statusCode:{
                414:function(){
                    alert("add AddDicAfterUpLoad fail!");
                },
                406:function (){
                    IfDispatcher.dispatch({
                        actionType:Comment.LOGOUT
                    });
                }
            }
        });
    },
    /**
     *The function name: chooseFile.
     *Function used: Choose the dictionary file import.
     *Create time: 2017/4/5
     */
    chooseFile(){
        var that = this;

        $('#fileDialog').unbind('change');
        $('#fileDialog').change(function(evt) {
            that.readcsvfile($('#fileDialog'));
            $(this).val('');

        });
        $('#fileDialog').trigger('click');
    },
    /**
     *The function name: importdictClick.
     *Function used: Import the dictionary file.
     *Create time: 2017/4/5
     */
    importdictClick(){
        this.setState({
            confFiledata:null,
        });
        this.chooseFile();
    },
    /**
     *The function name: exportcsv.
     *Function used: Export the dictionary file.
     *Create time: 2017/4/5
     */
    exportcsv(){
        var that = this;
        var dic_id = that.state.ifdicid;
        var hash = crypto.createHash("md5");
        hash.update(dic_id.toString());
        $.ajax({
            url: Comment.DATA_URL+"KDB/DownloadDic",
            data: $.param({
                dicid:dic_id,
                paramcheck:hash.digest("hex"),
            }),
            contentType: 'application/x-www-form-urlencoded',
            type: 'GET',
            success: function (data) {
                $("#ajaxloading").hide();
                var msg = data;
                that.setState({
                    ifexport:true,
                    exporturl:msg.url,
                });
            },
            error:function(jxr,scode){
                $("#ajaxloading").hide();
                //alert("DownloadDic fail!");
            },
            statusCode:{
                406:function (){
                    IfDispatcher.dispatch({
                        actionType:Comment.LOGOUT
                    });
                },
                414:function(){

                },

            }
        });
    },
    /**
     *The function name: versiondictClick.
     *Function used: To obtain a list dictionary version.
     *Create time: 2017/4/5
     */
    versiondictClick(){
        var that =this;

        that.setState({
            versionDetaillist:[],
            thisDictIsNull:false,
            versionnameifDu:false,
        });
        $.ajax({
            url: Comment.DATA_URL+"KDB/GetAllSaveDic",
            data: $.param({
                dicid:that.state.ifdicid,
            }),
            contentType: 'application/x-www-form-urlencoded',
            type: 'GET',
            success: function (data) {

                var msg = data;

                that.setState({
                    versionlist:msg.data,
                });
                $("#ajaxloading").hide();
                $("#versionDic").modal("show");
            },
            error:function(){
                $("#ajaxloading").hide();
            },
            statusCode:{
                413:function(){

                },
                409:function(){

                },
                406:function (){
                    IfDispatcher.dispatch({
                        actionType:Comment.LOGOUT
                    });
                },
            }
        });
    },
    /**
     *The function name: saveVersionCli.
     *Function used: Save the current dictionary version.
     *Create time: 2017/4/5
     */
    saveVersionCli(){
        var dicdata=this.props.dictdatalist;
        var that=this;
        that.setState({
            versionDetaillist:[],
            thisDictIsNull:false,
            versionnameifDu:false,
        });

        if(!(dicdata.length>0)){
            that.setState({
                thisDictIsNull:true,
            });
        }else{
            $("#versionEditDic").modal("show");
        }

    },
    /**
     *The function name: addDictVersionComf.
     *Function used: Save the current dictionary version.
     *Create time: 2017/4/5
     */
    addDictVersionComf(){
        var that=this;
         $.ajax({
            url: Comment.DATA_URL+"KDB/AddDicSave",
            data: $.param({
                dicid:that.state.ifdicid,
                savename:that.state.versionname,
            }),
            contentType: 'application/x-www-form-urlencoded',
            type: 'POST',
            success: function (data) {
                $("#versionEditDic").modal("hide");
                $("#ajaxloading").hide();
                $.ajax({
                    url: Comment.DATA_URL+"KDB/GetAllSaveDic",
                    data: $.param({
                        dicid:that.state.ifdicid,
                    }),
                    contentType: 'application/x-www-form-urlencoded',
                    type: 'GET',
                    success: function (data) {
                        $("#ajaxloading").hide();

                        var msg = data;
                        that.setState({
                            versionlist:msg.data,
                        });
                    },
                    error:function(){
                        $("#ajaxloading").hide();
                    },
                    statusCode:{
                        413:function(){

                        },
                        406:function (){
                            IfDispatcher.dispatch({
                                actionType:Comment.LOGOUT
                            });
                        },
                    }
                });
            },
            error:function(){
                $("#ajaxloading").hide();
            },
            statusCode:{
                413:function(){
                    that.setState({
                        versionnameifDu:true,
                    });
                },
                406:function (){
                    IfDispatcher.dispatch({
                        actionType:Comment.LOGOUT
                    });
                },
            }
          });
    },
    /**
     *The function name: delDicVCli.
     *Function used: Delete the corresponding dictionary version.
     *Create time: 2017/4/5
     */
    delDicVCli(name){
        var that = this;
        var inFunc = function(){
            that.state.thisDictVName = name;
            $("#deleteDictVersion").modal("show");
        };
        return inFunc;

    },
    /**
     *The function name: deleteDicVerConf.
     *Function used: Delete the corresponding dictionary version.
     *Create time: 2017/4/5
     */
    deleteDicVerConf(){
        var that=this;
        $.ajax({
            url: Comment.DATA_URL+"KDB/DeleteSaveDic",
            data: $.param({
                _method:"delete",
                dicid:that.state.ifdicid,
                savename:that.state.thisDictVName,
            }),
            contentType: 'application/x-www-form-urlencoded',
            type: 'POST',
            success: function (data) {
                $("#deleteDictVersion").modal("hide");
                $("#ajaxloading").hide();
                $.ajax({
                    url: Comment.DATA_URL+"KDB/GetAllSaveDic",
                    data: $.param({
                        dicid:that.state.ifdicid,
                    }),
                    contentType: 'application/x-www-form-urlencoded',
                    type: 'GET',
                    success: function (data) {
                        $("#ajaxloading").hide();

                        var msg = data;
                        that.setState({
                            versionlist:msg.data,
                        });
                    },
                    error:function(){
                        $("#ajaxloading").hide();
                    },
                    statusCode:{
                        414:function(){

                        },
                        406:function (){
                            IfDispatcher.dispatch({
                                actionType:Comment.LOGOUT
                            });
                        },
                    }
                });
            },
            error:function(){
                $("#ajaxloading").hide();
            },
            statusCode:{
                414:function(){

                },
                406:function (){
                    IfDispatcher.dispatch({
                        actionType:Comment.LOGOUT
                    });
                },
            }
        });
    },
    /**
     *The function name: fallbackDicVCli.
     *Function used: Roll back to the version of this dictionary.
     *Create time: 2017/4/5
     */
    fallbackDicVCli(name){
        var that = this;
        var inFunc = function(){
            that.state.thisDictVName = name;
            $("#fbDictVersion").modal("show");
        };
        return inFunc;
    },
    /**
     *The function name: fallbackDicVComf.
     *Function used: Roll back to the version of this dictionary.
     *Create time: 2017/4/5
     */
    fallbackDicVComf(){
        var that=this;
        $.ajax({
            url: Comment.DATA_URL+"KDB/ResetDic",
            data: $.param({
                savename:that.state.thisDictVName,
                dicid:that.state.ifdicid,
            }),
            contentType: 'application/x-www-form-urlencoded',
            type: 'POST',
            success: function (data) {
                $("#ajaxloading").hide();
                var hash = crypto.createHash("md5");
                hash.update(that.state.ifdicid.toString());
                $.ajax({
                    url: Comment.DATA_URL+"KDB/FindDic",
                    data:$.param({
                        dicid:that.state.ifdicid,
                        paramcheck:hash.digest("hex"),
                    }),
                    type: 'GET',
                    success: function(data){
                        $("#versionDic").modal("hide");
                        $("#fbDictVersion").modal("hide");
                        $("#ajaxloading").hide();
                        var msg = data;
                        IfDispatcher.dispatch({
                            actionType: Comment.GETDICTDATA,
                            dictdatalist:msg.data
                        });

                    },
                    error:function(){
                        $("#ajaxloading").hide();
                    },
                    statusCode:{
                        414:function(){

                        },
                        406:function (){
                            IfDispatcher.dispatch({
                                actionType:Comment.LOGOUT
                            });
                        },
                    }
                });

            },
            error:function(){
                $("#ajaxloading").hide();
            },
            statusCode:{
                414:function(){

                },
                406:function (){
                    IfDispatcher.dispatch({
                        actionType:Comment.LOGOUT
                    });
                },
            }
        });
    },
    /**
     *The function name: getDicVDetail.
     *Function used: Detail view this dictionary version.
     *Create time: 2017/4/5
     */
    getDicVDetail(name){
        var that = this;
        var inFunc = function(){
            that.state.thisDictVName = name;
            $.ajax({
                url: Comment.DATA_URL+"KDB/GetSaveDicSynDetail",
                data: $.param({
                    savename:that.state.thisDictVName,
                    dicid:that.state.ifdicid,
                }),
                contentType: 'application/x-www-form-urlencoded',
                type: 'GET',
                success: function (data) {
                    $("#ajaxloading").hide();

                    var msg = data;
                    that.setState({
                        versionDetaillist:msg.data,
                    });
                },
                error:function(){
                    $("#ajaxloading").hide();
                },
                statusCode:{
                    413:function(){

                    },
                    409:function(){

                    },
                    406:function (){
                        IfDispatcher.dispatch({
                            actionType:Comment.LOGOUT
                        });
                    },
                }
            });

        };
        return inFunc;
    },
    /**
     *The function name: handleeditChange.
     *Function used: Modify the barcode input to monitor.
     *Create time: 2017/4/5
     */
    handleeditChange(index){
        var that = this;
        var inFunc = function(event){
            IfDispatcher.dispatch({
                actionType: Comment.EDITINDEXTEXT,
                index:index-1,
                text:event.target.value,
                iferror:false,
                ifsourceeqltar:false,
                ifduplicate:false,
                confictdata:null,
                //iflonger:false,
                //ifnameerror:false,
                iflucked:false,
                ifexist:false,
            });
        };
        return inFunc;
    },
    canCelzzlAdddict(index,collapseid,inputid){
        var that = this;
        var inFunc = function(event){
            var data = that.props.inputtext;
            IfDispatcher.dispatch({
                actionType: Comment.EDITINDEXTEXT,
                index:index,
                text:"",
                iferror:false,
                ifsourceeqltar:false,
                ifduplicate:false,
                confictdata:null,
                //iflonger:false,
                //ifnameerror:false,
                iflucked:false,
                ifexist:false,
            });
            $("#"+inputid).val("");
            $('#'+collapseid).collapse('hide');
        };
        return inFunc;
    },
    permitzzlAdddict(index,collapseid,inputid){
        var that = this;
        var inFunc = function(event){
            $("#ajaxloading").show();
            var hashs = crypto.createHash("md5");
            hashs.update(JSON.stringify(that.props.inputtext[index].confictdata));
            var permitdata = {
                dicId:that.state.ifdicid,
                data:that.props.inputtext[index].confictdata,
                paramcheck:hashs.digest("hex"),
            }
            $.ajax({
                url: Comment.DATA_URL+"KDB/UpdateDicAfterCheck ",
                data:JSON.stringify(permitdata),
                dataType:"json",
                type: 'POST',
                contentType: 'application/json;charset=utf-8',
                success: function (data) {
                    $("#"+inputid).val("");
                    $('#'+collapseid).collapse('hide');
                    var hash = crypto.createHash("md5");
                    hash.update(that.state.ifdicid.toString());
                    $.ajax({
                        url: Comment.DATA_URL+"KDB/FindDic",
                        data:$.param({
                            dicid:that.state.ifdicid,
                            paramcheck:hash.digest("hex"),
                        }),
                        type: 'GET',
                        success: function(data){
                            $("#ajaxloading").hide();
                            var msg = data;
                            IfDispatcher.dispatch({
                                actionType: Comment.GETDICTDATA,
                                dictdatalist:msg.data
                            });

                        },
                        error:function(){
                            $("#ajaxloading").hide();
                            // alert("get dictdata fail");
                        },
                        statusCode:{
                            406:function (){
                                IfDispatcher.dispatch({
                                    actionType:Comment.LOGOUT
                                });
                            },
                            414:function(){
                                //alert("add entries fail!");
                            },
                        }
                    });
                },
                error:function(){
                    $("#ajaxloading").hide();
                },
                statusCode:{
                    414:function(){
                        alert("add UpdateDicAfterCheck fail!");
                    },
                    406:function (){
                        IfDispatcher.dispatch({
                            actionType:Comment.LOGOUT
                        });
                    }

                }
            });
        };
        return inFunc;
    },
    /**
     *The function name: editEntrieOk.
     *Function used: Modify the entry.
     *Create time: 2017/4/5
     */
    editEntrieOk(collapseid,inputid,index,dicid,tname,sname){
        var that = this;
        var inFunc = function(){
            var msg = that.props.inputtext;
            if(typeof(msg[index-1]) != "undefined"){
                if(!msg[index-1].text){
                    IfDispatcher.dispatch({
                        actionType: Comment.EDITINDEXTEXT,
                        index:index-1,
                        text:"",
                        iferror:true,
                        ifsourceeqltar:false,
                        ifduplicate:false,
                        confictdata:null,
                        //ifnameerror:false,
                        //iflonger:false,
                        iflucked:false,
                        ifexist:false,
                    });
                }
                //else if(that.checkusername(msg[index-1].text)){
                //    IfDispatcher.dispatch({
                //        actionType: Comment.EDITINDEXTEXT,
                //        index:index-1,
                //        text:msg[index-1].text,
                //        iferror:false,
                //        ifsourceeqltar:false,
                //        ifduplicate:false,
                //        ifnameerror:true,
                //        iflonger:false,
                //        iflucked:false,
                //        ifexist:false,
                //    });
                //}
                //else if(msg[index-1].text.length >40){
                //    IfDispatcher.dispatch({
                //        actionType: Comment.EDITINDEXTEXT,
                //        index:index-1,
                //        text:msg[index-1].text,
                //        iferror:false,
                //        ifsourceeqltar:false,
                //        ifduplicate:false,
                //        ifnameerror:false,
                //        iflonger:true,
                //        iflucked:false,
                //        ifexist:false,
                //    });
                //}
                else if(msg[index-1].text == tname){
                    IfDispatcher.dispatch({
                        actionType: Comment.EDITINDEXTEXT,
                        index:index-1,
                        text:tname,
                        iferror:false,
                        ifsourceeqltar:true,
                        ifduplicate:false,
                        confictdata:null,
                        //iflonger:false,
                        //ifnameerror:false,
                        iflucked:false,
                        ifexist:false,
                    });
                }else{
                    $("#ajaxloading").show();
                    //$.ajax({
                    //    url: Comment.DATA_URL+"KDB/UpdateDic",
                    //    data:$.param({
                    //        dicid:dicid,
                    //        userId:that.props.loginInfo.userid,
                    //        targetname:tname,
                    //        sourcename:sname,
                    //        sourcenameresult:msg[index-1].text,
                    //    }),
                    //    type: 'POST',
                    //    success: function(data){
                    //        IfDispatcher.dispatch({
                    //            actionType: Comment.EDITINDEXTEXT,
                    //            index:index-1,
                    //            text:"",
                    //            iferror:false,
                    //            ifsourceeqltar:false,
                    //            ifduplicate:false,
                    //            //ifnameerror:false,
                    //            //iflonger:false,
                    //            iflucked:false,
                    //            ifexist:false,
                    //        });
                    //        $("#"+inputid).val("");
                    //        $('#'+collapseid).collapse('hide');
                    //        $.ajax({
                    //            url: Comment.DATA_URL+"KDB/FindDic",
                    //            data:$.param({
                    //                dicid:dicid,
                    //            }),
                    //            type: 'GET',
                    //            success: function(data){
                    //                // alert("success");
                    //                $("#ajaxloading").hide();
                    //                var msg = data;
                    //                IfDispatcher.dispatch({
                    //                    actionType: Comment.GETDICTDATA,
                    //                    dictdatalist:msg.data
                    //                });
                    //            },
                    //            error:function(){
                    //                $("#ajaxloading").hide();
                    //            },
                    //            statusCode:{
                    //                414:function(){
                    //                },
                    //                406:function (){
                    //                    IfDispatcher.dispatch({
                    //                        actionType:Comment.LOGOUT
                    //                    });
                    //                }
                    //            }
                    //        });
                    //    },
                    //    error:function(){
                    //        $("#ajaxloading").hide();
                    //    },
                    //    statusCode:{
                    //        406:function (){
                    //            IfDispatcher.dispatch({
                    //                actionType:Comment.LOGOUT
                    //            });
                    //        },
                    //        413:function(){
                    //            IfDispatcher.dispatch({
                    //                actionType: Comment.EDITINDEXTEXT,
                    //                index:index-1,
                    //                text:msg[index-1].text,
                    //                iferror:false,
                    //                ifsourceeqltar:false,
                    //                ifduplicate:true,
                    //                //iflonger:false,
                    //                //ifnameerror:false,
                    //                iflucked:false,
                    //                ifexist:false,
                    //            });
                    //        },
                    //        415:function(){
                    //            IfDispatcher.dispatch({
                    //                actionType: Comment.EDITINDEXTEXT,
                    //                index:index-1,
                    //                text:msg[index-1].text,
                    //                iferror:false,
                    //                ifsourceeqltar:false,
                    //                ifduplicate:false,
                    //                //iflonger:false,
                    //                //ifnameerror:false,
                    //                iflucked:true,
                    //                ifexist:false,
                    //            });
                    //        },
                    //        417:function(){
                    //            IfDispatcher.dispatch({
                    //                actionType: Comment.EDITINDEXTEXT,
                    //                index:index-1,
                    //                text:msg[index-1].text,
                    //                iferror:false,
                    //                ifsourceeqltar:false,
                    //                ifduplicate:false,
                    //                //iflonger:false,
                    //                //ifnameerror:false,
                    //                iflucked:false,
                    //                ifexist:true,
                    //            });
                    //        }
                    //    }
                    //});
                    $.ajax({
                        url: Comment.DATA_URL+"KDB/CheckUpdateDic",
                        data:$.param({
                            dicId:dicid,
                            userId:that.props.loginInfo.userid,
                            targetname:tname,
                            sourcename:sname,
                            sourcenameresult:msg[index-1].text,
                        }),
                        type: 'POST',
                        success: function(data){
                            var msgzzl = data;
                            if(200 == parseInt(msgzzl.status)){
                                var hashs = crypto.createHash("md5");
                                var msgfg = sname+tname+msg[index-1].text+dicid+that.props.loginInfo.userid;
                                hashs.update(msgfg);
                                $.ajax({
                                    url: Comment.DATA_URL+"KDB/UpdateDic",
                                    data:$.param({
                                        dicid:dicid,
                                        userId:that.props.loginInfo.userid,
                                        targetname:tname,
                                        sourcename:sname,
                                        sourcenameresult:msg[index-1].text,
                                        paramcheck:hashs.digest("hex"),
                                    }),
                                    type: 'POST',
                                    success: function(data){
                                        IfDispatcher.dispatch({
                                            actionType: Comment.EDITINDEXTEXT,
                                            index:index-1,
                                            text:"",
                                            iferror:false,
                                            ifsourceeqltar:false,
                                            ifduplicate:false,
                                            confictdata:null,
                                            //ifnameerror:false,
                                            //iflonger:false,
                                            iflucked:false,
                                            ifexist:false,
                                        });
                                        $("#"+inputid).val("");
                                        $('#'+collapseid).collapse('hide');
                                        var hash = crypto.createHash("md5");
                                        hash.update(dicid.toString());
                                        $.ajax({
                                            url: Comment.DATA_URL+"KDB/FindDic",
                                            data:$.param({
                                                dicid:dicid,
                                                paramcheck:hash.digest("hex"),
                                            }),
                                            type: 'GET',
                                            success: function(data){
                                                // alert("success");
                                                $("#ajaxloading").hide();
                                                var msg = data;
                                                IfDispatcher.dispatch({
                                                    actionType: Comment.GETDICTDATA,
                                                    dictdatalist:msg.data
                                                });
                                            },
                                            error:function(){
                                                $("#ajaxloading").hide();
                                            },
                                            statusCode:{
                                                414:function(){
                                                },
                                                406:function (){
                                                    IfDispatcher.dispatch({
                                                        actionType:Comment.LOGOUT
                                                    });
                                                }
                                            }
                                        });
                                    },
                                    error:function(){
                                        $("#ajaxloading").hide();
                                    },
                                    statusCode:{
                                        406:function (){
                                            IfDispatcher.dispatch({
                                                actionType:Comment.LOGOUT
                                            });
                                        },
                                        415:function(){
                                            IfDispatcher.dispatch({
                                                actionType: Comment.EDITINDEXTEXT,
                                                index:index-1,
                                                text:msg[index-1].text,
                                                iferror:false,
                                                ifsourceeqltar:false,
                                                ifduplicate:false,
                                                confictdata:null,
                                                //iflonger:false,
                                                //ifnameerror:false,
                                                iflucked:true,
                                                ifexist:false,
                                            });
                                        },
                                    }
                                });
                            }else{
                                $("#ajaxloading").hide();
                                IfDispatcher.dispatch({
                                    actionType: Comment.EDITINDEXTEXT,
                                    index:index-1,
                                    text:msg[index-1].text,
                                    iferror:false,
                                    ifsourceeqltar:false,
                                    ifduplicate:false,
                                    confictdata:msgzzl.data,
                                    //iflonger:false,
                                    //ifnameerror:false,
                                    iflucked:false,
                                    ifexist:false,
                                });
                            }
                        },
                        error:function(){
                            $("#ajaxloading").hide();
                        },
                        statusCode:{
                            406:function (){
                                IfDispatcher.dispatch({
                                    actionType:Comment.LOGOUT
                                });
                            },
                            413:function(){
                                IfDispatcher.dispatch({
                                    actionType: Comment.EDITINDEXTEXT,
                                    index:index-1,
                                    text:msg[index-1].text,
                                    iferror:false,
                                    ifsourceeqltar:false,
                                    ifduplicate:true,
                                    confictdata:null,
                                    //iflonger:false,
                                    //ifnameerror:false,
                                    iflucked:false,
                                    ifexist:false,
                                });
                            },
                            417:function(){
                                IfDispatcher.dispatch({
                                    actionType: Comment.EDITINDEXTEXT,
                                    index:index-1,
                                    text:msg[index-1].text,
                                    iferror:false,
                                    ifsourceeqltar:false,
                                    ifduplicate:false,
                                    confictdata:null,
                                    //iflonger:false,
                                    //ifnameerror:false,
                                    iflucked:false,
                                    ifexist:true,
                                });
                            }
                        }
                    });
                }
            }else{
                IfDispatcher.dispatch({
                    actionType: Comment.EDITINDEXTEXT,
                    index:index-1,
                    text:"",
                    iferror:true,
                    ifsourceeqltar:false,
                    ifduplicate:false,
                    confictdata:null,
                    //ifnameerror:false,
                    //iflonger:false,
                    iflucked:false,
                    ifexist:false,
                });
            }
        };
        return inFunc;
    },
    render(){
        var pageHeight = this.props.propsheight;
        var propswidth = this.props.propswidth;
        var dictslist  = this.props.dictslist;
        var dictdatalist = this.props.dictdatalist;
        var loginInfo  = this.props.loginInfo;
        var colorClass = "white";
        var adminDisableStyle = "disabled";
        var imptDisableStyle = "disabled";
        if(loginInfo.ifadmin ||(this.state.glabolid != this.state.ifdicid)){
            adminDisableStyle = "";
        }
        var removeDisableStyle = "disabled";
        var zzlSty = "disabled";
        if((this.state.glabolid == this.state.ifdicid ||this.state.adminid == this.state.ifdicid)&&loginInfo.ifadmin){
            removeDisableStyle = "";
        }
        else if((this.state.glabolid != this.state.ifdicid &&this.state.adminid != this.state.ifdicid)&&!loginInfo.ifadmin){
            removeDisableStyle = "";
        }
        if(dictdatalist.length>0){
            zzlSty = removeDisableStyle;
        }
        if(loginInfo.ifadmin ||(this.state.adminid != this.state.ifdicid&&this.state.glabolid != this.state.ifdicid)){
            imptDisableStyle = "";
        }
        var emptyStyle = {display:"none"};
        if(this.state.iferror){
            emptyStyle = {}
        }
        var ifinfluenceStyle = {display:"none"};
        if(this.state.ifinfluence){
            ifinfluenceStyle = {};
        }
        var ifluckedStyles ={display:"none"};
        if(this.state.iflocked){
            ifluckedStyles = {};
        }
        var duplicateStyle = {display:"none"};
        var admindupdictStyle = {display:"none"};
        if(this.state.ifduplicate){
            duplicateStyle = {}
        }
        if(this.state.ifadmindupdict){
            admindupdictStyle = {}
        }

        var globalStylel = {display:"none"};
        if(this.state.ifglobal){
            globalStylel = {}
        }

        var dictnameStyle = {display:"none"};
        if(this.state.iftoolong){
            dictnameStyle = {}
        }

        var sorcetargetNameStyles = {display:"none"}
        if(this.state.ifsourceeqltar){
            sorcetargetNameStyles = {}
        }

        var selectStylel=[];
        var selectColor = [];
        for(var i in dictslist){
           // selectStylel[i]={display:"none"};
            selectStylel[i]={float:"left",color:"#fff"};
            selectColor[i]="#000";
        }

        if(this.state.selectItem){
            //selectStylel[0]={display:"none"}
            //selectStylel[this.state.selectItem]={float:"left",color:"#FF6670"}
            selectStylel[0]={float:"left",color:"#fff"}
            selectStylel[this.state.selectItem]={float:"left",color:"#379cf8"}
            selectColor[0]="#000";
            selectColor[this.state.selectItem]="#379cf8";
        }else{
            //selectStylel[0]={float:"left",color:"#FF6670"}
            selectStylel[0]={float:"left",color:"#379cf8"}
            selectColor[0]="#379cf8";
        }

        var qlist = [];

        if(this.state.filtertext && this.state.filtertext.length>0) {

            var egx = ".*" + this.state.filtertext + ".*";
            var str2 = [];
            var g = 0;
            for (var m in dictdatalist) {
                if (dictdatalist[m][0].target_name.toLowerCase().match(egx.toLowerCase())) {
                    str2[g] = dictdatalist[m];
                    g++;
                }else{
                    for(var n in dictdatalist[m]){
                        if(dictdatalist[m][n].source_name.toLowerCase().match(egx.toLowerCase())){
                            str2[g] = dictdatalist[m];
                            n=dictdatalist[m][0].length+1;
                            g++;
                        }
                    }
                }
            }
            dictdatalist=str2;
        }

        var zzl = 0;
        var zzlinput = this.props.inputtext;
        var emptyzzlStyle = {display:"none"};
        var duplicatezzlStyle = {display:"none"};
        var sorcetargetNamezzlStyles = {display:"none"};
        var ifluckedzzlStyle = {display:"none"};
        var ifexistzzlStyle = {display:"none"};
        var iflongerzStyle = {display:"none"};
        var ifnameerrorzStyle = {display:"none"};
        var editConfictdata = "";
        for(var j in dictdatalist){
            var mlist = [];
            for(var k in dictdatalist[j]){
                zzl++;
                var nk = zzl;
                if(typeof(zzlinput[nk-1]) != "undefined"){
                    if(zzlinput[nk-1].iferror){
                        emptyzzlStyle = {};
                    }else{
                        emptyzzlStyle = {display:"none"};
                    }
                    if(zzlinput[nk-1].confictdata !=null){
                        var datazzlz = zzlinput[nk-1].confictdata;
                        var editdataconfict = [];
                        for(var a in datazzlz.impactdata){
                            editdataconfict.push(
                                <div className="panel panel-default">
                                    <div className="panel-heading" style={{backgroundColor:"#FFE4CA"}}>
                                        <span className="green">{parseInt(a)+1}</span>
                                        <span>{"Conflict entries"}</span>
                                    </div>
                                    <div className="panel-body">
                                        <div className="row">
                                            <table  className="table table-bordered table-hover table-condensed">
                                                <thead>
                                                <tr style={{backgroundColor:"#F3F3FA"}}>
                                                    <th >Source Name</th>
                                                    <th >Target Name</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>{datazzlz.impactdata[a].ori_source}</td>
                                                    <td>{datazzlz.impactdata[a].ori_target}</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div className="row" style={{marginTop:"10px"}}>
                                            <table  className="table table-bordered table-hover table-condensed">
                                                <thead>
                                                <tr style={{backgroundColor:"#FFE4CA"}}>
                                                    <th >Generate Source Name</th>
                                                    <th >Generate Target Name</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>{datazzlz.impactdata[a].edit_source}</td>
                                                    <td>{datazzlz.impactdata[a].edit_target}</td>
                                                </tr>
                                                </tbody>

                                            </table>
                                        </div>
                                    </div>
                                </div>
                            );
                        }
                        editConfictdata =
                            <div className="panel panel-default">
                                <div className="panel-heading" style={{backgroundColor:"#FFE4CA"}}>
                                    <h4>Conflict Data</h4>
                                </div>
                                <div className="panel-body">
                                    {editdataconfict}
                                </div>
                                <div className="panel-footer">
                                    <div className="row">
                                        <div className="col-md-12" style={{textAlign:"right"}}>
                                            <a type="button" className="btn btn-primary" onClick={this.permitzzlAdddict(nk-1,"collapse"+parseInt(j)+parseInt(k),'inputcps'+parseInt(j)+parseInt(k))}>Permit</a>
                                            <a type="button" className="btn btn-default" onClick={this.canCelzzlAdddict(nk-1,"collapse"+parseInt(j)+parseInt(k),'inputcps'+parseInt(j)+parseInt(k))}>Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </div>;
                    }else{
                        editConfictdata = "";
                    }
                    //if(zzlinput[nk-1].iflonger){
                    //    iflongerzStyle = {};
                    //}else{
                    //    iflongerzStyle = {display:"none"};
                    //}
                    //if(zzlinput[nk-1].ifnameerror){
                    //    ifnameerrorzStyle = {};
                    //}else{
                    //    ifnameerrorzStyle = {display:"none"};
                    //}
                    if(zzlinput[nk-1].ifsourceeqltar){
                        sorcetargetNamezzlStyles = {};
                    }else{
                        sorcetargetNamezzlStyles = {display:"none"};
                    }
                    if(zzlinput[nk-1].ifduplicate){
                        duplicatezzlStyle = {};
                    }else{
                        duplicatezzlStyle = {display:"none"};
                    }
                    if(zzlinput[nk-1].iflucked){
                        ifluckedzzlStyle = {};
                    }else{
                        ifluckedzzlStyle = {display:"none"};
                    }
                    if(zzlinput[nk-1].ifexist){
                        ifexistzzlStyle = {};
                    }else{
                        ifexistzzlStyle = {display:"none"};
                    }
                }else{
                    emptyzzlStyle = {display:"none"};
                    duplicatezzlStyle = {display:"none"};
                    sorcetargetNamezzlStyles = {display:"none"};
                    ifluckedzzlStyle = {display:"none"};
                    ifexistzzlStyle = {display:"none"};
                    editConfictdata = "";
                    //iflongerzStyle = {display:"none"};
                    //ifnameerrorzStyle = {display:"none"};
                }
                mlist.push(
                    //<div className="row" style={{marginTop:"5px",borderTop:"0px none",borderLeft:"0px none",borderRight:"0px none",borderBottom:"1px solid #F0F0F0"}}>
                    //    <div className="col-sm-3 col-sm-offset-1">
                    //        <span >{dictdatalist[j][k].source_name}</span>
                    //    </div>
                    //    <div className="col-sm-8 " style={{textAlign:"right"}}>
                    //        <button
                    //            className="btn btn-default glyphicon glyphicon-edit"
                    //            style={{borderTop:"0px none",borderLeft:"0px none",borderRight:"0px none",borderBottom:"1px solid #F0F0F0"}}
                    //            onClick={this.amendentrie(dictdatalist[j][k].dic_id,dictdatalist[j][k].target_name,dictdatalist[j][k].source_name)}
                    //            disabled={adminDisableStyle}
                    //            >
                    //        </button>
                    //        <button
                    //            className="btn btn-default glyphicon glyphicon-trash"
                    //            disabled={adminDisableStyle}
                    //            onClick={this.deletentries(dictdatalist[j][k].dic_id,dictdatalist[j][k].target_name,dictdatalist[j][k].source_name)}
                    //            style={{marginLeft:"5px",borderTop:"0px none",borderLeft:"0px none",borderRight:"0px none",borderBottom:"1px solid #F0F0F0"}}
                    //            >
                    //        </button>
                    //    </div>
                    //
                    //</div>
                    <div>
                        <div className="row" style={{marginTop:"5px",borderTop:"0px none",borderLeft:"0px none",borderRight:"0px none",borderBottom:"1px solid #F0F0F0"}}>
                            <div className="col-sm-9 wrap" style={{textAlign:"left"}}>
                                <span style={{marginLeft:"30px",color:"#379cf8"}}>{dictdatalist[j][k].source_name}</span>
                            </div>
                            <div className="col-sm-3 " style={{textAlign:"right"}}>
                                <div className="row" style={{marginLeft:"-15px",marginRight:"-15px"}}>
                                    <button
                                        className="btn btn-default glyphicon glyphicon-edit"
                                        style={{borderTop:"0px none",borderLeft:"0px none",borderRight:"0px none",borderBottom:"1px solid #F0F0F0",color:"#379cf8"}}
                                        disabled={adminDisableStyle}
                                        role="button"
                                        data-toggle="collapse"
                                        data-target={"#collapse"+parseInt(j)+parseInt(k)}
                                        aria-expanded="false"
                                        aria-controls={"collapse"+parseInt(j)+parseInt(k)}>
                                    </button>
                                    <button
                                        className="btn btn-default glyphicon glyphicon-trash"
                                        disabled={adminDisableStyle}
                                        onClick={this.deletentries(dictdatalist[j][k].dic_id,dictdatalist[j][k].target_name,dictdatalist[j][k].source_name)}
                                        style={{marginLeft:"0px",borderTop:"0px none",borderLeft:"0px none",borderRight:"0px none",borderBottom:"1px solid #F0F0F0",color:"#379cf8"}}
                                        >
                                    </button>
                                </div>
                            </div>

                        </div>
                        <div className="row">
                            <div className="collapse" id={"collapse"+parseInt(j)+parseInt(k)}>
                                <div className="row" style={{marginTop:"10px",borderTop:"0px none",borderLeft:"0px none",borderRight:"0px none",borderBottom:"1px solid #F0F0F0"}}>
                                    <div className="row" style={{marginLeft:"0px",marginRight:"15px",marginBottom:"10px"}}>
                                        <div className="col-sm-6 col-sm-offset-3">
                                            <input type="text" className="form-control zzlbuttoncolborder" placeholder="Edit Source Name"
                                                   id={'inputcps'+parseInt(j)+parseInt(k)}
                                                   onChange={this.handleeditChange(zzl)}
                                                />
                                        </div>
                                        <div className="col-sm-3" style={{textAlign:"right"}}>
                                            <button
                                                role="button"
                                                className="btn btn-default glyphicon glyphicon-ok zzlbuttoncolborder"
                                                // style={{borderTop:"0px none",borderLeft:"0px none",borderRight:"0px none",borderBottom:"1px solid #F0F0F0",backgroundColor:"#E0E0E0"}}
                                                onClick={this.editEntrieOk("collapse"+parseInt(j)+parseInt(k),'inputcps'+parseInt(j)+parseInt(k),zzl,dictdatalist[j][k].dic_id,dictdatalist[j][k].target_name,dictdatalist[j][k].source_name)}
                                                >
                                            </button>
                                        </div>
                                    </div>
                                    <div className="row" style={{marginLeft:"15px",marginRight:"15px",marginBottom:"10px"}}>
                                        {editConfictdata}
                                    </div>
                                    <div className="row" style={{marginLeft:"0px",marginRight:"15px",marginBottom:"10px"}}>
                                        <div className="col-md-10 col-md-offset-1">
                                            <div style={iflongerzStyle}>
                                                <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                    SourceName must be at least  0 characters and at most 40 characters.
                                                </div>
                                            </div>
                                            <div style={ifnameerrorzStyle}>
                                                <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                    SourceName not allowed to enter special characters.
                                                </div>
                                            </div>
                                            <div style={duplicatezzlStyle}>
                                                <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                    Please enter a unique name for your source name!
                                                </div>
                                            </div>
                                            <div style={sorcetargetNamezzlStyles}>
                                                <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                    SourceName can't be as same as targetName in a entry!
                                                </div>
                                            </div>
                                            <div style={emptyzzlStyle}>
                                                <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                    New Source Name  can't be empty.
                                                </div>
                                            </div>
                                            <div style={ifluckedzzlStyle}>
                                                <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                    This item has been locked,the operation is not permitted.
                                                </div>
                                            </div>
                                            <div style={ifexistzzlStyle}>
                                                <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                    The existing source same name waiting for approval.
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                );
            }
            qlist.push(
                <div className="panel panel-default zzldictdiv" style={{border:"0px none"}}>
                    <div className="panel-heading" style={{backgroundColor:"#F5F5F5"}}>
                        <div className="row" >
                            <div className="col-sm-9 " style={{textAlign:"left"}}>
                                <span><span className={colorClass}>{parseInt(j)+1}</span> <span className="wrap" style={{marginLeft:"5px",color:"#379cf8"}}>{dictdatalist[j][0].target_name}</span></span>
                            </div>

                            <div className="col-md-3 " style={{textAlign:"right"}}>
                                <div className="row" style={{marginLeft:"-15px",marginRight:"-15px"}}>
                                    <button
                                        className="btn btn-default glyphicon glyphicon-plus"
                                        style={{borderTop:"0px none",borderLeft:"0px none",borderRight:"0px none",borderBottom:"1px solid #F0F0F0",color:"#379cf8"}}
                                        onClick={this.addentrieclick(dictdatalist[j][0].dic_id,dictdatalist[j][0].target_name)}
                                        // disabled={adminDisableStyle}
                                        >
                                    </button>
                                    <button
                                        className="btn btn-default glyphicon glyphicon-trash"
                                        style={{marginLeft:"0px",borderTop:"0px none",borderLeft:"0px none",borderRight:"0px none",borderBottom:"1px solid #F0F0F0",color:"#379cf8"}}
                                        onClick={this.deletallEntries(dictdatalist[j][0].dic_id,dictdatalist[j][0].target_name)}
                                        disabled={adminDisableStyle}
                                        >
                                    </button>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div className="panel-body" >
                        {mlist}
                    </div>
                </div>
            );
        }
        var zklist =  [];
        if(qlist.length !=0){
            if(qlist.length>1){
                for(var i=0;i <qlist.length;i=i+2){
                    if((i+1) ==(qlist.length)){
                        zklist.push(
                            <div className="row" style={{marginLeft:"0px",marginRight:"0px"}}>
                                <div className="col-sm-6 " >
                                    {qlist[i]}
                                </div>
                            </div>
                        );
                    }else{
                        zklist.push(
                            <div className="row" style={{marginLeft:"0px",marginRight:"0px"}}>
                                <div className="col-sm-6 " >
                                    {qlist[i]}
                                </div>
                                <div className="col-sm-6">
                                    {qlist[i+1]}
                                </div>
                            </div>
                        );
                    }
                }
            }else{
                zklist.push(
                    <div className="row" style={{marginLeft:"0px",marginRight:"0px"}}>
                        <div className="col-sm-6 " >
                            {qlist[0]}
                        </div>
                    </div>
                );
            }
        }

        var dlist = [];
        var zzldxlist=[];
        if(dictslist.length >0){
            for(var i in dictslist){
                dlist.push(
                    <div className="row zzltr"  style={{marginLeft:"0px",marginRight:"0px",borderBottom:"1px solid #ccc"}}>
                        <div className="col-sm-11" style={{marginLeft:"-15px",marginRight:"0px"}}>
                            <a
                                role="button"
                                className="col-sm-12  btn btn-default zzlbuttoncol"
                                //className="btn btn-default glyphicon glyphicon-trash"
                                onClick={this.dictdataclick(dictslist[i].dic_id,i,dictslist[i].app_range)}
                                style={{textAlign:"left",border:"0px none",marginTop:"5px",marginBottom:"5px",color:selectColor[i]}}
                                >
                                {dictslist[i].dic_name}

                            </a>
                        </div>
                        <div className="col-sm-1" style={{textAlign:"left",marginTop:"10px"}}>
                            <div  style={selectStylel[i]}>
                                 <span
                                     className="zzlselectstyle"
                                     >√</span>
                            </div>
                        </div>
                    </div>
                );

            }
            zzldxlist.push(
                <div>
                    <div className="row" style={{height:(pageHeight-245+"px"),overflowY:"auto",marginTop:"0px",marginLeft:"0px",marginRight:"0px",background:"#fff"}}>
                        {dlist}
                    </div>
                </div>
            );
        }
        var elink = ""
        if(this.state.ifexport){
            elink = <a
                className="btn btn-info"
                href={this.state.exporturl}
                style={{marginRight:"5px"}}>
                <span className="glyphicon glyphicon-floppy-save" aria-hidden="true"></span>Click To Download
            </a>;
        }

        var zlist = [];
        for(var p in dictslist){
            if(this.state.ifdicid == dictslist[p].dic_id)
            {
                zlist.push(
                    <div>
                        <div className="row" style={{height:"130px",marginLeft:"0px",marginRight:"0px"}}>
                            <div className="row" style={{height:"60px",marginLeft:"0px",marginRight:"0px"}}>
                                <div className="col-sm-6" style={{marginTop:"5px"}}>
                                    <h4 style={{color:"#379cf8"}}>{dictslist[p].dic_name}</h4>
                                </div>
                                <div className="col-sm-6" >
                                    <input type="text"
                                           className="form-control zzlborder zzlbuttoncolborder"
                                           value={this.state.filtertext}
                                           onChange={this.filterChange}
                                           style={{height:"35px",marginTop:"15px"}}
                                           placeholder="Search by TargetName or SourceName"/>
                                </div>
                            </div>
                            <div className="row" style={{height:"70px",marginLeft:"0px",marginRight:"0px"}}>
                                <div className="row" style={{marginLeft:"0px",marginRight:"0px",marginTop:"20px"}}>
                                    <div className="col-md-6">
                                        <div className="btn-group" role="group" >
                                            <button className="btn btn-default zzlbuttoncolborder" role="button"
                                                    onClick={this.addNewEntries}
                                                >
                                                Add Entry
                                            </button>
                                            <button className="btn btn-default zzlbuttoncolborder"
                                                    onClick={this.removeDictClick}
                                                    //style={{marginLeft:"5px"}}
                                                    disabled={zzlSty}
                                                >
                                                Remove Dict
                                            </button>
                                            <button className="btn btn-default zzlbuttoncolborder"
                                                    onClick={this.importdictClick}
                                                    disabled={imptDisableStyle}
                                                    //style={{marginLeft:"5px"}}
                                                >
                                                Import Dict
                                            </button>
                                        </div>
                                    </div>
                                    <div className="col-md-6 " style={{textAlign:"right"}}>
                                        {elink}
                                        <div className="btn-group" role="group" >
                                            <button className="btn btn-default zzlbuttoncolborder"
                                                    onClick={this.exportcsv}
                                                    style={{marginLeft:"5px"}}
                                                >
                                                Export Dict
                                            </button>
                                            <button className="btn btn-default zzlbuttoncolborder" role="button"
                                                    onClick={this.versiondictClick}
                                                    disabled={imptDisableStyle}
                                                >
                                                DictVersionConfig
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row" style={{height:(pageHeight-190+"px"),marginLeft:"0px",marginRight:"0px"}}>
                            <div className="row" style={{height:"40px",marginLeft:"0px",marginRight:"0px",marginTop:"15px",borderBottom:"1px solid #ccc"}}>

                            </div>
                            <div className="row" style={{height:(pageHeight-245+"px"),overflow:"auto",marginTop:"0px",marginLeft:"0px",marginRight:"0px"}}>
                                <div style={{marginTop:"10px"}}>
                                    {zklist}
                                </div>

                            </div>

                        </div>
                    </div>
                );
            }
        }


        var verlist=this.state.versionlist;
        var dicvlist=[];
        for(var i in verlist){
            for(var j in verlist[i]){
                dicvlist.push(
                    <tr >
                        <td ><span className="grey">{parseInt(i)+1}</span></td>
                        <td ><a onClick={this.getDicVDetail(verlist[i][j].dicsavename)} role="button" style={{color:"#000000",textDecoration:"none"}}>{verlist[i][j].dicsavename}</a></td>
                        <td >{verlist[i][j].savetime}</td>
                        <td className="list_btn" >
                            <div className="btn-group" role="group" >
                                <button
                                    className="btn btn-default"
                                    style={{backgroundColor:"#E0E0E0"}}
                                    onClick={this.delDicVCli(verlist[i][j].dicsavename)}
                                >Delete</button>
                                <button
                                    className="btn btn-default"
                                    style={{backgroundColor:"#46A3FF"}}
                                    onClick={this.fallbackDicVCli(verlist[i][j].dicsavename)}
                                >Fallback</button>
                            </div>

                        </td>

                    </tr>
                )
            }

        }

        var vdlist=this.state.versionDetaillist;
        var vdplist=[];
        var vhead=[];
        if(vdlist.length>0){
            vhead.push(
                <div className="modal-body" style={{backgroundColor:"#EdE0E0",marginTop:"-15px"}} id="versiondetail" >
                    <div className="row" style={{backgroundColor:"#EdE0E0",marginTop:"-15px",height:(250+"px"),overflowY:"auto",marginBottom:"-15px"}}>
                        <div className="row" style={{height:"5px",backgroundColor:"#408080",marginLeft:"0px",marginRight:"0px"}}>
                        </div>
                        <div className="row" style={{marginLeft:"0px",marginRight:"0px"}}>
                            <table  className="table table-bordered table-hover table-condensed">
                                <thead>
                                <tr style={{backgroundColor:"#F3F3FA"}}>
                                    <th style={{width:"30px"}}></th>
                                    <th >Source Name</th>
                                    <th >Target Name</th>
                                </tr>
                                </thead>
                                <tbody>
                                {vdplist}
                                </tbody>

                            </table>
                        </div>

                    </div>
                </div>

            );

            for(var i in vdlist){
                vdplist.push(
                    <tr>
                        <td ><span className={colorClass}>{parseInt(i)+1}</span></td>
                        <td >{vdlist[i].target_name}</td>
                        <td >{vdlist[i].source_name}</td>
                    </tr>
                );
            }
        }


        var vnamestyle = {display:"none"}
        if(this.state.versionnameifDu){
            vnamestyle = {}
        }

        var versionstyle = {display:"none"}
        if(this.state.thisDictIsNull){
            versionstyle = {}
        }

        var dictnameusedStyle = {display:"none"}
        if(this.state.ifnameused){
            dictnameusedStyle = {}
        }
        var nameIfMatchlength = this.state.ifnamematchlength?{display:"none"}:{};
        var nameIfMatchkinds = this.state.ifnamematchkinds?{display:"none"}:{};

        var viewConfictData = [];
        var dataconfict = [];
        if(this.state.conflictzzldata != null){
            var datazzl = this.state.conflictzzldata;
            for(var a in datazzl.impactdata){
                dataconfict.push(
                    <div className="panel panel-default">
                        <div className="panel-heading" style={{backgroundColor:"#FFE4CA"}}>
                            <span className="green">{parseInt(a)+1}</span>
                            <span>{"Conflict entries"}</span>
                        </div>
                        <div className="panel-body">
                            <div className="row">
                                <table  className="table table-bordered table-hover table-condensed">
                                    <thead>
                                    <tr style={{backgroundColor:"#F3F3FA"}}>
                                        <th >Source Name</th>
                                        <th >Target Name</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>{datazzl.impactdata[a].ori_source}</td>
                                        <td>{datazzl.impactdata[a].ori_target}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div className="row" style={{marginTop:"10px"}}>
                                <table  className="table table-bordered table-hover table-condensed">
                                    <thead>
                                    <tr style={{backgroundColor:"#FFE4CA"}}>
                                        <th >Generate Source Name</th>
                                        <th >Generate Target Name</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>{datazzl.impactdata[a].edit_source}</td>
                                        <td>{datazzl.impactdata[a].edit_target}</td>
                                    </tr>
                                    </tbody>

                                </table>
                            </div>
                        </div>
                    </div>
                );
            }
            viewConfictData.push(
                <div className="panel panel-default">
                    <div className="panel-heading" style={{backgroundColor:"#FFE4CA"}}>
                        <h4>Conflict Data</h4>
                    </div>
                    <div className="panel-body">
                        {dataconfict}
                    </div>
                    <div className="panel-footer">
                        <div className="row">
                            <div className="col-md-12" style={{textAlign:"right"}}>
                                <a type="button" className="btn btn-primary" onClick={this.permitAdddict}>Permit</a>
                                <a type="button" className="btn btn-default" data-dismiss="modal">Cancel</a>
                            </div>
                        </div>
                    </div>
                </div>
            );
        }

        var viewConFileData = [];
        var dataFileconfict = [];
        if(this.state.confFiledata != null){
            var datazzl = this.state.confFiledata;
            for(var a in datazzl.impactdata){
                dataFileconfict.push(
                    <div className="panel panel-default">
                        <div className="panel-heading" style={{backgroundColor:"#FFE4CA"}}>
                            <span className="green">{parseInt(a)+1}</span>
                            <span>{"Conflict entries"}</span>
                        </div>
                        <div className="panel-body">
                            <div className="row">
                                <table  className="table table-bordered table-hover table-condensed">
                                    <thead>
                                    <tr style={{backgroundColor:"#F3F3FA"}}>
                                        <th >Source Name</th>
                                        <th >Target Name</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>{datazzl.impactdata[a].ori_source}</td>
                                        <td>{datazzl.impactdata[a].ori_target}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div className="row" style={{marginTop:"10px"}}>
                                <table  className="table table-bordered table-hover table-condensed">
                                    <thead>
                                    <tr style={{backgroundColor:"#FFE4CA"}}>
                                        <th >Generate Source Name</th>
                                        <th >Generate Target Name</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <tr>
                                        <td>{datazzl.impactdata[a].edit_source}</td>
                                        <td>{datazzl.impactdata[a].edit_target}</td>
                                    </tr>
                                    </tbody>

                                </table>
                            </div>
                        </div>
                    </div>
                );
            }
            viewConFileData.push(
                <div className="panel panel-default">
                    <div className="panel-heading" style={{backgroundColor:"#FFE4CA"}}>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                        <h4>Conflict Data</h4>
                    </div>
                    <div className="panel-body">
                        {dataFileconfict}
                    </div>
                    <div className="panel-footer">
                        <div className="row">
                            <div className="col-md-12" style={{textAlign:"right"}}>
                                <a type="button" className="btn btn-primary" onClick={this.permitFilezzldict}>Permit</a>
                                <a type="button" className="btn btn-default" data-dismiss="modal">Cancel</a>
                            </div>
                        </div>
                    </div>
                </div>
            );
        }
        return(
            <div>
                <div  style={{float:"left",height:(pageHeight-60+"px"),width:"400px"}}>
                    <div className="row" style={{height:"130px",marginLeft:"0px",marginRight:"0px"}}>
                        <div className="row" style={{height:"60px",marginLeft:"0px",marginRight:"0px"}}>
                            <div className="col-sm-12"  style={{marginTop:"5px"}}>
                                <h4 style={{color:"#379cf8"}}>Dictionary List</h4>
                            </div>
                        </div>
                        <div className="row" style={{background:"#fff",height:"70px",marginLeft:"0px",marginRight:"0px"}}>
                            <div className="col-sm-6">
                                <button className="btn btn-default zzlbuttoncolborder" role="button"
                                        style={{width:"70px",marginTop:"20px"}}
                                        onClick={this.adddictclick}
                                    >
                                    Create
                                </button>
                            </div>
                            <div className="col-sm-6" style={{textAlign:"right"}}>
                                <button className="btn btn-default zzlbuttoncolborder" role="button"
                                        style={{width:"70px",marginTop:"20px"}}
                                        onClick={this.deleteDicCli}
                                    >
                                    Delete
                                </button>
                            </div>
                        </div>

                    </div>
                    <div className="row" style={{height:(pageHeight-190+"px"),marginLeft:"0px",marginRight:"0px"}}>
                        <div className="row" style={{height:"40px",marginLeft:"0px",marginRight:"0px",marginTop:"15px",borderBottom:"1px solid #ccc"}}>
                            <div className="col-sm-6" style={{textAlign:"left"}}>
                                <span style={{color:"#379cf8"}}>Dict Name</span>
                            </div>
                            <div className="col-sm-6" style={{textAlign:"right"}}>
                                <span style={{color:"#379cf8"}}>Current Logo</span>
                            </div>
                        </div>
                        {zzldxlist}
                    </div>
                </div>
                <div style={{float:"left",height:(pageHeight-60+"px"),width:"5px",background:"#F1F1F1"}}>
                </div>
                <div style={{float:"left",width:(propswidth-405+"px"),height:(pageHeight-60+"px")}}>
                    {zlist}
                </div>

                <div className="modal fade" id="versionDic" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <div className="row">
                                    <div className="col-sm-6">
                                        <h4 className="modal-title"> Dictionary Version Configuration</h4>
                                    </div>
                                    <div className="col-sm-6" style={{textAlign:"right"}}>
                                        <a type="button" className="btn btn-default"
                                           style={{backgroundColor:"#E0E0E0"}}
                                           onClick={this.saveVersionCli}
                                        >Save</a>
                                    </div>
                                </div>
                            </div>
                            <div className="modal-body">
                                <div className="row" style={{backgroundColor:"#EdE0E0",marginTop:"-15px",height:(200+"px"),overflowY:"auto"}}>
                                    <table  className="table table-bordered table-hover table-condensed">
                                        <thead>
                                        <tr style={{backgroundColor:"#F3F3FA"}}>
                                            <th style={{width:"30px"}}></th>
                                            <th >Version Name</th>
                                            <th >Version Time</th>
                                            <th >Operation</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {dicvlist}
                                        </tbody>

                                    </table>
                                </div>
                            </div>
                            {vhead}

                            <div className="modal-footer">
                                <div className="row">
                                    <div className="col-md-8">
                                        <div style={versionstyle}>
                                            <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                This dictionary is empty!
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-4">
                                        <a type="button" className="btn btn-default" data-dismiss="modal">Cancel</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="modal fade" id="adddict" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                                <h4 className="modal-title">New Dictionary</h4>
                            </div>
                            <div className="modal-body">
                                <form>
                                    <div className="form-group">
                                        <label htmlFor="mediaNameInput">Dict Name</label>
                                        <input type="text" className="form-control"  placeholder="Dict Name"
                                               onChange={this.handleChange.bind(this,"dictname")} value={this.state.dictname}/>

                                    </div>

                                </form>

                            </div>
                            <div className="modal-footer">
                                <div className="row">
                                    <div className="col-md-8">
                                        <div style={emptyStyle}>
                                            <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                Dict name  can't be empty
                                            </div>
                                        </div>
                                        <div style={duplicateStyle}>
                                            <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                Please enter a unique name for your dictionary!
                                            </div>
                                        </div>
                                        <div style={nameIfMatchkinds}>
                                            <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                Dict name not allowed to enter special characters, please try again.
                                            </div>
                                        </div>
                                        <div style={admindupdictStyle}>
                                            <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                Existing global dictionary
                                            </div>
                                        </div>
                                        <div style={dictnameStyle}>
                                            <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                Please enter no more than 40 characters.
                                            </div>
                                        </div>
                                        <div style={dictnameusedStyle}>
                                            <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                This dictName is used by others or yourself.
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-4">
                                        <a type="button" className="btn btn-default" data-dismiss="modal">Cancel</a>
                                        <a type="button" className="btn btn-primary" onClick={this.confirmAdddict}>Confirm</a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
                <div className="modal fade" id="addentrie" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-body">
                                <div className="row" style={{marginTop:"-15px"}}>
                                    <div className="panel panel-default">
                                        <div className="panel-heading" style={{backgroundColor:"#FFE4CA"}}>
                                            <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                                            <h4>New Entry</h4>
                                        </div>
                                        <div className="panel-body">
                                            <form>
                                                <div className="form-group">
                                                    <label htmlFor="mediaNameInput">Source Name</label>
                                                    <input type="text" className="form-control"  placeholder="Source Name"
                                                           onChange={this.handleChange.bind(this,"sourcename")} value={this.state.sourcename}/>
                                                </div>
                                            </form>
                                        </div>
                                        <div className="panel-footer">
                                            <div className="row">
                                                <div className="col-md-8">
                                                    <div style={emptyStyle}>
                                                        <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                            Source Name  can't be empty
                                                        </div>
                                                    </div>
                                                    <div style={ifinfluenceStyle}>
                                                        <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                            No dictionary entry that affects the existence of data is added.
                                                        </div>
                                                    </div>
                                                    <div style={duplicateStyle}>
                                                        <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                            Please enter a unique name for your source name!
                                                        </div>
                                                    </div>
                                                    <div style={sorcetargetNameStyles}>
                                                        <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                            SourceName can't be as same as targetName in a entry!
                                                        </div>
                                                    </div>
                                                    <div style={ifluckedStyles}>
                                                        <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                            This item has been locked,the operation is not permitted
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-md-4">
                                                    <a type="button" className="btn btn-primary" onClick={this.confirmaddentrie}>Add</a>
                                                    <a type="button" className="btn btn-default" data-dismiss="modal">Cancel</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div className="row">
                                    {viewConfictData}
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div className="modal fade" id="deleteentrie" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                                <h4 className="modal-title">Delete Entry</h4>
                            </div>
                            <div className="modal-body">
                                You will delete the target field and all its corresponding source entries!
                            </div>
                            <div className="modal-footer">
                                <div className="row">
                                    <div className="col-md-8">
                                        <div style={ifluckedStyles}>
                                            <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                This item has been locked,the operation is not permitted
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-4">
                                        <a type="button" className="btn btn-default" data-dismiss="modal">Cancel</a>
                                        <a type="button" className="btn btn-primary" onClick={this.confirmdeleteentries}>Confirm</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div className="modal fade" id="deletezzldd" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                                <h4 className="modal-title">Delete Entry</h4>
                            </div>
                            <div className="modal-body">
                                Are you sure to delete the entry?
                            </div>
                            <div className="modal-footer">
                                <div className="row">
                                    <div className="col-md-8">
                                        <div style={ifluckedStyles}>
                                            <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                This item has been locked,the operation is not permitted
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-4">
                                        <a type="button" className="btn btn-default" data-dismiss="modal">Cancel</a>
                                        <a type="button" className="btn btn-primary" onClick={this.confirmdeleteentrie}>Confirm</a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
                <div className="modal fade" id="addNewEntries" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-body">
                                <div className="row" style={{marginTop:"-15px"}}>
                                    <div className="panel panel-default">
                                        <div className="panel-heading" style={{backgroundColor:"#FFE4CA"}}>
                                            <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                                            <h4>New Entry</h4>
                                        </div>
                                        <div className="panel-body">
                                            <form>
                                                <div className="form-group">
                                                    <label htmlFor="mediaNameInput">Target Name</label>
                                                    <input type="text" className="form-control"  placeholder="Target Name"
                                                           onChange={this.handleChange.bind(this,"targetnamezzl")} value={this.state.targetnamezzl}/>
                                                    <label htmlFor="mediaNameInput">Source Name</label>
                                                    <input type="text" className="form-control"  placeholder="Source Name"
                                                           onChange={this.handleChange.bind(this,"sourcenamezzl")} value={this.state.sourcenamezzl}/>
                                                </div>
                                            </form>
                                        </div>
                                        <div className="panel-footer">
                                            <div className="row">
                                                <div className="col-md-8">
                                                    <div style={emptyStyle}>
                                                        <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                            Source Name or Target Name  can't be empty
                                                        </div>
                                                    </div>
                                                    <div style={ifinfluenceStyle}>
                                                        <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                            No dictionary entry that affects the existence of data is added.
                                                        </div>
                                                    </div>
                                                    <div style={duplicateStyle}>
                                                        <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                            Please enter a unique name for your source name!
                                                        </div>
                                                    </div>
                                                    <div style={sorcetargetNameStyles}>
                                                        <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                            SourceName can't be as same as targetName in a entry!
                                                        </div>
                                                    </div>
                                                    <div style={ifluckedStyles}>
                                                        <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                            This item has been locked,the operation is not permitted
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-md-4" style={{textAlign:"right"}}>
                                                    <a type="button" className="btn btn-primary" onClick={this.comfrimaddNewEntries}>Add</a>
                                                    <a type="button" className="btn btn-default" data-dismiss="modal">Cancel</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                               <div className="row">
                                   {viewConfictData}
                               </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="modal fade" id="confFileEntries" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-body">
                                <div className="row" style={{marginTop:"-15px"}}>
                                    {viewConFileData}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="modal fade" id="amendentrie" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                                <h4 className="modal-title">Edit Entry</h4>
                            </div>
                            <div className="modal-body">
                                <form>
                                    <div className="form-group">
                                        <label htmlFor="mediaNameInput">New Source Name</label>
                                        <input type="text" className="form-control"  placeholder="New Source Name"
                                               onChange={this.handleChange.bind(this,"sourcenameresult")} value={this.state.sourcenameresult}/>

                                    </div>

                                </form>
                            </div>
                            <div className="modal-footer">
                                <div className="row">
                                    <div className="col-md-8">
                                        <div style={duplicateStyle}>
                                            <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                Please enter a unique name for your source name!
                                            </div>
                                        </div>
                                        <div style={sorcetargetNameStyles}>
                                            <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                SourceName can't be as same as targetName in a entry!
                                            </div>
                                        </div>
                                        <div style={emptyStyle}>
                                            <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                New Source Name  can't be empty.
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-4">
                                        <a type="button" className="btn btn-default" data-dismiss="modal">Cancel</a>
                                        <a type="button" className="btn btn-primary" onClick={this.confirmamendentrie}>Confirm</a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div className="modal fade" id="deleteDic" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                                <h4 className="modal-title" >Confirm the delete</h4>
                            </div>
                            <div className="modal-body">
                                <h3>
                                    Are you sure to delete this Dictionary?
                                </h3>
                            </div>
                            <div className="modal-footer">
                                <div className="row">
                                    <div className="col-md-8">
                                        <div style={globalStylel}>
                                            <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                You can't delete this global dictionary.
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-4">
                                        <a type="button" className="btn btn-default" data-dismiss="modal">Cancel</a>
                                        <a type="button"
                                           onClick={this.deleteDicConf}
                                           className="btn btn-primary" >Confirm</a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div className="modal fade" id="removeDic" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                                <h4 className="modal-title" >Confirm the remove</h4>
                            </div>
                            <div className="modal-body">
                                <h3>
                                    Are you sure to remove this Dictionary?
                                </h3>
                            </div>
                            <div className="modal-footer">
                                <div className="row">
                                    <div className="col-md-8">
                                    </div>
                                    <div className="col-md-4">
                                        <a type="button" className="btn btn-default" data-dismiss="modal">Cancel</a>
                                        <a type="button"
                                           onClick={this.removeDtConf}
                                           className="btn btn-primary" >Confirm</a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div className="modal fade" id="versionEditDic" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                                <h4 className="modal-title" >Please enter this version name</h4>
                            </div>
                            <div className="modal-body">

                                <form>
                                    <div className="form-group">
                                        <label htmlFor="mediaNameInput">Vesion Name</label>
                                        <input type="text" className="form-control"  placeholder="Version Name"
                                               onChange={this.handleChange.bind(this,"versionname")} value={this.state.versionname}/>

                                    </div>

                                </form>


                            </div>
                            <div className="modal-footer">
                                <div className="row">
                                    <div className="col-md-7">
                                        <div style={vnamestyle}>
                                            <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                Please enter a unique name for your version!
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-5">
                                        <a type="button" className="btn btn-default" data-dismiss="modal">Cancel</a>
                                        <a type="button"
                                           onClick={this.addDictVersionComf}
                                           className="btn btn-primary" >Confirm</a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div className="modal fade" id="deleteDictVersion" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                                <h4 className="modal-title" >Confirm the delete</h4>
                            </div>
                            <div className="modal-body">
                                <h3>
                                    Are you sure to delete this dictionary version?
                                </h3>

                            </div>
                            <div className="modal-footer">
                                <a type="button" className="btn btn-default" data-dismiss="modal">Cancel</a>
                                <a type="button"
                                   onClick={this.deleteDicVerConf}
                                   className="btn btn-primary" >Confirm</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="modal fade" id="fbDictVersion" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                                <h4 className="modal-title" >Confirm the fallback</h4>
                            </div>
                            <div className="modal-body">
                                <h3>
                                    Are you sure to swap back to the dictionary version?
                                </h3>
                            </div>
                            <div className="modal-footer">
                                <a type="button" className="btn btn-default" data-dismiss="modal">Cancel</a>
                                <a type="button"
                                   onClick={this.fallbackDicVComf}
                                   className="btn btn-primary" >Confirm</a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        )
    }

});