/**
 * The module file:This is the project specific module for details.
 * Create time:2017/3/6
 */
import React from 'react';
import crypto from "crypto";
import {Comment} from "../comment";
import {IfDispatcher} from "../store/dispatcher";
import Underscore from "underscore";
import {Participle} from "./participle";
export var Dictdata = React.createClass({
    contextTypes: {
        router: React.PropTypes.object.isRequired,
    },
    getInitialState(){
        return{
            ifdicid:null,
            thisfdefno:null,
            thisfdefnname:null,
            datasiginNo:[],
            checkselects:[],
            fdetifCheck:[],
            confirmdata:[],
            checkconfirms:[],
            confirmdatasiginNo:[],
            dictindex:null,
            ifdatanull:false,
            ifdicname:null,
            selectItem:null,
            selectFdefItem:null,
            selectSourceItem:null,
            selectresultItem:null,
            featuresSelects:[],
            feanoindex:null,
            ifods:null,
            ifmeta:false,
            setencode:"UTF-8",
            setlanuage:"Chinese",
            setseparate:"|",
            ifseter:false,
            ifseterdd:false,
            ifseterss:false,
            timebarId:null,
            timebar:0,
            ifbar:false,
            ifbad:false,
            pagesize:500,
            datacounts:0,
            pagenum:7,
            currentpage:0,
            currentpages:0,
            pagesizes:100,
            datacountss:0,
            ifexport:false,
            exporturl:null,
            ifdds:0,
            ifrun:false,
            resultdata:[],
            ifodss:null,
            columnname:[],
            setmethod:"LOCAL",
            Uploadpath:null,
            Uploadpass:null,
            Uploaduser:null,
            Uploadhost:null,
            Uploadport:null,
            ifuploaderror:false,
            ifrunnig:false,
            metadatatext:null,
            ifmetdtext:false,
            searchcount:0,
            searchdata:[],
            searchname:[],
            currentpagech:0,
            pagesizech:100,
            searchtablecount:0,
            searchtabledata:[],
            currentpagebl:0,
            fileNum:0,
            fileData:[],
            ifselectfiles:false,
            covername:null,
            covernamelist:[],
            ckockselects:[],
            newcoverfile:null,
            currentPleFileId:null,
            currentPleFileName:null,
            selectPlefItem:null,
            dataoptmsg:[],
            dataopts:[],
            tpt_name:"",
            en_table_name:"",
            en_col_name:"",
            la_table_name:"",
            la_col_name:"",
            token_bit:"",
            col_data_type:"",
            col_des:"",
            table_cycle:"",
            table_des:"",
            order_seq:"",
            codename:"",
            ext1_name:"",
            ext2_name:"",
            ext3_name:"",
            ext4_name:"",
            savasuccess:false,
            errormsg:null,
        }
    },
    componentDidMount(){
        var loginInfo = this.props.loginInfo;
        if(!loginInfo.name || !this.props.projectid || !this.props.projectname){
            this.context.router.push("/fronpage");
            IfDispatcher.dispatch({
                actionType:Comment.LOGOUT
            });
        }else{
            this.getdictsList();
        }
    },
    handleChange(name,event){
        var newstate = {};
        newstate[name] = event.target.value;
        newstate.ifuploaderror = false;
        this.setState(newstate);
    },
    /**
     *The function name: getdictsList.
     *Function used: To obtain a list dictionary.
     *Create time: 2017/4/5
     */
    getdictsList(){
        $("#ajaxloading").show();
        var loginInfo = this.props.loginInfo;
        var that = this;
        that.setState({
           ifdds:0,
        })
        $.ajax({
            url: Comment.DATA_URL+"KDB/GetAllDictionary",
            data:$.param({
                userid:loginInfo.userid,
            }),
            type: 'GET',
            success: function(data){
                var msg = data;
                IfDispatcher.dispatch({
                    actionType: Comment.GETDICTDATALIST,
                    dictslist:msg.data
                });
                if(that.state.ifdicid==null){
                    var dic_id = msg.data[0].dic_id;
                    var dic_name = msg.data[0].dic_name;
                    that.setState({
                        ifdicid:dic_id ,
                        ifdicname:dic_name,
                    })
                }
                var hash = crypto.createHash("md5");
                hash.update(that.state.ifdicid.toString());
                $.ajax({
                    url: Comment.DATA_URL+"KDB/FindDic",
                    data:$.param({
                        dicid:that.state.ifdicid,
                        paramcheck:hash.digest("hex"),
                    }),
                    type: 'GET',
                    success: function(data){
                        $("#ajaxloading").hide();
                        var msg = data;
                        var datalist = [];
                        var dataSigin =[];
                        var checkselects =[];
                        for(var i=0;i<msg.data.length;i++){
                            for(var j=0;j<msg.data[i].length;j++){
                                datalist.push(msg.data[i][j]);
                                checkselects.push({
                                    dicsyn_no:msg.data[i][j].dicsyn_no,
                                    ifckecked:true,
                                });
                                dataSigin.push({
                                    project_id:that.props.projectid,
                                    dic_id:msg.data[i][j].dic_id,
                                    dicsyn_no:msg.data[i][j].dicsyn_no,
                                });
                            }
                        }
                        that.setState({datasiginNo:dataSigin,checkselects:checkselects});
                        IfDispatcher.dispatch({
                            actionType: Comment.GETDICTDATAFILE,
                            dictdatalist:datalist
                        });

                        that.getProjectDict();
                    },
                    error:function(){
                        $("#ajaxloading").hide();
                       // alert("get globaldata fail");
                    },
                    statusCode:{
                        414:function(){
                        },
                        406:function (){
                            IfDispatcher.dispatch({
                                actionType:Comment.LOGOUT
                            });
                        }
                    }
                });

            },
            error:function(){
                $("#ajaxloading").hide();
                //alert("get dictslist fail");
            },
            statusCode:{
                414:function(){
                },
                406:function (){
                    IfDispatcher.dispatch({
                        actionType:Comment.LOGOUT
                    });
                }
            }
        });

    },
    /**
     *The function name: getProjectDict.
     *Function used: To obtain a list has been added to dictionary project.
     *Create time: 2017/4/5
     */
    getProjectDict(){
        var projectid = this.props.projectid;
        $.ajax({
            url: Comment.DATA_URL+"KDB/ShowProjectDic",
            data:$.param({
                projectid:projectid,
            }),
            type: 'GET',
            success: function(data){
                $("#ajaxloading").hide();
                var msg = data;
                IfDispatcher.dispatch({
                    actionType: Comment.GETPROJECTDICT,
                    projectdictlist:msg.data
                });

            },
            error:function(){
                $("#ajaxloading").hide();
               // alert("get projectlist fail");
            },
            statusCode:{
                414:function(){
                },
                406:function (){
                    IfDispatcher.dispatch({
                        actionType:Comment.LOGOUT
                    });
                }
            }
        });
    },
    /**
     *The function name: dictfileclick.
     *Function used: Get a dictionary entry details.
     *Create time: 2017/4/5
     */
    dictfileclick(dicid,name,item){
        var that = this;
        var inFunc = function(){
            var dic_id = dicid;
            that.setState({
                ifdicid:dicid,
                ifdicname:name,
                selectItem:item,
            })
            var hash = crypto.createHash("md5");
            hash.update(dic_id.toString());
            $.ajax({
                url: Comment.DATA_URL+"KDB/FindDic",
                data:$.param({
                    dicid:dic_id,
                    paramcheck:hash.digest("hex"),
                }),
                type: 'GET',
                success: function(data){
                    var msg = data;
                    var datalist = [];
                    var dataSigin =[];
                    var checkselect =[];
                    for(var i=0;i<msg.data.length;i++){
                        for(var j=0;j<msg.data[i].length;j++){
                            datalist.push(msg.data[i][j]);
                            checkselect.push({
                                dicsyn_no:msg.data[i][j].dicsyn_no,
                                ifckecked:true,
                            });
                            dataSigin.push({
                                project_id:that.props.projectid,
                                dic_id:msg.data[i][j].dic_id,
                                dicsyn_no:msg.data[i][j].dicsyn_no,
                            });
                        }
                    }
                    that.setState({datasiginNo:dataSigin,checkselects:checkselect});
                    IfDispatcher.dispatch({
                        actionType: Comment.GETDICTDATAFILE,
                        dictdatalist:datalist
                    });
                },
                error:function(){
                    $("#ajaxloading").hide();
                   // alert("get FindDic fail");
                },
                statusCode:{
                    414:function(){
                    },
                    406:function (){
                        IfDispatcher.dispatch({
                            actionType:Comment.LOGOUT
                        });
                    }
                }
            });
        };
        return inFunc;
    },
    /**
     *The function name: checkconfirmclick.
     *Function used: To join the project dictionary entry for effective choice.
     *Create time: 2017/4/5
     */
    checkconfirmclick(synno,dicid){
        var that = this;
        var inFunc = function(event){
            if(event.target.checked){
                var vlist =that.state.checkconfirms;
                var data = {};
                for(var v in vlist){
                    if(synno == vlist[v][0].dicsyn_no){
                        vlist[v][0].ifckecked = true;
                        vlist[v][1].ifckecked = false;
                        data = {
                            project_id:that.props.projectid,
                            dic_id:vlist[v][1].dic_id,
                            dicsyn_no:vlist[v][1].dicsyn_no,
                        };
                        that.setState({checkconfirms:vlist});
                    }
                    else if(synno == vlist[v][1].dicsyn_no){
                        vlist[v][1].ifckecked = true;
                        vlist[v][0].ifckecked = false;
                        data = {
                            project_id:that.props.projectid,
                            dic_id:vlist[v][0].dic_id,
                            dicsyn_no:vlist[v][0].dicsyn_no,
                        };
                        that.setState({checkconfirms:vlist});
                    }
                }
                 var qlist = that.state.confirmdatasiginNo;
                 var ifdatazzl = false;
                for(var j in qlist) {
                    if (synno == qlist[j].dicsyn_no) {
                        ifdatazzl = true;
                        qlist.splice(j,1);
                    }
                }
                if(ifdatazzl){
                    var ifconfirm = false;
                    for(var g in qlist){
                        if(data.dicsyn_no == qlist[g].dicsyn_no){
                            ifconfirm = true;
                        }
                    }
                    if(ifconfirm){
                        that.setState({confirmdatasiginNo:qlist});
                    }else{
                        qlist.push(data);
                        that.setState({confirmdatasiginNo:qlist});
                    }
                }else{
                    var ifdata = false;
                    for(var j in qlist){
                        if(data.dicsyn_no == qlist[j].dicsyn_no){
                            ifdata = true;
                        }
                    }
                    if(ifdata){
                       // that.setState({confirmdatasiginNo:qlist});
                    }else{
                        qlist.push(data);
                        that.setState({confirmdatasiginNo:qlist});
                    }
                }

            }else{
                var vlist =that.state.checkconfirms;
                var data = {};
                for(var v in vlist){
                    if(synno == vlist[v][0].dicsyn_no){
                        vlist[v][0].ifckecked = false;
                        vlist[v][1].ifckecked = true;
                        data = {
                            project_id:that.props.projectid,
                            dic_id:vlist[v][1].dic_id,
                            dicsyn_no:vlist[v][1].dicsyn_no,
                        };
                        that.setState({checkconfirms:vlist});
                    }
                    else if(synno == vlist[v][1].dicsyn_no){
                        vlist[v][1].ifckecked = false;
                        vlist[v][0].ifckecked = true;
                        data = {
                            project_id:that.props.projectid,
                            dic_id:vlist[v][0].dic_id,
                            dicsyn_no:vlist[v][0].dicsyn_no,
                        };
                        that.setState({checkconfirms:vlist});
                    }
                }
                var qlist = that.state.confirmdatasiginNo;
                var ifdatazzl = false;
                for(var j in qlist) {
                    if (synno == qlist[j].dicsyn_no) {
                        ifdatazzl = true;
                    }
                }
                if(ifdatazzl){

                    for(var g in qlist){
                        if(data.dicsyn_no == qlist[g].dicsyn_no){
                            qlist.splice(g,1);
                            that.setState({confirmdatasiginNo:qlist});
                        }
                    }
                }else{
                    var ifdata = false;
                    for(var j in qlist){
                        if(data.dicsyn_no == qlist[j].dicsyn_no){
                            ifdata = true;
                            qlist.splice(j,1);
                        }
                    }
                    qlist.push({
                        project_id:that.props.projectid,
                        dic_id:dicid,
                        dicsyn_no:synno,
                    });
                    that.setState({confirmdatasiginNo:qlist});
                }
            }
        };
        return inFunc;
    },
    /**
     *The function name: confirmdataadd.
     *Function used: To submit has chosen a good conflict entries.
     *Create time: 2017/4/5
     */
    confirmdataadd(){
        if(true){
            $("#ajaxloading").show();
            var datalist = this.state.confirmdatasiginNo;
            var that = this;
            $.ajax({
                url: Comment.DATA_URL+"KDB/ConfirmDicToProject",
                data:JSON.stringify(datalist),
                dataType:"json",
                type: 'POST',
                contentType: 'application/json;charset=utf-8',
                success: function (data) {
                    var msg = data;
                    $("#ajaxloading").hide();
                    $("#confirmdata").modal("hide");
                    that.getProjectDict();
                },
                error:function(){
                    $("#ajaxloading").hide();
                },
                statusCode:{
                    414:function(){
                        alert("add ConfirmDicToProject fail!");
                    },
                    406:function (){
                        IfDispatcher.dispatch({
                            actionType:Comment.LOGOUT
                        });
                    }
                }
            });
        }
    },
    /**
     *The function name: checkrowclick.
     *Function used: Entry by selecting the conflict.
     *Create time: 2017/4/5
     */
    checkrowclick(synno,dicid){
        var that = this;
        var inFunc = function(event){
                if(event.target.checked){
                    var vlist =that.state.checkselects;
                    for(var v in vlist){
                        if(synno == vlist[v].dicsyn_no){
                            vlist[v].ifckecked = true;
                            that.setState({checkselects:vlist});
                        }
                    }
                    var qlist = that.state.datasiginNo;
                    var ifsynno = true;
                    for(var j in qlist){
                        if(synno == qlist[j].dicsyn_no){
                            ifsynno = false;
                        }
                    }
                    if(ifsynno){
                        qlist.push({
                            project_id:that.props.projectid,
                            dic_id:dicid,
                            dicsyn_no:synno,
                        });
                        that.setState({datasiginNo:qlist});
                    }
                }else{
                    var dlist =that.state.datasiginNo;
                    var vlist =that.state.checkselects;
                    for(var v in vlist){
                        if(synno == vlist[v].dicsyn_no){
                            vlist[v].ifckecked = false;
                            that.setState({checkselects:vlist});
                        }
                    }
                    for(var k in dlist){
                        if(synno == dlist[k].dicsyn_no){
                            dlist.splice(k,1);
                            that.setState({datasiginNo:dlist});
                        }
                    }

                }

        };
        return inFunc;

    },

    checkCoverClick(event){
        if(event.target.checked){
            var that = this;
            $.ajax({
                url: Comment.DATA_URL+"KDB/GetTamrAllFiles",
                data: $.param({
                    authtoken:that.props.loginInfo.uuid,
                }),
                contentType: 'application/x-www-form-urlencoded',
                type: 'GET',
                success: function (data) {
                    var msg = data;
                    if(msg.status =="200"){
                        var datalist = [];
                        var ckeckselects=[];
                        for(var j=0;j<msg.data.length;j++){
                            datalist.push(msg.data[j]);
                            ckeckselects.push({
                                filename:msg.data[j],
                                ifckecked:false,
                            });
                        }
                        that.setState({
                            covernamelist:datalist,
                            ckockselects:ckeckselects
                        });
                    }else{
                        that.setState({
                            covernamelist:[],
                            ckockselects:[]
                        });
                        alert("GetTamrAllFiles fail.");
                    }
                },
                error:function(jxr,scode){
                    that.setState({
                        covernamelist:[],
                        ckockselects:[]
                    });
                },
                statusCode:{
                    414:function(){
                    },
                    406:function (){
                        IfDispatcher.dispatch({
                            actionType:Comment.LOGOUT
                        });
                    },
                }
            });
        }else{
            this.setState({
                covernamelist:[],
                ckockselects:[]
            });
        }
    },
    checkrowFileclick(filename){
        var that = this;
        var inFunc = function(event){
            if(event.target.checked){
                var vlist= that.state.ckockselects;
                for(var k in vlist){
                    if(vlist[k].filename == filename){
                        vlist[k].ifckecked = true;
                    }else{
                        vlist[k].ifckecked = false;
                    }
                }
                that.setState({
                    ckockselects:vlist,
                    newcoverfile:filename
                });
            }else{
                var vlist= that.state.ckockselects;
                for(var k in vlist){
                    vlist[k].ifckecked = false;
                }
                that.setState({
                    ckockselects:vlist,
                    newcoverfile:null
                });
            }
        }
        return inFunc;
    },
    /**
     *The function name: submitdictfile.
     *Function used: Dictionary to join the project.
     *Create time: 2017/4/5
     */
    submitdictfile(){
        var prodict = this.props.projectdictlist;
        var flag = true;
        for(var i in prodict){
            if(this.state.ifdicname==prodict[i].dic_name){
                $("#exsitprodict").modal("show");
                $("#ajaxloading").hide();
                flag=false;
            }
        }

        if(flag){
            $("#ajaxloading").show();
            var datalist = this.state.datasiginNo;
            var that = this;
            if(!(datalist.length>0)){
                $("#datanull").modal("show");
                $("#ajaxloading").hide();
            }else{
                $.ajax({
                    url: Comment.DATA_URL+"KDB/AddDicToProject ",
                    data:JSON.stringify(datalist),
                    dataType:"json",
                    type: 'POST',
                    contentType: 'application/json;charset=utf-8',
                    success: function (data) {
                        var msg = data;
                        if(msg.ifobj){
                            var confirmdatasiginNo =[];
                            var checkconfirmselectz =[];
                            for(var i=0;i<msg.data.length;i++){
                                var checkconfirmselect =[];
                                checkconfirmselect.push({
                                    dicsyn_no:msg.data[i][0].dicsyn_no,
                                    source_name:msg.data[i][0].source_name,
                                    dic_id:msg.data[i][0].dic_id,
                                    ifckecked:true,
                                });
                                checkconfirmselect.push({
                                    dicsyn_no:msg.data[i][1].dicsyn_no,
                                    source_name:msg.data[i][1].source_name,
                                    dic_id:msg.data[i][1].dic_id,
                                    ifckecked:false,
                                });
                                checkconfirmselectz.push(checkconfirmselect);

                                confirmdatasiginNo.push({
                                    project_id:that.props.projectid,
                                    dic_id:msg.data[i][1].dic_id,
                                    dicsyn_no:msg.data[i][1].dicsyn_no,
                                });
                            }
                            that.setState({confirmdata:msg.data,confirmdatasiginNo:confirmdatasiginNo,checkconfirms:checkconfirmselectz});
                            $("#ajaxloading").hide();
                            $("#confirmdata").modal("show");
                        }else{
                            that.getProjectDict();

                        }
                    },
                    error:function(){
                        $("#ajaxloading").hide();
                    },
                    statusCode:{
                        414:function(){
                            alert("add AddDicToProject fail!");
                        },
                        417:function(){
                            $("#forbid").modal("show");
                        },
                        416:function(){
                            $("#containsid").modal("show");
                        },
                        406:function (){
                            IfDispatcher.dispatch({
                                actionType:Comment.LOGOUT
                            });
                        }

                    }
                });
            }

        }
    },
    /**
     *The function name: projectDicDelclick.
     *Function used: Delete has been added to dictionary project.
     *Create time: 2017/4/5
     */
    projectDicDelclick(id){
        var that = this;
        var inFunc = function(){
            that.state.dictindex = id;
            $("#deleteprodict").modal("show");
        };
        return inFunc;
    },
    /**
     *The function name: deleteProdictComf.
     *Function used: Delete has been added to dictionary project.
     *Create time: 2017/4/5
     */
    deleteProdictComf(){
        $("#deleteprodict").modal("hide");
        var that = this;
        $("#ajaxloading").show();
        $.ajax({
            url:Comment.DATA_URL+"KDB/RemoveDicToProject  ",
            data: $.param({
                _method:"delete",
                projectid:that.props.projectid,
                dicid:that.state.dictindex,

            }),
            type:'POST',
            contentType: 'application/x-www-form-urlencoded',
            success:function(data){
                that.getProjectDict();
            },
            error:function () {
                $("#ajaxloading").hide();
            },
            statusCode:{
                416:function(){
                    $("#deleteprodictno").modal("show");
                },
                406:function (){
                    IfDispatcher.dispatch({
                        actionType:Comment.LOGOUT
                    });
                }

            }

        });
    },
    /**
     *The function name: projectFeaDelclick.
     *Function used: Delete has joined the data characteristics of the project file.
     *Create time: 2017/4/5
     */
    projectFeaDelclick(fedno){
        var that = this;
        var inFunc = function(){
            that.state.feanoindex = fedno;
            $("#deleteprofeano").modal("show");
        };
        return inFunc;
    },
    projectPartPleclick(confId){
        var that = this;
        var inFunc = function(){
            that.state.confIdPle = confId;
            $("#deletepartpleno").modal("show");
        };
        return inFunc;
    },
    deletePartPleComf(){
        var that = this;
        $("#ajaxloading").show();
        $.ajax({
            url:Comment.DATA_URL+"KDB/RemoveConfFromProject",
            data: $.param({
                _method:"delete",
                projectId:that.props.projectid,
                confDefineId:that.state.confIdPle,

            }),
            type:'POST',
            contentType: 'application/x-www-form-urlencoded',
            success:function(data){
                $("#deletepartpleno").modal("hide");
                that.getParticiPleProject();
            },
            error:function () {
                $("#ajaxloading").hide();
            },
            statusCode:{
                406:function (){
                    IfDispatcher.dispatch({
                        actionType:Comment.LOGOUT
                    });
                }

            }

        });
    },
    /**
     *The function name: deleteProfeanoComf.
     *Function used: Delete has joined the data characteristics of the project file.
     *Create time: 2017/4/5
     */
    deleteProfeanoComf(){
        var that = this;
        $("#ajaxloading").show();
        $.ajax({
            url:Comment.DATA_URL+"KDB/RemoveFeatureFromProject",
            data: $.param({
                _method:"delete",
                project_id:that.props.projectid,
                featuredef_no:that.state.feanoindex,

            }),
            type:'POST',
            contentType: 'application/x-www-form-urlencoded',
            success:function(data){
                $("#deleteprofeano").modal("hide");
                that.getFeatureProject();
            },
            error:function () {
                $("#ajaxloading").hide();
            },
            statusCode:{
                406:function (){
                    IfDispatcher.dispatch({
                        actionType:Comment.LOGOUT
                    });
                }

            }

        });
    },
    /**
     *The function name: getDataFeaturelist.
     *Function used: To get the data characteristics of the file list.
     *Create time: 2017/4/5
     */
    getDataFeaturelist(){
        $("#ajaxloading").show();
        var that = this;
        that.setState({
           ifdds:1,
        })
        $.ajax({
            url: Comment.DATA_URL+"KDB/GetAllFeatureDefine",
            type: 'GET',
            success: function(data){
                var msg = data;
                IfDispatcher.dispatch({
                    actionType: Comment.GETFEATURESdEFLIST,
                    featuredeflist:msg.data
                });
                if(that.state.thisfdefno==null && (msg.data.length>0)){
                    var tfeature_no = msg.data[0].featuredef_no;
                    var tfeature_name = msg.data[0].featuredef_name;
                    that.setState({
                        thisfdefno:tfeature_no ,
                        thisfdefnname:tfeature_name,
                    })
                }
                if(msg.data.length>0){
                    $.ajax({
                        url: Comment.DATA_URL+"KDB/GetAllFeaturesDetailByFeatureNo",
                        data:$.param({
                            featuredef_no:that.state.thisfdefno,
                        }),
                        type: 'GET',
                        success: function(data){
                            $("#ajaxloading").hide();
                            var msg = data;
                            IfDispatcher.dispatch({
                                actionType: Comment.GETFEATUREDETAILLIST,
                                featuredetlist:msg.data
                            });
                            that.getFeatureProject();
                        },
                        error:function(){
                            $("#ajaxloading").hide();
                        },
                        statusCode:{
                            406:function (){
                                IfDispatcher.dispatch({
                                    actionType:Comment.LOGOUT
                                });
                            }

                        }
                    });
                }else{
                    $("#ajaxloading").hide();
                }

            },
            error:function(){
                $("#ajaxloading").hide();
            },
            statusCode:{
                406:function (){
                    IfDispatcher.dispatch({
                        actionType:Comment.LOGOUT
                    });
                }

            }
        });
    },
    getParticiPlelist(){
        $("#ajaxloading").show();
        var that = this;
        that.setState({
            ifdds:4,
        })
        $.ajax({
            url: Comment.DATA_URL+"KDB/GetAllConfDefind",
            type: 'GET',
            success: function(data){
                var msg = data;
                IfDispatcher.dispatch({
                    actionType: Comment.GETPARTICIPLECONFLIST,
                    participleconflist:msg.data
                });
                if(that.state.currentPleFileName==null && (msg.data.length>0)){
                    var currentPleFileId = msg.data[0].conf_define_id;
                    var currentPleFileName = msg.data[0].conf_name;
                    that.setState({
                        currentPleFileId:currentPleFileId ,
                        currentPleFileName:currentPleFileId,
                    })
                }
                if(msg.data.length>0){
                    $.ajax({
                        url: Comment.DATA_URL+"KDB/getAllConfs",
                        data:$.param({
                            confDefineId:that.state.currentPleFileId,
                        }),
                        type: 'GET',
                        success: function(data){
                            $("#ajaxloading").hide();
                            var msg = data;
                            IfDispatcher.dispatch({
                                actionType: Comment.GETPARTICIPLEDATALZZ,
                                partpledatalist:msg.data
                            });
                            that.getParticiPleProject();
                        },
                        error:function(){
                            $("#ajaxloading").hide();
                        },
                        statusCode:{
                            406:function (){
                                IfDispatcher.dispatch({
                                    actionType:Comment.LOGOUT
                                });
                            }

                        }
                    });
                }else{
                    $("#ajaxloading").hide();
                }

            },
            error:function(){
                $("#ajaxloading").hide();
            },
            statusCode:{
                406:function (){
                    IfDispatcher.dispatch({
                        actionType:Comment.LOGOUT
                    });
                }

            }
        });
    },
    /**
     *The function name: featuretableclick.
     *Function used: To get the data feature file details.
     *Create time: 2017/4/5
     */
    featuretableclick(tfdefno,name,item){
        var that = this;
        var inFunc = function(){
            var tfdef_no = tfdefno;
            that.setState({
                thisfdefno:tfdef_no,
                thisfdefnname:name,
                selectFdefItem:item,
            })
            $.ajax({
                url: Comment.DATA_URL+"KDB/GetAllFeaturesDetailByFeatureNo",
                data:$.param({
                    featuredef_no:tfdef_no,
                }),
                type: 'GET',
                success: function(data){
                    $("#ajaxloading").hide();
                        var msg = data;
                    IfDispatcher.dispatch({
                        actionType: Comment.GETFEATUREDETAILLIST,
                        featuredetlist:msg.data
                    });
                    that.getFeatureProject();
                },
                error:function(){
                    $("#ajaxloading").hide();
                   // alert("get featuredetaillist fail");
                },
                statusCode:{
                    406:function (){
                        IfDispatcher.dispatch({
                            actionType:Comment.LOGOUT
                        });
                    }

                }
            });
        };
        return inFunc;
    },
    ParticiPletableClick(confId,fileName,item){
        var that = this;
        var inFunc = function(){
            that.setState({
                currentPleFileId:confId,
                currentPleFileName:fileName,
                selectPlefItem:item,
            })
            $.ajax({
                url: Comment.DATA_URL+"KDB/getAllConfs",
                data:$.param({
                    confDefineId:confId,
                }),
                type: 'GET',
                success: function(data){
                    $("#ajaxloading").hide();
                    var msg = data;
                    IfDispatcher.dispatch({
                        actionType: Comment.GETPARTICIPLEDATALZZ,
                        partpledatalist:msg.data
                    });
                    that.getParticiPleProject();
                },
                error:function(){
                    $("#ajaxloading").hide();
                },
                statusCode:{
                    406:function (){
                        IfDispatcher.dispatch({
                            actionType:Comment.LOGOUT
                        });
                    }

                }
            });
        };
        return inFunc;
    },
    /**
     *The function name: submitAddFeatdef.
     *Function used: New data files.
     *Create time: 2017/4/5
     */
    submitAddFeatdef(){
        var flag = true;
        var featurelist = this.props.datafeaturedetlist;
        if(featurelist.length==0){
            flag=false;
        }
        if(flag){
            $("#ajaxloading").show();
            var that = this;
            $.ajax({
                url: Comment.DATA_URL+"KDB/AddFeatureToProject ",
                data:$.param({
                    project_id:that.props.projectid,
                    featuredef_no:that.state.thisfdefno,
                }),
                type: 'POST',
                contentType: 'application/x-www-form-urlencoded',
                success: function (data) {
                    var msg = data;
                    that.getFeatureProject();
                },
                error:function(){
                    $("#ajaxloading").hide();
                },
                statusCode:{
                    406:function (){
                        IfDispatcher.dispatch({
                            actionType:Comment.LOGOUT
                        });
                    },
                    414:function(){
                        alert("add AddFeatureToProject fail!");
                    },
                    413:function(){
                        $("#exsitfeature").modal("show");
                        $("#ajaxloading").hide();
                    },
                }
            });
        }else{
            $("#fedetailNoData").modal("show");
        }
    },
    submitAddParticiplef(){
        var flag = true;
        var partpledatalist = this.props.partpledatalist;
        if(partpledatalist.length==0){
            flag=false;
        }
        if(flag){
            $("#ajaxloading").show();
            var that = this;
            $.ajax({
                url: Comment.DATA_URL+"KDB/AddConfToProject ",
                data:$.param({
                    projectId:that.props.projectid,
                    confDefineId:that.state.currentPleFileId,
                }),
                type: 'POST',
                contentType: 'application/x-www-form-urlencoded',
                success: function (data) {
                    var msg = data;
                    that.getParticiPleProject();
                },
                error:function(){
                    $("#ajaxloading").hide();
                },
                statusCode:{
                    406:function (){
                        IfDispatcher.dispatch({
                            actionType:Comment.LOGOUT
                        });
                    },
                    414:function(){
                        alert("add AddConfToProject fail!");
                    },
                    413:function(){
                        $("#exsitpartple").modal("show");
                        $("#ajaxloading").hide();
                    },
                }
            });
        }else{
            $("#partPleNoData").modal("show");
        }
    },
    /**
     *The function name: getFeatureProject.
     *Function used: For the characteristics existing data in the file.
     *Create time: 2017/4/5
     */
    getFeatureProject(){
        var projectid = this.props.projectid;
            $.ajax({
                url: Comment.DATA_URL+"KDB/FindFeatureFromProject",
                data:$.param({
                    project_id:projectid,
                }),
                type: 'GET',
                success: function(data){
                    $("#ajaxloading").hide();
                    var msg = data;
                    IfDispatcher.dispatch({
                        actionType: Comment.GETFEATUREPROJECT,
                        featureprojectlist:msg.data
                    });
                },
                error:function(){
                    $("#ajaxloading").hide();
                   // alert("get FeatureFromProject fail");
                },
                statusCode:{
                    406:function (){
                        IfDispatcher.dispatch({
                            actionType:Comment.LOGOUT
                        });
                    },
                }
            });
    },
    getParticiPleProject(){
        var projectid = this.props.projectid;
        $.ajax({
            url: Comment.DATA_URL+"KDB/FindConfFromProject",
            data:$.param({
                projectId:projectid,
            }),
            type: 'GET',
            success: function(data){
                $("#ajaxloading").hide();
                var msg = data;
                IfDispatcher.dispatch({
                    actionType: Comment.GETPARTICIPLEPROJECT,
                    partpleprojectlist:msg.data
                });
            },
            error:function(){
                $("#ajaxloading").hide();
                // alert("get FeatureFromProject fail");
            },
            statusCode:{
                406:function (){
                    IfDispatcher.dispatch({
                        actionType:Comment.LOGOUT
                    });
                },
            }
        });
    },
    /**
     *The function name: getsoureclick.
     *Function used: Get metadata list.
     *Create time: 2017/4/5
     */
    getsoureclick(num){
        var that = this;
        var inFunc = function(){
            that.setState({
                currentpage:0,
                ifods:num,
                ifbar:false,
                ifmeta:false,
                ifdds:2,
                selectSourceItem:num,
                metadatatext:null,
                metadatas:[],
                sourcesdata:[],
            });
            $("#ajaxloading").show();
            $.ajax({
                url: Comment.DATA_URL+"KDB/ViewMetaInputFromPage",
                data: $.param({
                    projectid: that.props.projectid,
                    flag: num,
                }),
                type:'GET',
                success:function(data){
                    $("#ajaxloading").hide();
                    var msg = data;
                    if(msg.datacounts != 0){
                        that.setState({
                            datacounts:msg.datacounts,
                            ifrunnig:true,
                        });
                    }else{
                        that.setState({
                            datacounts:msg.datacounts,
                            ifrunnig:false,
                        });
                    }

                    IfDispatcher.dispatch({
                        actionType:Comment.GETSOUREBDSODS,
                        sourcesdata:msg.data,
                    });
                },
                error:function(){
                    $("#ajaxloading").hide();
                },
                statusCode:{
                    406:function (){
                        IfDispatcher.dispatch({
                            actionType:Comment.LOGOUT
                        });
                    },
                }
            });
        }
        return inFunc;

    },
    keysourcesearch(event){
        if(event.key == "Enter"){
            if(this.state.metadatatext == null ||this.state.metadatatext == ""){
                $("#metadatanull").modal("show");
            }else{
                $("#ajaxloading").show();
                var that = this;
                $.ajax({
                    url: Comment.DATA_URL+"KDB/ViewMetaInputFromPage",
                    data: $.param({
                        projectid:that.props.projectid,
                        flag:that.state.ifods,
                        param:that.state.metadatatext,
                    }),
                    type:'GET',
                    success:function(data){
                        $("#ajaxloading").hide();
                        var msg = data;
                        that.setState({
                            searchtablecount:msg.searchcount,
                            searchtabledata:msg.data,
                            currentpagebl:0,
                        });
                        $("#searchzzltabledata").modal("show");
                    },
                    error:function(){
                        $("#ajaxloading").hide();
                        //alert("get ViewMetaInputFromPage fail");
                    },
                    statusCode:{
                        406:function (){
                            IfDispatcher.dispatch({
                                actionType:Comment.LOGOUT
                            });
                        },
                    }
                });
            }
        }
    },
    getsearchtableclick(){
        if(this.state.metadatatext == null ||this.state.metadatatext == ""){
            $("#metadatanull").modal("show");
        }else{
            $("#ajaxloading").show();
            var that = this;
            $.ajax({
                url: Comment.DATA_URL+"KDB/ViewMetaInputFromPage",
                data: $.param({
                    projectid:that.props.projectid,
                    flag:that.state.ifods,
                    param:that.state.metadatatext,
                }),
                type:'GET',
                success:function(data){
                    $("#ajaxloading").hide();
                    var msg = data;
                    that.setState({
                        searchtablecount:msg.searchcount,
                        searchtabledata:msg.data,
                        currentpagebl:0,
                    });
                    $("#searchzzltabledata").modal("show");
                },
                error:function(){
                    $("#ajaxloading").hide();
                    //alert("get ViewMetaInputFromPage fail");
                },
                statusCode:{
                    406:function (){
                        IfDispatcher.dispatch({
                            actionType:Comment.LOGOUT
                        });
                    },
                }
            });
        }
    },
    /**
     *The function name: getsoureresultclick.
     *Function used: Access to the results of the data after data processing.
     *Create time: 2017/4/5
     */
    getsoureresultclick(num){
        var that = this;
        var inFunc = function(){
            $("#ajaxloading").show();
            var hash = crypto.createHash("md5");
            var msgzl = that.props.projectid.toString()+num.toString();
            hash.update(msgzl);
            $.ajax({
                url: Comment.DATA_URL+"KDB/GetResultDataForProject",
                data: $.param({
                    projectid:that.props.projectid,
                    flag:num,
                    paramcheck:hash.digest("hex"),
                }),
                type:'GET',
                success:function(data){
                    $("#ajaxloading").hide();
                    var msg = data;
                    if(msg.datacounts != 0){
                        that.setState({
                            ifbad:true,
                            datacountss:msg.datacounts,
                            columnname:msg.columnname,
                            currentpages:0,
                            ifodss:num,
                            ifdds:3,
                            resultdata:msg.data,
                            ifexport:false,
                            exporturl:null,
                            selectresultItem:num,
                            metadatatext:null,
                        });
                    }else{
                        that.setState({
                            ifbad:false,
                            datacountss:msg.datacounts,
                            columnname:msg.columnname,
                            currentpages:0,
                            ifodss:num,
                            ifdds:3,
                            resultdata:msg.data,
                            ifexport:false,
                            exporturl:null,
                            selectresultItem:num,
                            metadatatext:null,
                        });
                    }
                },
                error:function(){
                    $("#ajaxloading").hide();
                    that.setState({
                        ifbad:false,
                        ifdds:3,
                        ifodss:num,
                        ifexport:false,
                        exporturl:null,
                        selectresultItem:num,
                        metadatatext:null,
                    });
                   // alert("get GetResultDataForProject fail");
                },
                statusCode:{
                    406:function (){
                        IfDispatcher.dispatch({
                            actionType:Comment.LOGOUT
                        });
                    },
                }
            });
        }
        return inFunc;

    },
    keyresultsearch(event){
        if(event.key == "Enter"){
            if(this.state.metadatatext == null ||this.state.metadatatext == ""){
                $("#metadatanull").modal("show");
            }else{
                $("#ajaxloading").show();
                var that = this;
                var hash = crypto.createHash("md5");
                var msgzl = that.props.projectid.toString()+that.state.ifodss.toString()+that.state.metadatatext;
                hash.update(msgzl);
                $.ajax({
                    url: Comment.DATA_URL+"KDB/GetResultDataForProject",
                    data: $.param({
                        projectid:that.props.projectid,
                        flag:that.state.ifodss,
                        param:that.state.metadatatext,
                        paramcheck:hash.digest("hex"),
                    }),
                    type:'GET',
                    success:function(data){
                        $("#ajaxloading").hide();
                        var msg = data;
                        that.setState({
                            searchcount:msg.searchcount,
                            searchdata:msg.data,
                            searchname:msg.columnname,
                            currentpagech:0,
                        });
                        $("#searchzzldata").modal("show");
                    },
                    error:function(){
                        $("#ajaxloading").hide();
                        // alert("get GetResultDataForProject fail");
                    },
                    statusCode:{
                        406:function (){
                            IfDispatcher.dispatch({
                                actionType:Comment.LOGOUT
                            });
                        },
                    }
                });
            }
        }
    },
    getsearchclick(){
        if(this.state.metadatatext == null ||this.state.metadatatext == ""){
            $("#metadatanull").modal("show");
        }else{
            $("#ajaxloading").show();
            var that = this;
            var hash = crypto.createHash("md5");
            var msgzl = that.props.projectid.toString()+that.state.ifodss.toString()+that.state.metadatatext;
            hash.update(msgzl);
            $.ajax({
                url: Comment.DATA_URL+"KDB/GetResultDataForProject",
                data: $.param({
                    projectid:that.props.projectid,
                    flag:that.state.ifodss,
                    param:that.state.metadatatext,
                    paramcheck:hash.digest("hex"),
                }),
                type:'GET',
                success:function(data){
                    $("#ajaxloading").hide();
                    var msg = data;
                    that.setState({
                        searchcount:msg.searchcount,
                        searchdata:msg.data,
                        searchname:msg.columnname,
                        currentpagech:0,
                    });
                    $("#searchzzldata").modal("show");
                },
                error:function(){
                    $("#ajaxloading").hide();
                   // alert("get GetResultDataForProject fail");
                },
                statusCode:{
                    406:function (){
                        IfDispatcher.dispatch({
                            actionType:Comment.LOGOUT
                        });
                    },
                }
            });
        }
    },
    /**
     *The function name: importodsbdsClick.
     *Function used: To import the metadata file.
     *Create time: 2017/4/5
     */
    importodsbdsClick(){
        this.setState({
            ifselectfiles:false,
            fileNum:0,
            fileData:[],
            setencode:"UTF-8",
            setlanuage:"Chinese",
            setseparate:"|",
            ifseter: false,
            ifseterdd:false,
            ifseterss:false,
            errormsg:null,
            savasuccess:false,
            dataoptmsg:[],
            dataopts:[],
            tpt_name:"",
            en_table_name:"",
            en_col_name:"",
            la_table_name:"",
            la_col_name:"",
            token_bit:"",
            col_data_type:"",
            col_des:"",
            table_cycle:"",
            table_des:"",
            order_seq:"",
            codename:"",
            ext1_name:"",
            ext2_name:"",
            ext3_name:"",
            ext4_name:"",
        });
        //$("#ckeckheader").prop("checked",false);
        var that = this;
        $("#ajaxloading").show();
        $.ajax({
            url: Comment.DATA_URL+"KDB/getAllMetaInputTemplates",
            type:'GET',
            success:function(data){
                $("#ajaxloading").hide();
                var msg = data;
                var options =[];
                for(var i=0;i <msg.data.length;i++){
                     options.push(<option value={msg.data[i].template_name}>{msg.data[i].template_name}</option>);
                }
                that.setState({
                   dataoptmsg:msg.data,
                   dataopts:options
                });
                $("#ckeckzd2").prop("checked",true);
                $("#ckeckzd1").prop("checked",false);
                $("#importodsbds").modal("show");
            },
            error:function(){
                $("#ajaxloading").hide();
                // alert("get GetResultDataForProject fail");
            },
            statusCode:{
                406:function (){
                    IfDispatcher.dispatch({
                        actionType:Comment.LOGOUT
                    });
                },
            }
        });
    },
    openNewlick(){
        window.open("./public/Mapping/scanning.html");
    },
    readcsvfile(obj){

           // var len=$(obj)[0].files.length;
           // alert("d"+len);
            var filenum=0;
            var flag=false;
            var data = new FormData();
            for(var j=0;j<$(obj)[0].files.length;j++){
                filenum++;
                data.append("name",$(obj)[0].files[j].name);
                data.append("file",$(obj)[0].files[j]);
            }
            this.setState({
                fileNum:filenum,
                fileData:data
            });
            //var Ifcsvfile = $(obj)[0].files[0].name.split(".");
            //if(Ifcsvfile[Ifcsvfile.length-1] == "csv"){
            //
            //}else{
            //    alert("Please upload the CSV file!");
            //}

    },
    replacename(str){
        var charstr = str.toString().split("");
        var newstr="";

        for(var j=0; j<charstr.length;j++){
            if(charstr[j] === " ")
                newstr += "%20";
             //   newstr=newstr+charstr[j].replace(/" "/g ,"%20");
            else if(charstr[j] === "+")
                newstr += "%2B";
               // newstr=newstr+charstr[j].replace(/"+"/g,"%2B");
            else if(charstr[j] === "/")
                newstr += "%2F";
               // newstr=newstr+charstr[j].replace(/"\/"/g,"%2F");
            else if(charstr[j] === "?")
                newstr += "%3F";
               // newstr=newstr+charstr[j].replace(/"?"/g,"%3F");
            else if(charstr[j] === "%")
                newstr += "%25";
               // newstr=newstr+charstr[j].replace(/"%"/g,"%25");
            else if(charstr[j] === "#")
                newstr += "%23";
                //newstr=newstr+charstr[j].replace(/"#"/g,"%23");
            else if(charstr[j] === "&")
                newstr += "%26";
               // newstr=newstr+charstr[j].replace(/"&"/g,"%26");
            else if(charstr[j] === ",")
                newstr += "%3D";
              //  newstr=newstr+charstr[j].replace(/","/g,"%3D");
            else
                newstr +=charstr[j];
        }
       // var newstr = str.replace(/''/g,"%20").replace(/'+'/g,"%2B").replace("/","%2F").replace("?","%3F").replace("%","%25").replace("#","%23").replace("&","%26").replace("=","%3D");
        return newstr;
    },
    importFiles(){
        if (this.state.tpt_name==null||this.state.tpt_name == "") {
            this.setState({ifseter: true});
        }
        else if(this.state.en_table_name==null|| this.state.en_table_name== ""){
            this.setState({ifseterdd: true});
        }
        else if(this.state.en_col_name==null||this.state.en_col_name == ""){
            this.setState({ifseterss: true});
        }
        else {
            if(this.state.fileNum != 0){
                var ifneedheader = 0;
                if($("#ckeckzd2").get(0).checked){
                    ifneedheader = 1;
                }
                $("#ajaxloading").show();
                $("#importodsbds").modal("hide");
                var that = this;
                var hash = crypto.createHash("md5");
               // var zlmsg = that.state.setencode+that.state.setlanuage+that.state.setseparate+that.props.projectid+that.state.ifods+ifneedheader;
                var zlmsg = that.props.projectid+that.state.ifods.toString()+that.state.tpt_name+that.state.en_table_name+that.state.en_col_name+that.state.la_table_name+that.state.la_col_name+that.state.token_bit+that.state.col_data_type+that.state.col_des+that.state.table_cycle+that.state.table_des+that.state.order_seq+that.state.codename+that.state.ext1_name+that.state.ext2_name+that.state.ext3_name+that.state.ext4_name+ifneedheader;
                hash.update(zlmsg);
                var msgzl = that.props.projectid + "&flag=" + that.state.ifods + "&template_name=" +that.replacename(that.state.tpt_name)+"&en_table_name=" +that.replacename( that.state.en_table_name) + "&en_col_name=" + that.replacename( that.state.en_col_name) + "&la_table_name=" + that.replacename( that.state.la_table_name)+ "&la_col_name=" + that.replacename( that.state.la_col_name)+ "&token_bit=" +that.replacename( that.state.token_bit)+ "&col_data_type=" +that.replacename( that.state.col_data_type)+ "&col_des=" + that.replacename( that.state.col_des)+ "&table_cycle=" + that.replacename( that.state.table_cycle)+ "&table_des=" + that.replacename( that.state.table_des)+ "&order_seq=" + that.replacename( that.state.order_seq)+"&table_code=" + that.replacename( that.state.codename)+"&ext1=" + this.replacename( that.state.ext1_name)+"&ext2=" + this.replacename( that.state.ext2_name)+"&ext3=" + this.replacename( that.state.ext3_name)+"&ext4=" + this.replacename( that.state.ext4_name)+ "&check_redio=" + parseInt(ifneedheader)+"&paramcheck=" + hash.digest("hex");
               //var msgzl = that.props.projectid + "&flag=" + that.state.ifods + "&template_name=" +that.state.tpt_name+"&en_table_name=" +that.state.en_table_name + "&en_col_name=" + that.state.en_col_name + "&la_table_name=" + that.state.la_table_name+ "&la_col_name=" + that.state.la_col_name+ "&token_bit=" +that.state.token_bit+ "&col_data_type=" +that.state.col_data_type+ "&col_des=" + that.state.col_des+ "&table_cycle=" + that.state.table_cycle+ "&table_des=" + that.state.table_des+ "&order_seq=" +that.state.order_seq+"&table_code=" + that.state.codename+"&ext1=" + that.state.ext1_name+"&ext2=" + that.state.ext2_name+"&ext3=" + that.state.ext3_name+"&ext4=" + that.state.ext4_name+ "&check_redio=" + parseInt(ifneedheader)+"&paramcheck=" + hash.digest("hex");
                $.ajax({
                    url: Comment.DATA_URL + "KDB/UploadFile?projectid=" +msgzl ,
                    data: that.state.fileData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success: function (data) {
                        $("#ajaxloading").hide();
                        var msg = data;
                        if(msg.status == "200"){
                            if (msg.datacounts != 0) {
                                that.setState({
                                    datacounts: msg.datacounts,
                                    ifrunnig: true,
                                    currentpage: 0,
                                    fileNum:0,
                                    fileData:[]
                                });
                            } else {
                                that.setState({
                                    datacounts: msg.datacounts,
                                    ifrunnig: false,
                                    currentpage: 0,
                                    fileNum:0,
                                    fileData:[]
                                });
                            }
                            IfDispatcher.dispatch({
                                actionType: Comment.GETSOUREBDSODS,
                                sourcesdata: msg.data,
                            });
                        }else{
                            that.setState({
                                fileNum:0,
                                fileData:[],
                                errormsg:msg.data
                            });
                           // alert(msg.data);
                            $("#importodsbdszzdf").modal("show");
                        }
                    },
                    error: function (jxr, scode) {
                        //alert("import odsbds fail!");
                        that.setState({
                            fileNum:0,
                            fileData:[]
                        });
                        $("#ajaxloading").hide();
                    },
                    statusCode: {
                        413: function () {
                            // alert("Invalid format of the file content!");
                        },
                        406:function (){
                            IfDispatcher.dispatch({
                                actionType:Comment.LOGOUT
                            });
                        },
                        415:function () {
                            alert("Uploading files exceeds the maximum limit.");
                        }
                    }
                });
            }else{
                this.setState({ifselectfiles: true});
            }

        }

    },
    ClearSelectFilesClick(){
        this.setState({
            fileNum:0,
            fileData:[]
        });
    },
    chooseFile(){
        /*var that = this;
        $('#fileDialog').unbind('change');
        $('#fileDialog').change(function(evt) {
            that.readcsvfile($('#fileDialog'));
            $(this).val('');
        });
        $('#fileDialog').trigger('click');*/
        var that = this;
        $('#morefileDialog').unbind('change');
        $('#morefileDialog').change(function(evt) {
            that.readcsvfile($('#morefileDialog'));
            $(this).val('');
        });
        $('#morefileDialog').trigger('click');
    },
    confirmimportdata(){
        this.setState({
            ifselectfiles:false,
            savasuccess:false,
            ifseter:false,
            ifseterdd:false,
            ifseterss:false,
            errormsg:null,
        });
        this.chooseFile();
    },
    //setencodeChange(event){
    //    this.setState({
    //        setencode:event.target.value,
    //        ifseter:false,
    //    })
    //},
    //setlanuageChange(event){
    //    this.setState({
    //        setlanuage:event.target.value,
    //        ifseter:false,
    //    })
    //},
    //setseparateChange(event){
    //    this.setState({
    //        setseparate:event.target.value,
    //        ifseter:false,
    //    })
    //},
    optfileHandleChange(event){
        var data = this.state.dataoptmsg;
        if(event.target.value == "-输入或选择-"){
            this.setState({
                tpt_name:event.target.value,
                en_table_name:"",
                en_col_name:"",
                la_table_name:"",
                la_col_name:"",
                token_bit:"",
                col_data_type:"",
                col_des:"",
                table_cycle:"",
                table_des:"",
                order_seq:"",
                codename:"",
                ext1_name:"",
                ext2_name:"",
                ext3_name:"",
                ext4_name:"",
                ifseter:false,
                ifseterdd:false,
                ifseterss:false,
                errormsg:null,
                ifselectfiles:false,
                savasuccess:false,
            });
        }else{
            if(data.length > 0){
                for(var i = 0;i<data.length;i++){
                    if(data[i].template_name == event.target.value){
                        this.setState({
                            tpt_name:event.target.value,
                            en_table_name:data[i].en_table_name,
                            en_col_name:data[i].en_col_name,
                            la_table_name:data[i].la_table_name,
                            la_col_name:data[i].la_col_name,
                            token_bit:data[i].token_bit,
                            col_data_type:data[i].col_data_type,
                            col_des:data[i].col_des,
                            table_cycle:data[i].table_cycle,
                            table_des:data[i].table_des,
                            order_seq:data[i].order_seq,
                            codename:data[i].table_code,
                            ext1_name:data[i].ext1,
                            ext2_name:data[i].ext2,
                            ext3_name:data[i].ext3,
                            ext4_name:data[i].ext4,
                            ifseter:false,
                            ifseterdd:false,
                            ifseterss:false,
                            errormsg:null,
                            ifselectfiles:false,
                            savasuccess:false,
                        });
                    }
                }
            }
        }
    },
    SaveSelectClick(){
        //if (this.state.tpt_name == null || this.state.en_table_name == null || this.state.en_col_name == null ||this.state.la_table_name == null||this.state.la_col_name == null) {
        //    this.setState({ifseter: true});
        //}else{
            $("#ajaxloading").show();
            var that = this;
            var ifneedheader = 0;
            if($("#ckeckzd2").get(0).checked){
                ifneedheader = 1;
            }
            $.ajax({
                url: Comment.DATA_URL+"KDB/saveMetaInputTemplate ",
                data:$.param({
                    template_name:that.state.tpt_name,
                    en_table_name:that.state.en_table_name,
                    en_col_name:that.state.en_col_name,
                    la_table_name:that.state.la_table_name,
                    la_col_name:that.state.la_col_name,
                    token_bit:that.state.token_bit,
                    col_data_type:that.state.col_data_type,
                    col_des:that.state.col_des,
                    table_cycle:that.state.table_cycle,
                    table_des:that.state.table_des,
                    order_seq:that.state.order_seq,
                    table_code:that.state.codename,
                    ext1:that.state.ext1_name,
                    ext2:that.state.ext2_name,
                    ext3:that.state.ext3_name,
                    ext4:that.state.ext4_name,
                    check_redio:ifneedheader
                }),
                type: 'POST',
                contentType: 'application/x-www-form-urlencoded',
                success: function (data) {
                    var msg = data;
                    if(msg.status =="200"){
                        $.ajax({
                            url: Comment.DATA_URL+"KDB/getAllMetaInputTemplates",
                            type:'GET',
                            success:function(data){
                                $("#ajaxloading").hide();
                                var msg = data;
                                var options =[];
                                for(var i=0;i <msg.data.length;i++){
                                    options.push(<option value={msg.data[i].template_name}>{msg.data[i].template_name}</option>);
                                }
                                that.setState({
                                    dataoptmsg:msg.data,
                                    dataopts:options,
                                    savasuccess:true
                                });
                            },
                            error:function(){
                                $("#ajaxloading").hide();
                                // alert("get GetResultDataForProject fail");
                            },
                            statusCode:{
                                406:function (){
                                    IfDispatcher.dispatch({
                                        actionType:Comment.LOGOUT
                                    });
                                },
                            }
                        });
                    }else{
                        $("#ajaxloading").hide();
                        that.setState({
                            errormsg:msg.data,
                        });
                       // alert(msg.data);
                    }
                },
                error:function(){
                    $("#ajaxloading").hide();
                },
                statusCode:{
                    406:function (){
                        IfDispatcher.dispatch({
                            actionType:Comment.LOGOUT
                        });
                    },
                }
            });
      //  }
    },
    trim(str){
        str = str.replace(/^(\s|\u00A0)+/,'');
        for(var i=str.length-1; i>=0; i--){
            if(/\S/.test(str.charAt(i))){
                str = str.substring(0, i+1);
                break;
            }
        }
        return str;
    },
    fileHandleChange(name,event){
        var newstate = {};
        newstate[name] = this.trim(event.target.value);
        newstate. ifseter=false;
        newstate.ifseterdd=false;
        newstate.ifseterss=false;
        newstate.errormsg=null;
        newstate.savasuccess=false;
        newstate.ifselectfiles=false;
        this.setState(newstate);
    },
    //runpythonfile(){
    //    if(!this.state.ifrun){
    //        var  that = this;
    //        that.setState({
    //            timebar:parseFloat(0),
    //            ifbar:false,
    //            ifrun:true,
    //        });
    //        $.ajax({
    //            url: Comment.DATA_URL+"KDB/RunFinalProject",
    //            data: $.param({
    //                projectid:that.props.projectid,
    //            }),
    //            timeout: 1800000,
    //            type: 'POST',
    //            success: function(data){
    //                $("#ajaxloading").hide();
    //                var msg = data;
    //                that.setState({
    //                    ifrun:false,
    //                });
    //                setTimeout(function(){
    //                    document.getElementById("resultonclick").click();
    //                },1000);
    //            },
    //            error:function(){
    //                $("#ajaxloading").hide();
    //                that.setState({
    //                    ifrun:false,
    //                });
    //                alert("Must first be configured dictionary, or data characteristic rules!");
    //            }
    //        });
    //        setTimeout(function(){
    //            that.setState({timebarId:setInterval(that.getprogressbar,700)});
    //        },100);
    //
    //    }
    //},
    clearDateBeforRun(){
        var  that = this;
        var clearDateID = setInterval(function(){
            $.ajax({
                url: Comment.DATA_URL+"KDB/ClearDataBeforeRun",
                data: $.param({
                    project_id:that.props.projectid,
                    _method:"delete",
                }),
                contentType: 'application/x-www-form-urlencoded',
                type: 'POST',
                success: function(data){
                    var msg = data;
                    if(msg.data =="success"){
                        clearInterval(clearDateID);
                        that.runpythonfile();
                    }
                },
                error:function(XMLHttpRequest, Status, errorThrown){

                },
                statusCode:{
                    406:function (){
                        IfDispatcher.dispatch({
                            actionType:Comment.LOGOUT
                        });
                    },
                    414:function (){
                        clearInterval(clearDateID);
                    },
                }
            });
        },1000);
    },
    /**
     *The function name: runpythonfile.
     *Function used: Start the data processing function.
     *Create time: 2017/4/5
     */
    runpythonfile(){
        if(typeof(this.props.ifrunbarmsg.projectid) == "undefined" ){
            IfDispatcher.dispatch({
                actionType:Comment.GETIFRUNBAR,
                ifrun:true,
                ifbar:false,
                projectid:this.props.projectid,
                timebar:0,
            });
            var  that = this;
            var hash = crypto.createHash("md5");
            hash.update(that.props.projectid.toString());
            $.ajax({
                url: Comment.DATA_URL+"KDB/RunFinalProject",
                data: $.param({
                    projectid:that.props.projectid,
                    paramcheck:hash.digest("hex"),
                }),
                timeout: 8000,
                type: 'POST',
                success: function(data){
                    var msg = data;
                    if(msg.data !="success"){
                        IfDispatcher.dispatch({
                            actionType:Comment.GETIFRUNBARS,
                            ifrun:false,
                            ifbar:false,
                            timebar:0,
                        });
                        alert(msg.data);
                    }
                },
                error:function(XMLHttpRequest, Status, errorThrown){
                    if(Status == "timeout"){
                       // alert("The background data analysis...!");
                    }else{
                        IfDispatcher.dispatch({
                            actionType:Comment.GETIFRUNBARS,
                            ifrun:false,
                            ifbar:false,
                            timebar:0,
                        });
                    }
                },
                statusCode:{
                    406:function (){
                        IfDispatcher.dispatch({
                            actionType:Comment.LOGOUT
                        });
                    },
                    415:function(){
                        //alert("The project is running, don't allow operation!");
                    },
                    417:function(){
                        //alert("Start the service failure!");
                    },
                    413:function(){
                        //alert("Start the service failure!");
                    },
                }
            });
            IfDispatcher.dispatch({
                actionType:Comment.GETIFRUNBARID,
                timebarId:setInterval(that.getprogressbar,2000),
            });
        }else{
            if(!this.props.ifrunbarmsg.ifrun){
                IfDispatcher.dispatch({
                    actionType:Comment.GETIFRUNBAR,
                    ifrun:true,
                    ifbar:false,
                    projectid:this.props.projectid,
                    timebar:0,
                });
                var  that = this;
                var hash = crypto.createHash("md5");
                hash.update(that.props.projectid.toString());
                $.ajax({
                    url: Comment.DATA_URL+"KDB/RunFinalProject",
                    data: $.param({
                        projectid:that.props.projectid,
                        paramcheck:hash.digest("hex"),
                    }),
                    timeout: 8000,
                    type: 'POST',
                    success: function(data){
                        var msg = data;
                        if(msg.data !="success"){
                            IfDispatcher.dispatch({
                                actionType:Comment.GETIFRUNBARS,
                                ifrun:false,
                                ifbar:false,
                                timebar:0,
                            });
                            alert(msg.data);
                        }
                    },
                    error:function(XMLHttpRequest, Status, errorThrown){
                        if(Status == "timeout"){
                            //IfDispatcher.dispatch({
                            //    actionType:Comment.GETIFRUNBARID,
                            //    timebarId:setInterval(that.getprogressbar,1000),
                            //});
                           // alert("The background data analysis...!");
                        }else{
                            IfDispatcher.dispatch({
                                actionType:Comment.GETIFRUNBARS,
                                ifrun:false,
                                ifbar:false,
                                timebar:0,
                            });
                        }
                    },
                    statusCode:{
                        406:function (){
                            IfDispatcher.dispatch({
                                actionType:Comment.LOGOUT
                            });
                        },
                        415:function(){
                            //alert("The project is running, don't allow operation!");
                        },
                        417:function(){
                            //alert("Start the service failure!");
                        },
                        413:function(){
                            //alert("Start the service failure!");
                        }
                    }
                });
                IfDispatcher.dispatch({
                    actionType:Comment.GETIFRUNBARID,
                    timebarId:setInterval(that.getprogressbar,2000),
                });
            }
        }
    },
    /**
     *The function name: getprogressbar.
     *Function used: The progress bar.
     *Create time: 2017/4/5
     */
    getprogressbar(){
        //  $("#ajaxloading").show();
        var that = this;
        $.ajax({
            url: Comment.DATA_URL+"KDB/RunProjectTimeForFind",
            data: $.param({
                project_id: that.props.ifrunbarmsg.projectid,
            }),
            type:'GET',
            success:function(data){
                //    $("#ajaxloading").hide();
                var msg = data;
                if(!(1 == parseInt(msg.process_status))){
                    IfDispatcher.dispatch({
                        actionType:Comment.GETIFRUNBARTIME,
                        timebar:parseInt(parseFloat(msg.data)*100),
                        ifbar:true,
                    });
                    if(parseFloat(msg.data) >= 1.00){
                        IfDispatcher.dispatch({
                            actionType:Comment.GETIFRUNBARRUN,
                            ifrun:false,
                        });
                        clearInterval(that.props.ifrunbarmsg.timebarId);
                    }

                }else{
                    IfDispatcher.dispatch({
                        actionType:Comment.GETIFRUNBARRUN,
                        ifrun:false,
                    });
                    clearInterval(that.props.ifrunbarmsg.timebarId);
                    alert("The service failure!");
                }
            },
            error:function(){
                $("#ajaxloading").hide();
                clearInterval(that.state.timebarId);
                //alert("get RunProjectTimeForFind fail");
            },
            statusCode:{
                406:function (){
                    IfDispatcher.dispatch({
                        actionType:Comment.LOGOUT
                    });
                },
                415:function(){
                    //alert("The project is running, don't allow operation!");
                },
                417:function(){
                    //alert("Start the service failure!");
                },
                413:function(){
                    //alert("Start the service failure!");
                }
            }

        });
    },
    //getprogressbar(){
    //  //  $("#ajaxloading").show();
    //    var that = this;
    //    $.ajax({
    //        url: Comment.DATA_URL+"KDB/RunProjectTimeForFind",
    //        data: $.param({
    //            project_id: that.props.projectid,
    //        }),
    //        type:'GET',
    //        success:function(data){
    //        //    $("#ajaxloading").hide();
    //            var msg = data;
    //            if(!(1 == parseInt(msg.process_status))){
    //                that.setState({
    //                    timebar:parseInt(parseFloat(msg.data)*100),
    //                    ifbar:true,
    //                });
    //                if(parseFloat(msg.data) >= 1.00){
    //                    clearInterval(that.state.timebarId);
    //                }
    //
    //            }else{
    //                clearInterval(that.state.timebarId);
    //                //alert("server fail");
    //            }
    //        },
    //        error:function(){
    //            $("#ajaxloading").hide();
    //            clearInterval(that.state.timebarId);
    //            alert("get RunProjectTimeForFind fail");
    //        }
    //    });
    //},
    firstpageclick(){
        if(this.state.currentpage != 0){
            $("#ajaxloading").show();
            var that = this;
            $.ajax({
                url: Comment.DATA_URL+"KDB/ViewMetaInput",
                data: $.param({
                    projectid: that.props.projectid,
                    flag: that.state.ifods,
                    pagination:parseInt(1),
                }),
                type:'GET',
                success:function(data){
                    $("#ajaxloading").hide();
                    var msg = data;
                    that.setState({
                        currentpage:0
                    });
                    IfDispatcher.dispatch({
                        actionType:Comment.GETSOUREBDSODS,
                        sourcesdata:msg.data,
                    });
                },
                error:function(){
                    $("#ajaxloading").hide();
                   // alert("get ViewMetaInput fail");
                },
                statusCode:{
                    406:function (){
                        IfDispatcher.dispatch({
                            actionType:Comment.LOGOUT
                        });
                    },
                    415:function(){
                        //alert("The project is running, don't allow operation!");
                    },
                    417:function(){
                        //alert("Start the service failure!");
                    },
                    413:function(){
                        //alert("Start the service failure!");
                    }
                }
            });
        }

    },
    leftpageclick(){
        if(this.state.currentpage!=0){
            $("#ajaxloading").show();
            var that = this;
            $.ajax({
                url: Comment.DATA_URL+"KDB/ViewMetaInput",
                data: $.param({
                    projectid: that.props.projectid,
                    flag: that.state.ifods,
                    pagination:parseInt(that.state.currentpage),
                }),
                type:'GET',
                success:function(data){
                    $("#ajaxloading").hide();
                    var msg = data;
                    that.setState({
                        currentpage:that.state.currentpage-1
                    });
                    IfDispatcher.dispatch({
                        actionType:Comment.GETSOUREBDSODS,
                        sourcesdata:msg.data,
                    });
                },
                error:function(){
                    $("#ajaxloading").hide();
                  //  alert("get ViewMetaInput fail");
                },
                statusCode:{
                    406:function (){
                        IfDispatcher.dispatch({
                            actionType:Comment.LOGOUT
                        });
                    },
                    415:function(){
                        //alert("The project is running, don't allow operation!");
                    },
                    417:function(){
                        //alert("Start the service failure!");
                    },
                    413:function(){
                        //alert("Start the service failure!");
                    }
                }
            });
        }
    },
    pnumclick(num){
        var that = this;
        var infun = function(){
            if(that.state.currentpage!=num){
                $("#ajaxloading").show();
                $.ajax({
                    url: Comment.DATA_URL+"KDB/ViewMetaInput",
                    data: $.param({
                        projectid: that.props.projectid,
                        flag: that.state.ifods,
                        pagination:parseInt(num+1),
                    }),
                    type:'GET',
                    success:function(data){
                        $("#ajaxloading").hide();
                        var msg = data;
                        that.setState({
                            currentpage:num
                        });
                        IfDispatcher.dispatch({
                            actionType:Comment.GETSOUREBDSODS,
                            sourcesdata:msg.data,
                        });
                    },
                    error:function(){
                        $("#ajaxloading").hide();
                       // alert("get ViewMetaInput fail");
                    },
                    statusCode:{
                        406:function (){
                            IfDispatcher.dispatch({
                                actionType:Comment.LOGOUT
                            });
                        },
                        415:function(){
                            //alert("The project is running, don't allow operation!");
                        },
                        417:function(){
                            //alert("Start the service failure!");
                        },
                        413:function(){
                            //alert("Start the service failure!");
                        }
                    }
                });
            }
        };
        return infun;
    },
    rightpageclick(maxPageNum){
        var that = this;
        var infun = function () {
            if(that.state.currentpage!=maxPageNum-1 &&(maxPageNum != 0)){
                $("#ajaxloading").show();
                $.ajax({
                    url: Comment.DATA_URL+"KDB/ViewMetaInput",
                    data: $.param({
                        projectid: that.props.projectid,
                        flag: that.state.ifods,
                        pagination:parseInt(that.state.currentpage+2),
                    }),
                    type:'GET',
                    success:function(data){
                        $("#ajaxloading").hide();
                        var msg = data;
                        that.setState({
                            currentpage:that.state.currentpage+1
                        });
                        IfDispatcher.dispatch({
                            actionType:Comment.GETSOUREBDSODS,
                            sourcesdata:msg.data,
                        });
                    },
                    error:function(){
                        $("#ajaxloading").hide();
                       // alert("get ViewMetaInput fail");
                    },
                    statusCode:{
                        406:function (){
                            IfDispatcher.dispatch({
                                actionType:Comment.LOGOUT
                            });
                        },
                        415:function(){
                            //alert("The project is running, don't allow operation!");
                        },
                        417:function(){
                            //alert("Start the service failure!");
                        },
                        413:function(){
                            //alert("Start the service failure!");
                        }
                    }
                });
            }
        };
        return infun;
    },
    trailpageclick(maxpn){
        var that = this;
        var infun = function(){
            if(that.state.currentpage!=maxpn-1 &&(maxpn != 0)){
                $("#ajaxloading").show();
                $.ajax({
                    url: Comment.DATA_URL+"KDB/ViewMetaInput",
                    data: $.param({
                        projectid: that.props.projectid,
                        flag: that.state.ifods,
                        pagination:parseInt(maxpn),
                    }),
                    type:'GET',
                    success:function(data){
                        $("#ajaxloading").hide();
                        var msg = data;
                        that.setState({
                            currentpage:maxpn-1
                        });
                        IfDispatcher.dispatch({
                            actionType:Comment.GETSOUREBDSODS,
                            sourcesdata:msg.data,
                        });
                    },
                    error:function(){
                        $("#ajaxloading").hide();
                      //  alert("get ViewMetaInput fail");
                    },
                    statusCode:{
                        406:function (){
                            IfDispatcher.dispatch({
                                actionType:Comment.LOGOUT
                            });
                        },
                        415:function(){
                            //alert("The project is running, don't allow operation!");
                        },
                        417:function(){
                            //alert("Start the service failure!");
                        },
                        413:function(){
                            //alert("Start the service failure!");
                        }
                    }
                });
            }
        };
        return infun;
    },
    exportcsv(){
        $("#ajaxloading").show();
        var that = this;
        $.ajax({
            url: Comment.DATA_URL+"KDB/DownloadOutPutResult",
            data: $.param({
                project_id:that.props.projectid,
                flag:that.state.ifodss
            }),
            contentType: 'application/x-www-form-urlencoded',
            type: 'GET',
            success: function (data) {
                $("#ajaxloading").hide();
                var msg = data;
                that.setState({
                    ifexport:true,
                    exporturl:msg.url,
                });
            },
            error:function(jxr,scode){
                $("#ajaxloading").hide();
               // alert("DownloadOutPutResult fail!");
            },
            statusCode:{
                406:function (){
                    IfDispatcher.dispatch({
                        actionType:Comment.LOGOUT
                    });
                },
                409:function(){

                }
            }
        });
    },
    firstpageclicks(){
        if(this.state.currentpages != 0){
            $("#ajaxloading").show();
            var that = this;
            $.ajax({
                url: Comment.DATA_URL+"KDB/ViewResultByPagination",
                data: $.param({
                    projectid: that.props.projectid,
                    pagination:parseInt(1),
                    flag:parseInt(that.state.ifodss),
                }),
                type:'GET',
                success:function(data){
                    $("#ajaxloading").hide();
                    var msg = data;
                    that.setState({
                        currentpages:0,
                        resultdata:msg.data,
                    });
                },
                error:function(){
                    $("#ajaxloading").hide();
                   // alert("get ViewResultByPagination fail");
                },
                statusCode:{
                    406:function (){
                        IfDispatcher.dispatch({
                            actionType:Comment.LOGOUT
                        });
                    },
                    409:function(){

                    }
                }
            });
        }
    },
    leftpageclicks(){
        if(this.state.currentpages!=0){
            $("#ajaxloading").show();
            var that = this;
            $.ajax({
                url: Comment.DATA_URL+"KDB/ViewResultByPagination",
                data: $.param({
                    projectid: that.props.projectid,
                    pagination:parseInt(that.state.currentpages),
                    flag:parseInt(that.state.ifodss),
                }),
                type:'GET',
                success:function(data){
                    $("#ajaxloading").hide();
                    var msg = data;
                    that.setState({
                        currentpages:that.state.currentpages-1,
                        resultdata:msg.data,
                    });
                },
                error:function(){
                    $("#ajaxloading").hide();
                  //  alert("get ViewResultByPagination fail");
                },
                statusCode:{
                    406:function (){
                        IfDispatcher.dispatch({
                            actionType:Comment.LOGOUT
                        });
                    },
                    409:function(){

                    }
                }
            });
        }
    },
    pnumclicks(num){
        var that = this;
        var infun = function(){
            if(that.state.currentpages!=num){
                $("#ajaxloading").show();
                $.ajax({
                    url: Comment.DATA_URL+"KDB/ViewResultByPagination",
                    data: $.param({
                        projectid: that.props.projectid,
                        pagination:parseInt(num+1),
                        flag:parseInt(that.state.ifodss),
                    }),
                    type:'GET',
                    success:function(data){
                        $("#ajaxloading").hide();
                        var msg = data;
                        that.setState({
                            currentpages:num,
                            resultdata:msg.data,
                        });
                    },
                    error:function(){
                        $("#ajaxloading").hide();
                      //  alert("get ViewResultByPagination fail");
                    },
                    statusCode:{
                        406:function (){
                            IfDispatcher.dispatch({
                                actionType:Comment.LOGOUT
                            });
                        },
                        409:function(){

                        }
                    }
                });
            }
        };
        return infun;
    },
    rightpageclicks(maxPageNum){
        var that = this;
        var infun = function () {
            if(that.state.currentpages!=maxPageNum-1 && (maxPageNum != 0)){
                $("#ajaxloading").show();
                $.ajax({
                    url: Comment.DATA_URL+"KDB/ViewResultByPagination",
                    data: $.param({
                        projectid: that.props.projectid,
                        pagination:parseInt(that.state.currentpages+2),
                        flag:parseInt(that.state.ifodss),
                    }),
                    type:'GET',
                    success:function(data){
                        $("#ajaxloading").hide();
                        var msg = data;
                        that.setState({
                            currentpages:that.state.currentpages+1,
                            resultdata:msg.data,
                        });
                    },
                    error:function(){
                        $("#ajaxloading").hide();
                     //   alert("get ViewResultByPagination fail");
                    },
                    statusCode:{
                        406:function (){
                            IfDispatcher.dispatch({
                                actionType:Comment.LOGOUT
                            });
                        },
                        409:function(){

                        }
                    }
                });
            }
        };
        return infun;
    },
    trailpageclicks(maxpn){
        var that = this;
        var infun = function(){
            if(that.state.currentpages!=maxpn-1 &&(maxpn != 0)){
                $("#ajaxloading").show();
                $.ajax({
                    url: Comment.DATA_URL+"KDB/ViewResultByPagination",
                    data: $.param({
                        projectid: that.props.projectid,
                        pagination:parseInt(maxpn),
                        flag:parseInt(that.state.ifodss),
                    }),
                    type:'GET',
                    success:function(data){
                        $("#ajaxloading").hide();
                        var msg = data;
                        that.setState({
                            currentpages:maxpn-1,
                            resultdata:msg.data,
                        });
                    },
                    error:function(){
                        $("#ajaxloading").hide();
                      //  alert("get ViewResultByPagination fail");
                    },
                    statusCode:{
                        406:function (){
                            IfDispatcher.dispatch({
                                actionType:Comment.LOGOUT
                            });
                        },
                        409:function(){

                        }
                    }
                });
            }
        };
        return infun;
    },
    getMetadatasoureclick(num){
        var that = this;
        var inFunc = function(){
            if(num == 0){
                that.setState({
                    ifods:num,
                    ifmeta:true,
                    ifdds:2,
                    selectSourceItem:2,
                    metadatatext:null,
                    metadatas:[],
                    sourcesdata:[],
                    setmethod:"LOCAL",
                    Uploadpath:null,
                    Uploadpass:null,
                    Uploaduser:null,
                    Uploadhost:null,
                    Uploadport:null,
                });
            }else{
                that.setState({
                    ifods:num,
                    ifmeta:true,
                    ifdds:2,
                    selectSourceItem:3,
                    metadatatext:null,
                    metadatas:[],
                    sourcesdata:[],
                    setmethod:"LOCAL",
                    Uploadpath:null,
                    Uploadpass:null,
                    Uploaduser:null,
                    Uploadhost:null,
                    Uploadport:null,
                });
            }
            $("#ajaxloading").show();
            $.ajax({
                url: Comment.DATA_URL+"KDB/GetTableListForMetaInput",
                data: $.param({
                    projectid: that.props.projectid,
                    flag: num,
                }),
                type:'GET',
                success:function(data){
                    $("#ajaxloading").hide();
                    var msg = data;
                    IfDispatcher.dispatch({
                        actionType:Comment.GETMETADATA,
                        metadatas:msg.data,
                    });
                },
                error:function(){
                    $("#ajaxloading").hide();
                 //   alert("get GetTableListForMetaInput fail");
                },
                statusCode:{
                    406:function (){
                        IfDispatcher.dispatch({
                            actionType:Comment.LOGOUT
                        });
                    },
                    409:function(){

                    }
                }
            });
        }
        return inFunc;
    },
    importMetadataClick(){
        $("#importmetadata").modal("show");
    },
    importmdClick(){
        if(this.state.setmethod == "LOCAL"){
           if(this.state.Uploadpath == null){
               this.setState({ifuploaderror:true});
           }else{
               $("#ajaxloading").show();
               $("#importmetadata").modal("hide");
               var that = this;
               var userlist = {
                   upload_method:that.state.setmethod,
                   project_id:that.props.projectid,
                   upload_path:that.state.Uploadpath,
                   upload_ip:"",
                   upload_port:null,
                   host_username:"",
                   host_password:"",
                   ifbdsods:that.state.ifods,
               }
               $.ajax({
                   url: Comment.DATA_URL+"KDB/CollectMetaInputByParameters",
                   data:JSON.stringify(userlist),
                   type: 'POST',
                   dataType:"json",
                   contentType: 'application/json;charset=utf-8',
                   success: function(data){
                       var msg = data;
                       if(msg.data == "success"){
                           $.ajax({
                               url: Comment.DATA_URL+"KDB/GetTableListForMetaInput",
                               data: $.param({
                                   projectid: that.props.projectid,
                                   flag: that.state.ifods,
                               }),
                               type:'GET',
                               success:function(data){
                                   $("#ajaxloading").hide();
                                   var msg = data;
                                   IfDispatcher.dispatch({
                                       actionType:Comment.GETMETADATA,
                                       metadatas:msg.data,
                                   });
                               },
                               error:function(){
                                   $("#ajaxloading").hide();
                                 //  alert("get GetTableListForMetaInput fail");
                               },
                               statusCode:{
                                   406:function (){
                                       IfDispatcher.dispatch({
                                           actionType:Comment.LOGOUT
                                       });
                                   },
                                   409:function(){

                                   }
                               }
                           });
                       }else{
                           $("#ajaxloading").hide();
                          alert(msg.data);
                       }

                   },
                   error:function(jxr,scode){
                      // alert("import metadata fail!");
                       $("#ajaxloading").hide();
                   },
                   statusCode:{
                       414:function(){
                           alert("import metadata fail!");
                       },
                       406:function (){
                           IfDispatcher.dispatch({
                               actionType:Comment.LOGOUT
                           });
                       },
                   }
               });
           }
        }else if(this.state.setmethod == "FTP" || this.state.setmethod == "SFTP"){
            if(!this.state.Uploadpath||!this.state.Uploadhost||!this.state.Uploaduser||!this.state.Uploadpass||this.state.Uploadport==0){
                this.setState({ifuploaderror:true});
            }else{
                $("#ajaxloading").show();
                $("#importmetadata").modal("hide");
                var that = this;
                var userlist = {
                    upload_method:that.state.setmethod,
                    project_id:that.props.projectid,
                    upload_path:that.state.Uploadpath,
                    upload_ip:that.state.Uploadhost,
                    upload_port:that.state.Uploadport,
                    host_username:that.state.Uploaduser,
                    host_password:that.state.Uploadpass,
                    ifbdsods:that.state.ifods,
                }
                $.ajax({
                    url: Comment.DATA_URL+"KDB/CollectMetaInputByParameters",
                    data:JSON.stringify(userlist),
                    type: 'POST',
                    dataType:"json",
                    contentType: 'application/json;charset=utf-8',
                    success: function(data){
                        var msg = data;
                        if(msg.data == "success"){
                            $.ajax({
                                url: Comment.DATA_URL+"KDB/GetTableListForMetaInput",
                                data: $.param({
                                    projectid: that.props.projectid,
                                    flag: that.state.ifods,
                                }),
                                type:'GET',
                                success:function(data){
                                    $("#ajaxloading").hide();
                                    var msg = data;
                                    IfDispatcher.dispatch({
                                        actionType:Comment.GETMETADATA,
                                        metadatas:msg.data,
                                    });
                                },
                                error:function(){
                                    $("#ajaxloading").hide();
                                   // alert("get GetTableListForMetaInput fail");
                                },
                                statusCode:{
                                    406:function (){
                                        IfDispatcher.dispatch({
                                            actionType:Comment.LOGOUT
                                        });
                                    },
                                    409:function(){

                                    }
                                }
                            });
                        }else{
                            $("#ajaxloading").hide();
                            alert(msg.data);
                        }

                    },
                    error:function(jxr,scode){
                       // alert("import metadata fail!");
                        $("#ajaxloading").hide();
                    },
                    statusCode:{
                        414:function(){
                            alert(" CollectMetaInputByParameters fail!");
                        },
                        406:function (){
                            IfDispatcher.dispatch({
                                actionType:Comment.LOGOUT
                            });
                        },
                    }
                });
            }

        }

    },
    ViewMetadataClick(enname){
        var that = this;
        var inFunc = function(){
            var namene = enname;
            $("#ajaxloading").show();
            $.ajax({
                url: Comment.DATA_URL+"KDB/ShowTablesForMetaInput",
                data: $.param({
                    projectid:that.props.projectid,
                    flag:that.state.ifods,
                    tablename:namene,
                }),
                type:'GET',
                success:function(data){
                    $("#ajaxloading").hide();
                    var msg = data;
                    IfDispatcher.dispatch({
                        actionType:Comment.GETTABLESMETADATA,
                        tablesmetadata:msg.data,
                        columnname:msg.columnname,
                    });
                    $("#viewmetadata").modal("show");
                },
                error:function(){
                    $("#ajaxloading").hide();
                   // alert("get ShowTablesForMetaInput fail");
                },
                statusCode:{
                    414:function(){
                     //   alert(" CollectMetaInputByParameters fail!");
                    },
                    406:function (){
                        IfDispatcher.dispatch({
                            actionType:Comment.LOGOUT
                        });
                    },
                }

            });
        }
        return inFunc;
    },
    setmethodChange(event){
        this.setState({
            setmethod:event.target.value,
        })
    },
    inputCoverName(event){
        this.setState({
            covername:event.target.value,
        });
    },
    ToTamrCancel(){
        this.setState({
            covername:null,
            ifbad:true,
        });
        $("#totamrresultdata").modal("hide");
    },
    ToTamrResultData(){
        var zzlifcover = 1;
        var filename = null;
        var zzl  = true;
        if($("#checkcovered").get(0).checked){//cover
            zzlifcover=0;
            filename = this.state.newcoverfile;
            if(!filename){
                zzl = false;
                alert("Please choose to override file");
            }
        }else{
            zzlifcover=1;
            filename = this.state.covername;
            if(!filename){
                zzl = false;
            }
        }
        if(zzl){
            $("#totamrresultdata").modal("hide");
            $("#ajaxloading").show();
            var that = this;
            $.ajax({
                url: Comment.DATA_URL+"KDB/SendResultsDataToTamrAPI",
                data: $.param({
                    projectid :that.props.projectid,
                    flag:that.state.ifodss,
                    authtoken:that.props.loginInfo.uuid,
                    filename:filename,
                    ifcover:zzlifcover,
                }),
                contentType: 'application/x-www-form-urlencoded',
                type: 'POST',
                success: function (data) {
                    $("#ajaxloading").hide();
                    var msg = data;
                    if(msg.status =="200"){
                        that.setState({
                            ifbad:true,
                        });
                    }else{
                        that.setState({
                            ifbad:true,
                        });
                        alert("SendResultsDataToTamrAPI fail.");
                    }
                },
                error:function(jxr,scode){
                    $("#ajaxloading").hide();
                    that.setState({
                        ifbad:true,
                    });
                    // alert("SendResultsDataToTamrAPI fail.");
                },
                statusCode:{
                    414:function(){
                        //  alert(" CollectMetaInputByParameters fail!");
                    },
                    406:function (){
                        IfDispatcher.dispatch({
                            actionType:Comment.LOGOUT
                        });
                    },
                }
            });
        }

    },
    ToTamrfeaturedataclick(){
        var that = this;
        $.ajax({
            url: Comment.DATA_URL+"KDB/GetToTamrFileName",
            data: $.param({
                projectid :that.props.projectid,
                flag:that.state.ifodss,
            }),
            contentType: 'application/x-www-form-urlencoded',
            type: 'GET',
            success: function (data) {
                var msg = data;
                if(msg.status =="200"){
                    that.setState({
                        covername:msg.data,
                        ifbad:false,
                        covernamelist:[],
                        ckockselects:[],
                        newcoverfile:null,
                    });
                    $("#checkcovered").prop("checked",false);
                    $("#totamrresultdata").modal("show");
                }else{
                    that.setState({
                        ifbad:true,
                    });
                    alert("GetToTamrFileName fail.");
                }
            },
            error:function(jxr,scode){
                that.setState({
                    ifbad:true,
                });
            },
            statusCode:{
                414:function(){
                },
                406:function (){
                    IfDispatcher.dispatch({
                        actionType:Comment.LOGOUT
                    });
                },
            }
        });
    },
    tablecontclick(tableid){
        //var that = this;
        var inFunc = function(){
            var tableCont = document.querySelector("#"+tableid)
            /**
             * scroll handle
             * @param {event} e -- scroll event
             */
            function scrollHandle (e){
                var scrollTop = this.scrollTop;
                this.querySelector('thead').style.transform = 'translateY(' + scrollTop + 'px)';
            }
            tableCont.addEventListener('scroll',scrollHandle)
        }
        return  inFunc;

    },
//    tablecellChange(){
//        var tTD; //用来存储当前更改宽度的Table Cell,避免快速移动鼠标的问题
//        var table = document.getElementById("tablecells");
//        for (var j = 0; j < table.rows[0].cells.length; j++) {
//            table.rows[0].cells[j].onmousedown = function () {
////记录单元格
//                tTD = this;
//                if (event.offsetX > tTD.offsetWidth - 10) {
//                    tTD.mouseDown = true;
//                    tTD.oldX = event.x;
//                    tTD.oldWidth = tTD.offsetWidth;
//                }
////记录Table宽度
////table = tTD; while (table.tagName != ‘TABLE') table = table.parentElement;
////tTD.tableWidth = table.offsetWidth;
//            };
//            table.rows[0].cells[j].onmouseup = function () {
////结束宽度调整
//                if (tTD == undefined) tTD = this;
//                tTD.mouseDown = false;
//                tTD.style.cursor = 'default';
//            };
//            table.rows[0].cells[j].onmousemove = function () {
////更改鼠标样式
//                if (event.offsetX > this.offsetWidth - 10)
//                    this.style.cursor = 'col-resize';
//                else
//                    this.style.cursor = 'default';
////取出暂存的Table Cell
//                if (tTD == undefined) tTD = this;
////调整宽度
//                if (tTD.mouseDown != null && tTD.mouseDown == true) {
//                    tTD.style.cursor = 'default';
//                    if (tTD.oldWidth + (event.x - tTD.oldX)>0)
//                        tTD.width = tTD.oldWidth + (event.x - tTD.oldX);
////调整列宽
//                    tTD.style.width = tTD.width;
//                    tTD.style.cursor = 'col-resize';
////调整该列中的每个Cell
//                    table = tTD; while (table.tagName != 'TABLE') table = table.parentElement;
//                    for (var i = 0; i < table.rows.length; i++) {
//                        table.rows[i].cells[tTD.cellIndex].width = tTD.width;
//                    }
////调整整个表
////table.width = tTD.tableWidth + (tTD.offsetWidth – tTD.oldWidth);
////table.style.width = table.width;
//                }
//            };
//        }
//    },
    metadataChange(event){
        this.setState({
            metadatatext:event.target.value,
        })
    },
    firstpageclickch(){
        if(this.state.currentpagech != 0){
            $("#ajaxloading").show();
            var that = this;
            $.ajax({
                url: Comment.DATA_URL+"KDB/ViewResultByPagination",
                data: $.param({
                    projectid: that.props.projectid,
                    pagination:parseInt(1),
                    flag:parseInt(that.state.ifodss),
                    param:that.state.metadatatext,
                }),
                type:'GET',
                success:function(data){
                    $("#ajaxloading").hide();
                    var msg = data;
                    that.setState({
                        currentpagech:0,
                        searchdata:msg.data,
                    });
                },
                error:function(){
                    $("#ajaxloading").hide();
                   // alert("get ViewResultByPagination fail");
                },
                statusCode:{
                    406:function (){
                        IfDispatcher.dispatch({
                            actionType:Comment.LOGOUT
                        });
                    },
                }
            });
        }
    },
    leftpageclickch(){
        if(this.state.currentpagech!=0){
            $("#ajaxloading").show();
            var that = this;
            $.ajax({
                url: Comment.DATA_URL+"KDB/ViewResultByPagination",
                data: $.param({
                    projectid: that.props.projectid,
                    pagination:parseInt(that.state.currentpagech),
                    flag:parseInt(that.state.ifodss),
                    param:that.state.metadatatext,
                }),
                type:'GET',
                success:function(data){
                    $("#ajaxloading").hide();
                    var msg = data;
                    that.setState({
                        currentpagech:that.state.currentpagech-1,
                        searchdata:msg.data,
                    });
                },
                error:function(){
                    $("#ajaxloading").hide();
                  //  alert("get ViewResultByPagination fail");
                },
                statusCode:{
                    406:function (){
                        IfDispatcher.dispatch({
                            actionType:Comment.LOGOUT
                        });
                    },
                }
            });
        }
    },
    pnumclickch(num){
        var that = this;
        var infun = function(){
            if(that.state.currentpagech!=num){
                $("#ajaxloading").show();
                $.ajax({
                    url: Comment.DATA_URL+"KDB/ViewResultByPagination",
                    data: $.param({
                        projectid: that.props.projectid,
                        pagination:parseInt(num+1),
                        flag:parseInt(that.state.ifodss),
                        param:that.state.metadatatext,
                    }),
                    type:'GET',
                    success:function(data){
                        $("#ajaxloading").hide();
                        var msg = data;
                        that.setState({
                            currentpagech:num,
                            searchdata:msg.data,
                        });
                    },
                    error:function(){
                        $("#ajaxloading").hide();
                      //  alert("get ViewResultByPagination fail");
                    },
                    statusCode:{
                        406:function (){
                            IfDispatcher.dispatch({
                                actionType:Comment.LOGOUT
                            });
                        },
                    }
                });
            }
        };
        return infun;
    },
    rightpageclickch(maxPageNum){
        var that = this;
        var infun = function () {
            if(that.state.currentpagech!=maxPageNum-1 && (maxPageNum != 0)){
                $("#ajaxloading").show();
                $.ajax({
                    url: Comment.DATA_URL+"KDB/ViewResultByPagination",
                    data: $.param({
                        projectid: that.props.projectid,
                        pagination:parseInt(that.state.currentpagech+2),
                        flag:parseInt(that.state.ifodss),
                        param:that.state.metadatatext,
                    }),
                    type:'GET',
                    success:function(data){
                        $("#ajaxloading").hide();
                        var msg = data;
                        that.setState({
                            currentpagech:that.state.currentpagech+1,
                            searchdata:msg.data,
                        });
                    },
                    error:function(){
                        $("#ajaxloading").hide();
                      //  alert("get ViewResultByPagination fail");
                    },
                    statusCode:{
                        406:function (){
                            IfDispatcher.dispatch({
                                actionType:Comment.LOGOUT
                            });
                        },
                    }
                });
            }
        };
        return infun;
    },
    trailpageclickch(maxpn){
        var that = this;
        var infun = function(){
            if(that.state.currentpagech!=maxpn-1 &&(maxpn != 0)){
                $("#ajaxloading").show();
                $.ajax({
                    url: Comment.DATA_URL+"KDB/ViewResultByPagination",
                    data: $.param({
                        projectid: that.props.projectid,
                        pagination:parseInt(maxpn),
                        flag:parseInt(that.state.ifodss),
                        param:that.state.metadatatext,
                    }),
                    type:'GET',
                    success:function(data){
                        $("#ajaxloading").hide();
                        var msg = data;
                        that.setState({
                            currentpagech:maxpn-1,
                            searchdata:msg.data,
                        });
                    },
                    error:function(){
                        $("#ajaxloading").hide();
                      //  alert("get ViewResultByPagination fail");
                    },
                    statusCode:{
                        406:function (){
                            IfDispatcher.dispatch({
                                actionType:Comment.LOGOUT
                            });
                        },
                    }
                });
            }
        };
        return infun;
    },
    firstpageclickbl(){
        if(this.state.currentpagebl != 0){
            $("#ajaxloading").show();
            var that = this;
            $.ajax({
                url: Comment.DATA_URL+"KDB/ViewMetaInput",
                data: $.param({
                    projectid: that.props.projectid,
                    pagination:parseInt(1),
                    flag:parseInt(that.state.ifods),
                    param:that.state.metadatatext,
                }),
                type:'GET',
                success:function(data){
                    $("#ajaxloading").hide();
                    var msg = data;
                    that.setState({
                        currentpagebl:0,
                        searchtabledata:msg.data,
                    });
                },
                error:function(){
                    $("#ajaxloading").hide();
                  //  alert("get ViewResultByPagination fail");
                },
                statusCode:{
                    406:function (){
                        IfDispatcher.dispatch({
                            actionType:Comment.LOGOUT
                        });
                    },
                }
            });
        }
    },
    leftpageclickbl(){
        if(this.state.currentpagebl!=0){
            $("#ajaxloading").show();
            var that = this;
            $.ajax({
                url: Comment.DATA_URL+"KDB/ViewMetaInput",
                data: $.param({
                    projectid: that.props.projectid,
                    pagination:parseInt(that.state.currentpagebl),
                    flag:parseInt(that.state.ifods),
                    param:that.state.metadatatext,
                }),
                type:'GET',
                success:function(data){
                    $("#ajaxloading").hide();
                    var msg = data;
                    that.setState({
                        currentpagebl:that.state.currentpagebl-1,
                        searchtabledata:msg.data,
                    });
                },
                error:function(){
                    $("#ajaxloading").hide();
                  //  alert("get ViewResultByPagination fail");
                },
                statusCode:{
                    406:function (){
                        IfDispatcher.dispatch({
                            actionType:Comment.LOGOUT
                        });
                    },
                }
            });
        }
    },
    pnumclickbl(num){
        var that = this;
        var infun = function(){
            if(that.state.currentpagebl!=num){
                $("#ajaxloading").show();
                $.ajax({
                    url: Comment.DATA_URL+"KDB/ViewMetaInput",
                    data: $.param({
                        projectid: that.props.projectid,
                        pagination:parseInt(num+1),
                        flag:parseInt(that.state.ifods),
                        param:that.state.metadatatext,
                    }),
                    type:'GET',
                    success:function(data){
                        $("#ajaxloading").hide();
                        var msg = data;
                        that.setState({
                            currentpagebl:num,
                            searchtabledata:msg.data,
                        });
                    },
                    error:function(){
                        $("#ajaxloading").hide();
                      //  alert("get ViewResultByPagination fail");
                    },
                    statusCode:{
                        406:function (){
                            IfDispatcher.dispatch({
                                actionType:Comment.LOGOUT
                            });
                        },
                    }
                });
            }
        };
        return infun;
    },
    rightpageclickbl(maxPageNum){
        var that = this;
        var infun = function () {
            if(that.state.currentpagebl!=maxPageNum-1 && (maxPageNum != 0)){
                $("#ajaxloading").show();
                $.ajax({
                    url: Comment.DATA_URL+"KDB/ViewMetaInput",
                    data: $.param({
                        projectid: that.props.projectid,
                        pagination:parseInt(that.state.currentpagebl+2),
                        flag:parseInt(that.state.ifods),
                        param:that.state.metadatatext,
                    }),
                    type:'GET',
                    success:function(data){
                        $("#ajaxloading").hide();
                        var msg = data;
                        that.setState({
                            currentpagebl:that.state.currentpagebl+1,
                            searchtabledata:msg.data,
                        });
                    },
                    error:function(){
                        $("#ajaxloading").hide();
                      //  alert("get ViewResultByPagination fail");
                    },
                    statusCode:{
                        406:function (){
                            IfDispatcher.dispatch({
                                actionType:Comment.LOGOUT
                            });
                        },
                    }
                });
            }
        };
        return infun;
    },
    trailpageclickbl(maxpn){
        var that = this;
        var infun = function(){
            if(that.state.currentpagebl!=maxpn-1 &&(maxpn != 0)){
                $("#ajaxloading").show();
                $.ajax({
                    url: Comment.DATA_URL+"KDB/ViewMetaInput",
                    data: $.param({
                        projectid: that.props.projectid,
                        pagination:parseInt(maxpn),
                        flag:parseInt(that.state.ifods),
                        param:that.state.metadatatext,
                    }),
                    type:'GET',
                    success:function(data){
                        $("#ajaxloading").hide();
                        var msg = data;
                        that.setState({
                            currentpagebl:maxpn-1,
                            searchtabledata:msg.data,
                        });
                    },
                    error:function(){
                        $("#ajaxloading").hide();
                     //   alert("get ViewResultByPagination fail");
                    },
                    statusCode:{
                        406:function (){
                            IfDispatcher.dispatch({
                                actionType:Comment.LOGOUT
                            });
                        },
                    }
                });
            }
        };
        return infun;
    },
    render(){
        var pageHeight = this.props.propsheight;
        var propswidth = this.props.propswidth;
        var loginInfo = this.props.loginInfo;
        var projectname= this.props.projectname;
        var selectStylel=[];
        var dlist = [];
        var mlist = [];
        var zklist = [];
        var qlist = [];
        var jlist=[];

        var selectFedefStylel=[];
        var fdeflist=[];
        var fdetlsit=[];
        var sslist=[];
        var pageButtonGrp = [];
        var pageButtonGrpbl=[];
        var zzlslistbl = [];
        var ylist = [];
        //var setEncodingSelect = [ <option value="UTF-8">
        //    UTF-8
        //</option>];
        //var setLanguageSelect =[ <option value="Chinese">
        //    Chinese
        //</option>];
        //var setSeparateSelect = [ <option value="|">
        //    |
        //</option>];
        var setmethodSelect = [ <option value="LOCAL">
            LOCAL
        </option>];
        var zzllist = [];
        var zzlslist = [];
        var elink = "";
        var pageButtonGrps = [];
        var pageButtonGrpch =[];
        var sadminDisableStyle = "";
        var sadminDisablesss ="";
        var dictadminDisable ="";
        if(loginInfo.ifadmin){
            dictadminDisable = "disabled";
        }
        var ifbaddisableStyle = "disabled";
        var iftotamrdisableStyle = "disabled";
        var ifmetaStyle = {display: "none"};
        var ifmetaStylel = {};
        var barStyle = {display:"none"};
        var timebar = 0;
        var metalist = [];
        var viewmeta = [];

        var selectParticiStylel = [];
        var pleflist = [];
        var pletlsit = [];
        var pleplist = [];
        if(!this.state.ifmeta)
        {
            ifmetaStylel={};
            ifmetaStyle={display: "none"};
        }else{
            ifmetaStylel = {display: "none"};
            ifmetaStyle = {};
        }
        var ifLocalFtpStyle = {display: "none"};
        if(this.state.setmethod != "LOCAL"){
            ifLocalFtpStyle = {};
        };
        var ifemptystyle = {display: "none"};
        if(this.state.ifuploaderror){
            ifemptystyle = {};
        }
        var fileupdateStyle = {display: "none"};
        if(this.state.ifselectfiles){
            fileupdateStyle = {};
        }
        var ifseterStyle = {display: "none"};
        if(this.state.ifseter){
            ifseterStyle = {};
        }
        var ifseterddStyle = {display: "none"};
        if(this.state.ifseterdd){
            ifseterddStyle = {};
        }
        var ifseterssStyle = {display: "none"};
        if(this.state.ifseterss){
            ifseterssStyle = {};
        }
        var errormsgStyle = {display: "none"};
        if(this.state.errormsg){
            errormsgStyle = {};
        }
        var savasuccessStyle={display:"none"};
        if(this.state.savasuccess){
            savasuccessStyle ={};
        }
        var zzlselectsStyle = {display: "none"};
        if(this.state.fileNum != 0){
            zzlselectsStyle = {};
        }
        if(this.state.ifdds == 2){
            //source
            if(!this.state.ifmeta){
                var datacounts = this.state.datacounts;
                var maxPageNum = Math.ceil(datacounts/this.state.pagesize);
                if(loginInfo.ifadmin){
                    sadminDisableStyle = "disabled";
                    sadminDisablesss = "disabled";
                }else{
                    if(this.state.ifrunnig){
                        if(typeof(this.props.ifrunbarmsg.projectid) == "undefined" ){
                            sadminDisableStyle = "";
                            sadminDisablesss = "";
                        }else{
                            if(this.props.ifrunbarmsg.ifrun){
                                sadminDisableStyle = "disabled";
                                sadminDisablesss = "disabled";
                            }else{
                                sadminDisableStyle = "";
                                sadminDisablesss = "";
                            }
                        }
                    }else{
                        sadminDisableStyle = "disabled";
                    }
                }

                if(typeof(this.props.ifrunbarmsg.projectid) != "undefined"){
                    if(this.props.ifrunbarmsg.projectid == this.props.projectid){
                        if(this.props.ifrunbarmsg.ifbar){
                            barStyle =  {display:""};
                            timebar = this.props.ifrunbarmsg.timebar;
                        }else{
                            barStyle =  {display:"none"};
                        }
                    }else{
                        barStyle =  {display:"none"};
                    }
                }else{
                    barStyle =  {display:"none"};
                }

                var leftButtonClass = "";
                if(this.state.currentpage == 0){
                    leftButtonClass = "disabled";
                }
                var rightButtonClass = "";
                if((this.state.currentpage == maxPageNum-1)||(maxPageNum == 0)){
                    rightButtonClass = "disabled";
                }
                pageButtonGrp.push(<li className={leftButtonClass}>
                    <a
                        onClick={this.firstpageclick}
                        style={{color:"#000000"}}>
                        <span>First</span>
                    </a>
                </li>);

                if(maxPageNum <= this.state.pagenum){
                    pageButtonGrp.push(<li className={leftButtonClass}>
                        <a
                            onClick={this.leftpageclick}
                            >
                            <span style={{color:"#000000"}}>Previous</span>
                        </a>
                    </li>);

                    for(var pn = 0;pn<=maxPageNum-1;pn++){
                        var currentPageButtonCls = "";
                        if(this.state.currentpage == pn){
                            currentPageButtonCls = "active";
                        }
                        pageButtonGrp.push(<li className={currentPageButtonCls}>
                            <a
                                onClick={this.pnumclick(pn)}
                                >
                                {parseInt(pn)+1}

                            </a>
                        </li>);

                    }
                    pageButtonGrp.push(<li className={rightButtonClass}>
                        <a
                            onClick={this.rightpageclick(maxPageNum)}
                            >
                            <span style={{color:"#000000"}}>Next</span>
                        </a>
                    </li>);
                }else{
                    if(this.state.currentpage < maxPageNum-7){
                        pageButtonGrp.push(<li className={leftButtonClass}>
                            <a
                                onClick={this.leftpageclick}
                                >
                                <span style={{color:"#000000"}}>Previous</span>
                            </a>
                        </li>);
                        for(var pn = this.state.currentpage;pn<=this.state.currentpage+2;pn++){
                            var currentPageButtonCls = "";
                            if(this.state.currentpage == pn){
                                currentPageButtonCls = "active";
                            }
                            pageButtonGrp.push(<li className={currentPageButtonCls}>
                                <a
                                    onClick={this.pnumclick(pn)}

                                    >
                                    {parseInt(pn)+1}

                                </a>
                            </li>);
                        }
                        pageButtonGrp.push(<li className={"disabled"}>
                            <a>
                                {"..."}

                            </a>
                        </li>);
                        for(var pn =maxPageNum-3;pn<=maxPageNum-1;pn++ ){
                            var currentPageButtonCls = "";
                            if(this.state.currentpage == pn){
                                currentPageButtonCls = "active";
                            }
                            pageButtonGrp.push(<li className={currentPageButtonCls}>
                                <a
                                    onClick={this.pnumclick(pn)}
                                    >
                                    {parseInt(pn)+1}

                                </a>
                            </li>);
                        }
                        pageButtonGrp.push(<li className={rightButtonClass}>
                            <a
                                onClick={this.rightpageclick(maxPageNum)}
                                >
                                <span style={{color:"#000000"}}>Next</span>
                            </a>
                        </li>);
                    }else{
                        pageButtonGrp.push(<li className={leftButtonClass}>
                            <a
                                onClick={this.leftpageclick}
                                >
                                <span style={{color:"#000000"}}>Previous</span>
                            </a>
                        </li>);
                        for(var pn = maxPageNum-7;pn<=maxPageNum-1;pn++){
                            var currentPageButtonCls = "";
                            if(this.state.currentpage == pn){
                                currentPageButtonCls = "active";
                            }
                            pageButtonGrp.push(<li className={currentPageButtonCls}>
                                <a
                                    onClick={this.pnumclick(pn)}
                                    >
                                    {parseInt(pn)+1}

                                </a>
                            </li>);
                        }
                        pageButtonGrp.push(<li className={rightButtonClass}>
                            <a
                                onClick={this.rightpageclick(maxPageNum)}
                                >
                                <span style={{color:"#000000"}}>Next</span>
                            </a>
                        </li>);
                    }
                }

                pageButtonGrp.push(<li className={rightButtonClass}>
                    <a
                        onClick={this.trailpageclick(maxPageNum)}
                        >
                        <span style={{color:"#000000"}}>Last</span>
                    </a>
                </li>);
                var sourcedatas = this.props.sourcesdata;
                if(sourcedatas.length >0){
                    var zlist = [];
                    for(var r in sourcedatas){
                        var mli = sourcedatas[r];
                        zlist.push(
                            <tr >
                                <td style={{width:"40px",color:"#379cf8"}}>{parseInt(r)+1}</td>
                                <td style={{color:"#379cf8"}}>{mli.la_col_name}</td>
                                <td style={{color:"#379cf8"}}>{mli.la_table_name}</td>
                                <td style={{color:"#379cf8"}}>{mli.en_table_name}</td>
                                <td style={{color:"#379cf8"}}>{mli.en_col_name}</td>
                                <td style={{color:"#379cf8"}}>{mli.order_seq}</td>
                                <td style={{color:"#379cf8"}}>{mli.col_data_type}</td>
                            </tr>
                        );
                    }
                    ylist.push(
                        <div className="row" style={{marginTop:"-15px"}}>
                            <table  className="table table-striped table-bordered table-hover table-condensed" >
                            <thead>
                            <tr >
                                <th style={{width:"40px",background:"#F0F0F0",color:"#379cf8"}}></th>
                                <th style={{background:"#dddddd",color:"#379cf8"}}>ch_col_name</th>
                                <th style={{background:"#F0F0F0",color:"#379cf8"}}>ch_table_name</th>
                                <th style={{background:"#dddddd",color:"#379cf8"}}>en_table_name</th>
                                <th style={{background:"#F0F0F0",color:"#379cf8"}}>en_col_name</th>
                                <th style={{background:"#dddddd",color:"#379cf8"}}>order_seq</th>
                                <th style={{background:"#F0F0F0",color:"#379cf8"}}>col_data_type</th>
                            </tr>
                            </thead>
                            <tbody>
                            {zlist}
                            </tbody>

                            </table>
                        </div>
                    );
                }

                var searchtablecount = this.state.searchtablecount;
                var maxPageNumbl = Math.ceil(searchtablecount/this.state.pagesize);

                var leftButtonClasbl = "";
                if(this.state.currentpagebl == 0){
                    leftButtonClasss = "disabled";
                }
                var rightButtonClasbl = "";
                if(this.state.currentpagebl == maxPageNumbl-1 ||(maxPageNumbl == 0)){
                    rightButtonClasss = "disabled";
                }
                pageButtonGrpbl.push(<li className={leftButtonClasbl}>
                    <a
                        onClick={this.firstpageclickbl}
                        style={{color:"#000000"}}>
                        <span>First</span>
                    </a>
                </li>);

                if(maxPageNumbl <= this.state.pagenum){
                    pageButtonGrpbl.push(<li className={leftButtonClasbl}>
                        <a
                            onClick={this.leftpageclickbl}
                            >
                            <span style={{color:"#000000"}}>Previous</span>
                        </a>
                    </li>);

                    for(var pn = 0;pn<=maxPageNumbl-1;pn++){
                        var currentPageButtonClss = "";
                        if(this.state.currentpagebl == pn){
                            currentPageButtonClss = "active";
                        }
                        pageButtonGrpbl.push(<li className={currentPageButtonClss}>
                            <a
                                onClick={this.pnumclickbl(pn)}
                                >
                                {parseInt(pn)+1}

                            </a>
                        </li>);

                    }
                    pageButtonGrpbl.push(<li className={rightButtonClasbl}>
                        <a
                            onClick={this.rightpageclickbl(maxPageNumbl)}
                            >
                            <span style={{color:"#000000"}}>Next</span>
                        </a>
                    </li>);
                }else{
                    if(this.state.currentpagebl < maxPageNumbl-7){
                        pageButtonGrpbl.push(<li className={leftButtonClasbl}>
                            <a
                                onClick={this.leftpageclickbl}
                                >
                                <span style={{color:"#000000"}}>Previous</span>
                            </a>
                        </li>);
                        for(var pn = this.state.currentpagebl;pn<=this.state.currentpagebl+2;pn++){
                            var currentPageButtonClss = "";
                            if(this.state.currentpagebl == pn){
                                currentPageButtonClss = "active";
                            }
                            pageButtonGrpbl.push(<li className={currentPageButtonClss}>
                                <a
                                    onClick={this.pnumclickbl(pn)}

                                    >
                                    {parseInt(pn)+1}

                                </a>
                            </li>);
                        }
                        pageButtonGrpbl.push(<li className={"disabled"}>
                            <a>
                                {"..."}

                            </a>
                        </li>);
                        for(var pn =maxPageNumbl-3;pn<=maxPageNumbl-1;pn++ ){
                            var currentPageButtonClss = "";
                            if(this.state.currentpagebl == pn){
                                currentPageButtonClss = "active";
                            }
                            pageButtonGrpbl.push(<li className={currentPageButtonClss}>
                                <a
                                    onClick={this.pnumclickbl(pn)}
                                    >
                                    {parseInt(pn)+1}

                                </a>
                            </li>);
                        }
                        pageButtonGrpbl.push(<li className={rightButtonClasbl}>
                            <a
                                onClick={this.rightpageclickbl(maxPageNumbl)}
                                >
                                <span style={{color:"#000000"}}>Next</span>
                            </a>
                        </li>);
                    }else{
                        pageButtonGrpbl.push(<li className={leftButtonClasbl}>
                            <a
                                onClick={this.leftpageclickbl}
                                >
                                <span style={{color:"#000000"}}>Previous</span>
                            </a>
                        </li>);
                        for(var pn = maxPageNumbl-7;pn<=maxPageNumbl-1;pn++){
                            var currentPageButtonClss = "";
                            if(this.state.currentpagebl == pn){
                                currentPageButtonClss = "active";
                            }
                            pageButtonGrpbl.push(<li className={currentPageButtonClss}>
                                <a
                                    onClick={this.pnumclickbl(pn)}
                                    >
                                    {parseInt(pn)+1}

                                </a>
                            </li>);
                        }
                        pageButtonGrpbl.push(<li className={rightButtonClasbl}>
                            <a
                                onClick={this.rightpageclickbl(maxPageNumbl)}
                                >
                                <span style={{color:"#000000"}}>Next</span>
                            </a>
                        </li>);
                    }
                }

                pageButtonGrpbl.push(<li className={rightButtonClasbl}>
                    <a
                        onClick={this.trailpageclickbl(maxPageNumbl)}
                        >
                        <span style={{color:"#000000"}}>Last</span>
                    </a>
                </li>);
                var searchtabledata = this.state.searchtabledata;
                if(searchtabledata.length >0){
                    var zlistbl = [];
                    for(var r in searchtabledata){
                        var mli = searchtabledata[r];
                        zlistbl.push(
                            <tr >
                                <td style={{width:"40px"}}>{parseInt(r)+1}</td>
                                <td >{mli.la_col_name}</td>
                                <td >{mli.la_table_name}</td>
                                <td >{mli.en_table_name}</td>
                                <td >{mli.en_col_name}</td>
                                <td >{mli.project_id}</td>
                                <td >{mli.col_data_type}</td>
                            </tr>
                        );
                    }
                    zzlslistbl.push(
                        <div className="row" style={{marginTop:"-15px"}}>
                            <table  className="table table-bordered table-hover table-condensed" >
                                <thead>
                                <tr >
                                    <th style={{width:"40px",background:"#F0F0F0"}}></th>
                                    <th style={{background:"#dddddd"}}>la_col_name</th>
                                    <th style={{background:"#F0F0F0"}}>la_table_name</th>
                                    <th style={{background:"#dddddd"}}>en_table_name</th>
                                    <th style={{background:"#F0F0F0"}}>en_col_name</th>
                                    <th style={{background:"#dddddd"}}>project_id</th>
                                    <th style={{background:"#F0F0F0"}}>col_data_type</th>
                                </tr>
                                </thead>
                                <tbody>
                                {zlistbl}
                                </tbody>

                            </table>
                        </div>
                    );
                }

                //for(var num in Comment.SETENCODING){
                //    setEncodingSelect.push(<option value={Comment.SETENCODING[num]}>
                //        {Comment.SETENCODING[num]}
                //    </option>)
                //}
                //for(var num in Comment.SETLANGUAGE){
                //    setLanguageSelect.push(<option value={Comment.SETLANGUAGE[num]}>
                //        {Comment.SETLANGUAGE[num]}
                //    </option>)
                //}
                //for(var num in Comment.SETSEPARATE){
                //    setSeparateSelect.push(<option value={Comment.SETSEPARATE[num]}>
                //        {Comment.SETSEPARATE[num]}
                //    </option>)
                //}

            }else{
                var metadatas = this.props.metadatas;
                if(this.state.metadatatext && this.state.metadatatext.length>0) {

                    var egx = ".*" + this.state.metadatatext + ".*";
                    var str2 = [];
                    var g = 0;
                    for (var m in metadatas) {
                        if (metadatas[m].en_name.toLowerCase().match(egx.toLowerCase())) {
                            str2[g] = metadatas[m];
                            g++;
                        }else{
                            if(metadatas[m].ch_name.toLowerCase().match(egx.toLowerCase())){
                                str2[g] = metadatas[m];
                                g++;
                            }
                        }
                    }
                    metadatas=str2;
                }
                var metadatalist = [];
                if(metadatas.length > 0){
                    for(var k in metadatas){
                        metadatalist.push(
                            <tr style={{display:"table"}}>
                                <td style={{textAlign:"center",width:"60px",borderTop:"0px none",borderBottom:"1px solid #ccc",borderLeft:"0px none",borderRight:"0px none",color:"#379cf8"}}>{parseInt(k)+1}</td>
                                <td  className="wrap" style={{width:((propswidth-334)/3+"px"),borderTop:"0px none",borderBottom:"1px solid #ccc",borderLeft:"0px none",borderRight:"0px none",color:"#379cf8"}}>{metadatas[k].en_name}</td>
                                <td  className="wrap" style={{width:((propswidth-334)/3+"px"),borderTop:"0px none",borderBottom:"1px solid #ccc",borderLeft:"0px none",borderRight:"0px none",color:"#379cf8"}}>{metadatas[k].ch_name}</td>
                                <td  className="wrap" style={{width:((propswidth-334)/3+"px"),borderTop:"0px none",borderBottom:"1px solid #ccc",borderLeft:"0px none",borderRight:"0px none",color:"#379cf8"}}>
                                    <button
                                        className="btn btn-default glyphicon glyphicon-eye-open"
                                        onClick={this.ViewMetadataClick(metadatas[k].en_name)}
                                        style={{border:"0px none",color:"#379cf8"}}
                                        ></button>
                                </td>
                            </tr>
                        );
                    }
                    metalist.push(
                        <div className="row"  style={{marginTop:"-15px"}}>
                            <div className="row" style={{height:"30px",marginLeft:"0px",marginRight:"0px"}}>
                                <table  className="table  table-hover table-condensed" >
                                    <thead  style={{display:"block"}}>
                                    <tr style={{backgroundColor:"#F5F5F5",display:"block", width:"100%",tableLayout:"fixed",borderBottom:"1px solid #ccc"}}>
                                        <th className="wrap" style={{width:"60px",textAlign:"center",border:"0px none",color:"#379cf8"}}>Rows</th>
                                        <th className="wrap"  style={{width:((propswidth-334)/3+"px"),border:"0px none",color:"#379cf8"}}>En_name</th>
                                        <th className="wrap" style={{width:((propswidth-334)/3+"px"),border:"0px none",color:"#379cf8"}}>Ch_name</th>
                                        <th className="wrap" style={{width:((propswidth-334)/3+"px"),border:"0px none",color:"#379cf8"}}>Operation</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                            <div className="row" style={{height:(pageHeight-259+"px"),overflowY:"auto",marginTop:"2px",marginLeft:"0px",marginRight:"0px"}}>
                                <table  className="table  table-hover table-condensed" >
                                    <tbody style={{display:"block",marginTop:"0px"}}>
                                    {metadatalist}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    );
                }
                var viewmetadata = this.props.tablesmetadata;
                var viewmd = [];
                if(viewmetadata.columnnamedata.length > 0){
                    var viewzzlname = [];
                    for(var j in viewmetadata.columnname){
                        viewzzlname.push(<th>{viewmetadata.columnname[j]}</th>);
                    }
                    for(var r in viewmetadata.columnnamedata){
                        var mli = viewmetadata.columnnamedata[r];
                        var columnamelist = [];
                        for(var g in viewmetadata.columnname){
                            var namevalue = eval("("+"mli["+"'"+viewmetadata.columnname[g]+"'"+"])");
                            columnamelist.push(<td >{namevalue}</td>);
                        }
                        viewmd.push(
                            <tr>
                                <td style={{textAlign:"center",width:"60px"}}>{parseInt(r)+1}</td>
                                {columnamelist}
                            </tr>
                        );
                    }
                    viewmeta.push(
                        <div className="row"  style={{marginTop:"-15px"}}>
                            <table  className="table table-bordered table-hover table-condensed">
                                <thead>
                                <tr style={{backgroundColor:"#F3F3FA"}}>
                                    <th style={{textAlign:"center",width:"60px"}}>Rows</th>
                                    {viewzzlname}
                                </tr>
                                </thead>
                                <tbody>
                                {viewmd}
                                </tbody>

                            </table>
                        </div>
                    );

                }
                for(var num in Comment.SETMETHOD){
                    setmethodSelect.push(<option value={Comment.SETMETHOD[num]}>
                        {Comment.SETMETHOD[num]}
                    </option>)
                }
            }


        }else if(this.state.ifdds == 3){
            //resuflt
            if(loginInfo.iftamr){
                if(this.state.ifbad){
                    iftotamrdisableStyle = "";
                }
            }
            if(!loginInfo.ifadmin){
                if(this.state.ifbad){
                    ifbaddisableStyle = "";
                }
            }
            if(this.state.ifexport){
                elink = <a
                    className="btn btn-info"
                    href={this.state.exporturl}
                    style={{marginRight:"5px"}}>
                    <span className="glyphicon glyphicon-floppy-save" aria-hidden="true"></span>Click to download.
                </a>;
            }
            var datacountss = this.state.datacountss;
            var maxPageNums = Math.ceil(datacountss/this.state.pagesizes);

            var leftButtonClasss = "";
            if(this.state.currentpages == 0){
                leftButtonClasss = "disabled";
            }
            var rightButtonClasss = "";
            if(this.state.currentpages == maxPageNums-1 ||(maxPageNums == 0)){
                rightButtonClasss = "disabled";
            }
            pageButtonGrps.push(<li className={leftButtonClasss}>
                <a
                    onClick={this.firstpageclicks}
                    style={{color:"#000000"}}>
                    <span>First</span>
                </a>
            </li>);

            if(maxPageNums <= this.state.pagenum){
                pageButtonGrps.push(<li className={leftButtonClasss}>
                    <a
                        onClick={this.leftpageclicks}
                        >
                        <span style={{color:"#000000"}}>Previous</span>
                    </a>
                </li>);

                for(var pn = 0;pn<=maxPageNums-1;pn++){
                    var currentPageButtonClss = "";
                    if(this.state.currentpages == pn){
                        currentPageButtonClss = "active";
                    }
                    pageButtonGrps.push(<li className={currentPageButtonClss}>
                        <a
                            onClick={this.pnumclicks(pn)}
                            >
                            {parseInt(pn)+1}

                        </a>
                    </li>);

                }
                pageButtonGrps.push(<li className={rightButtonClasss}>
                    <a
                        onClick={this.rightpageclicks(maxPageNums)}
                        >
                        <span style={{color:"#000000"}}>Next</span>
                    </a>
                </li>);
            }else{
                if(this.state.currentpages < maxPageNums-7){
                    pageButtonGrps.push(<li className={leftButtonClasss}>
                        <a
                            onClick={this.leftpageclicks}
                            >
                            <span style={{color:"#000000"}}>Previous</span>
                        </a>
                    </li>);
                    for(var pn = this.state.currentpages;pn<=this.state.currentpages+2;pn++){
                        var currentPageButtonClss = "";
                        if(this.state.currentpages == pn){
                            currentPageButtonClss = "active";
                        }
                        pageButtonGrps.push(<li className={currentPageButtonClss}>
                            <a
                                onClick={this.pnumclicks(pn)}

                                >
                                {parseInt(pn)+1}

                            </a>
                        </li>);
                    }
                    pageButtonGrps.push(<li className={"disabled"}>
                        <a>
                            {"..."}

                        </a>
                    </li>);
                    for(var pn =maxPageNums-3;pn<=maxPageNums-1;pn++ ){
                        var currentPageButtonClss = "";
                        if(this.state.currentpages == pn){
                            currentPageButtonClss = "active";
                        }
                        pageButtonGrps.push(<li className={currentPageButtonClss}>
                            <a
                                onClick={this.pnumclicks(pn)}
                                >
                                {parseInt(pn)+1}

                            </a>
                        </li>);
                    }
                    pageButtonGrps.push(<li className={rightButtonClasss}>
                        <a
                            onClick={this.rightpageclicks(maxPageNums)}
                            >
                            <span style={{color:"#000000"}}>Next</span>
                        </a>
                    </li>);
                }else{
                    pageButtonGrps.push(<li className={leftButtonClasss}>
                        <a
                            onClick={this.leftpageclicks}
                            >
                            <span style={{color:"#000000"}}>Previous</span>
                        </a>
                    </li>);
                    for(var pn = maxPageNums-7;pn<=maxPageNums-1;pn++){
                        var currentPageButtonClss = "";
                        if(this.state.currentpages == pn){
                            currentPageButtonClss = "active";
                        }
                        pageButtonGrps.push(<li className={currentPageButtonClss}>
                            <a
                                onClick={this.pnumclicks(pn)}
                                >
                                {parseInt(pn)+1}

                            </a>
                        </li>);
                    }
                    pageButtonGrps.push(<li className={rightButtonClasss}>
                        <a
                            onClick={this.rightpageclicks(maxPageNums)}
                            >
                            <span style={{color:"#000000"}}>Next</span>
                        </a>
                    </li>);
                }
            }

            pageButtonGrps.push(<li className={rightButtonClasss}>
                <a
                    onClick={this.trailpageclicks(maxPageNums)}
                    >
                    <span style={{color:"#000000"}}>Last</span>
                </a>
            </li>);
            var viewodsbds = this.state.resultdata;
            var columnname = this.state.columnname;
            if(viewodsbds.length > 0){
                var zcolumnlist = [];
                for(var j in columnname){
                    if(j%2 == 0){
                        zcolumnlist.push(<th style={{background:"#dddddd",color:"#379cf8"}}>{columnname[j]}</th>);
                    }else{
                        zcolumnlist.push(<th style={{background:"#F0F0F0",color:"#379cf8"}}>{columnname[j]}</th>);
                    }
                }
                var zllist = [];
                for(var r in viewodsbds){
                    var mli = viewodsbds[r];
                    var columnamelist = [];
                    for(var g in columnname){
                        var namevalue = eval("mli."+columnname[g]);
                        columnamelist.push(<td style={{whiteSpace:"nowrap",color:"#379cf8"}}>{namevalue}</td>);
                    }
                    zllist.push(
                        <tr>
                            <td style={{color:"#379cf8"}}>{parseInt(r)+1}</td>
                            {columnamelist}
                        </tr>
                    );
                }
                zzllist.push(
                    <div className="row"  style={{marginTop:"-15px"}}>
                        <table  className="table table-striped table-bordered table-hover table-condensed"
                               // id="tablecells"  onMouseMove={this.tablecellChange}
                            >
                            <thead>
                            <tr>
                                <th style={{background:"#F0F0F0"}}></th>
                                {zcolumnlist}
                            </tr>
                            </thead>
                            <tbody>
                            {zllist}
                            </tbody>

                        </table>
                    </div>
                );
            }

            var searchcount = this.state.searchcount;
            var maxPageNumch = Math.ceil(searchcount/this.state.pagesizech);

            var leftButtonClasch = "";
            if(this.state.currentpagech == 0){
                leftButtonClasss = "disabled";
            }
            var rightButtonClasch = "";
            if(this.state.currentpagech == maxPageNumch-1 ||(maxPageNumch == 0)){
                rightButtonClasss = "disabled";
            }
            pageButtonGrpch.push(<li className={leftButtonClasch}>
                <a
                    onClick={this.firstpageclickch}
                    style={{color:"#000000"}}>
                    <span>First</span>
                </a>
            </li>);

            if(maxPageNumch <= this.state.pagenum){
                pageButtonGrpch.push(<li className={leftButtonClasch}>
                    <a
                        onClick={this.leftpageclickch}
                        >
                        <span style={{color:"#000000"}}>Previous</span>
                    </a>
                </li>);

                for(var pn = 0;pn<=maxPageNumch-1;pn++){
                    var currentPageButtonClss = "";
                    if(this.state.currentpagech == pn){
                        currentPageButtonClss = "active";
                    }
                    pageButtonGrpch.push(<li className={currentPageButtonClss}>
                        <a
                            onClick={this.pnumclickch(pn)}
                            >
                            {parseInt(pn)+1}

                        </a>
                    </li>);

                }
                pageButtonGrpch.push(<li className={rightButtonClasch}>
                    <a
                        onClick={this.rightpageclickch(maxPageNumch)}
                        >
                        <span style={{color:"#000000"}}>Next</span>
                    </a>
                </li>);
            }else{
                if(this.state.currentpagech < maxPageNumch-7){
                    pageButtonGrpch.push(<li className={leftButtonClasch}>
                        <a
                            onClick={this.leftpageclickch}
                            >
                            <span style={{color:"#000000"}}>Previous</span>
                        </a>
                    </li>);
                    for(var pn = this.state.currentpagech;pn<=this.state.currentpagech+2;pn++){
                        var currentPageButtonClss = "";
                        if(this.state.currentpagech == pn){
                            currentPageButtonClss = "active";
                        }
                        pageButtonGrpch.push(<li className={currentPageButtonClss}>
                            <a
                                onClick={this.pnumclickch(pn)}

                                >
                                {parseInt(pn)+1}

                            </a>
                        </li>);
                    }
                    pageButtonGrpch.push(<li className={"disabled"}>
                        <a>
                            {"..."}

                        </a>
                    </li>);
                    for(var pn =maxPageNumch-3;pn<=maxPageNumch-1;pn++ ){
                        var currentPageButtonClss = "";
                        if(this.state.currentpagech == pn){
                            currentPageButtonClss = "active";
                        }
                        pageButtonGrpch.push(<li className={currentPageButtonClss}>
                            <a
                                onClick={this.pnumclickch(pn)}
                                >
                                {parseInt(pn)+1}

                            </a>
                        </li>);
                    }
                    pageButtonGrpch.push(<li className={rightButtonClasch}>
                        <a
                            onClick={this.rightpageclickch(maxPageNumch)}
                            >
                            <span style={{color:"#000000"}}>Next</span>
                        </a>
                    </li>);
                }else{
                    pageButtonGrpch.push(<li className={leftButtonClasch}>
                        <a
                            onClick={this.leftpageclickch}
                            >
                            <span style={{color:"#000000"}}>Previous</span>
                        </a>
                    </li>);
                    for(var pn = maxPageNumch-7;pn<=maxPageNumch-1;pn++){
                        var currentPageButtonClss = "";
                        if(this.state.currentpagech == pn){
                            currentPageButtonClss = "active";
                        }
                        pageButtonGrpch.push(<li className={currentPageButtonClss}>
                            <a
                                onClick={this.pnumclickch(pn)}
                                >
                                {parseInt(pn)+1}

                            </a>
                        </li>);
                    }
                    pageButtonGrpch.push(<li className={rightButtonClasch}>
                        <a
                            onClick={this.rightpageclickch(maxPageNumch)}
                            >
                            <span style={{color:"#000000"}}>Next</span>
                        </a>
                    </li>);
                }
            }

            pageButtonGrpch.push(<li className={rightButtonClasch}>
                <a
                    onClick={this.trailpageclickch(maxPageNumch)}
                    >
                    <span style={{color:"#000000"}}>Last</span>
                </a>
            </li>);
            var searchdata = this.state.searchdata;
            var searchname = this.state.searchname;
            if(searchdata.length >0){
                var scolumnlist = [];
                for(var j in searchname){
                    if(j%2 == 0){
                        scolumnlist.push(<th style={{background:"#dddddd"}}>{searchname[j]}</th>);
                    }else{
                        scolumnlist.push(<th style={{background:"#F0F0F0"}}>{searchname[j]}</th>);
                    }
                }
                var sllist = [];
                for(var r in searchdata){
                    var sli = searchdata[r];
                    var scolumnamelist = [];
                    for(var g in searchname){
                        var snamevalue = eval("sli."+searchname[g]);
                        scolumnamelist.push(<td  style={{whiteSpace:"nowrap"}}>{snamevalue}</td>);
                    }
                    sllist.push(
                        <tr>
                            <td style={{width:"40px"}}>{parseInt(r)+1}</td>
                            {scolumnamelist}
                        </tr>
                    );
                }
                zzlslist.push(
                    <div className="row" style={{marginTop:"-15px"}}>
                        <table  className="table table-bordered table-hover table-condensed">
                            <thead>
                            <tr>
                                <th style={{width:"40px",background:"#F0F0F0"}}></th>
                                {scolumnlist}
                            </tr>
                            </thead>
                            <tbody>
                            {sllist}
                            </tbody>

                        </table>
                    </div>
                );

            }
            var coveredfilelist = this.state.covernamelist;
            var coveredselects = this.state.ckockselects;
            var zkklist = [];
            if(coveredselects.length == coveredfilelist.length && coveredselects.length>0){
                for(var k in coveredfilelist){
                    zkklist.push(
                        <tr>
                            <td>
                                <input type="checkbox"
                                       checked={coveredselects[k].ifckecked}
                                       onChange={this.checkrowFileclick(coveredfilelist[k])}
                                    />
                                <span style={{marginLeft:"10px"}}>{coveredfilelist[k]}</span>
                            </td>
                        </tr>
                    );
                }
                zklist.push(
                    <div className="row" style={{marginTop:"20px",height:(200+"px"),overflowY:"auto"}}>
                        <div className="col-sm-12 ">
                            <table  className="table table-bordered table-hover table-condensed">
                                <tbody>
                                    {zkklist}
                                </tbody>

                            </table>
                        </div>
                    </div>
                );
            }
        } else if(this.state.ifdds == 1){
            var featuredeflist=this.props.datafeaturedeflist;
            var selectColor = [];
            for(var i in featuredeflist){
                selectFedefStylel[i]={display:"none"};
                selectColor[0]="#000";
            }
            if(this.state.selectFdefItem){
                selectFedefStylel[0]={display:"none"}
                selectFedefStylel[this.state.selectFdefItem]={float:"left",color:"#379cf8"}
                selectColor[0]="#000";
                selectColor[this.state.selectFdefItem]="#379cf8";
            }else{
                selectFedefStylel[0]={float:"left",color:"#379cf8"}
                selectColor[0]="#379cf8";
            }
            if(featuredeflist.length>0){
                for(var i in featuredeflist){
                    fdeflist.push(
                        <li>
                            <a
                                onClick={this.featuretableclick(featuredeflist[i].featuredef_no,featuredeflist[i].featuredef_name,i)}
                            ><p style={{whiteSpace:"nowrap"}}><span
                                style={selectFedefStylel[i]}

                            >√</span>
                                <span style={{color:selectColor[i]}}> {featuredeflist[i].featuredef_name}</span>
                            </p>
                            </a>
                        </li>
                    );
                }
            }
            var featuredetaillist=this.props.datafeaturedetlist;
            if(featuredetaillist.length>0){
                for (var j in featuredetaillist) {
                    fdetlsit.push(
                        <tr>
                            <td className="wrap" style={{width:((((propswidth-250)/12*9)-23-120)*0.45+"px"),marginLeft:"5px",borderTop:"0px none",borderBottom:"1px solid #ccc",borderLeft:"0px none",borderRight:"0px none"}}>{featuredetaillist[j].featuredetail_name}</td>
                            <td className="wrap" style={{width:((((propswidth-250)/12*9)-23-120)*0.55+"px"),borderTop:"0px none",borderBottom:"1px solid #ccc",borderLeft:"0px none",borderRight:"0px none"}}>{featuredetaillist[j].featuredetail_context}</td>
                            <td className="wrap" style={{width:"120px",borderTop:"0px none",borderBottom:"1px solid #ccc",borderLeft:"0px none",borderRight:"0px none"}}>{featuredetaillist[j].featuredetail_type==1?"regex":"others"}</td>
                        </tr>
                    );
                }
            }

            var featprolist = this.props.featureprojectlist;
            var zzlsslistp = [];
            if(featprolist.length>0){
                for(var m in featprolist) {
                    zzlsslistp.push(
                        <div className="row zzltrjion" style={{marginLeft:"0px",marginRight:"0px",marginTop:"5px",borderBottom:"1px solid #ddd"}}>
                            <div className="col-sm-8 wrap">
                                <span>{featprolist[m].featuredef_name}</span>
                            </div>
                            <div className="col-sm-4 wrap" style={{textAlign:"right"}}>
                                <button
                                    role="button"
                                    className="btn btn-default glyphicon glyphicon-trash"
                                    onClick={this.projectFeaDelclick(featprolist[m].featuredef_no)}
                                    disabled={dictadminDisable}
                                    style={{border:"0px none",borderBottom:"1px solid #ddd",color:"#379cf8"}}
                                    >
                                </button>
                            </div>
                        </div>
                    );
                }
                sslist.push(
                    <div className="row">
                        <div className="col-sm-12">
                            <div className="panel panel-default" style={{border:"0px none"}}>
                                <div className="panel-heading" style={{background:"#ECECEC"}}>
                                    <div className="row " style={{marginLeft:"-15px",marginRight:"-15px"}}>
                                        <div className="col-sm-8 wrap">
                                            <span>Featurefile Name</span>
                                        </div>
                                        <div className="col-sm-4 wrap" style={{textAlign:"right"}}>
                                            <span>Operation</span>
                                        </div>
                                    </div>
                                </div>
                                <div className="panel-body" >
                                    <div className="row" style={{marginTop:"-20px",backgroundColor:"#F7F7F7"}}>
                                        {zzlsslistp}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                );
            }

        }else if(this.state.ifdds == 4){
            var participleconflist=this.props.participleconflist;
            var selectColor = [];
            for(var i in participleconflist){
                selectParticiStylel[i]={display:"none"};
                selectColor[0]="#000";
            }
            if(this.state.selectPlefItem){
                selectParticiStylel[0]={display:"none"}
                selectParticiStylel[this.state.selectPlefItem]={float:"left",color:"#379cf8"}
                selectColor[0]="#000";
                selectColor[this.state.selectPlefItem]="#379cf8";
            }else{
                selectParticiStylel[0]={float:"left",color:"#379cf8"}
                selectColor[0]="#379cf8";
            }
            if(participleconflist.length>0){
                for(var i in participleconflist){
                    pleflist.push(
                        <li>
                            <a
                                onClick={this.ParticiPletableClick(participleconflist[i].conf_define_id,participleconflist[i].conf_name,i)}
                                ><p style={{whiteSpace:"nowrap"}}><span
                                style={selectParticiStylel[i]}

                                >√</span>
                                <span style={{color:selectColor[i]}}> {participleconflist[i].conf_name}</span>
                            </p>
                            </a>
                        </li>
                    );
                }
            }

            var partpledatalist=this.props.partpledatalist;
            if(partpledatalist.length !=0){
                if(partpledatalist.length>1){
                    for(var i=0;i <partpledatalist.length;i=i+2){
                        if((i+1) ==(partpledatalist.length)){
                            pletlsit.push(
                                <div className="row " style={{marginLeft:"0px",marginRight:"0px"}}>
                                    <div className="col-sm-6 " >
                                        <div className="row zzltrjion"  style={{marginLeft:"0px",marginRight:"0px",borderBottom:"1px solid #ccc"}}>
                                            <div className="row" style={{marginLeft:"0px",marginRight:"-15px",marginBottom:"5px",marginTop:"5px"}}>
                                                <div className="col-sm-12" >
                                                    <div className="row wrap"  style={{marginLeft:"0px",marginRight:"0px"}}>
                                                        <span>{partpledatalist[i].word}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            );
                        }else{
                            pletlsit.push(
                                <div className="row " style={{marginLeft:"0px",marginRight:"0px"}}>
                                    <div className="col-sm-6 " >
                                        <div className="row zzltrjion"  style={{marginLeft:"0px",marginRight:"0px",borderBottom:"1px solid #ccc"}}>
                                            <div className="row" style={{marginLeft:"0px",marginRight:"-15px",marginBottom:"5px",marginTop:"5px"}}>
                                                <div className="col-sm-12" >
                                                    <div className="row wrap"  style={{marginLeft:"0px",marginRight:"0px"}}>
                                                        <span>{partpledatalist[i].word}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-6">
                                        <div className="row zzltrjion"  style={{marginLeft:"0px",marginRight:"0px",borderBottom:"1px solid #ccc"}}>
                                            <div className="row" style={{marginLeft:"0px",marginRight:"-15px",marginBottom:"5px",marginTop:"5px"}}>
                                                <div className="col-sm-12" >
                                                    <div className="row wrap"  style={{marginLeft:"0px",marginRight:"0px"}}>
                                                        <span>{partpledatalist[i+1].word}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            );
                        }
                    }
                }else{
                    pletlsit.push(
                        <div className="row" style={{marginLeft:"0px",marginRight:"0px"}}>
                            <div className="col-sm-6 " >
                                <div className="row zzltrjion"  style={{marginLeft:"0px",marginRight:"0px",borderBottom:"1px solid #ccc"}}>
                                    <div className="row" style={{marginLeft:"0px",marginRight:"-15px",marginBottom:"5px",marginTop:"5px"}}>
                                        <div className="col-sm-12" >
                                            <div className="row wrap"  style={{marginLeft:"0px",marginRight:"0px"}}>
                                                <span>{partpledatalist[0].word}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    );
                }
            }

            var partpleprojectlist = this.props.partpleprojectlist;
            var zzlsslistp = [];
            if(partpleprojectlist.length>0){
                for(var m in partpleprojectlist) {
                    zzlsslistp.push(
                        <div className="row zzltrjion" style={{marginLeft:"0px",marginRight:"0px",marginTop:"5px",borderBottom:"1px solid #ddd"}}>
                            <div className="col-sm-8 wrap">
                                <span>{partpleprojectlist[m].conf_name}</span>
                            </div>
                            <div className="col-sm-4 wrap" style={{textAlign:"right"}}>
                                <button
                                    role="button"
                                    className="btn btn-default glyphicon glyphicon-trash"
                                    onClick={this.projectPartPleclick(partpleprojectlist[m].conf_define_id)}
                                    disabled={dictadminDisable}
                                    style={{border:"0px none",borderBottom:"1px solid #ddd",color:"#379cf8"}}
                                    >
                                </button>
                            </div>
                        </div>
                    );
                }
                pleplist.push(
                    <div className="row">
                        <div className="col-sm-12">
                            <div className="panel panel-default" style={{border:"0px none"}}>
                                <div className="panel-heading" style={{background:"#ECECEC"}}>
                                    <div className="row " style={{marginLeft:"-15px",marginRight:"-15px"}}>
                                        <div className="col-sm-8 wrap">
                                            <span>Token Name</span>
                                        </div>
                                        <div className="col-sm-4 wrap" style={{textAlign:"right"}}>
                                            <span>Operation</span>
                                        </div>
                                    </div>
                                </div>
                                <div className="panel-body" >
                                    <div className="row" style={{marginTop:"-20px",backgroundColor:"#F7F7F7"}}>
                                        {zzlsslistp}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                );
            }
        }
        else{
            var dictfiles = this.props.dictfilelist;
            var selectColor = [];
            for(var i in dictfiles){
                selectStylel[i]={display:"none"};
                selectColor[i]="#000";
            }
            if(this.state.selectItem){
                selectStylel[0]={display:"none"}
                selectStylel[this.state.selectItem]={float:"left",color:"#379cf8"}
                selectColor[0]="#000";
                selectColor[this.state.selectItem]="#379cf8";
            }else{
                selectStylel[0]={float:"left",color:"#379cf8"}
                selectColor[0]="#379cf8";
            }

            for(var i in dictfiles){
                dlist.push(
                    <li>
                        <a
                            onClick={this.dictfileclick(dictfiles[i].dic_id,dictfiles[i].dic_name,i)}
                            ><p style={{whiteSpace:"nowrap"}}><span
                            style={selectStylel[i]}
                            >√</span>
                            <span style={{color:selectColor[i]}}> {dictfiles[i].dic_name}</span>
                            </p>
                        </a>
                    </li>
                );
            }
            var  dictfiledata = this.props.dictfiledata;
            var checkselects = this.state.checkselects;

            if(checkselects.length >0 && checkselects.length == dictfiledata.length){
                for(var k in dictfiledata){
                    mlist.push(
                        <tr>
                            <td style={{width:"30px",borderTop:"0px none",borderBottom:"1px solid #ccc",borderLeft:"0px none",borderRight:"0px none"}}>
                               <div className="checkboxFive">
                                   <input type="checkbox"
                                       // className="checkbox"
                                          id={"checkboxFiveInput"+k}
                                          checked={checkselects[k].ifckecked}
                                          onChange={this.checkrowclick(dictfiledata[k].dicsyn_no,dictfiledata[k].dic_id)}
                                       />
                                   <label htmlFor={"checkboxFiveInput"+k}></label>
                               </div>

                            </td>
                            <td className="wrap" style={{width:((((propswidth-250)/12*9)-23-30)*0.50+"px"),borderTop:"0px none",borderBottom:"1px solid #ccc",borderLeft:"0px none",borderRight:"0px none"}}>{dictfiledata[k].target_name}</td>
                            <td className="wrap" style={{width:((((propswidth-250)/12*9)-23-30)*0.50+"px"),borderTop:"0px none",borderBottom:"1px solid #ccc",borderLeft:"0px none",borderRight:"0px none"}}>{dictfiledata[k].source_name}</td>

                        </tr>
                    );
                }
            }
            var confirmdata = this.state.confirmdata;
            var checkconfirms = this.state.checkconfirms;
            if(checkconfirms.length >0 && checkconfirms.length == confirmdata.length){
                for(var j in confirmdata){
                    var klist = [];
                    for(var k in confirmdata[j]){
                        klist.push(
                            <tr>
                                <td>
                                    <input type="checkbox"
                                           checked={checkconfirms[j][k].ifckecked}
                                           onChange={this.checkconfirmclick(confirmdata[j][k].dicsyn_no,confirmdata[j][k].dic_id)}
                                        />
                                </td>
                                <td>{confirmdata[j][k].dicsyn_no}</td>
                                <td>{confirmdata[j][k].target_name}</td>
                                <td>{confirmdata[j][k].source_name}</td>
                                <td>{confirmdata[j][k].ext4}</td>
                            </tr>
                        );
                    }
                    qlist.push(
                        <div className="panel panel-default">
                            <div className="panel-heading" style={{backgroundColor:"#FFE4CA"}}>
                                <span className="green">{parseInt(j)+1}</span>
                                <span>{"Conflict entries"}</span>
                            </div>
                            <div className="panel-body">
                                <table  className="table table-bordered table-hover table-condensed">
                                    <thead>
                                    <tr style={{backgroundColor:"#F3F3FA"}}>
                                        <th style={{width:"30px"}}></th>
                                        <th>Item No</th>
                                        <th >Target Name</th>
                                        <th >Source Name</th>
                                        <th >Dict Name</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {klist}
                                    </tbody>

                                </table>
                            </div>
                        </div>
                    );
                }
            }

            var joinedPlist = this.props.projectdictlist;
            var zzljlist = [];
            if(joinedPlist.length >0){
                for(var m in joinedPlist){
                    //jlist.push(
                    //    <div className="row">
                    //        <div className="col-sm-12">
                    //            <div className="panel panel-default">
                    //                <div className="panel-heading">
                    //                    <span className="grey">{parseInt(m)+1}</span>
                    //                    <span>{joinedPlist[m].dic_name}</span>
                    //                </div>
                    //                <div className="panel-body">
                    //                    <div className="row" >
                    //                        <div className="col-sm-12" style={{textAlign:"right"}}>
                    //                            <button
                    //                                role="button"
                    //                                className="btn btn-default glyphicon glyphicon-trash"
                    //                                onClick={this.projectDicDelclick(joinedPlist[m].dic_id)}
                    //                                disabled={dictadminDisable}
                    //                                >
                    //
                    //                            </button>
                    //                        </div>
                    //                    </div>
                    //                </div>
                    //            </div>
                    //        </div>
                    //    </div>
                    //);
                    zzljlist.push(
                        <div className="row zzltrjion" style={{marginLeft:"0px",marginRight:"0px",marginTop:"5px",borderBottom:"1px solid #ddd"}}>
                            <div className="col-sm-8 wrap">
                                <span>{joinedPlist[m].dic_name}</span>
                            </div>
                            <div className="col-sm-4 wrap" style={{textAlign:"right"}}>
                                <button
                                    role="button"
                                    className="btn btn-default glyphicon glyphicon-trash"
                                    onClick={this.projectDicDelclick(joinedPlist[m].dic_id)}
                                    disabled={dictadminDisable}
                                    style={{border:"0px none",borderBottom:"1px solid #ddd",color:"#379cf8"}}
                                    >
                                </button>
                            </div>
                        </div>
                    );
                }
                jlist.push(
                    <div className="row">
                        <div className="col-sm-12">
                            <div className="panel panel-default" style={{border:"0px none"}}>
                                <div className="panel-heading" style={{background:"#ECECEC"}}>
                                    <div className="row " style={{marginLeft:"-15px",marginRight:"-15px"}}>
                                        <div className="col-sm-8 wrap">
                                            <span>Dict Name</span>
                                        </div>
                                        <div className="col-sm-4 wrap" style={{textAlign:"right"}}>
                                            <span>Operation</span>
                                        </div>
                                    </div>
                                </div>
                                <div className="panel-body" >
                                    <div className="row" style={{marginTop:"-20px",backgroundColor:"#F7F7F7"}}>
                                        {zzljlist}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                );
            }
        }
        var selectSourceStylel=[];
        var selectcolor = [];
        if(this.state.selectSourceItem==0 || this.state.selectSourceItem==null){
            selectSourceStylel[1]={display:"none"}
            selectSourceStylel[2]={display:"none"}
            selectSourceStylel[3]={display:"none"}
            selectSourceStylel[0]={float:"left",color:"#379cf8"}
            selectcolor[1]="#000"
            selectcolor[2]="#000"
            selectcolor[3]="#000"
            selectcolor[0]="#379cf8"

        }else if(this.state.selectSourceItem==1){
            selectSourceStylel[0]={display:"none"}
            selectSourceStylel[2]={display:"none"}
            selectSourceStylel[3]={display:"none"}
            selectSourceStylel[1]={float:"left",color:"#379cf8"}
            selectcolor[0]="#000"
            selectcolor[2]="#000"
            selectcolor[3]="#000"
            selectcolor[1]="#379cf8"
        }else if(this.state.selectSourceItem==2){
            selectSourceStylel[0]={display:"none"}
            selectSourceStylel[1]={display:"none"}
            selectSourceStylel[3]={display:"none"}
            selectSourceStylel[2]={float:"left",color:"#379cf8"}
            selectcolor[0]="#000"
            selectcolor[1]="#000"
            selectcolor[3]="#000"
            selectcolor[2]="#379cf8"
        }else{
            selectSourceStylel[0]={display:"none"}
            selectSourceStylel[1]={display:"none"}
            selectSourceStylel[2]={display:"none"}
            selectSourceStylel[3]={float:"left",color:"#379cf8"}
            selectcolor[0]="#000"
            selectcolor[1]="#000"
            selectcolor[2]="#000"
            selectcolor[3]="#379cf8"
        }
        var selectSourceStylell=[];
        var selectcolorss = [];
        if(this.state.selectresultItem==0 || this.state.selectresultItem==null){
            selectSourceStylell[1]={display:"none"}
            selectSourceStylell[0]={float:"left",color:"#379cf8"}
            selectcolorss[1] = "#000"
            selectcolorss[0] = "#379cf8"
        }else{
            selectSourceStylell[0]={display:"none"}
            selectSourceStylell[1]={float:"left",color:"#379cf8"}
            selectcolorss[0] = "#000"
            selectcolorss[1] = "#379cf8"
        }
        return(
            <div>
                <div style={{float:"left",width:(propswidth+"px")}}>
                    <div className="row" style={{height:"70px",backgroundColor:"#FFFFFF",marginLeft:"0px",marginRight:"0px"}}>
                        <div className="col-sm-8 " >
                            <h4 style={{marginTop:"20px"}}>{projectname} project</h4>
                        </div>
                    </div>
                    <div className="row" style={{height:"5px",backgroundColor:"#282E3E",marginLeft:"0px",marginRight:"0px"}}>
                    </div>
                    <div className="row" style={{height:(pageHeight-135+"px"),backgroundColor:"#FFFFFF",marginLeft:"0px",marginRight:"0px"}}>
                        <ul className="nav nav-tabs " role="tablist"  >
                            <li role="presentation" className="active" style={{height:"42px"}}><a href="#dictionary" aria-controls="dictionary" role="tab" data-toggle="tab" onClick={this.getdictsList}
                                >Dictionaries</a></li>
                            <li role="presentation" style={{height:"42px"}}><a href="#features" aria-controls="features" role="tab" data-toggle="tab" onClick={this.getDataFeaturelist}
                                >Data Features</a></li>
                            <li role="presentation" style={{height:"42px"}}><a href="#participles" aria-controls="participles" role="tab" data-toggle="tab" onClick={this.getParticiPlelist}
                                >Tokens</a></li>
                            <li role="presentation" style={{height:"42px"}}
                                onClick={this.getsoureclick(parseInt(0))}
                                ><a href="#suorces" aria-controls="suorces" role="tab" data-toggle="tab"
                                >Sources</a></li>
                            <li role="presentation" style={{height:"42px"}}

                                ><a href="#resultd" aria-controls="resultd" role="tab" data-toggle="tab"
                                    id="resultonclick"
                                    onClick={this.getsoureresultclick(parseInt(0))}
                                >Feature Result</a></li>
                        </ul>
                        <div className="tab-content ">
                            <div role="tabpanel" className="tab-pane active" id="dictionary">
                                <div id="wrapper" >
                                    <div id="sidebar-wrapper" style={{height:(pageHeight-177+"px"),width:"250px",borderTop:"1px solid #ccc"}}>
                                        <ul className="sidebar-nav" >
                                            <li className="sidebar-brand" style={{border:"0px none"}}>
                                                <a>
                                                    Dictionary List
                                                </a>
                                            </li>
                                            <div className="row" style={{height:"55px",borderBottom:"1px solid #ccc"}}>
                                                <div className="col-sm-11 col-sm-offset-1" style={{marginBottom:"10px"}}>
                                                    <button
                                                        className="btn btn-default zzlbuttoncolborder"
                                                        role="button"
                                                        onClick={this.submitdictfile}
                                                        style={{height:"35px"}}
                                                        disabled={dictadminDisable}
                                                        >
                                                        Add to Project
                                                    </button>
                                                </div>
                                            </div>
                                            <div style={{height:(pageHeight-282+"px"),overflow:"auto"}}>
                                                {dlist}
                                            </div>
                                        </ul>
                                    </div>
                                    <div id="page-content-wrapper" style={{height:(pageHeight-177+"px"),backgroundColor:"#FFFFFF",border:"0px none"}}>
                                        <div className="row" style={{marginTop:"-20px",marginLeft:"-20px",marginRight:"-20px",height:(pageHeight-177+"px")}}>
                                            <div className="col-sm-9" style={{backgroundColor:"#FFFFFF",borderLeft:"1px solid #ccc",borderRight:"1px solid #ccc"}}>
                                                <div className="panel panel-default" style={{marginLeft:"-15px",marginRight:"-15px",border:"0px none"}}>
                                                    <div className="panel-body" style={{height:(pageHeight-177+"px"),border:"0px none"}}>
                                                        <div className="row"  style={{marginTop:"-15px"}}>
                                                            <div className="row" style={{height:"40px",marginLeft:"0px",marginRight:"0px"}}>
                                                                <table  className="table  table-hover table-condensed" >
                                                                    <thead  style={{display:"block"}}>
                                                                    <tr style={{height:"40px",backgroundColor:"#F5F5F5",display:"block", width:"100%",tableLayout:"fixed",borderBottom:"1px solid #ccc"}}>
                                                                        <th style={{width:"30px",border:"0px none"}}></th>
                                                                        <th style={{width:((((propswidth-250)/12*9)-23-30)*0.50+"px"),border:"0px none"}}>Target Name</th>
                                                                        <th style={{width:((((propswidth-250)/12*9)-23-30)*0.50+"px"),border:"0px none"}}>Source Name</th>

                                                                    </tr>
                                                                    </thead>
                                                                </table>
                                                            </div>

                                                            <div className="row" style={{height:(pageHeight-214+"px"),overflowY:"auto",marginTop:"0px",marginLeft:"0px",marginRight:"0px"}}>
                                                                <table  className="table  table-hover table-condensed">
                                                                    <tbody style={{display:"block",marginTop:"0px"}}>
                                                                    {mlist}
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-sm-3" style={{height:(pageHeight-177+"px"),backgroundColor:"#FFFFFF",border:"0px none"}}>
                                                <div className="panel panel-default" style={{marginLeft:"-15px",marginRight:"-15px",border:"0px none"}}>
                                                    <div className="panel-heading" style={{height:"40px"}}>
                                                        <span className="wraps"><strong>Dictionaries Used</strong></span>
                                                    </div>
                                                    <div className="panel-body" style={{height:(pageHeight-217+"px"),overflowY:"auto"}}>
                                                        {jlist}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                            <div  role="tabpanel" className="tab-pane " id="features">
                                <div id="wrapper" >
                                    <div id="sidebar-wrapper" style={{height:(pageHeight-177+"px"),width:"250px",borderTop:"1px solid #ccc"}}>
                                        <ul className="sidebar-nav" >
                                            <li className="sidebar-brand" style={{border:"0px none"}}>
                                                <a >
                                                    Data Feature List
                                                </a>

                                            </li>
                                            <div className="row" style={{height:"55px",borderBottom:"1px solid #ccc"}}>
                                                <div className="col-sm-11 col-sm-offset-1" style={{marginBottom:"10px"}}>
                                                    <button className="btn btn-default zzlbuttoncolborder" role="button"
                                                            onClick={this.submitAddFeatdef}
                                                            style={{height:"35px"}}
                                                            disabled={dictadminDisable}
                                                        >
                                                        Add to Project
                                                    </button>
                                                </div>
                                            </div>
                                            <div style={{height:(pageHeight-282+"px"),overflow:"auto"}}>
                                                {fdeflist}
                                            </div>

                                        </ul>
                                    </div>


                                    <div id="page-content-wrapper" style={{height:(pageHeight-177+"px"),backgroundColor:"#FFFFFF",border:"0px none"}}>
                                        <div className="row" style={{marginTop:"-20px",marginLeft:"-20px",marginRight:"-20px",height:(pageHeight-177+"px")}}>
                                            <div className="col-sm-9" style={{backgroundColor:"#FFFFFF",borderLeft:"1px solid #ccc",borderRight:"1px solid #ccc"}}>
                                                <div className="panel panel-default" style={{marginLeft:"-15px",marginRight:"-15px",border:"0px none"}}>
                                                    <div className="panel-body" style={{height:(pageHeight-177+"px"),border:"0px none"}}>
                                                        <div className="row"  style={{marginTop:"-15px"}}>
                                                            <div className="row" style={{height:"40px",marginLeft:"0px",marginRight:"0px"}}>
                                                                <table  className="table table-hover table-condensed" >
                                                                    <thead  style={{display:"block"}}>
                                                                    <tr style={{height:"40px",backgroundColor:"#F5F5F5",display:"block", width:"100%",tableLayout:"fixed",borderBottom:"1px solid #ccc"}}>
                                                                        <th style={{width:((((propswidth-250)/12*9)-23-120)*0.45+"px"),marginLeft:"5px",border:"0px none"}}>Feature Name</th>
                                                                        <th style={{width:((((propswidth-250)/12*9)-23-120)*0.55+"px"),border:"0px none"}}>Feature Content</th>
                                                                        <th style={{width:"120px",border:"0px none"}}>Feature Type</th>
                                                                    </tr>
                                                                    </thead>
                                                                </table>
                                                            </div>

                                                            <div className="row" style={{height:(pageHeight-214+"px"),overflowY:"auto",marginTop:"0px",marginLeft:"0px",marginRight:"0px"}}>
                                                                <table  className="table table-hover table-condensed">
                                                                    <tbody style={{display:"block",marginTop:"0px"}}>
                                                                    {fdetlsit}
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-sm-3" style={{height:(pageHeight-177+"px"),backgroundColor:"#FFFFFF",border:"0px none"}}>
                                                <div className="panel panel-default" style={{marginLeft:"-15px",marginRight:"-15px",border:"0px none"}}>
                                                    <div className="panel-heading" style={{height:"40px"}}>
                                                        <span className="wraps"><strong>DataFeatures Used</strong></span>
                                                    </div>
                                                    <div className="panel-body" style={{height:(pageHeight-217+"px"),overflowY:"auto"}}>
                                                        {sslist}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div  role="tabpanel" className="tab-pane " id="participles">
                                <div id="wrapper" >
                                    <div id="sidebar-wrapper" style={{height:(pageHeight-177+"px"),width:"250px",borderTop:"1px solid #ccc"}}>
                                        <ul className="sidebar-nav" >
                                            <li className="sidebar-brand" style={{border:"0px none"}}>
                                                <a >
                                                    Tokens List
                                                </a>

                                            </li>
                                            <div className="row" style={{height:"55px",borderBottom:"1px solid #ccc"}}>
                                                <div className="col-sm-11 col-sm-offset-1" style={{marginBottom:"10px"}}>
                                                    <button className="btn btn-default zzlbuttoncolborder" role="button"
                                                            onClick={this.submitAddParticiplef}
                                                            style={{height:"35px"}}
                                                            disabled={dictadminDisable}
                                                        >
                                                        Add to Project
                                                    </button>
                                                </div>
                                            </div>
                                            <div style={{height:(pageHeight-282+"px"),overflow:"auto"}}>
                                                {pleflist}
                                            </div>

                                        </ul>
                                    </div>


                                    <div id="page-content-wrapper" style={{height:(pageHeight-177+"px"),backgroundColor:"#FFFFFF",border:"0px none"}}>
                                        <div className="row" style={{marginTop:"-20px",marginLeft:"-20px",marginRight:"-20px",height:(pageHeight-177+"px")}}>
                                            <div className="col-sm-9" style={{backgroundColor:"#FFFFFF",borderTop:"1px solid #ccc",borderLeft:"1px solid #ccc",borderRight:"1px solid #ccc"}}>
                                                <div className="panel panel-default" style={{marginLeft:"-15px",marginRight:"-15px",border:"0px none"}}>
                                                    <div className="panel-body" style={{height:(pageHeight-177+"px"),border:"0px none"}}>
                                                        <div className="row"  style={{marginTop:"-15px"}}>
                                                            <div className="row" style={{height:(pageHeight-174+"px"),overflowY:"auto",marginTop:"0px",marginLeft:"0px",marginRight:"0px"}}>
                                                                {pletlsit}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-sm-3" style={{height:(pageHeight-177+"px"),backgroundColor:"#FFFFFF",border:"0px none"}}>
                                                <div className="panel panel-default" style={{marginLeft:"-15px",marginRight:"-15px",border:"0px none"}}>
                                                    <div className="panel-heading" style={{height:"40px"}}>
                                                        <span className="wraps"><strong>Tokens Used</strong></span>
                                                    </div>
                                                    <div className="panel-body" style={{height:(pageHeight-217+"px"),overflowY:"auto"}}>
                                                        {pleplist}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div  role="tabpanel" className="tab-pane " id="suorces">
                                <div id="wrapper" >
                                    <div id="sidebar-wrapper" style={{height:(pageHeight-177+"px"),borderTop:"1px solid #ddd"}}>
                                        <ul className="sidebar-nav" >
                                            <li className="sidebar-brand" style={{borderBottom:"1px solid #ddd"}}>
                                                <a >
                                                    Data Source List
                                                </a>

                                            </li>
                                            <li style={{textIndent:"20px",lineHeight:"60px"}}>
                                                <a
                                                    onClick={this.getsoureclick(parseInt(0))}
                                                    ><span
                                                    style={selectSourceStylel[0]}
                                                    >√</span>
                                                    <span style={{color:selectcolor[0]}}>
                                                        ODS Metadata
                                                    </span>

                                                </a>
                                            </li>
                                            <li style={{textIndent:"20px",lineHeight:"60px"}}>
                                                <a
                                                    onClick={this.getsoureclick(parseInt(1))}
                                                    ><span
                                                    style={selectSourceStylel[1]}
                                                    >√</span>
                                                     <span style={{color:selectcolor[1]}}>
                                                        BDS Metadata
                                                    </span>

                                                </a>
                                            </li>
                                            <li style={{textIndent:"20px",lineHeight:"60px"}}>
                                                <a
                                                    onClick={this.getMetadatasoureclick(parseInt(0))}
                                                    ><span
                                                    style={selectSourceStylel[2]}
                                                    >√</span>
                                                     <span style={{color:selectcolor[2]}}>
                                                         ODS Raw Data
                                                    </span>

                                                </a>
                                            </li>
                                            <li style={{textIndent:"20px",lineHeight:"60px"}}>
                                                <a
                                                    onClick={this.getMetadatasoureclick(parseInt(1))}
                                                    ><span
                                                    style={selectSourceStylel[3]}
                                                    >√</span>
                                                    <span style={{color:selectcolor[3]}}>
                                                         BDS Raw Data
                                                    </span>

                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div id="page-content-wrapper" style={{height:(pageHeight-177+"px"),backgroundColor:"#FFFFFF"}}>
                                        <div className="row" style={{marginTop:"-20px",marginLeft:"-20px",marginRight:"-20px",height:(pageHeight-177+"px")}}>
                                            <div style={ifmetaStylel}>
                                                <div className="panel panel-default" >
                                                    <div className="panel-heading" style={{height:"50px"}}>
                                                        <div className="row" >
                                                            <div className="col-sm-6" >
                                                                <div style={{float:"left"}}>
                                                                    <button
                                                                        className="btn btn-default  glyphicon glyphicon-search zzlbuttoncolborder"
                                                                        onClick={this.getsearchtableclick}
                                                                        style={{height:"30px",float:"left"}}
                                                                        >
                                                                    </button>
                                                                </div>
                                                                <div style={{marginLeft:"10px",float:"left"}}>
                                                                    <input type="text"
                                                                           className="form-control zzlbuttoncolborder"
                                                                           value={this.state.metadatatext}
                                                                           onChange={this.metadataChange}
                                                                           onKeyPress={this.keysourcesearch}
                                                                           style={{height:"30px",float:"left"}}
                                                                           placeholder="Search"/>
                                                                </div>
                                                            </div>
                                                            <div className="col-sm-6" style={{textAlign:"right"}}>
                                                                <div className="btn-group" role="group" >
                                                                    <button className="btn btn-default zzlbuttoncolborder"
                                                                            role="button"
                                                                            onClick={this.importodsbdsClick}
                                                                            style={{width:"150px"}}
                                                                            disabled={sadminDisablesss}
                                                                        >
                                                                        Import Metadata
                                                                    </button>
                                                                    </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="panel-body table-cont"
                                                         id='table-cont'
                                                         style={{height:(pageHeight-277+"px")}}
                                                         onScroll={this.tablecontclick('table-cont')}
                                                        >
                                                        {ylist}
                                                    </div>
                                                    <div className="panel-footer" style={{height:"50px"}}>
                                                        <div className="row" style={{marginTop:"-15px",height:"15px"}}>
                                                            <div style={barStyle}>
                                                                <div className="progress" style={{height:"15px"}}>
                                                                    <div className="progress-bar progress-bar-success" role="progressbar"  aria-valuemin="0" aria-valuemax="100" style={{width:timebar+"%",minWidth:"2em"}}>
                                                                        {timebar}%
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="row">
                                                            <div className="col-sm-9" style={{textAlign:"left"}}>
                                                                <nav aria-label="Page navigation" style={{height:"40px"}}>
                                                                    <ul className="pagination" style={{marginTop:"0px"}}>
                                                                        {pageButtonGrp}
                                                                    </ul>
                                                                </nav>
                                                            </div>
                                                            <div className="col-sm-3"  style={{textAlign:"right"}}>
                                                                <button className="btn btn-default zzlbuttoncolborder"
                                                                        role="button"
                                                                        onClick={this.clearDateBeforRun}
                                                                        style={{width:"150px"}}
                                                                        disabled={sadminDisableStyle}
                                                                    >
                                                                    Run
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div style={ifmetaStyle}>
                                                <div className="panel panel-default" >
                                                    <div className="panel-heading" style={{height:"50px"}}>
                                                        <div className="row" >
                                                            <div className="col-sm-4">
                                                                <div className="input-group">
                                                                    <span className="input-group-addon" style={{backgroundColor:"#337ab7",height:"30px",color:"#ffffff"}}><span className="glyphicon glyphicon-search" aria-hidden="true"></span></span>
                                                                    <input type="text"
                                                                           className="form-control zzlbuttoncolborder"
                                                                           value={this.state.metadatatext}
                                                                           onChange={this.metadataChange}
                                                                           style={{height:"30px"}}
                                                                           placeholder="Search by En/Ch Name"/>
                                                                </div>
                                                            </div>
                                                            <div className="col-sm-8" style={{textAlign:"right"}}>
                                                                <div className="btn-group" role="group" >
                                                                    <button className="btn btn-default zzlbuttoncolborder"
                                                                            role="button"
                                                                            onClick={this.importMetadataClick}
                                                                           // style={{width:"150px"}}
                                                                        >
                                                                        Import Raw Data
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="panel-body" style={{height:(pageHeight-227+"px")}}>
                                                        {metalist}
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div  role="tabpanel" className="tab-pane " id="resultd">
                                <div id="wrapper" >
                                    <div id="sidebar-wrapper" style={{height:(pageHeight-177+"px"),borderTop:"1px solid #ddd"}}>
                                        <ul className="sidebar-nav" >
                                            <li className="sidebar-brand" style={{borderBottom:"1px solid #ddd"}}>
                                                <a >
                                                    Feature Result List
                                                </a>

                                            </li>
                                            <li style={{textIndent:"20px",lineHeight:"60px"}}>
                                                <a
                                                    onClick={this.getsoureresultclick(parseInt(0))}
                                                    ><span
                                                    style={selectSourceStylell[0]}

                                                    >√</span>
                                                     <span style={{color:selectcolorss[0]}}>
                                                         ODS Feature Result
                                                    </span>

                                                </a>
                                            </li>
                                            <li style={{textIndent:"20px",lineHeight:"60px"}}>
                                                <a
                                                    onClick={this.getsoureresultclick(parseInt(1))}
                                                    ><span
                                                    style={selectSourceStylell[1]}

                                                    >√</span>
                                                     <span style={{color:selectcolorss[1]}}>
                                                         BDS Feature Result
                                                    </span>

                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div id="page-content-wrapper" style={{height:(pageHeight-177+"px"),backgroundColor:"#FFFFFF"}}>
                                        <div className="row" style={{marginTop:"-20px",marginLeft:"-20px",marginRight:"-20px",height:(pageHeight-172+"px")}}>
                                            <div className="panel panel-default" >
                                                <div className="panel-heading" style={{height:"50px"}}>
                                                    <div className="row" >
                                                        <div className="col-sm-6 " >
                                                            <div style={{float:"left"}}>
                                                                <button
                                                                    className="btn btn-default glyphicon glyphicon-search zzlbuttoncolborder"
                                                                    onClick={this.getsearchclick}
                                                                    style={{height:"30px",float:"left"}}
                                                                    >
                                                                </button>
                                                            </div>
                                                            <div style={{marginLeft:"10px",float:"left"}}>
                                                                <input type="text"
                                                                       className="form-control zzlbuttoncolborder"
                                                                       value={this.state.metadatatext}
                                                                       onChange={this.metadataChange}
                                                                       onKeyPress={this.keyresultsearch}
                                                                       style={{height:"30px",float:"left"}}
                                                                       placeholder="Search"/>
                                                            </div>
                                                        </div>
                                                        <div className="col-sm-6" style={{textAlign:"right"}}>

                                                            <button className="btn btn-default zzlbuttoncolborder" role="button"
                                                                    onClick={this.openNewlick}
                                                                  //  disabled={ifbaddisableStyle}
                                                                >
                                                                Schema Mapping
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="panel-body table-cont"
                                                     id='resulttable'
                                                     style={{height:(pageHeight-277+"px")}}
                                                     onScroll={this.tablecontclick('resulttable')}
                                                    >
                                                    {zzllist}
                                                </div>
                                                <div className="panel-footer" style={{height:"50px"}}>
                                                    <div className="row">
                                                        <div className="col-sm-12" style={{textAlign:"left"}}>
                                                            <nav aria-label="Page navigation" style={{height:"40px"}}>
                                                                <ul className="pagination" style={{marginTop:"0px"}}>
                                                                    {pageButtonGrps}
                                                                </ul>
                                                            </nav>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="modal fade" id="confirmdata" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h4 className="modal-title">Resolve the conflict</h4>
                            </div>
                            <div className="modal-body" style={{height:(200+"px"),overflowY:"auto"}}>
                                {qlist}
                            </div>
                            <div className="modal-footer">
                                <a type="button" className="btn btn-primary" onClick={this.confirmdataadd}>Confirm</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="modal fade" id="totamrresultdata" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h4 className="modal-title">Import Feature Result to Tamr</h4>
                            </div>
                            <div className="modal-body">
                               <div className="row">
                                   <div className="col-sm-10 col-sm-offset-1">
                                           <div className="row">
                                               <label className="col-sm-2   control-label" ><h5 style={{marginLeft:"5px"}}>FileName:</h5></label>
                                               <div className="col-sm-10" >
                                                   <input type="text"  className="form-control" id="covernameeedit" style={{marginLeft:"-10px"}}
                                                          value={this.state.covername}
                                                          onChange={this.inputCoverName}
                                                       />
                                               </div>
                                           </div>
                                       <div className="row" style={{marginTop:"15px"}}>
                                           <div className="col-sm-1 ">
                                               <input type="checkbox"
                                                      style={{marginLeft:"5px"}}
                                                      id="checkcovered"
                                                      onChange={this.checkCoverClick}
                                                   />
                                           </div>
                                           <div className="col-sm-6 col-sm-offset-1">
                                               <span style={{marginLeft:"-10px"}}>Whether or not covered</span>
                                           </div>
                                       </div>
                                   </div>
                                   <div className="col-sm-10 col-sm-offset-1">
                                       {zklist}
                                   </div>
                               </div>
                            </div>
                            <div className="modal-footer">
                                <a type="button" className="btn btn-default" onClick={this.ToTamrCancel}>Cancel</a>
                                <a type="button" className="btn btn-primary" onClick={this.ToTamrResultData}>Confirm</a>
                            </div>
                        </div>
                    </div>

                </div>
                <div className="modal fade" id="viewmetadata" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" >
                    <div className="modal-dialog modal-lg">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h4 className="modal-title">View MetaData</h4>
                            </div>
                            <div className="modal-body" style={{height:(450+"px"),overflow:"auto"}}>
                                {viewmeta}
                            </div>
                            <div className="modal-footer">
                                <a type="button" className="btn btn-primary" data-dismiss="modal">Confirm</a>
                            </div>
                        </div>
                    </div>

                </div>
                <div className="modal fade" id="searchzzldata" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div className="modal-dialog modal-lg">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h4 className="modal-title">View SearchData</h4>
                            </div>
                            <div className="modal-body"
                                 id="searchresultdata"
                                 style={{height:(600+"px"),overflow:"auto"}}
                                 onScroll={this.tablecontclick('searchresultdata')}
                                >
                                {zzlslist}
                            </div>
                            <div className="modal-footer">
                                <div className="row">
                                    <div className="col-sm-10" style={{textAlign:"left"}}>
                                        <nav aria-label="Page navigation" style={{height:"40px"}}>
                                            <ul className="pagination" style={{marginTop:"0px"}}>
                                                {pageButtonGrpch}
                                            </ul>
                                        </nav>

                                    </div>
                                    <div className="col-sm-2">
                                        <a type="button" className="btn btn-primary" data-dismiss="modal">Confirm</a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
                <div className="modal fade" id="searchzzltabledata" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
                    <div className="modal-dialog modal-lg">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h4 className="modal-title">View SearchData</h4>
                            </div>
                            <div className="modal-body"
                                 id="searchtabledata"
                                 style={{height:(600+"px"),overflow:"auto"}}
                                 onScroll={this.tablecontclick('searchtabledata')}
                                >
                                {zzlslistbl}
                            </div>
                            <div className="modal-footer">
                                <div className="row">
                                    <div className="col-sm-10" style={{textAlign:"left"}}>
                                        <nav aria-label="Page navigation" style={{height:"40px"}}>
                                            <ul className="pagination" style={{marginTop:"0px"}}>
                                                {pageButtonGrpbl}
                                            </ul>
                                        </nav>

                                    </div>
                                    <div className="col-sm-2">
                                        <a type="button" className="btn btn-primary" data-dismiss="modal">Confirm</a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
                <div className="modal fade" id="deleteprodict" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                                <h4 className="modal-title" >Confirm the delete</h4>
                            </div>
                            <div className="modal-body">
                                <h3>
                                    Are you sure to delete this ProjectDict?
                                </h3>

                            </div>
                            <div className="modal-footer">
                                <a type="button" className="btn btn-default" data-dismiss="modal">Cancel</a>
                                <a type="button"
                                   onClick={this.deleteProdictComf}
                                   className="btn btn-primary" >Confirm</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="modal fade" id="deleteprofeano" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                                <h4 className="modal-title" >Confirm the delete</h4>
                            </div>
                            <div className="modal-body">
                                <h3>
                                    Are you sure to delete this ProjectFeature?
                                </h3>

                            </div>
                            <div className="modal-footer">
                                <a type="button" className="btn btn-default" data-dismiss="modal">Cancel</a>
                                <a type="button"
                                   onClick={this.deleteProfeanoComf}
                                   className="btn btn-primary" >Confirm</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="modal fade" id="deletepartpleno" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                                <h4 className="modal-title" >Confirm The Delete</h4>
                            </div>
                            <div className="modal-body">
                                <h3>
                                    Are you sure to delete this Token?
                                </h3>

                            </div>
                            <div className="modal-footer">
                                <a type="button" className="btn btn-default" data-dismiss="modal">Cancel</a>
                                <a type="button"
                                   onClick={this.deletePartPleComf}
                                   className="btn btn-primary" >Confirm</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="modal fade" id="datanull" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                                <h4 className="modal-title" >{this.state.ifdicname}</h4>
                            </div>

                            <div className="modal-body">
                                <h4 className="modal-title" >This dictionary may be empty.If not,please check the required entries!</h4>
                            </div>

                            <div className="modal-footer">
                                <a type="button" className="btn btn-default" data-dismiss="modal">Confirm</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="modal fade" id="exsitprodict" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                                <h4 className="modal-title"> {this.state.ifdicname}</h4>
                            </div>

                            <div className="modal-body">
                                <h5 className="modal-title" >Already exsit! Please go to delete this ProjectDict first on your right side!</h5>
                            </div>

                            <div className="modal-footer">
                                <a type="button" className="btn btn-default" data-dismiss="modal">Confirm</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="modal fade" id="metadatanull" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                                <h4 className="modal-title" >Error Msg</h4>
                            </div>

                            <div className="modal-body">
                                <h4 className="modal-title" >Search cannot be empty. Please enter a search content!</h4>
                            </div>

                            <div className="modal-footer">
                                <a type="button" className="btn btn-default" data-dismiss="modal">Confirm</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="modal fade" id="exsitfeature" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                                <h4 className="modal-title" >{this.state.thisfdefnname}</h4>
                            </div>

                            <div className="modal-body">
                                <h5 className="modal-title" >Already exsit! Please go to delete this feature first on your right side!</h5>
                            </div>

                            <div className="modal-footer">
                                <a type="button" className="btn btn-default" data-dismiss="modal">Confirm</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="modal fade" id="exsitpartple" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                                <h4 className="modal-title" >{this.state.currentPleFileName}</h4>
                            </div>

                            <div className="modal-body">
                                <h5 className="modal-title" >Already exsit! Please go to delete this token first on your right side!</h5>
                            </div>

                            <div className="modal-footer">
                                <a type="button" className="btn btn-default" data-dismiss="modal">Confirm</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="modal fade" id="importmetadata" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h4 className="modal-title" >Import Metadata</h4>
                            </div>
                            <div className="modal-body">
                                <div className="row">
                                    <label className="col-sm-2 col-sm-offset-2  control-label" ><h5>Method:</h5></label>
                                    <div className="col-sm-6">
                                        <select className="form-control"
                                                value={this.state.setmethod}
                                                onChange={this.setmethodChange}
                                            >

                                            {setmethodSelect}
                                        </select>

                                    </div>
                                </div>
                                <div style={ifLocalFtpStyle}>
                                    <div className="row">
                                        <label className="col-sm-2 col-sm-offset-2  control-label" ><h5>Host:</h5></label>
                                        <div className="col-sm-6">
                                            <input type="text" className="form-control" placeholder=""
                                                  onChange={this.handleChange.bind(this,"Uploadhost")} value={this.state.Uploadhost}
                                                />
                                        </div>
                                    </div>
                                    <div className="row">
                                        <label className="col-sm-2 col-sm-offset-2  control-label" ><h5>Port:</h5></label>
                                        <div className="col-sm-6">
                                            <input type="text" className="form-control" placeholder=""
                                                  onChange={this.handleChange.bind(this,"Uploadport")} value={this.state.Uploadport}
                                                />
                                        </div>
                                    </div>
                                    <div className="row">
                                        <label className="col-sm-2 col-sm-offset-2  control-label" ><h5>Username:</h5></label>
                                        <div className="col-sm-6">
                                            <input type="text" className="form-control" placeholder=""
                                                  onChange={this.handleChange.bind(this,"Uploaduser")} value={this.state.Uploaduser}
                                                />
                                        </div>
                                    </div>
                                    <div className="row">
                                        <label className="col-sm-2 col-sm-offset-2  control-label" ><h5>Password:</h5></label>
                                        <div className="col-sm-6">
                                            <input type="password" className="form-control" placeholder=""
                                                  onChange={this.handleChange.bind(this,"Uploadpass")} value={this.state.Uploadpass}
                                                />
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <label className="col-sm-2 col-sm-offset-2  control-label" ><h5>Path:</h5></label>
                                    <div className="col-sm-6">
                                        <input type="text" className="form-control" placeholder=""
                                              onChange={this.handleChange.bind(this,"Uploadpath")} value={this.state.Uploadpath}
                                            />
                                    </div>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <div className="row">
                                    <div className="col-md-8">
                                        <div style={ifemptystyle}>
                                            <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                Each entry can not be empty.
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-4">
                                        <a type="button" className="btn btn-default" data-dismiss="modal">Cancel</a>
                                        <a type="button" className="btn btn-primary" onClick={this.importmdClick}>Import</a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
                <div className="modal fade" id="importodsbdszzdf" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                                <h4 className="modal-title" >Error Msg</h4>
                            </div>

                            <div className="modal-body">
                               <span>{this.state.errormsg}</span>
                            </div>

                            <div className="modal-footer">
                                <a type="button" className="btn btn-default" data-dismiss="modal">Confirm</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="modal fade bs-example-modal-lg" id="importodsbds" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div className="modal-dialog modal-lg">
                        <div className="modal-content">
                            <div className="modal-header" style={{backgroundColor:"#ddd"}}>
                                <div className="row">
                                    <div className="col-md-8">
                                        <h4 className="modal-title" id="myModalLabel">Metadata File Configuration And Import</h4>
                                    </div>
                                    <div className="col-md-3">
                                        <a type="button" className="btn btn-primary"
                                           onClick={this.SaveSelectClick}
                                            >保存模板</a>
                                    </div>
                                    <div className="col-md-1">
                                        <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    </div>
                                </div>
                            </div>
                            <div className="modal-body" >
                                <div className="row" style={{marginTop:"0px"}}>
                                    <div className="col-md-6">
                                        <div className="row" style={{backgroundColor:"#ddd",marginLeft:"0px",marginRight:"0px"}}>
                                            <div className="row" style={{marginLeft:"0px",marginRight:"0px"}}>
                                                <label className="col-sm-4   control-label" style={{textAlign:"right"}}><h5>模版名称:</h5></label>
                                                <div className="col-sm-8" >
                                                    <div style={{position:"relative"}}>
                                                        <span style={{marginLeft:"100px",width:"18px",overflow:"hidden"}}>
                                                            <select style={{width:"118px",marginLeft:"-100px",height:"30px"}}
                                                                   // onchange="this.parentNode.nextSibling.value=this.value"
                                                                    value={this.state.tpt_name}
                                                                    onChange={this.optfileHandleChange}
                                                                >
                                                                <option value="-输入或选择-" style={{color:"#ddd"}}>
                                                                    -输入或选择-
                                                                </option>
                                                                {this.state.dataopts}
                                                            </select>
                                                        </span>
                                                        <input type="text"   placeholder="-输入或选择-"  style={{width:"100px",position:"absolute",left:"0px",height:"30px"}}
                                                               onChange={this.fileHandleChange.bind(this,"tpt_name")}
                                                               value={this.state.tpt_name}
                                                            />
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row" style={{marginLeft:"0px",marginRight:"0px"}}>
                                                <label className="col-sm-4   control-label" style={{textAlign:"right"}}><h5>英文表名:</h5></label>
                                                <div className="col-sm-8" >
                                                    <input type="text" className="form-control"
                                                         onChange={this.fileHandleChange.bind(this,"en_table_name")}
                                                         value={this.state.en_table_name}
                                                        />

                                                </div>
                                            </div>
                                            <div className="row" style={{marginLeft:"0px",marginRight:"0px"}}>
                                                <label className="col-sm-4   control-label" style={{textAlign:"right"}}><h5>英文列名:</h5></label>
                                                <div className="col-sm-8" >
                                                    <input type="text" className="form-control"
                                                         onChange={this.fileHandleChange.bind(this,"en_col_name")}
                                                         value={this.state.en_col_name}
                                                        />

                                                </div>
                                            </div>
                                            <div className="row" style={{marginLeft:"0px",marginRight:"0px"}}>
                                                <label className="col-sm-4   control-label" style={{textAlign:"right"}}><h5>本地表名:</h5></label>
                                                <div className="col-sm-8" >
                                                    <input type="text" className="form-control"
                                                         onChange={this.fileHandleChange.bind(this,"la_table_name")}
                                                         value={this.state.la_table_name}
                                                        />

                                                </div>
                                            </div>
                                            <div className="row" style={{marginLeft:"0px",marginRight:"0px"}}>
                                                <label className="col-sm-4   control-label" style={{textAlign:"right"}}><h5>本地列名:</h5></label>
                                                <div className="col-sm-8" >
                                                    <input type="text" className="form-control"
                                                         onChange={this.fileHandleChange.bind(this,"la_col_name")}
                                                         value={this.state.la_col_name}
                                                        />

                                                </div>
                                            </div>
                                            <div className="row" style={{marginLeft:"0px",marginRight:"0px"}}>
                                                <label className="col-sm-4   control-label" style={{textAlign:"right"}}><h5>是否分词:</h5></label>
                                                <div className="col-sm-8" >
                                                    <input type="text" className="form-control"
                                                         onChange={this.fileHandleChange.bind(this,"token_bit")}
                                                         value={this.state.token_bit}
                                                        />

                                                </div>
                                            </div>
                                            <div className="row" style={{marginLeft:"0px",marginRight:"0px"}}>
                                                <label className="col-sm-4   control-label" style={{textAlign:"right"}}><h5>数据类型:</h5></label>
                                                <div className="col-sm-8" >
                                                    <input type="text" className="form-control"
                                                         onChange={this.fileHandleChange.bind(this,"col_data_type")}
                                                         value={this.state.col_data_type}
                                                        />

                                                </div>
                                            </div>
                                            <div className="row" style={{marginLeft:"0px",marginRight:"0px"}}>
                                                <label className="col-sm-4   control-label" style={{textAlign:"right"}}><h5>数据类型描述:</h5></label>
                                                <div className="col-sm-8" >
                                                    <input type="text" className="form-control"
                                                         onChange={this.fileHandleChange.bind(this,"col_des")}
                                                         value={this.state.col_des}
                                                        />

                                                </div>
                                            </div>
                                            <div className="row" style={{marginLeft:"0px",marginRight:"0px"}}>
                                                <label className="col-sm-4   control-label" style={{textAlign:"right"}}><h5>表周期:</h5></label>
                                                <div className="col-sm-8" >
                                                    <input type="text" className="form-control"
                                                         onChange={this.fileHandleChange.bind(this,"table_cycle")}
                                                         value={this.state.table_cycle}
                                                        />

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-6" >
                                        <div className="row" style={{backgroundColor:"#ddd",marginLeft:"0px",marginRight:"0px"}}>
                                            <div className="row" style={{marginLeft:"0px",marginRight:"0px"}}>
                                                <label className="col-sm-4 control-label" style={{textAlign:"right"}}><h5>文件选择:</h5></label>
                                                <div className="col-sm-8">
                                                    <div className="col-sm-6" style={{marginLeft:"-15px"}}>
                                                        <a className="btn btn-primary"
                                                           type="buton"
                                                           onClick={this.confirmimportdata}
                                                            >
                                                    <span>
                                                        Choose files <span style={zzlselectsStyle} className="badge">{this.state.fileNum}</span>
                                                    </span>
                                                        </a>
                                                    </div>
                                                    <div className="col-sm-6" style={{textAlign:"right"}}>
                                                        <div style={zzlselectsStyle}>
                                                            <a className="btn btn-default"
                                                               type="buton"
                                                                //style={{marginLeft:"5px"}}
                                                               onClick={this.ClearSelectFilesClick}
                                                                >
                                                        <span>
                                                            Clear
                                                         </span>
                                                            </a>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <div className="row" style={{marginLeft:"0px",marginRight:"0px"}}>
                                                <label className="col-sm-4   control-label" style={{textAlign:"right"}}><h5>表描述:</h5></label>
                                                <div className="col-sm-8" >
                                                    <input type="text" className="form-control"
                                                           onChange={this.fileHandleChange.bind(this,"table_des")}
                                                           value={this.state.table_des}
                                                        />

                                                </div>
                                            </div>
                                            <div className="row" style={{marginLeft:"0px",marginRight:"0px"}}>
                                                <label className="col-sm-4   control-label" style={{textAlign:"right"}}><h5>字段顺序:</h5></label>
                                                <div className="col-sm-8" >
                                                    <input type="text" className="form-control"
                                                         onChange={this.fileHandleChange.bind(this,"order_seq")}
                                                         value={this.state.order_seq}
                                                        />

                                                </div>
                                            </div>
                                            <div className="row" style={{marginLeft:"0px",marginRight:"0px"}}>
                                                <label className="col-sm-4   control-label" style={{textAlign:"right"}}><h5>表编号:</h5></label>
                                                <div className="col-sm-8" >
                                                    <input type="text" className="form-control"
                                                         onChange={this.fileHandleChange.bind(this,"codename")}
                                                         value={this.state.codename}
                                                        />

                                                </div>
                                            </div>
                                            <div className="row" style={{marginLeft:"0px",marginRight:"0px"}}>
                                                <label className="col-sm-4   control-label" style={{textAlign:"right"}}><h5>扩展字段1:</h5></label>
                                                <div className="col-sm-8" >
                                                    <input type="text" className="form-control"
                                                         onChange={this.fileHandleChange.bind(this,"ext1_name")}
                                                         value={this.state.ext1_name}
                                                        />

                                                </div>
                                            </div>
                                            <div className="row" style={{marginLeft:"0px",marginRight:"0px"}}>
                                                <label className="col-sm-4   control-label" style={{textAlign:"right"}}><h5>扩展字段2:</h5></label>
                                                <div className="col-sm-8" >
                                                    <input type="text" className="form-control"
                                                         onChange={this.fileHandleChange.bind(this,"ext2_name")}
                                                         value={this.state.ext2_name}
                                                        />

                                                </div>
                                            </div>
                                            <div className="row" style={{marginLeft:"0px",marginRight:"0px"}}>
                                                <label className="col-sm-4   control-label" style={{textAlign:"right"}}><h5>扩展字段3:</h5></label>
                                                <div className="col-sm-8" >
                                                    <input type="text" className="form-control"
                                                         onChange={this.fileHandleChange.bind(this,"ext3_name")}
                                                         value={this.state.ext3_name}
                                                        />

                                                </div>
                                            </div>
                                            <div className="row" style={{marginLeft:"0px",marginRight:"0px"}}>
                                                <label className="col-sm-4   control-label" style={{textAlign:"right"}}><h5>扩展字段4:</h5></label>
                                                <div className="col-sm-8" >
                                                    <input type="text" className="form-control"
                                                         onChange={this.handleChange.bind(this,"ext4_name")}
                                                         value={this.state.ext4_name}
                                                        />

                                                </div>
                                            </div>
                                            <div className="row" style={{marginLeft:"0px",marginRight:"0px"}}>
                                                <div className="row" style={{marginLeft:"0px",marginRight:"0px"}}>
                                                    <div className="col-sm-11 col-sm-offset-1">
                                                        <p><span>是否需要英文表名列名合法性校验</span></p>

                                                    </div>
                                                </div>
                                                <div className="row" style={{marginLeft:"0px",marginRight:"0px"}}>
                                                    <div className="col-sm-8 col-sm-offset-4">
                                                        <input type="radio" value="0" name="check_redio" id="ckeckzd1" />校验
                                                        <input type="radio" value="1" name="check_redio" id="ckeckzd2" />不校验
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div className="modal-footer" style={{backgroundColor:"#ddd"}}>
                                <div className="col-md-8">
                                    <div style={fileupdateStyle}>
                                        <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                            Can't be empty,please select files to upload!
                                        </div>
                                    </div>

                                    <div style={ifseterStyle}>
                                        <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                            模板名称不能为空!
                                        </div>
                                    </div>
                                    <div style={ifseterddStyle}>
                                        <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                            英文表名不能为空!
                                        </div>
                                    </div>
                                    <div style={ifseterssStyle}>
                                        <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                            英文列名不能为空!
                                        </div>
                                    </div>
                                    <div style={errormsgStyle}>
                                        <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                            {this.state.errormsg}
                                        </div>
                                    </div>
                                    <div style={savasuccessStyle}>
                                        <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                           Save Success !
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <a type="button" className="btn btn-default" data-dismiss="modal">Cancel</a>
                                    <a type="button" className="btn btn-primary" onClick={this.importFiles}>Import</a>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
                <div className="modal fade" id="fedetailNoData" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                                <h4 className="modal-title" >{this.state.thisfdefnname}</h4>
                            </div>

                            <div className="modal-body">
                                <h4 className="modal-title" >This FeatureTable is empty!</h4>
                            </div>

                            <div className="modal-footer">
                                <a type="button" className="btn btn-default" data-dismiss="modal">Confirm</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="modal fade" id="partPleNoData" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                                <h4 className="modal-title" >{this.state.currentPleFileName}</h4>
                            </div>

                            <div className="modal-body">
                                <h4 className="modal-title" >This Token File is empty!</h4>
                            </div>

                            <div className="modal-footer">
                                <a type="button" className="btn btn-default" data-dismiss="modal">Confirm</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="modal fade" id="forbid" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                            </div>

                            <div className="modal-body">
                                The project is banned to add.
                            </div>

                            <div className="modal-footer">
                                <a type="button" className="btn btn-default" data-dismiss="modal">Confirm</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="modal fade" id="containsid" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                            </div>

                            <div className="modal-body">
                                Project for dictionary contains the dictionary all the entries, so don't need to add the dictionary!
                            </div>

                            <div className="modal-footer">
                                <a type="button" className="btn btn-default" data-dismiss="modal">Confirm</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="modal fade" id="deleteprodictno" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                            </div>

                            <div className="modal-body">
                                Delete this dictionary is prohibited!
                            </div>

                            <div className="modal-footer">
                                <a type="button" className="btn btn-default" data-dismiss="modal">Confirm</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        )
    }

});