/**
 * The module function:the module which administrator manage account .
 * Create time:2017/3/6
*/
import React from 'react';
import crypto from "crypto";
import {Comment} from "../comment";
import {IfDispatcher} from "../store/dispatcher";
import Underscore from "underscore";

export var Account = React.createClass({
    contextTypes: {
        router: React.PropTypes.object.isRequired,
    },

    getInitialState(){
        return{
            username:null,
            password:null,
            passwordzzl:null,
            currentpage:0,
            pagesize:16,
            pagenum:7,
            filtertext:"",
            iferror:false,
            ifduplicate:false,
            ifuserexist:false,
            userindex:null,
            password1st:"",
            password2nd:"",
            ifpassequal:true,
            thisname:"",
            //ifcheckac:"",
            ifpassmatchkinds:true,
            ifpassmatchlength:true,
            ifnamematchkinds:true,
            ifnamematchlength:true,
            ifpassmatchname:true,
            oldPassEqualNewPass:false,
           // thisuserpass:"",
            oldpassword:"",
            oldPassEqualoldPass:true,
            ifexistId:false,
            ifFourSame:false,
            ifTimeBt:false,
            ifemptyer:false,
            oldIfopt:false,
            timeinterval:false,
           // ifckecked:false,
        }
    },
    componentDidMount(){
        var loginInfo = this.props.loginInfo;
        if(!loginInfo.name){
            this.context.router.push("/fronpage");
            IfDispatcher.dispatch({
                actionType:Comment.LOGOUT
            });
        }else{
            this.getuserList();
        }

    },
    h(){
        $('#editUser').on('hide.bs.modal', function () {
            this.state.ifpassequal=true;
        })
     },

    /**
     *The function name: adminEditUser.
     *Function used: Pop-upwindow displays the information of a user and administrator can re-edit the user's information in it.
     *Create time: 2017/3/6
     */
    adminEditUser(id,vstate,name,password){
        var that = this;
        var inFunc = function(){
            that.state.userindex = id;
            //that.state.ifpassequal=true;
            //that.state.password1st="";
            //that.state.password2nd="";
            that.state.thisname=name;
           // that.state.thisuserpass=password;
            var check = false;
            if(vstate==1){
               // that.state.ifcheckac=true;
                check = true;
            }else{
               // that.state.ifcheckac=false;
                check = false;
            }
            $("#usernameedit").val(that.state.thisname);
            $("#checkactive").prop("checked",check);
           // $("#personnewpass1st").val("");
           // $("#personnewpass2nd").val("");
            if(that.props.loginInfo.userid === id){
                that.setState({
                    ifpassequal: true,
                    ifpassmatchkinds:true,
                    ifpassmatchlength:true,
                    ifpassmatchname:true,
                    oldPassEqualoldPass:true,
                    ifexistId:false,
                    ifFourSame:false,
                    ifTimeBt:false,
                    ifemptyer:false,
                    password1st:"",
                    password2nd:"",
                    oldpassword:"",
                    oldIfopt:true,
                    timeinterval:false,
                    //ifckecked:check,
                });
            }else{
                that.setState({
                    ifpassequal: true,
                    ifpassmatchkinds:true,
                    ifpassmatchlength:true,
                    ifpassmatchname:true,
                    oldPassEqualoldPass:true,
                    ifexistId:false,
                    ifFourSame:false,
                    ifTimeBt:false,
                    ifemptyer:false,
                    password1st:"",
                    password2nd:"",
                    oldpassword:"",
                    oldIfopt:false,
                    timeinterval:false,
                    //ifckecked:check,
                });
            }

            $("#adminEditUser").modal("show");
        };
        return inFunc;
    },

    /**
     *The function name: editConfirmlick.
     *Function used: API for sending request to update the information of account.
     *Create time: 2017/3/6
     */
    editConfirmlick(){
        var flag = true;
        if(!this.state.oldpassword){
            flag = false;
            this.setState({ifemptyer: true});
        }else if(this.state.password1st){
            if(this.state.password1st != this.state.password2nd){
                flag = false;
                this.setState({ifpassequal: false});
            }else if(this.state.password1st.length<6 || this.state.password1st.length>20){
                flag = false;
                this.setState({ifpassmatchlength:false});
            }else if(this.validatePassword(this.state.password1st)==1){
                flag = false;
                this.setState({ifpassmatchkinds:false});
            }
        }

        if(flag){
            var that = this;
            $("#ajaxloading").show();
            $.ajax({
                url: Comment.DATA_URL+"KDB/GetSalt",
                data: $.param({
                    username:that.state.thisname
                }),
                type: 'GET',
                contentType: 'application/x-www-form-urlencoded',
                success: function (data) {
                    var msg = data;
                    var salt1 = msg.data;
                    $.ajax({
                        url: Comment.DATA_URL+"KDB/GetSalt",
                        data: $.param({
                            username:that.props.loginInfo.name
                        }),
                        type: 'GET',
                        contentType: 'application/x-www-form-urlencoded',
                        success: function (data) {
                            var msg = data;
                            var salt2 = msg.data;
                            var ustate=0;
                            if($("#checkactive").get(0).checked){
                                ustate=1;
                            }
                           // alert("1");
                            var pass="";
                            if(that.state.password1st){
                                //var hash = crypto.createHash("md5");
                                //hash.update(this.state.password1st);
                                //pass=hash.digest("hex");
                                var keys = crypto.pbkdf2Sync(that.state.password1st, salt1, 10000, 16, 'sha512');
                                pass=keys.toString('hex');
                            }
                            var inputOldPass="";
                            if(that.state.oldpassword){
                                //var hashs = crypto.createHash("md5");
                                //hashs.update(this.state.oldpassword);
                                //inputOldPass=hashs.digest("hex");
                                var keys = crypto.pbkdf2Sync(that.state.oldpassword, salt2, 10000, 16, 'sha512');
                                inputOldPass=keys.toString('hex');
                            }
                           // alert("2");
                            $.ajax({
                                url:Comment.DATA_URL+"KDB/EditUser",
                                data: $.param({
                                    userId:that.state.userindex,
                                    optId:that.props.loginInfo.userid,
                                    state:ustate,
                                    password:pass,
                                    oldPassword:inputOldPass,
                                }),
                                type:'POST',
                                contentType: 'application/x-www-form-urlencoded',
                                success:function(data){
                                    $("#ajaxloading").hide();
                                    $("#adminEditUser").modal("hide");
                                    that.setState({
                                        ifckecked:ustate=1?true:false,
                                    })
                                    that.getuserList();
                                },
                                error:function () {
                                    $("#ajaxloading").hide();
                                },
                                statusCode:{
                                    406:function (){
                                        IfDispatcher.dispatch({
                                            actionType:Comment.LOGOUT
                                        });
                                    },
                                    414:function(){
                                        that.setState({
                                            ifexistId:true,
                                        })
                                    },
                                    415:function(){
                                        that.setState({
                                            ifTimeBt:true,
                                        })
                                    },
                                    413:function(){
                                        that.setState({
                                            ifFourSame:true,
                                        })
                                    },
                                    416:function(){
                                        that.setState({ifpassmatchname:false});
                                    },
                                    422:function(){
                                        that.setState({oldPassEqualoldPass:false});
                                    },
                                    429:function(){
                                        that.setState({timeinterval:true});
                                    }
                                }
                            });
                        },
                        error:function(){
                             $("#ajaxloading").hide();
                        },
                        statusCode:{
                            424:function(){
                                alert("It doesn't match the salt value.");
                            },
                        }
                    });

                },
                error:function(){
                     $("#ajaxloading").hide();
                },
                statusCode:{
                    424:function(){
                        alert("It doesn't match the salt value.");
                    },
                }
            });
        }
        //var flag=true;
        //if(true) {
        //    if (this.state.password1st != this.state.password2nd) {
        //        flag=false;
        //        this.setState({ifpassequal: false});
        //    }else if(this.state.password1st){
        //        //var inputOldPass="";
        //        //if(this.state.oldpassword){
        //        //    var hash = crypto.createHash("md5");
        //        //    hash.update(this.state.oldpassword);
        //        //    inputOldPass=hash.digest("hex");
        //        //}
        //        if(this.state.password1st.length<6 || this.state.password1st.length>20){
        //            flag=false;
        //            this.setState({ifpassmatchlength:false});
        //        }else if(this.validatePassword(this.state.password1st)==1){
        //            flag=false;
        //            this.setState({ifpassmatchkinds:false});
        //        }
        //        //else if(this.state.oldpassword==this.state.password1st){
        //        //    flag=false;
        //        //    this.setState({oldPassEqualNewPass:true});
        //        //}
        //        //else if(inputOldPass!=this.state.thisuserpass){
        //        //    flag=false;
        //        //    this.setState({oldPassEqualoldPass:false});
        //        //}
        //        else if(this.state.password1st==this.state.thisname){
        //            flag=false;
        //            this.setState({ifpassmatchname:false});
        //        }
        //    }
        //
        //    if(flag){
        //        var ustate=0;
        //        if($("#checkactive").get(0).checked){
        //            ustate=1;
        //        }
        //        var pass="";
        //        if(this.state.password1st){
        //            var hash = crypto.createHash("md5");
        //            hash.update(this.state.password1st);
        //            pass=hash.digest("hex");
        //        }
        //        var inputOldPass="";
        //        if(this.state.oldpassword){
        //            var hashs = crypto.createHash("md5");
        //            hashs.update(this.state.oldpassword);
        //            inputOldPass=hashs.digest("hex");
        //        }
        //        var that = this;
        //        $("#ajaxloading").show();
        //        $.ajax({
        //            url:Comment.DATA_URL+"KDB/EditUser",
        //            data: $.param({
        //                userId:that.state.userindex,
        //                optId:that.props.loginInfo.userid,
        //                state:ustate,
        //                password:pass,
        //                oldPassword:inputOldPass,
        //            }),
        //            type:'POST',
        //            contentType: 'application/x-www-form-urlencoded',
        //            success:function(data){
        //                $("#ajaxloading").hide();
        //                $("#adminEditUser").modal("hide");
        //                that.getuserList();
        //            },
        //            error:function () {
        //                $("#ajaxloading").hide();
        //            },
        //            statusCode:{
        //                406:function (){
        //                    IfDispatcher.dispatch({
        //                        actionType:Comment.LOGOUT
        //                    });
        //                },
        //                414:function(){
        //                    that.setState({
        //                        ifexistId:true,
        //                    })
        //                },
        //                415:function(){
        //                    that.setState({
        //                        ifTimeBt:true,
        //                    })
        //                },
        //                413:function(){
        //                    that.setState({
        //                        ifFourSame:true,
        //                    })
        //                },
        //            }
        //
        //        });
        //
        //    }
        //}
    },

    /**
     *The function name: getuserList.
     *Function used: API for getting the list of account.
     *Create time: 2017/3/6
     */
    getuserList(){
        $("#ajaxloading").show();
        $.ajax({
            url: Comment.DATA_URL+"KDB/GetAllUsers",
            type: 'GET',
            contentType: 'application/x-www-form-urlencoded',
            success: function(data){
                // alert("success");
                $("#ajaxloading").hide();
                var msg = data;
                IfDispatcher.dispatch({
                    actionType: Comment.GETUSERLIST,
                    userlist:msg.data
                });
            },
            error:function(){
                $("#ajaxloading").hide();
            },
            statusCode:{
                414:function(){
                },
                406:function (){
                    IfDispatcher.dispatch({
                        actionType:Comment.LOGOUT
                    });
                }
            }
        });
    },

    /**
     *The function name: firstpageclick.
     *Function used: The ability to jump to the first page.
     *Create time: 2017/3/6
     */
    firstpageclick(){
            this.setState({
                currentpage:0
            })

    },

    /**
     *The function name: leftpageclick.
     *Function used: The ability to jump to the previous page.
     *Create time: 2017/3/6
     */
    leftpageclick(){
        if(this.state.currentpage!=0){
            this.setState({
                currentpage:this.state.currentpage-1
            })
        }
    },

    /**
     *The function name: pnumclick.
     *Function used: The ability to jump to the specify page.
     *Create time: 2017/3/6
     */
    pnumclick(num){
        var that = this;
        var infun = function(){
            if(that.state.currentpage!=num){
                that.setState({
                    currentpage:num
                })
            }
        };
        return infun;
    },

    /**
     *The function name: rightpageclick.
     *Function used: The ability to jump to the next page.
     *Create time: 2017/3/6
     */
    rightpageclick(maxPageNum){
        var that = this;
        var infun = function () {
           if(that.state.currentpage!=maxPageNum-1){
                that.setState({
                    currentpage:that.state.currentpage+1
                })
           }
        };
        return infun;
    },

    /**
     *The function name: trailpageclick.
     *Function used: The ability to jump to the last page.
     *Create time: 2017/3/6
     */
    trailpageclick(maxpn){
        var that = this;
        var infun = function(){
            if(that.state.currentpage!=maxpn-1){
                that.setState({
                    currentpage:maxpn-1
                })
            }
        };
        return infun;
    },

    /**
     *The function name:filterChange.
     *Function used:Get the value of the search box's in real time.
     *Create time: 2017/3/6
     */
    filterChange(event){
        this.setState({
            filtertext:event.target.value,
            currentpage:0
        })
    },

    /**
     *The function name:handleChange.
     *Function used:Get the value of the input box's and change some state of error in real time .
     *Create time: 2017/3/6
     */
    handleChange(name,event){
        var newstate = {};
        newstate[name] = event.target.value;
        newstate.iferror = false;
        newstate.ifduplicate = false;
        newstate.ifpassequal=true;
        newstate.ifpassmatchkinds = true;
        newstate.ifpassmatchlength=true;
        newstate.ifpassmatchname=true;
        newstate.oldPassEqualNewPass=false;
        newstate.oldPassEqualoldPass=true;
        newstate.ifexistId=false;
        newstate.ifFourSame=false;
        newstate.ifTimeBt=false;
        newstate.ifemptyer=false;
        newstate.timeinterval=false;
        newstate.ifnamematchkinds=true;
        newstate.ifnamematchlength=true,
        this.setState(newstate);
    },
    /**
     *The function name:newUser.
     *Function used:Pop up a window to create a new user
     *Create time: 2017/3/6
     */
    newUser(){
        this.setState({
            ifpassequal: true,
            iferror:false,
            ifpassmatchkinds:true,
            ifpassmatchlength:true,
            ifpassmatchname:true,
            ifduplicate:false,
            username:null,
            password:null,
            passwordzzl:null,
            ifnamematchkinds:true,
            ifnamematchlength:true,
        });
        $("#createUser").modal("show");
    },
    CharMode(iN) {
        if (iN >= 48 && iN <= 57)//数字
            return 1;
        if (iN >= 65 && iN <= 90) //大写字母
            return 2;
        if ((iN >= 97 && iN <= 122)) //小写
            return 4;
        else  return 8; //特殊字符
    },
    bitTotal(num) {
        var modes = 0;
        for (var i = 0; i < 4; i++) {
            if (num & 1) modes++;  num >>>= 1;
        }
        return modes;
    },
    validatePassword(password){
        var Modes = 0;
        for (var i = 0; i < password.length; i++) {
            Modes |= this.CharMode(password.charCodeAt(i));
        }
        return this.bitTotal(Modes); //CharMode函数
    },
    checkusername(Yname){
        var regular = /^\w+$/ ;
        if(!regular.test(Yname)){
           // alert("用户名只能包括英文字母 数字下划线！");
            return true;
        }
        //for(var i=0;i<Yname.length;i++){
        //    var Ytext= Yname.charCodeAt(i);
        //    if(!((Ytext>=48)&&(Ytext<=57) || (Ytext >= 97 && Ytext <= 122) || (Ytext >= 65 && Ytext <= Ytext) || String.fromCharCode(Ytext)=="_")){
        //        //alert("用户名只能包括英文字母 数字下划线！");
        //        return true;
        //    }
        //}
        return false;
    },
    /**
     *The function name:newUser.
     *Function used:A API for creating a new user
     *Create time: 2017/3/6
     */
    createConfirmlick(){
        if(!this.state.username || !this.state.password ||!this.state.passwordzzl){
            this.setState({iferror:true});
        }
        else if(this.checkusername(this.state.username)){
            this.setState({ifnamematchkinds:false});
        }
        else if(this.state.username.length<6 ||this.state.username.length>32){
            this.setState({ifnamematchlength:false});
        }
        else if(this.validatePassword(this.state.password)==1){
            this.setState({ifpassmatchkinds:false});
        }else if(this.state.password.length<6 || this.state.password.length>20){
            this.setState({ifpassmatchlength:false});
        }
        //else if(this.state.username==this.state.password){
        //    this.setState({ifpassmatchname:false});
        //}
        else if(this.state.passwordzzl != this.state.password){
            this.setState({ifpassequal:false});
        }else{
            $("#ajaxloading").show();
            //var hash = crypto.createHash("md5");
            //hash.update(this.state.password);
            var setsalt = Math.round(Math.random()*1000000000).toString();
            //alert(setsalt);
            var key = crypto.pbkdf2Sync(this.state.password, setsalt, 10000, 16, 'sha512');
            //alert("ok");
            var that = this;
            $.ajax({
                url:Comment.DATA_URL+"KDB/Register",
                data: $.param({
                    name:that.state.username,
                    password:key.toString('hex'),
                    setSalt:setsalt,
                    //password:hash.digest("hex")
                }),
                type:'POST',
                contentType: 'application/x-www-form-urlencoded',
                success:function(data){
                    $("#createUser").modal("hide");
                    $("#ajaxloading").hide();
                    that.getuserList();
                },
                error:function () {
                    $("#ajaxloading").hide();
                },
                statusCode:{
                    406:function (){
                        IfDispatcher.dispatch({
                            actionType:Comment.LOGOUT
                        });
                    },
                    413:function(){
                        that.setState({
                            ifduplicate:true
                        })
                    },
                    416:function(){
                        that.setState({ifpassmatchname:false});
                    }
                }
            });
        }
    },

    /**
     *The function name:deleteButtonClick.
     *Function used:Pop up a window to comfirm delete a  user
     *Create time: 2017/3/6
     */
    deleteButtonClick(id){
        var that = this;
        var inFunc = function(){
            that.state.userindex = id;
            $("#deleteuser").modal("show");
        };
        return inFunc;
    },

    /**
     *The function name:deleteuserlick.
     *Function used:API for sending a request to delete a user
     *Create time: 2017/3/6
     */
    deleteuserlick(){
        $("#deleteuser").modal("hide");
        var that = this;
        $("#ajaxloading").show();
        $.ajax({
            url:Comment.DATA_URL+"KDB/DeleteUser  ",
            data: $.param({
                userId:that.state.userindex,
                _method:"delete"
            }),
            type:'POST',
            contentType: 'application/x-www-form-urlencoded',
            success:function(data){
                $("#ajaxloading").hide();
                that.getuserList();
            },
            error:function () {
                $("#ajaxloading").hide();
            },

        });

    },

    /**
     *The function name:pagesizeChange.
     *Function used:To change a page size
     *Create time: 2017/3/6
     */
    pagesizeChange(event){
        this.setState({
            pagesize:parseInt(event.target.value),
            currentpage:0
        })
    },

    /*resetPassButtonClick(id){

        var that = this;
        var inFunc = function(){
            that.state.userindex = id;
            $("#editUser").modal("show");
        };
        return inFunc;
    },*/
    render(){
       // var divStyle = {display:"none"};
       // var Ifdisplay = {};

       // var colorClass = "grey";
       // var loginInfo = this.props.loginInfo;
        var pageHeight = this.props.propsheight;
        var propswidth = this.props.propswidth;
        var mlist = this.props.userslist;
        var that  = this;

        if(this.state.filtertext && this.state.filtertext.length>0){
            mlist = Underscore.filter(mlist,function (item) {
                return item.user_name.toLowerCase().indexOf(that.state.filtertext.toLowerCase())>=0;
            });
        }

       // var startPos = this.state.currentpage*this.state.pagesize;
        var maxPageNum = Math.ceil(mlist.length/this.state.pagesize);
        var endPos = mlist.length-(this.state.currentpage)*this.state.pagesize;
        var firstPOS= Math.max(endPos-this.state.pagesize,0);
        mlist = mlist.slice(firstPOS,endPos);

        /*var startPos = this.state.currentpage*this.state.pagesize;
        var stopPos = Math.min(startPos+this.state.pagesize,mlist.length);
        var maxPageNum = Math.ceil(mlist.length/this.state.pagesize);
        mlist = mlist.slice(startPos,stopPos);*/

        var leftButtonClass = "";
        if(this.state.currentpage == 0){
            leftButtonClass = "disabled";
        }
        var rightButtonClass = "";
        if(this.state.currentpage == maxPageNum-1){
            rightButtonClass = "disabled";
        }

        var pageButtonGrp = [];
        pageButtonGrp.push(<li className={leftButtonClass}>
            <a
                onClick={this.firstpageclick}
                style={{color:"#000000"}}>
                <span>First</span>
            </a>
        </li>);

        if(maxPageNum <= this.state.pagenum){
            pageButtonGrp.push(<li className={leftButtonClass}>
                <a
                    onClick={this.leftpageclick}
                >
                    <span style={{color:"#000000"}}>Previous</span>
                </a>
            </li>);

            for(var pn = 0;pn<=maxPageNum-1;pn++){
                var currentPageButtonCls = "";
                if(this.state.currentpage == pn){
                    currentPageButtonCls = "active";
                }
                pageButtonGrp.push(<li className={currentPageButtonCls}>
                    <a
                        onClick={this.pnumclick(pn)}
                    >
                        {parseInt(pn)+1}
                    </a>
                </li>);

            }
            pageButtonGrp.push(<li className={rightButtonClass}>
                <a
                    onClick={this.rightpageclick(maxPageNum)}
                >
                    <span style={{color:"#000000"}}>Next</span>
                </a>
            </li>);
        }else{
            if(this.state.currentpage < maxPageNum-7){
                pageButtonGrp.push(<li className={leftButtonClass}>
                    <a
                        onClick={this.leftpageclick}
                    >
                        <span style={{color:"#000000"}}>Previous</span>
                    </a>
                </li>);
                for(var pn = this.state.currentpage;pn<=this.state.currentpage+2;pn++){
                    var currentPageButtonCls = "";
                    if(this.state.currentpage == pn){
                        currentPageButtonCls = "active";
                    }
                    pageButtonGrp.push(<li className={currentPageButtonCls}>
                        <a
                            onClick={this.pnumclick(pn)}
                        >
                            {parseInt(pn)+1}
                        </a>
                    </li>);
                }
                pageButtonGrp.push(<li className={"disabled"}>
                    <a>
                        {"..."}

                    </a>
                </li>);
                for(var pn =maxPageNum-3;pn<=maxPageNum-1;pn++ ){
                    var currentPageButtonCls = "";
                    if(this.state.currentpage == pn){
                        currentPageButtonCls = "active";
                    }
                    pageButtonGrp.push(<li className={currentPageButtonCls}>
                        <a
                            onClick={this.pnumclick(pn)}
                        >
                            {parseInt(pn)+1}
                        </a>
                    </li>);
                }
                pageButtonGrp.push(<li className={rightButtonClass}>
                    <a
                        onClick={this.rightpageclick(maxPageNum)}
                    >
                        <span style={{color:"#000000"}}>Next</span>
                    </a>
                </li>);
            }else{
                pageButtonGrp.push(<li className={leftButtonClass}>
                    <a
                        onClick={this.leftpageclick}
                    >
                        <span style={{color:"#000000"}}>Previous</span>
                    </a>
                </li>);
                for(var pn = maxPageNum-7;pn<=maxPageNum-1;pn++){
                    var currentPageButtonCls = "";
                    if(this.state.currentpage == pn){
                     currentPageButtonCls = "active";
                     }
                    pageButtonGrp.push(<li className={currentPageButtonCls}>
                        <a
                            onClick={this.pnumclick(pn)}
                        >
                            {parseInt(pn)+1}

                        </a>
                    </li>);
                }
                pageButtonGrp.push(<li className={rightButtonClass}>
                    <a
                       onClick={this.rightpageclick(maxPageNum)}
                    >
                        <span style={{color:"#000000"}}>Next</span>
                    </a>
                </li>);
            }
        }

        pageButtonGrp.push(<li className={rightButtonClass}>
            <a
                onClick={this.trailpageclick(maxPageNum)}
            >
                <span style={{color:"#000000"}}>Last</span>
            </a>
        </li>);
        var zlistl =[];
        var llist = [];
        var i=mlist.length-1;
        //var j=0;
        if(i>=0){
            for(i;i>=0;i--) {
                var mli = mlist[i];
                if(mli.tamr_flag == 1){
                    llist.push(
                        <tr className="zzltr" style={{display:"table",border:"0px none"}}>
                            <td className="list_btn" style={{width:"60px",textAlign:"center",borderTop:"0px none",borderLeft:"1px solid #ccc",borderBottom:"1px solid #ccc"}}>
                                <div className="btn-group zzlview" role="group" >
                                    <button
                                        className="btn btn-default glyphicon glyphicon-pencil zzlbutton3" data-toggle="tooltip" title="Edit User" data-placement="left"
                                        onClick={this.adminEditUser(mli.user_id,mli.state,mli.user_name)}
                                        style={{border:"0px none"}}
                                        disabled={"disabled"}
                                        >
                                    </button>
                                </div>
                            </td>
                            <td style={{width:((propswidth-310)/3+"px"),borderTop:"0px none",borderBottom:"1px solid #ccc",color:"#379cf8"}}>{mli.user_name}</td>
                            <td style={{width:((propswidth-310)/3+"px"),borderTop:"0px none",borderBottom:"1px solid #ccc",color:"#379cf8"}}>{mli.is_admin?"admin":"user"}</td>
                            <td style={{width:((propswidth-310)/3+"px"),borderTop:"0px none",borderBottom:"1px solid #ccc",color:"#379cf8"}}>{mli.state==1?"true":"false"}</td>
                            <td style={{width:"200px",borderTop:"0px none",borderRight:"1px solid #ccc",borderBottom:"1px solid #ccc",color:"#379cf8"}}>{mli.create_time}</td>
                        </tr>
                    );
                }else{
                    llist.push(
                        <tr className="zzltr" style={{display:"table",border:"0px none"}}>
                            <td className="list_btn"  style={{width:"60px",textAlign:"center",borderTop:"0px none",borderLeft:"1px solid #ccc",borderBottom:"1px solid #ccc"}}>
                                <div className="btn-group zzlview" role="group" >
                                    <button
                                        className="btn btn-default glyphicon glyphicon-pencil zzlbutton3" data-toggle="tooltip" title="Edit User" data-placement="left"
                                        onClick={this.adminEditUser(mli.user_id,mli.state,mli.user_name)}
                                        style={{border:"0px none"}}
                                        >
                                    </button>
                                </div>
                            </td>
                            <td style={{width:((propswidth-310)/3+"px"),borderTop:"0px none",borderBottom:"1px solid #ccc",color:"#379cf8"}}>{mli.user_name}</td>
                            <td style={{width:((propswidth-310)/3+"px"),borderTop:"0px none",borderBottom:"1px solid #ccc",color:"#379cf8"}}>{mli.is_admin?"admin":"user"}</td>
                            <td style={{width:((propswidth-310)/3+"px"),borderTop:"0px none",borderBottom:"1px solid #ccc",color:"#379cf8"}}>{mli.state==1?"true":"false"}</td>
                            <td style={{width:"200px",borderTop:"0px none",borderRight:"1px solid #ccc",borderBottom:"1px solid #ccc",color:"#379cf8"}}>{mli.create_time}</td>
                        </tr>
                    );
                }

            }
            zlistl.push(
                <div>
                    <div className="row" style={{height:(pageHeight-185+"px"),marginLeft:"0px",marginRight:"0px"}}>
                        <div className="row" style={{height:"30px",marginLeft:"15px",marginRight:"15px",marginTop:"30px"}}>
                            <table  className="table  table-hover table-condensed" >
                                <thead  style={{display:"block",border:"0px none"}}>
                                <tr style={{display:"block", width:"100%",tableLayout:"fixed",border:"0px none",color:"#379cf8"}}>
                                    <th  style={{width:"60px",textAlign:"center",borderBottom:"1px solid #ccc",color:"#379cf8"}}></th>
                                    <th  style={{width:((propswidth-310)/3+"px"),borderBottom:"1px solid #ccc",color:"#379cf8"}}>Name</th>
                                    <th style={{width:((propswidth-310)/3+"px"),borderBottom:"1px solid #ccc",color:"#379cf8"}}>Role</th>
                                    <th style={{width:((propswidth-310)/3+"px"),borderBottom:"1px solid #ccc",color:"#379cf8"}}>Is Active</th>
                                    <th style={{width:"200px",borderBottom:"1px solid #ccc",color:"#379cf8"}}>Created Time</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                        <div className="row" style={{height:(pageHeight-245+"px"),overflowY:"auto",marginTop:"0px",marginLeft:"15px",marginRight:"0px"}}>
                            <table  className="table  table-condensed" >
                                <tbody style={{display:"block",marginTop:"0px",border:"0px none"}}>
                                {llist}
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div className="row" style={{height:"50px",backgroundColor:"#fff", marginLeft:"0px",marginRight:"0px"}}>
                        <div className="col-sm-9" style={{textAlign:"left"}}>
                            <nav aria-label="Page navigation">
                                <ul className="pagination" style={{paddingRight:"0px",marginTop:"0px"}}>
                                    {pageButtonGrp}
                                </ul>
                            </nav>
                        </div>

                    </div>
                </div>
            );
        }

        var setPagesizeSelect = [
            <option value="10">
                10
            </option>
        ];

        for(var num in Comment.SETPAGESIZE){
            setPagesizeSelect.push(<option value={Comment.SETPAGESIZE[num]}>
                {Comment.SETPAGESIZE[num]}
            </option>)
        }
        var disStyle = this.state.ifduplicate?{}:{display:"none"};
        var passIfMatchlength = this.state.ifpassmatchlength?{display:"none"}:{};
        var passIfMatchkinds = this.state.ifpassmatchkinds?{display:"none"}:{};
        var passIfMatchname = this.state.ifpassmatchname?{display:"none"}:{};
        var resetPassStyle = this.state.ifpassequal?{display:"none"}:{};
        var newPassIfMatchOldPass = this.state.oldPassEqualNewPass?{}:{display:"none"};
        var oldPassIfMatchOldPass = this.state.oldPassEqualoldPass?{display:"none"}:{};
        var createStyle=this.state.iferror?{}:{display:"none"};
        var ifExistIdStyle = this.state.ifexistId?{}:{display:"none"};
        var ifFourSameStyle= this.state.ifFourSame?{}:{display:"none"};
        var ifTimeBtStyle= this.state.ifTimeBt?{}:{display:"none"};
        var nameIfMatchlength = this.state.ifnamematchlength?{display:"none"}:{};
        var nameIfMatchkinds = this.state.ifnamematchkinds?{display:"none"}:{};
        var ifemptyerStyle = this.state.ifemptyer?{}:{display:"none"};
        var oldPassstyle = this.state.oldIfopt?{}:{display:"none"};
        var OptPassstyle = this.state.oldIfopt?{display:"none"}:{};
        var iftimeinterval = this.state.timeinterval?{}:{display:"none"};
        return(
            <div>
                <div className="row" style={{height:"75px",backgroundColor:"#fff",marginLeft:"0px",marginRight:"0px",borderBottom:"1px solid #ccc"}}>
                        <div className="col-sm-7" >
                            <h3 style={{color:"#379cf8"}}>Users</h3>
                        </div>
                        <div className="col-sm-4 " >
                            <input type="text"
                                   className="form-control zzlborder zzlbuttoncolborder"
                                   value={this.state.filtertext}
                                   onChange={this.filterChange}
                                   style={{height:"40px",marginTop:"15px"}}
                                   placeholder="Search by Staff Name"/>

                        </div>
                        <div className="col-sm-1" style={{textAlign:"left",marginTop:"10px"}}>
                            <button
                                className="btn btn-default zzlborder"
                                //role="button"
                                id="zzlbutton2"
                                onClick={this.newUser}
                                style={{backgroundColor:"#379cf8",color:"#FFFFFF",width:"50px",height:"50px"}}
                                >
                                +
                            </button>
                        </div>
                </div>

                <div className="row" style={{height:(pageHeight-135+"px"),marginLeft:"0px",marginRight:"0px"}}>
                    {zlistl}
                </div>

                <div className="modal fade" id="adminEditUser" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <div className="row">
                                    <label className="col-sm-5 col-sm-offset-5 control-label" ><h4>EditUser</h4></label>
                                </div>
                            </div>
                            <div className="modal-body">
                                <form>
                                    <div className="row">
                                        <label className="col-sm-2 col-sm-offset-2  control-label" ><h5>UserName:</h5></label>
                                        <div className="col-sm-6" >
                                            <input type="button" disabled="true" className="form-control" id="usernameedit" style={{marginLeft:"-10px"}}
                                                   value={this.state.thisname}
                                            />
                                        </div>
                                    </div>
                                    <div className="row" style={{height:"3px"}}>
                                    </div>

                                    <div style={oldPassstyle}>
                                        <div className="row" >
                                            <label className="col-sm-2 col-sm-offset-2  control-label" ><h5>OldPass:</h5></label>
                                            <div className="col-sm-6" >
                                                <input type="password" className="form-control" id="personnewpass3st" placeholder="OldPass" style={{marginLeft:"-10px"}}
                                                       onChange={this.handleChange.bind(this,"oldpassword")}
                                                       onMouseOver={copyPaste("personnewpass3st")}
                                                       value={this.state.oldpassword}
                                                    />
                                            </div>
                                        </div>
                                    </div>
                                    <div style={OptPassstyle}>
                                        <div className="row">
                                            <div className="col-sm-2 col-sm-offset-2">
                                                <input type="checkbox"
                                                       //ckecked={this.state.ifckecked}
                                                       id="checkactive"/>
                                            </div>
                                            <div className="col-sm-6">
                                                <span style={{marginLeft:"-10px"}}>Is Active</span>
                                            </div>
                                        </div>
                                        <div className="row" >
                                            <label className="col-sm-2 col-sm-offset-2  control-label" ><h5>OptPass:</h5></label>
                                            <div className="col-sm-6" >
                                                <input type="password" className="form-control" id="personnewpass3st" placeholder="OptPass" style={{marginLeft:"-10px"}}
                                                       onChange={this.handleChange.bind(this,"oldpassword")}
                                                       onMouseOver={copyPaste("personnewpass3st")}
                                                       value={this.state.oldpassword}
                                                    />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <label className="col-sm-2 col-sm-offset-2  control-label" ><h5>NewPass:</h5></label>
                                        <div className="col-sm-6" >
                                            <input type="password" className="form-control" id="personnewpass1st" placeholder="New Password" style={{marginLeft:"-10px"}}
                                                   onChange={this.handleChange.bind(this,"password1st")}
                                                   onMouseOver={copyPaste("personnewpass1st")}
                                                   value={this.state.password1st}
                                            />
                                        </div>
                                    </div>
                                    <div className="row">
                                        <label className="col-sm-2 col-sm-offset-2  control-label" ><h5>Comfirm:</h5></label>
                                        <div className="col-sm-6" >
                                            <input type="password" className="form-control" id="personnewpass2nd" placeholder="New Password Again" style={{marginLeft:"-10px"}}
                                                   onChange={this.handleChange.bind(this,"password2nd")}
                                                   onMouseOver={copyPaste("personnewpass2nd")}
                                                   value={this.state.password2nd}
                                            />
                                        </div>
                                    </div>

                                </form>

                            </div>
                            <div className="modal-footer">
                                <div className="row">
                                    <div className="col-md-8">
                                        <div style={resetPassStyle}>
                                            <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                Two input new passwords are not the same!
                                            </div>
                                        </div>
                                        <div style={newPassIfMatchOldPass}>
                                            <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                New password is equal old passwords.
                                            </div>
                                        </div>
                                        <div style={iftimeinterval}>
                                            <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                The interval cannot be less than the minimum period.
                                            </div>
                                        </div>
                                        <div style={ifemptyerStyle}>
                                            <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                The old/opt password cannot be empty.
                                            </div>
                                        </div>
                                        <div style={oldPassIfMatchOldPass}>
                                            <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                Old/Opt password is not equal your original password.
                                            </div>
                                        </div>
                                        <div style={ifExistIdStyle}>
                                            <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                This id does not exist!
                                            </div>
                                        </div>
                                        <div style={ifFourSameStyle}>
                                            <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                New password cannot be the same as the previous four password.
                                            </div>
                                        </div>
                                        <div style={ifTimeBtStyle}>
                                            <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                Password change interval for 5 minutes, not frequent revision.
                                            </div>
                                        </div>
                                        <div style={passIfMatchlength}>
                                            <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                Your new passwords must be at least  six characters and at most 20 characters.
                                            </div>
                                        </div>
                                        <div style={passIfMatchkinds}>
                                            <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                Your new passwords must be at least  two kinds of characters.
                                            </div>
                                        </div>
                                        <div style={passIfMatchname}>
                                            <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                The new password cannot be the same as an account or a reverse account.
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-4">
                                        <a type="button" className="btn btn-default" data-dismiss="modal">Cancel</a>
                                        <a type="button" className="btn btn-default" onClick={this.editConfirmlick} style={{backgroundColor:"#46A3FF"}}>Confirm</a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div className="modal fade" id="createUser" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 className="modal-title" >Create New User</h4>
                            </div>
                            <div className="modal-body">
                                <form>
                                    <div className="form-group">
                                        <label htmlFor="personnewname">User Name</label>
                                        <input type="text" className="form-control" id="personnewzzlname" placeholder="UserName"
                                               onChange={this.handleChange.bind(this,"username")}
                                               onMouseOver={copyPaste("personnewzzlname")}
                                               value={this.state.username}
                                        />
                                        <label htmlFor="personnewpass">User Password</label>
                                        <input type="password" className="form-control" id="personnewpass" placeholder="Password"
                                               onChange={this.handleChange.bind(this,"password")}
                                               onMouseOver={copyPaste("personnewpass")}
                                               value={this.state.password}
                                        />
                                        <label htmlFor="personnewpass">Comfirm</label>
                                        <input type="password" className="form-control" id="personnewpasszzl" placeholder="Password Again"
                                               onChange={this.handleChange.bind(this,"passwordzzl")}
                                               onMouseOver={copyPaste("personnewpasszzl")}
                                               value={this.state.passwordzzl}
                                            />
                                    </div>
                                </form>
                            </div>
                            <div className="modal-footer">
                                <div className="row">
                                    <div className="col-md-8">
                                        <div style={createStyle}>
                                            <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                Username and password can not be empty!
                                            </div>
                                        </div>
                                        <div style={resetPassStyle}>
                                            <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                Two input password are not the same!
                                            </div>
                                        </div>
                                        <div style={disStyle}>
                                            <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                This user already exist!
                                            </div>
                                        </div>
                                        <div style={passIfMatchlength}>
                                            <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                Your passwords must be at least  six characters and at most 20 characters.
                                            </div>
                                        </div>
                                        <div style={passIfMatchkinds}>
                                            <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                Your passwords must be at least  two kinds of characters.
                                            </div>
                                        </div>
                                        <div style={nameIfMatchlength}>
                                            <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                Your username must be at least  six characters and at most 32 characters.
                                            </div>
                                        </div>
                                        <div style={nameIfMatchkinds}>
                                            <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                User name only supports the alphanumeric underline, please try again.
                                            </div>
                                        </div>
                                        <div style={passIfMatchname}>
                                            <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                The password cannot be the same as an account or a reverse account.
                                            </div>
                                        </div>

                                    </div>
                                    <div className="col-md-4">
                                        <a type="button" className="btn btn-default" data-dismiss="modal">Cancel</a>
                                        <a type="button" className="btn btn-default" onClick={this.createConfirmlick} style={{backgroundColor:"#46A3FF"}}>Confirm</a>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>

                <div className="modal fade" id="deleteuser" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                                <h4 className="modal-title" >Confirm the delete action</h4>
                            </div>
                            <div className="modal-body">
                                <h3>
                                    Are you sure to delete this user?
                                </h3>

                            </div>
                            <div className="modal-footer">
                                <a type="button" className="btn btn-default" data-dismiss="modal">Cancel</a>
                                <a type="button"
                                   onClick={this.deleteuserlick}
                                   className="btn btn-primary" >Confirm</a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        )
    }
});