/**
 * Created by zzl on 2017/6/2.
 */
import React from 'react';
import crypto from "crypto";
import {Comment} from "../comment";
import {IfDispatcher} from "../store/dispatcher";
import Underscore from "underscore";

export var Dictdata = React.createClass({
    contextTypes: {
        router: React.PropTypes.object.isRequired,
    },
    getInitialState(){
        return{
            ifdicid:null,
            thisfdefno:null,
            thisfdefnname:null,
            datasiginNo:[],
            checkselects:[],
            fdetifCheck:[],
            confirmdata:[],
            checkconfirms:[],
            confirmdatasiginNo:[],
            dictindex:null,
            ifdatanull:false,
            ifdicname:null,
            selectItem:null,
            selectFdefItem:null,
            selectSourceItem:null,
            selectresultItem:null,
            featuresSelects:[],
            feanoindex:null,
            ifods:null,
            ifmeta:false,
            setencode:"UTF-8",
            setlanuage:"Chinese",
            setseparate:"|",
            ifseter:false,
            timebarId:null,
            timebar:0,
            ifbar:false,
            ifbad:false,
            pagesize:500,
            datacounts:0,
            pagenum:7,
            currentpage:0,
            currentpages:0,
            pagesizes:100,
            datacountss:0,
            ifexport:false,
            exporturl:null,
            ifdds:0,
            ifrun:false,
            resultdata:[],
            ifodss:null,
            columnname:[],
            setmethod:"LOCAL",
            Uploadpath:null,
            Uploadpass:null,
            Uploaduser:null,
            Uploadhost:null,
            Uploadport:null,
            ifuploaderror:false,
            ifrunnig:false,
            metadatatext:null,
            ifmetdtext:false,
            searchcount:0,
            searchdata:[],
            searchname:[],
            currentpagech:0,
            pagesizech:100,
            searchtablecount:0,
            searchtabledata:[],
            currentpagebl:0,
            fileNum:0,
            fileData:[],
            ifselectfiles:false,
            covername:null,
            covernamelist:[],
            ckockselects:[],
            newcoverfile:null,
        }
    },
    componentDidMount(){
        var loginInfo = this.props.loginInfo;
        if(!loginInfo.name){
            this.context.router.push("/fronpage");
        }else{
            this.getdictsList();
        }
    },
    render(){
        var pageHeight = this.props.propsheight;
        var propswidth = this.props.propswidth;
        var projectname= this.props.projectname;
        return(
            <div>
                <div style={{float:"left",width:(propswidth+"px")}}>
                    <div className="row" style={{height:"70px",backgroundColor:"#FFFFFF",marginLeft:"0px",marginRight:"0px"}}>
                        <div className="col-sm-8 " >
                            <h4 style={{marginTop:"20px"}}>{projectname} project</h4>
                        </div>
                    </div>
                    <div className="row" style={{height:"5px",backgroundColor:"#408080",marginLeft:"0px",marginRight:"0px"}}>
                    </div>
                    <div className="row" style={{height:(pageHeight-135+"px"),backgroundColor:"#FFFFFF",marginLeft:"0px",marginRight:"0px"}}>
                        <ul className="nav nav-tabs " role="tablist"  >
                            <li role="presentation" className="active" style={{height:"42px"}}><a href="#dictionary" aria-controls="dictionary" role="tab" data-toggle="tab" onClick={this.getdictsList}
                                >Dictionaries</a></li>
                            <li role="presentation" style={{height:"42px"}}><a href="#features" aria-controls="features" role="tab" data-toggle="tab" onClick={this.getDataFeaturelist}
                                >Data Features</a></li>
                            <li role="presentation" style={{height:"42px"}}
                                onClick={this.getsoureclick(parseInt(0))}
                                ><a href="#suorces" aria-controls="suorces" role="tab" data-toggle="tab"
                                >Sources</a></li>
                            <li role="presentation" style={{height:"42px"}}

                                ><a href="#resultd" aria-controls="resultd" role="tab" data-toggle="tab"
                                    id="resultonclick"
                                    onClick={this.getsoureresultclick(parseInt(0))}
                                >Feature Result</a></li>
                        </ul>
                        <div className="tab-content ">
                            <div role="tabpanel" className="tab-pane active" id="dictionary">
                            </div>
                            <div  role="tabpanel" className="tab-pane " id="features">
                            </div>
                            <div  role="tabpanel" className="tab-pane " id="suorces">
                            </div>
                            <div  role="tabpanel" className="tab-pane " id="resultd">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
});