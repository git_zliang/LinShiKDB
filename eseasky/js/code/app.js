/**
 * Created by ��־�� on 2017/2/16.
 */
import React from 'react';
import crypto from "crypto";
import {Comment} from "../comment";
import {IfDispatcher} from "../store/dispatcher";
import {loginStore} from "../store/loginstore";
import {usersStore} from "../store/userstore";
import {projectsStore} from "../store/projectlstore";
import {dictsStore} from "../store/dictsstore";
import {dictdataStore} from "../store/dictdatastore";
import {datafeaturesStore} from "../store/datafeaturesStore";
import {participleStore} from "../store/participlestore";

export var App = React.createClass({
    contextTypes: {
        router: React.PropTypes.object.isRequired,
    },
    getInitialState(){
        return {
            pageHeight:document.documentElement.clientHeight,
            pageWidth:document.documentElement.clientWidth,
            getApprovenum:0,
        }
    },
    componentDidMount(){
        this.token=loginStore.addListener(this._onChange);
        this.Utoken=usersStore.addListener(this._onChange);
        this.Ptoken=projectsStore.addListener(this._onChange);
        this.Dtoken=dictsStore.addListener(this._onChange);
        this.Ddtoken=dictdataStore.addListener(this._onChange);
        this.Dftoken=datafeaturesStore.addListener(this._onChange);
        this.Pltoken=participleStore.addListener(this._onChange);
        var loginInfo = loginStore.getAll();
        //$("#zzlloginname").popover({
        //    title:loginInfo.name,
        //    placement:"bottom",
        //    content:loginInfo.name,
        //    trigger:"manual"
        //});

    },
    mousemoveYes(){
        //setTimeout(() =>{
        //    $("#zzlloginname").popover("show");
        //},2000);
        $("#zzlloginname").popover("show");
    },
    mouseoutNo(){
        $("#zzlloginname").popover("hide");
    },
    componentWillUnmount(){
        loginStore.remove(this.token);
        usersStore.remove(this.Utoken);
        projectsStore.remove(this.Ptoken);
        dictsStore.remove(this.Dtoken);
        dictdataStore.remove(this.Ddtoken);
        datafeaturesStore.remove(this.Dftoken);
        participleStore.remove(this.Pltoken);

    },
    _onChange() {
        var loginInfo = loginStore.getAll();
        //alert(loginInfo.name);
        if(!loginInfo.name){
            //alert(2);
            this.context.router.push("/fronpage");
        }else{
            //var cpath = this.props.routes[this.props.routes.length-1]['path']
            //alert(cpath);
            //alert(3);
            if(!loginInfo.iffrist){
                this.getapprovenum();
            }
        }
        //this.setState({a:1});
    },
    getapprovenum(){
        var loginInfo = loginStore.getAll();
        var that = this;
        if(loginInfo.userid){
            $.ajax({
                url: Comment.DATA_URL+"KDB/GetApproveDic",
                data:$.param({
                    userId:loginInfo.userid,
                }),
                type: 'GET',
                contentType: 'application/x-www-form-urlencoded',
                success: function(data){
                    var msg = data;
                    var num = 0;
                    for(var k in msg.data){
                        if(msg.data[k].state == 0){
                            num++
                        }
                    }
                    that.setState({
                        getApprovenum:num
                    });
                },
                error:function(){
                    that.setState({a:1});
                }
            });
        }

    },
    accountClick(){
        var loginInfo = loginStore.getAll();
        if(loginInfo.ifadmin){
            this.context.router.push("/account");
        }else{
            this.context.router.push("/staff");
        }
    },
    logoutClick(){
        IfDispatcher.dispatch({
            actionType:Comment.LOGOUT
        });
    },
    dictClick(){
        var loginInfo = loginStore.getAll();
        if(!loginInfo.name){
            this.context.router.push("/fronpage");
        }else{
            this.context.router.push("/dicts");
        }
    },
    projectClick(){
        var loginInfo = loginStore.getAll();
        if(!loginInfo.name){
            this.context.router.push("/fronpage");
        }else{
            this.context.router.push("/project");
        }

    },
    datafeaureClick(){
        var loginInfo = loginStore.getAll();
        if(!loginInfo.name){
            this.context.router.push("/fronpage");
        }else{
            this.context.router.push("/datafeaure");
        }
    },
    DataParticipleClick(){
        var loginInfo = loginStore.getAll();
        if(!loginInfo.name){
            this.context.router.push("/fronpage");
        }else{
            this.context.router.push("/partdata");
        }
    },
    dictapproveClick(){
        var loginInfo = loginStore.getAll();
        if(!loginInfo.name){
            this.context.router.push("/fronpage");
        }else{
            this.context.router.push("/dictapprove");
        }
    },
    pjoverClick(){
        var btn = document.getElementById("Projectzzl");
        btn.style.background = "#E0E0E0";
        btn.onmouseout = function () {
            btn.style.background = '#ADADAD';
        };
    },
    pjoutClick(){
        var btn = document.getElementById("Projectzzl");
        btn.style.background = "#ADADAD";
    },
    render(){
        var logoutStyle = {display: "none"};
        var logoutStylel={};
        var loginInfo = loginStore.getAll();
        var numStyle = {display: "none"};
        var cpath = this.props.routes[this.props.routes.length-1]['path']
        var clickcolor = [];
        if(cpath === "project" || cpath === "dictdata"){
            clickcolor[0] = "#379cf8"
            clickcolor[1] = "#777"
            clickcolor[2] = "#777"
            clickcolor[3] = "#777"
            clickcolor[4] = "#777"
        }else if(cpath === "dicts"){
            clickcolor[0] = "#777"
            clickcolor[1] = "#379cf8"
            clickcolor[2] = "#777"
            clickcolor[3] = "#777"
            clickcolor[4] = "#777"
        }else if(cpath === "datafeaure"){
            clickcolor[0] = "#777"
            clickcolor[1] = "#777"
            clickcolor[2] = "#379cf8"
            clickcolor[3] = "#777"
            clickcolor[4] = "#777"
        }else if(cpath === "partdata"){
            clickcolor[0] = "#777"
            clickcolor[1] = "#777"
            clickcolor[2] = "#777"
            clickcolor[3] = "#379cf8"
            clickcolor[4] = "#777"
        }else if(cpath === "dictapprove"){
            clickcolor[0] = "#777"
            clickcolor[1] = "#777"
            clickcolor[2] = "#777"
            clickcolor[3] = "#777"
            clickcolor[4] = "#379cf8"
        }else{
            clickcolor[0] = "#777"
            clickcolor[1] = "#777"
            clickcolor[2] = "#777"
            clickcolor[3] = "#777"
            clickcolor[4] = "#777"
        }
        if (loginInfo.name) {
            if(loginInfo.iffrist){
                logoutStylel = {};
                logoutStyle = {display: "none"};
            }else{
                logoutStyle = {};
                logoutStylel = {display: "none"};
                var getApprovenum = this.state.getApprovenum;
                if(getApprovenum != 0){
                    numStyle = {};
                }
            }
        }
        //var cpath = this.props.routes[this.props.routes.length-1]['path']
        //// alert(cpath);
        //var btnClor ={};
        //if(cpath == "project" || cpath == "dictdata"){
        //    btnClor .btn ="#FFFFFF" ;
        //    btnClor .btn1 = "#E0E0E0";
        //    btnClor .btn2 = "#E0E0E0";
        //}else if(cpath == "dicts"){
        //    btnClor .btn = "#E0E0E0";
        //    btnClor .btn1 = "#FFFFFF";
        //    btnClor .btn2 = "#E0E0E0";
        //}else if(cpath == "datafeaure"){
        //    btnClor .btn = "#E0E0E0";
        //    btnClor .btn1 = "#E0E0E0";
        //    btnClor .btn2 = "#FFFFFF";
        //}else{
        //    btnClor .btn = "#E0E0E0";
        //    btnClor .btn1 = "#E0E0E0";
        //    btnClor .btn2 = "#E0E0E0";
        //}
        var that = this;
        window.onresize = function () {
            //let domheight = document.documentElement.clientHeight;
            //let domwidth  = document.documentElement.clientWidth;
            //    if(parseInt(domwidth) <= 1400){
            //        if(parseInt(domheight) <= 800 ){
            //            that.setState({pageHeight:800,pageWidth:1400});
            //        }else{
            //            that.setState({pageHeight:domheight,pageWidth:1400});
            //        }
            //    }else{
            //        if(parseInt(domheight) <= 800 ){
            //            that.setState({pageHeight:800,pageWidth:domwidth});
            //        }else{
            //            that.setState({pageHeight:domheight,pageWidth:domwidth});
            //        }
            //    }
                that.setState({pageHeight:document.documentElement.clientHeight,pageWidth:document.documentElement.clientWidth});
        }

        return (
            <div>
                <div style={logoutStylel}>
                    <div className="row" style={{width:this.state.pageWidth,height:"50px",backgroundColor:"#282E3E",marginLeft:"0px",marginRight:"0px"}}>
                        <nav className="navbar navbar-default " style={{height:"50px",backgroundColor:"#282E3E",border:"0px none"}}>
                            <div className="container-fluid">
                                <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1" >

                                    <ul className="nav navbar-nav" style={{height:"50px"}}>
                                        <li >
                                            <p  style={{marginTop:"10px"}}>
                                                <img style={{marginLeft:"-15px",marginTop:"-5px",width:"150px",height:"40px"}} src="./public/image/logo.png"/>
                                            </p>

                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </nav>
                    </div>
                </div>
                <div style={logoutStyle}>
                    <div className="row" style={{width:this.state.pageWidth,height:"60px",backgroundColor:"#282E3E ",marginLeft:"0px",marginRight:"0px",borderBottom:"1px solid #ccc"}}>
                        <nav className="navbar navbar-default " style={{height:"50px",backgroundColor:"#282E3E ",border:"0px none"}}>
                            <div className="container-fluid">
                                <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1" >

                                    <ul className="nav navbar-nav" style={{height:"50px"}}>
                                        <li >
                                            <p  style={{marginTop:"10px"}}>
                                                <img style={{marginLeft:"-15px",marginTop:"-5px",width:"150px",height:"40px"}} src="./public/image/logo.png"/>
                                            </p>
                                        </li>

                                    </ul>
                                    <ul className="nav navbar-nav navbar-right" style={{height:"50px"}}>

                                        <li>
                                            <a
                                                type="button"
                                                id="Projectzzl"
                                                onClick={this.projectClick}
                                                // onMouseMove={this.pjoverClick}
                                                //onMouseOut={this.pjoutClick}
                                                className="btn default "
                                                style={{color:clickcolor[0]}}
                                                >Projects</a>
                                        </li>
                                        <li>
                                            <a
                                                type="button"
                                                id="Dictionaryzzl"
                                                onClick={this.dictClick}
                                                className="btn default "
                                                style={{color:clickcolor[1]}}
                                               // style={{background:btnClor.btn1,height:"30px",width:"100px",border:"0px none"}}
                                                >Dictionaries</a>
                                        </li>
                                        <li>
                                            <a
                                                type="button"
                                                id="DataFeaturezzl"
                                                onClick={this.datafeaureClick}
                                                className="btn default "
                                                style={{color:clickcolor[2]}}
                                                //style={{background:btnClor.btn2,height:"30px",width:"110px",border:"0px none"}}
                                                >Data Features</a>
                                        </li>
                                        <li>
                                            <a
                                                type="button"
                                                id="DataParticiplezzl"
                                                onClick={this.DataParticipleClick}
                                                className="btn default "
                                                style={{color:clickcolor[3]}}
                                                //style={{background:btnClor.btn2,height:"30px",width:"110px",border:"0px none"}}
                                                >Tokens</a>
                                        </li>
                                        <li>
                                            <a
                                                type="button"
                                                onClick={this.dictapproveClick}
                                                className="btn default "
                                                style={{color:clickcolor[4]}}
                                                //style={{marginTop:"10px",backgroundColor:"#ADADAD",border:"0px none"}}
                                                >Approvais <span style={numStyle} className="badge">{getApprovenum}</span></a>
                                        </li>
                                        <li >
                                            <a type="button"  id="zzlloginname" className="btn dropdown-toggle  glyphicon glyphicon-user zzlborder"
                                               data-toggle="popover" data-trigger="focus" data-content={loginInfo.name} data-placement="bottom"
                                                data-toggle="dropdown" style={{background:"#282E3E",border:"0px none"}}
                                                onMouseMove={this.mousemoveYes}
                                                onMouseOut={this.mouseoutNo}
                                                >
                                                </a>
                                            <ul className="dropdown-menu" role="menu">
                                                <li>
                                                    <a
                                                        type="button"
                                                        onClick={this.accountClick}
                                                        className="btn default"
                                                        //style={{marginTop:"10px",backgroundColor:"#ADADAD",border:"0px none"}}
                                                        >View Users</a>
                                                </li>
                                                <li><a
                                                    type="button"
                                                    className="btn default"
                                                    onClick={this.logoutClick}
                                                    >Log out</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </nav>
                    </div>
                </div>

                {
                    this.props.children && React.cloneElement(this.props.children,
                        {
                            loginInfo: loginStore.getAll(),
                            propsheight:this.state.pageHeight,
                            propswidth:this.state.pageWidth,
                            userslist:usersStore.getAll(),
                            projectslist:projectsStore.getAll(),
                            projectname:projectsStore.getProname(),
                            approvedatalist:projectsStore.getApprovenum(),
                            dictslist:dictsStore.getAll(),
                            dictdatalist:dictsStore.getBll(),
                            inputtext:dictsStore.getCll(),
                            dictfilelist:dictdataStore.getAll(),
                            dictfiledata:dictdataStore.getBll(),
                            projectid:projectsStore.getBll(),
                            projectdictlist:dictdataStore.getProjectDict(),
                            sourcesdata:dictdataStore.getCll(),
                            viewodsbds:dictdataStore.getDll(),
                            metadatas:dictdataStore.getFll(),
                            tablesmetadata:dictdataStore.getVll(),
                            ifrunbarmsg:dictdataStore.getZll(),
                            featuredeflist:datafeaturesStore.getfedeflist(),
                            featuredetlist:datafeaturesStore.getfedetlist(),
                            datafeaturedeflist:dictdataStore.getfeaturedeflist(),
                            datafeaturedetlist:dictdataStore.getfeaturedetlist(),
                            featureprojectlist:dictdataStore.getfeatureproject(),
                            participlemsg:participleStore.getA(),
                            participleconflist:dictdataStore.getKll(),
                            partpledatalist:dictdataStore.getPll(),
                            partpleprojectlist:dictdataStore.getPPl(),
                        })
                }
            </div>

        )
    }
});