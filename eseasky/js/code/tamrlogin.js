/**
 * Created by ��־�� on 2017/4/12.
 */
import React from 'react';
import crypto from "crypto";
import {Comment} from "../comment";
import {IfDispatcher} from "../store/dispatcher";

export var Tamrlogin = React.createClass({

    contextTypes: {
        router: React.PropTypes.object.isRequired,
    },
    getInitialState(){
        return{
            username:"",
            password:"",
            iferror:false,
            ifactive:true,
        }
    },
    componentDidMount (){

    },
    handleChange(name,event){
        var newstate ={};
        newstate[name] = event.target.value;
        newstate.iferror = false;
        newstate.ifactive= true;
        this.setState(newstate);
    },
    keyLogin(event){
        if(event.key == "Enter"){
            this.inputClick(event);
        }
    },
    proonclick(){
        this.context.router.push("/project");
    },
    inputClick(){
        if(!this.state.username || !this.state.password){
            this.setState({iferror:true});
        } else{
            $("#ajaxloading").show();
            //var hash = crypto.createHash("md5");
            //hash.update(this.state.password);
            var that = this;
            $.ajax({
                url: Comment.DATA_URL+"KDB/SendTokenToTamr",
                data: $.param({
                    username:that.state.username,
                    password:that.state.password,
                }),
                type: 'POST',
                contentType: 'application/x-www-form-urlencoded',
                success: function (data) {
                    // alert("success");
                    var msg = data;
                    $("#ajaxloading").hide();
                    IfDispatcher.dispatch({
                        actionType: Comment.TAMRLOGINSUCCESS,
                        name:msg.data.userName,
                        pass:msg.data.Password,
                        userid:msg.data.userId,
                        ifadmin:msg.data.Isadmin,
                        uuid:msg.data.uuid,
                    });

                    setTimeout(that.proonclick(),500);

                },
                error:function(){
                    $("#ajaxloading").hide();
                },
                statusCode:{
                    414:function(){
                    },
                    415:function(){
                        that.setState({
                            ifactive:false
                        })
                    },
                    417:function(){
                    },
                }
            });
        }
    },
    render(){
        var disStyle = this.state.iferror?{}:{display:"none"};
        var ifactiveStyle = this.state.ifactive?{display:"none"}:{};
        return(
            <div className="divrow" style={{marginTop:"0px",height:(this.props.propsheight+"px")}} >

                <div className="row" style={{marginLeft:"0px",clear:"both",marginRight:"0px"}}>
                    <div className="col-sm-4 col-sm-offset-4" style={{paddingTop:"100px"}}>
                        <div className="panel panel-default" >
                            <div className="panel-body formstyleone" style={{backgroundColor:"#FCFCFC"}}>
                                <form>
                                    <div className="row">
                                        <div className="col-sm-12" style={{textAlign:"center",color:"#000000",fontSize:"20px"}}>
                                            <span>Tamr User Login</span>
                                        </div>
                                    </div>
                                    <div className="row" style={{marginTop:"10px"}}>
                                        <label className="col-sm-3 control-label" ><h5>Username:</h5></label>
                                        <div className="col-sm-8">
                                            <input type="text"
                                                   className="form-control"
                                                   placeholder="Username"
                                                   id="tamrzusername"
                                                   style={{height:"40px"}}
                                                   value={this.state.username}
                                                   onChange={this.handleChange.bind(this,"username")}
                                                   onMouseOver={copyPaste("tamrzusername")}
                                                />
                                        </div>
                                    </div>
                                    <div className="row" style={{marginTop:"10px"}}>
                                        <label className="col-sm-3 control-label" ><h5>Password:</h5></label>
                                        <div className="col-sm-8">
                                            <input type="password"
                                                   className="form-control"
                                                   placeholder="Password"
                                                   style={{height:"40px"}}
                                                //onKeyPress={this.keypress}
                                                    id="tamrzpasswd"
                                                   onMouseOver={copyPaste("tamrzpasswd")}
                                                   value={this.state.password}
                                                   onChange={this.handleChange.bind(this,"password")}
                                                   onKeyPress={this.keyLogin}
                                                />
                                        </div>
                                    </div>
                                    <div className="row" style={{marginTop:"10px"}}>
                                        <div className="col-sm-4 col-sm-offset-7">
                                            <input type="button"
                                                   onClick={this.inputClick}
                                                   value="Log In"
                                                   className="form-control btn btn-primary"
                                                   style={{height:"40px",color:"#F0F0F0"}}
                                                />
                                        </div>
                                    </div>
                                    <div className="row" style={{marginTop:"10px"}}>
                                        <div className="col-sm-offset-1 col-sm-10" style={disStyle}>
                                            <p  style={{color:"#750000",textAlign:"center"}}>The sign in information you entered was not correct. Please try again.</p>
                                        </div>
                                        <div className="col-sm-offset-1 col-sm-10" style={ifactiveStyle}>
                                            <p  style={{color:"#750000",textAlign:"center"}}>Tamr check does not fail.</p>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        )
    }
});