/**
 * Created by zzl on 2017/5/31.
 */
import React from 'react';
import crypto from "crypto";
import {Comment} from "../comment";
import {IfDispatcher} from "../store/dispatcher";
import Underscore from "underscore";
import {Participle} from "./participle";

export var PartData = React.createClass({
    contextTypes: {
        router: React.PropTypes.object.isRequired,
    },
    getInitialState(){
        return {
            currentPartId: null,
            currentPartName: null,
            currentPartNo: 0,
            PartFilename: null,
            ifempty: false,
            ifexist: false,
            ifspecial: false,
            iflonger: false,

        }
    },
    componentDidMount(){
        var loginInfo = this.props.loginInfo;
        if(!loginInfo.name){
            this.context.router.push("/fronpage");
            IfDispatcher.dispatch({
                actionType:Comment.LOGOUT
            });
        }else{
            this.getDataPart();
        }
    },
    getDataPart(){
        $("#ajaxloading").show();
        var loginInfo = this.props.loginInfo;
        var that = this;
        $.ajax({
            url: Comment.DATA_URL+"KDB/GetAllConfByUserIdDefind",
            data:$.param({
                userId:loginInfo.userid,
            }),
            type: 'GET',
            contentType: 'application/x-www-form-urlencoded',
            success: function(data){
                var msg = data;
                if(msg.data.length>0){
                    $.ajax({
                        url: Comment.DATA_URL+"KDB/getAllConfs",
                        data:$.param({
                            confDefineId:msg.data[0].conf_define_id,
                        }),
                        type: 'GET',
                        contentType: 'application/x-www-form-urlencoded',
                        success: function(data){
                            $("#ajaxloading").hide();
                            var msg2 = data;
                            that.setState({
                                currentPartId:msg.data[0].conf_define_id,
                                currentPartName:msg.data[0].conf_name,
                                currentPartNo:0,
                            });
                            IfDispatcher.dispatch({
                                actionType: Comment.GETPARTICIPLELIST,
                                participleList:msg.data,
                                getparticipledata:msg2.data
                            });
                        },
                        error:function(){
                            $("#ajaxloading").hide();
                        },
                        statusCode:{
                            414:function(){

                            },
                            406:function (){
                                IfDispatcher.dispatch({
                                    actionType:Comment.LOGOUT
                                });
                            }
                        }
                    });
                }else{
                    $("#ajaxloading").hide();
                    that.setState({
                        currentPartId:null,
                        currentPartName:null,
                        currentPartNo:0,
                    });
                    IfDispatcher.dispatch({
                        actionType: Comment.GETPARTICIPLELIST,
                        participleList:[],
                        getparticipledata:[]
                    });
                }
            },
            error:function(){
                $("#ajaxloading").hide();
            },
            statusCode:{
                414:function(){
                },
                406:function (){
                    IfDispatcher.dispatch({
                        actionType:Comment.LOGOUT
                    });
                }
            }
        });
    },
    addPartFileclick(){
        this.setState({
            ifempty:false,
            ifexist:false,
            ifspecial:false,
            iflonger:false,
            PartFilename:null,
        });
        $("#addPartFile").modal("show");
    },
    addPartFileConf(){
        if(!this.state.PartFilename ){
            this.setState({ifempty:true});
        }
        else if(this.state.PartFilename.length>40) {
            this.setState({iflonger: true});
        }else{
            $("#ajaxloading").show();
            var loginInfo = this.props.loginInfo;
            var that = this;
            $.ajax({
                url: Comment.DATA_URL+"KDB/AddConfDefind",
                data: $.param({
                    confName:that.state.PartFilename,
                    userId:loginInfo.userid,
                }),
                type: 'POST',
                contentType: 'application/x-www-form-urlencoded',
                success: function (data) {
                    $("#addPartFile").modal("hide");
                    $("#ajaxloading").hide();
                    $.ajax({
                        url: Comment.DATA_URL+"KDB/GetAllConfByUserIdDefind",
                        data:$.param({
                            userId:loginInfo.userid,
                        }),
                        type: 'GET',
                        contentType: 'application/x-www-form-urlencoded',
                        success: function(data){
                            var msg = data;
                            IfDispatcher.dispatch({
                                actionType: Comment.GETPARTICIPLELISTZZL,
                                participleList:msg.data
                            });
                        },
                        error:function(){
                        },
                        statusCode:{
                            414:function(){
                            },
                            406:function (){
                                IfDispatcher.dispatch({
                                    actionType:Comment.LOGOUT
                                });
                            }
                        }
                    });
                },
                error:function(){
                    $("#ajaxloading").hide();
                },
                statusCode:{
                    406:function (){
                        IfDispatcher.dispatch({
                            actionType:Comment.LOGOUT
                        });
                    },
                    414:function(){

                    },
                    413:function(){
                        that.setState({
                            ifexist:true,
                            });
                    },
                    422:function(){
                        that.setState({
                            ifspecial:true,
                        });
                    }
                }
            });
        }
    },
    handleChange(name,event){
        var newstate ={};
        newstate[name] = event.target.value;
        newstate.ifempty = false;
        newstate.ifexist = false;
        newstate.ifspecial=false;
        newstate.iflonger=false;
        this.setState(newstate);

    },
    PartFilelistClick(confId,NoId,confName){
        var that = this;
        var inFunc = function(){
            $("#ajaxloading").show();
            $.ajax({
                url: Comment.DATA_URL+"KDB/getAllConfs",
                data:$.param({
                    confDefineId:confId,
                }),
                type: 'GET',
                contentType: 'application/x-www-form-urlencoded',
                success: function(data){
                    $("#ajaxloading").hide();
                    var msg = data;
                    that.setState({
                        currentPartId:confId,
                        currentPartName:confName,
                        currentPartNo:NoId,
                    });
                    IfDispatcher.dispatch({
                        actionType: Comment.GETPARTICIPLEDATADDLIST,
                        getparticipledata:msg.data,
                        exporturl:null,
                        filtertext:null,
                    });
                },
                error:function(){
                    $("#ajaxloading").hide();
                },
                statusCode:{
                    414:function(){

                    },
                    406:function (){
                        IfDispatcher.dispatch({
                            actionType:Comment.LOGOUT
                        });
                    }
                }
            });
        }
        return inFunc;
    },
    deletePartFileClick(){
        $("#deletePartFile").modal("show");
    },
    deletePartFileConfirm(){
        var that = this;
        $.ajax({
            url: Comment.DATA_URL+"KDB/DeleteConfDefind",
            data: $.param({
                confDefineId:that.state.currentPartId,
                _method :"delete",
            }),
            type: 'POST',
            contentType: 'application/x-www-form-urlencoded',
            success: function (data) {
                $("#deletePartFile").modal("hide");
                that.getDataPart();
            },
            error:function(){
                $("#ajaxloading").hide();
            },
            statusCode:{
                406:function (){
                    IfDispatcher.dispatch({
                        actionType:Comment.LOGOUT
                    });
                },
                414:function(){
                },
                413:function(){
                }
            }
        });
    },
    render(){
        var pageHeight = this.props.propsheight;
        var propswidth = this.props.propswidth;
        var participlefile = this.props.participlemsg.partfileist;

        var Pfile =[];
        var zzlParticiple =[];
        var selectStylel =[];
        var selectColor = [];
        var datapart = [];
        var FileIsNull = "";
        if( participlefile.length > 0){
            for(var k=0;k<participlefile.length;k++){
                if(k == this.state.currentPartNo){
                    selectStylel[k]={float:"left",color:"#0F7BB5"};
                    selectColor[k]="#379cf8";
                }else{
                    selectStylel[k]={float:"left",color:"#fff"};
                    selectColor[k]="#000";
                }
            }
            for(var i in participlefile){
                Pfile.push(
                    <div className="row zzltr"  style={{marginLeft:"0px",marginRight:"0px",borderBottom:"1px solid #ccc"}}>
                        <div className="col-sm-11" style={{marginLeft:"-15px",marginRight:"0px"}}>
                            <a
                                role="button"
                                className="col-sm-12  btn btn-default zzlbuttoncol"
                                onClick={this.PartFilelistClick(participlefile[i].conf_define_id,i,participlefile[i].conf_name)}
                                style={{textAlign:"left",border:"0px none",marginTop:"5px",marginBottom:"5px",color:selectColor[i]}}
                                >
                                {participlefile[i].conf_name}

                            </a>
                        </div>
                        <div className="col-sm-1" style={{textAlign:"left",marginTop:"10px"}}>
                            <div  style={selectStylel[i]}>
                                 <span
                                     className="zzlselectstyle"
                                     >√</span>
                            </div>

                        </div>
                    </div>
                );
            }
            zzlParticiple.push(
                <div>
                    <div className="row" style={{height:(pageHeight-245+"px"),overflowY:"auto",marginTop:"0px",marginLeft:"0px",marginRight:"0px",background:"#fff"}}>
                        {Pfile}
                    </div>
                </div>
            );

            datapart=<Participle
                pageHeight={this.props.propsheight}
                loginInfo={this.props.loginInfo}
                currentPartFileId={this.state.currentPartId}
                currentPartName={this.state.currentPartName}
                getparticipledata={this.props.participlemsg.getpartdata}
                getexporturl={this.props.participlemsg.exporturl}
                getfiltertext={this.props.participlemsg.filtertext}
                >
            </Participle>;
        }else{
            FileIsNull = true;
        }

        var longerStyle = this.state.iflonger?{}:{display:"none"};
        var emptyStyle  = this.state.ifempty?{}:{display:"none"};
        var existStyle  = this.state.ifexist?{}:{display:"none"};
        var specialStyle  = this.state.ifspecial?{}:{display:"none"};
        return(
            <div>
                <div  style={{float:"left",height:(pageHeight-60+"px"),width:"400px"}}>
                    <div className="row" style={{height:"130px",marginLeft:"0px",marginRight:"0px"}}>
                        <div className="row" style={{height:"60px",marginLeft:"0px",marginRight:"0px"}}>
                            <div className="col-sm-12"  style={{marginTop:"5px"}}>
                                <h4 style={{color:"#379cf8"}}>Tokens List</h4>
                            </div>
                        </div>
                        <div className="row" style={{background:"#fff",height:"70px",marginLeft:"0px",marginRight:"0px"}}>
                            <div className="col-sm-6">
                                <button className="btn btn-default zzlbuttoncolborder" role="button"
                                        style={{width:"70px",marginTop:"20px"}}
                                        onClick={this.addPartFileclick}
                                    >
                                    Create
                                </button>
                            </div>
                            <div className="col-sm-6" style={{textAlign:"right"}}>
                                <button className="btn btn-default zzlbuttoncolborder" role="button"
                                        style={{width:"70px",marginTop:"20px"}}
                                        onClick={this.deletePartFileClick}
                                        disabled={FileIsNull}
                                    >
                                    Delete
                                </button>
                            </div>
                        </div>

                    </div>
                    <div className="row" style={{height:(pageHeight-190+"px"),marginLeft:"0px",marginRight:"0px"}}>
                        <div className="row" style={{height:"40px",marginLeft:"0px",marginRight:"0px",marginTop:"15px",borderBottom:"1px solid #ccc"}}>
                            <div className="col-sm-6" style={{textAlign:"left"}}>
                                <span style={{color:"#379cf8"}}>File Name</span>
                            </div>
                            <div className="col-sm-6" style={{textAlign:"right"}}>
                                <span style={{color:"#379cf8"}}>Current Logo</span>
                            </div>
                        </div>
                        {zzlParticiple}
                    </div>
                </div>
                <div style={{float:"left",height:(pageHeight-60+"px"),width:"5px",background:"#F1F1F1"}}>
                </div>
                <div style={{float:"left",width:(propswidth-405+"px"),height:(pageHeight-60+"px")}}>
                    {datapart}
                </div>

                <div className="modal fade" id="addPartFile" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                                <h4 className="modal-title">New Token FileName</h4>
                            </div>
                            <div className="modal-body">
                                <form>
                                    <div className="form-group">
                                        <label htmlFor="mediaNameInput">Token File Name</label>
                                        <input type="text" className="form-control"  placeholder="File Name"
                                               onChange={this.handleChange.bind(this,"PartFilename")} value={this.state.PartFilename}/>
                                    </div>
                                </form>
                            </div>
                            <div className="modal-footer">
                                <div className="row">
                                    <div className="col-md-8">
                                        <div style={emptyStyle}>
                                            <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                Token File name  can't be empty!
                                            </div>
                                        </div>
                                        <div style={existStyle}>
                                            <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                Please enter a unique name for your Token File!
                                            </div>
                                        </div>
                                        <div style={specialStyle}>
                                            <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                Your Token File Name not allowed to enter special characters, please try again.
                                            </div>
                                        </div>
                                        <div style={longerStyle}>
                                            <div className="alert alert-danger col-md-12" role="alert" style={{marginBottom:"-5px",marginTop:"-10px"}} >
                                                Please enter no more than 40 characters.
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-4">
                                        <a type="button" className="btn btn-default" data-dismiss="modal">Cancel</a>
                                        <a type="button" className="btn btn-primary" onClick={this.addPartFileConf}>Confirm</a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
                <div className="modal fade" id="deletePartFile" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                                <h4 className="modal-title" >Confirm The Delete</h4>
                            </div>
                            <div className="modal-body">
                                <h3>
                                    Are you sure to delete current Token file?
                                </h3>

                            </div>
                            <div className="modal-footer">
                                <a type="button" className="btn btn-default" data-dismiss="modal">Cancel</a>
                                <a type="button"
                                   onClick={this.deletePartFileConfirm}
                                   className="btn btn-primary" >Confirm</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
});