/**
 * Created by ��־�� on 2017/2/20.
 */
import {Store} from 'flux/utils';
import {IfDispatcher} from "./dispatcher";
import {Comment} from "../comment";
var projectsList = [];

var proname="";
var projectid = null;
var approvedatalist=[];
class ProjectsStore extends Store{
    getAll(){
        return projectsList;
    }

    getProname() {
        if(localStorage.getItem("proname")){
            proname = JSON.parse(localStorage.getItem("proname"));
        }
        return proname;
    }
    getBll(){
        if(localStorage.getItem("projectid")){
            projectid = JSON.parse(localStorage.getItem("projectid"));
        }
        return projectid;

    }
    getApprovenum(){
        return approvedatalist;
    }
    __onDispatch(payload){
        if(payload.actionType == Comment.GETPROJECTSLIST){
            projectsList = payload.projectslist;
            this.__emitChange();
        }
        else if(payload.actionType == Comment.PROJECTCLICK){
            proname = payload.porname;
            projectid = payload.projectid;
            localStorage.setItem("proname",JSON.stringify(proname));
            localStorage.setItem("projectid",JSON.stringify(projectid));
            this.__emitChange();
        }
        else if(payload.actionType == Comment.GETAPPROVERNUM){
            approvedatalist = payload.approvedatalist;
            this.__emitChange();
        }
    }
}

export var projectsStore = new ProjectsStore(IfDispatcher);