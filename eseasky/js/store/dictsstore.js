/**
 * Created by ��־�� on 2017/2/22.
 */
import {Store} from 'flux/utils';
import {IfDispatcher} from "./dispatcher";
import {Comment} from "../comment";
var dictsList = [];
var dictdataList =[];
var inputtext = [];
class DictsStore extends Store{
    getAll(){
        return dictsList;
    }
    getBll(){
        return dictdataList;
    }
    getCll(){
        return inputtext;
    }
    __onDispatch(payload){
        if(payload.actionType == Comment.GETDICTSLIST){
            dictsList = payload.dictslist;
            this.__emitChange();
        }
        else if(payload.actionType == Comment.DELETEDICT){
            this.__emitChange();
        }
        else if(payload.actionType == Comment.GETDICTDATA){
            dictdataList = payload.dictdatalist;
            inputtext  = [];
            this.__emitChange();
        }
        else if(payload.actionType == Comment.EDITINDEXTEXT){
            //inputtext[payload.index] = payload.text;
            inputtext[payload.index] = {
                text:payload.text,
                iferror:payload.iferror,
                ifsourceeqltar:payload.ifsourceeqltar,
                ifduplicate:payload.ifduplicate,
                iflucked:payload.iflucked,
                ifexist:payload.ifexist,
                confictdata:payload.confictdata,
                //ifnameerror:payload.ifnameerror,
                //iflonger:payload.iflonger,
            };
            this.__emitChange();
        }
    }
}

export var dictsStore = new DictsStore(IfDispatcher);