/**
 * Created by ��־�� on 2017/2/20.
 */
import {Store} from 'flux/utils';
import {IfDispatcher} from "./dispatcher";
import {Comment} from "../comment";

var loginInfo = {
    name:null,
    pass:null,
    userid:null,
    ifadmin:null,
    uuid:null,
    iftamr:false,
    iffrist:false,
};

class LoginStore extends Store{
    getAll(){
        //if(localStorage.getItem("loginInfo")){
        //    loginInfo = JSON.parse(localStorage.getItem("loginInfo"));
        //}
        if($.cookie('loginInfo')){
            //alert($.cookie('loginInfo'));
            loginInfo = JSON.parse($.cookie('loginInfo'));
        }
        return loginInfo;
    }
    __onDispatch(payload){
        if(payload.actionType == Comment.LOGINSUCCESS){
            loginInfo.name = payload.name;
            loginInfo.pass   = payload.pass;
            loginInfo.ifadmin = payload.ifadmin;
            loginInfo.userid = payload.userid;
            loginInfo.iffrist = payload.iffrist;
            loginInfo.uuid = null;
            loginInfo.iftamr = false;
            //localStorage.setItem("loginInfo",JSON.stringify(loginInfo));
            $.cookie('loginInfo',JSON.stringify(loginInfo),{path: '/'});
            this.__emitChange();
        }
        else if(payload.actionType == Comment.TAMRLOGINSUCCESS){
            loginInfo.name = payload.name;
            loginInfo.pass   = payload.pass;
            loginInfo.ifadmin = payload.ifadmin;
            loginInfo.userid = payload.userid;
            loginInfo.uuid = payload.uuid;
            loginInfo.iftamr = true;
            loginInfo.iffrist = false;
            $.cookie('authToken',payload.uuid,{path: '/'});
            $.cookie('loginInfo',JSON.stringify(loginInfo),{path: '/'});
            //alert($.cookie('loginInfo'));
            this.__emitChange();
        }
        else if(payload.actionType == Comment.ERROR500){
            //if(loginInfo.iftamr){
            //    $.cookie('authToken',"",{path: '/'});
            //}
            loginInfo.ifadmin = null;
            loginInfo.pass = null;
            loginInfo.name = null;
            loginInfo.userid = null;
            loginInfo.uuid = null;
            loginInfo.iftamr = false;
            loginInfo.iffrist = false;
           // localStorage.setItem("loginInfo",JSON.stringify(loginInfo));
            $.cookie('loginInfo',JSON.stringify(loginInfo),{path: '/'});
            this.__emitChange();
        }
        else if(payload.actionType == Comment.LOGOUT){
            //if(loginInfo.iftamr){
            //    $.cookie('authToken',"",{path: '/'});
            //}
            loginInfo.ifadmin = null;
            loginInfo.pass = null;
            loginInfo.name = null;
            loginInfo.userid = null;
            loginInfo.uuid = null;
            loginInfo.iftamr = false;
            loginInfo.iffrist = false;
            //localStorage.setItem("loginInfo",JSON.stringify(loginInfo))
            $.cookie('loginInfo',JSON.stringify(loginInfo),{path: '/'});
            localStorage.setItem("proname",JSON.stringify(null));
            localStorage.setItem("projectid",JSON.stringify(null));
            this.__emitChange();
        }
    }
}

export var loginStore = new LoginStore(IfDispatcher);