/**
 * Created by zzl on 2017/6/1.
 */
import {Store} from 'flux/utils';
import {IfDispatcher} from "./dispatcher";
import {Comment} from "../comment";

var participlemsg = {
    partfileist:[],
    getpartdata:[],
    exporturl:null,
    filtertext:null,
};
class ParticipleStore extends Store{
    getA(){
        return participlemsg;
    }
    __onDispatch(payload){
        if(payload.actionType == Comment.GETPARTICIPLELIST){
            participlemsg.partfileist = payload.participleList;
            participlemsg.getpartdata = payload.getparticipledata;
            this.__emitChange();
        }
        else if(payload.actionType == Comment.GETPARTICIPLELISTZZL){
            participlemsg.partfileist = payload.participleList;
            this.__emitChange();
        }
        else if(payload.actionType == Comment.GETPARTICIPLEDATALIST){
            participlemsg.getpartdata = payload.getparticipledata;
            this.__emitChange();
        }else if(payload.actionType == Comment.GETPARTICIPLEDATADDLIST){
            participlemsg.getpartdata = payload.getparticipledata;
            participlemsg.exporturl = payload.exporturl;
            participlemsg.filtertext = payload.filtertext;
            this.__emitChange();
        }
        else if(payload.actionType == Comment.GETPARTICIPLEDATADDLISTZZL){
            participlemsg.filtertext = payload.filtertext;
            this.__emitChange();
        }
        else if(payload.actionType == Comment.GETPARTICIPLEDATADDLISTZZLURL){
            participlemsg.exporturl = payload.exporturl;
            this.__emitChange();
        }
    }
}

export var participleStore = new ParticipleStore(IfDispatcher);
