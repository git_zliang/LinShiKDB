/**
 * Created by hasee on 2017/3/1.
 */
import {Store} from 'flux/utils';
import {IfDispatcher} from "./dispatcher";
import {Comment} from "../comment";
var featuredeflist = [];
var featureDetlist =[];
class DatafeaturesStore extends Store{
    getfedeflist(){
        return featuredeflist;
    }
    getfedetlist() {
        return featureDetlist;
    }
    __onDispatch(payload){
        if(payload.actionType == Comment.GETFEATURESdEF){
            featuredeflist = payload.featuredeflist;
            this.__emitChange();
        }
        else if(payload.actionType == Comment.GETFEATUREDETAIL){
            featureDetlist = payload.featureDetlist;
            this.__emitChange();
        }
    }
}

export var datafeaturesStore = new DatafeaturesStore(IfDispatcher);
