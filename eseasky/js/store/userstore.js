/**
 * Created by ��־�� on 2017/2/20.
 */
import {Store} from 'flux/utils';
import {IfDispatcher} from "./dispatcher";
import {Comment} from "../comment";
var usersList = [];

class UsersStore extends Store{
    getAll(){
        return usersList;
    }
    __onDispatch(payload){
        if(payload.actionType == Comment.GETUSERLIST){
            usersList = payload.userlist;
            this.__emitChange();
        }
        else if(payload.actionType == Comment.DELETEUSER){
            this.__emitChange();
        }
    }
}

export var usersStore = new UsersStore(IfDispatcher);