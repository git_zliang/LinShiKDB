/**
 * Created by ��־�� on 2017/2/27.
 */
import {Store} from 'flux/utils';
import {IfDispatcher} from "./dispatcher";
import {Comment} from "../comment";
var dictdataList = [];
var dictfiledata =[];
var projectdictlist = [];
var featuredeflist = [];
var featuredetlist = [];
var featureproject = [];
var viewodsbds = [];
var sourcesdata = [];
var metadatas = [];
var tablesmetadata = {
    columnnamedata:[],
    columnname:[],
};
var ifrunbarmsg ={

};
var participleconflist=[];
var partpledatalist=[];
var partpleprojectlist=[];
class DictdataStore extends Store{
    getAll(){
        return dictdataList;
    }
    getBll(){
        return dictfiledata;
    }
    getProjectDict(){
        return projectdictlist;
    }

    getfeaturedeflist(){
        return featuredeflist;
    }
    getfeaturedetlist(){
        return featuredetlist;
    }
    getfeatureproject() {
        return featureproject;
    }
    getCll(){
        return sourcesdata;

    }
    getDll(){
        return viewodsbds;
    }
    getFll(){
        return metadatas;
    }
    getVll(){
        return tablesmetadata;
    }
    getZll(){
        return ifrunbarmsg;
    }
    getKll(){
        return participleconflist;
    }
    getPll(){
        return partpledatalist;
    }
    getPPl(){
        return partpleprojectlist;
    }
    __onDispatch(payload){
        if(payload.actionType == Comment.GETDICTDATALIST){
            dictdataList = payload.dictslist;
            this.__emitChange();
        }
        else if(payload.actionType == Comment.DELETEDICTFILE){
            this.__emitChange();
        }
        else if(payload.actionType == Comment.GETDICTDATAFILE){
            dictfiledata = payload.dictdatalist;
            this.__emitChange();
        }
        else if(payload.actionType == Comment.GETPROJECTDICT){
            projectdictlist = payload.projectdictlist;
            this.__emitChange();
        }else if(payload.actionType == Comment.GETSOUREBDSODS){
            sourcesdata = payload.sourcesdata;
            this.__emitChange();
        }
        else if(payload.actionType == Comment.GETFEATURESdEFLIST){
            featuredeflist = payload.featuredeflist;
            this.__emitChange();
        }
        else if(payload.actionType == Comment.GETFEATUREDETAILLIST){
            featuredetlist = payload.featuredetlist;
            this.__emitChange();
        }
        else if(payload.actionType == Comment.GETFEATUREPROJECT){
            featureproject = payload.featureprojectlist;
            this.__emitChange();
        }
        else if(payload.actionType == Comment.GETSUCCESSBDSODS){
            viewodsbds = payload.viewodsbds;
            this.__emitChange();
        }else if(payload.actionType == Comment.GETMETADATA){
            metadatas = payload.metadatas;
            this.__emitChange();
        }else if(payload.actionType == Comment.GETTABLESMETADATA){
            tablesmetadata.columnnamedata = payload.tablesmetadata;
            tablesmetadata.columnname = payload.columnname;
            this.__emitChange();
        }else if(payload.actionType == Comment.GETIFRUNBAR){
            ifrunbarmsg.ifrun = payload.ifrun;
            ifrunbarmsg.ifbar = payload.ifbar;
            ifrunbarmsg.timebar = payload.timebar;
            ifrunbarmsg.projectid = payload.projectid;
            this.__emitChange();
        }else if(payload.actionType == Comment.GETIFRUNBARID){
            ifrunbarmsg.timebarId = payload.timebarId;
            this.__emitChange();
        }else if(payload.actionType == Comment.GETIFRUNBARTIME){
            ifrunbarmsg.ifbar = payload.ifbar;
            ifrunbarmsg.timebar = payload.timebar;
            this.__emitChange();
        }else if(payload.actionType == Comment.GETIFRUNBARRUN){
            ifrunbarmsg.ifrun = payload.ifrun;
            this.__emitChange();
        }else if(payload.actionType == Comment.GETIFRUNBARS){
            ifrunbarmsg.ifrun = payload.ifrun;
            ifrunbarmsg.ifbar = payload.ifbar;
            ifrunbarmsg.timebar = payload.timebar;
            this.__emitChange();
        }else if(payload.actionType == Comment.GETPARTICIPLECONFLIST){
            participleconflist = payload.participleconflist;
            this.__emitChange();
        }
        else if(payload.actionType == Comment.GETPARTICIPLEDATALZZ){
            partpledatalist = payload.partpledatalist;
            this.__emitChange();
        }
        else if(payload.actionType == Comment.GETPARTICIPLEPROJECT){
            partpleprojectlist = payload.partpleprojectlist;
            this.__emitChange();
        }
    }
}

export var dictdataStore = new DictdataStore(IfDispatcher);