/**
 * Created by ��־�� on 2016/10/19.
 */
import React from 'react';
import ReactDom from 'react-dom';
import {Router, Route, IndexRoute, Link, hashHistory,browserHistory } from 'react-router';
import {App} from "./code/app";
import {Account} from "./code/account";
import {Staff} from "./code/staff";
import {Dictdata} from "./code/dictdata";
import {Fronpage} from "./code/fronpage" ;
import {Project} from "./code/project";
import {Dicts} from "./code/dicts";
import {Datafeature} from "./code/datafeature";
import {Dictapprove} from "./code/dictapprove";
import {Tamrlogin} from "./code/tamrlogin";
import {PartData} from "./code/participledata";
var main = document.getElementsByTagName('main')[0];

ReactDom.render(
<Router history={hashHistory}>
        <Route path='/' component={App}>
            <IndexRoute component={Fronpage}/>
            <Route path="fronpage" component={Fronpage} />
            <Route path="dictdata" component={Dictdata} />
            <Route path="account" component={Account} />
            <Route path="staff" component={Staff} />
            <Route path="dicts" component={Dicts} />
            <Route path="project" component={Project} />
            <Route path="datafeaure" component={Datafeature} />
            <Route path="dictapprove" component={Dictapprove} />
            <Route path="tamrlogin" component={Tamrlogin} />
            <Route path="partdata" component={PartData}/>
        </Route>
</Router>,
    main
);