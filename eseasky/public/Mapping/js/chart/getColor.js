/**
 * @namespace UCD
 */
var UCD = UCD || {
    Core: jQuery
};
(function(ucd, $) { // "use strict";
    var UCDStyle = "gray";
    /**
     * 设置图表风格
     *
     * @function setStyle
     * @memberOf UCD
     * @static
     * 
     * @param {String} [sty = "gray"] "gray" || "black"
     */
    ucd.setStyle = function(sty) {
        UCDStyle = sty || "gray"; //风格分为gray、black
    }

    /**
     * 获取图表风格
     *
     * @function setStyle
     * @memberOf UCD
     * @static
     * 
     * @return {String} 图表风格
     */
    ucd.getStyle = function() {
        return UCDStyle;
    }

    /**
     * 告警颜色
     *
     * @function getAlarmColors
     * @memberOf UCD
     * @static
     * 
     * @param  {Array} alarmArray 告警级别数据[1,2,3]
     * @return {Array}            [description]
     */
    ucd.getAlarmColors = function(alarmArray) {
        var newColors = [];
        var colors = [{
            level: 1,
            color: "#AF1010"
        }, {
            level: 2,
            color: "#FF9900"
        }, {
            level: 3,
            color: "#E0C53F"
        }, {
            level: 4,
            color: "#83AECA"
        }];
        for (var i = 0; i < alarmArray.length; i++) {
            for (var j = 0; j < colors.length; j++) {
                if (alarmArray[i] == colors[j].level) {
                    newColors.push(colors[j].color);
                    break;
                }
            }
        }
        return newColors;
    }

    var UCDChartColors;

    /**
     * 设置图表颜色集合，默认使用图表专项设计的颜色，请慎重修改。
     *
     * @function setColors
     * @memberOf UCD
     * @static
     * 
     * @param {Array} colors 图表颜色集合
     */
    ucd.setColors = function(colors) {
        UCDChartColors = colors;
    }

    /**
     * 获取图表颜色集合
     *
     * @function getColors
     * @memberOf UCD
     * @static
     * 
     * @param  {Number} count 数量
     * @return {Array}        图表颜色集合
     */
    ucd.getColors = function(count) {
        if (!UCDChartColors) {
            var colors = ["#CC8F52", "#CCA352", "#CCB852", "#CCCC52", "#B8CC52",
                "#A3CC52", "#8FCC52", "#7BCC52", "#66CC52", "#52CC52",
                "#52CC66", "#52CC7B", "#52CC8F", "#52CCA3", "#52CCB8",
                "#52CCCC", "#52B8CC", "#52A3CC", "#528FCC", "#527BCC"
            ];
            var counts = [1, 2, 3, 4, 6, 8, 13, colors.length];
            //
            var pos = 0,
                start = 0,
                end = counts.length - 1;
            for (var i = 0; i < counts.length; i++) {

                if (count > counts[start] && start + 1 <= counts.length - 1 && count >= counts[start + 1]) {
                    start++;
                }
                if (count < counts[end] && end - 1 >= 0 && count <= counts[end - 1]) {
                    end--;
                }
                if (Math.abs(start - end) <= 1) {
                    break;
                }
            }
            if (count > counts[start]) {
                count = counts[end];
            } else {
                count = counts[start];
            }
            var arr = new Array();

            for (var i = 0; i < count; i++) {
                pos = Math.ceil(colors.length / (count + 1) * (i + 1)) - 1;
                var curColor = colors[pos];
                arr.push(curColor);
            }
            return arr;
        } else {
            return UCDChartColors.slice(0, count);
        }
    }

})(UCD, UCD.Core);
