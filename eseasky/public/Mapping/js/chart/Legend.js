var UCD = UCD || {Core:jQuery};
(function(ucd)
{
	var id = 0;
	var counter= 0;

	var defaultOptions =
	{
		width: 8,
		height: 8,
		name:[],
		color:[]
	}

	function Legend(container, options)
	{
		this.$container = $(container);
		this.container = document.createElement('div');
		this.container.setAttribute('id', 'Legends_' + id++);
		this.owner = new UCD.MyD3();
		this.$container.append(this.container);
		this.owner.container(this.container);
		this.options = $.extend({}, defaultOptions, options);
	}

	var proto = Legend.prototype;

	proto.init = function()
	{
		var opts = this.options;
		for(var i = 0; opts.name[i]; i ++)
		{
			this.appendLegend();
		}
	}

	proto.appendLegend = function()
	{
		var opts = this.options;
		var legend = this.owner.append('div', true).css('display', 'inline-block').attr('id', 'Legend_' + counter);
		var legendImg = legend.append('div', true).css('display', 'inline-block').css('width', opts.width + 'px').css('height', opts.height + 'px').css('background-color', opts.color[counter]).css('margin-left', '20px');
		var legendName = legend.append('p', true).css('display', 'inline-block').css('margin', '0').css('margin-left', '6px');
		legendName.owner.innerHTML= opts.name[counter];
		counter ++;
	}

	ucd.Legend = Legend;
}(UCD))