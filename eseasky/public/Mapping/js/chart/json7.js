var json7 = 
{
 "name":"nodes",
 "children" :
 [
   {
     "name": "Participate","size": 0.093,
     "children": [
      {"name": "Custmoer", "size": 0.03,"num": 61},
      {"name": "Channel", "size": 0.05,"num": 83},
      {"name": "Partner", "size": 0.01,"num": 22},
      {"name": "Competitor", "size": 0.003,"num": 2}
     ]
   },
   {
     "name": "Resource","size": 0.10,
     "children": [
      {"name": "Internet Resource", "size": 0.003,"num": 2},
      {"name": "Network Resource", "size": 0.02,"num": 33},
      {"name": "Service Resource", "size": 0.05,"num": 88},
      {"name": "Terminal Resource", "size": 0.027,"num": 51}
     ]
   },
    {
     "name": "Event","size": 0.563,
     "children": [
      {"name": "Channel Contact", "size": 0.1,"num": 180},
      {"name": "Network log", "size": 0.03,"num": 62},
      {"name": "Network Performance", "size": 0.003,"num": 2},
      {"name": "Network Signaling", "size": 0.19,"num": 320},
      {"name": "Service Log", "size": 0.17,"num": 338},
      {"name": "Settlement Record", "size": 0.02,"num": 31},
      {"name": "System Log", "size": 0.02,"num": 38},
      {"name": "Test Log", "size": 0.03,"num": 5}
     ]
   },
 {
     "name": "Finance ","size": 0.09,
     "children": [
      {"name": "Finance management", "size": 0.05,"num": 86},
      {"name": "Finance Relationship", "size": 0.04,"num": 54}
     ]
   },
   {
     "name": "Offering","size": 0.11,
     "children": [
      {"name": "Product", "size": 0.07,"num": 116},
      {"name": "Subscriber", "size": 0.04,"num": 63},
     ]
   },
   {
     "name": "Management","size": 0.03,
     "children": [
      {"name": "Management", "size": 0.03,"num": 80}
     ]
   },
    {
   "name": "Marketing ","size": 0.014,
   "children": [
    {"name": "Marketing Activity", "size": 0.014,"num": 22}
   ]
 }
  ]
}
