var nodes = [			
			{"col":0, "row": 0},
			{"col":0, "row": 1},
			{"col":0, "row": 2},
			{"col":0, "row": 3},
			{"col":0, "row": 4},
			{"col":0, "row": 5},
			{"col":0, "row": 6},
			{"col":0, "row": 7},
			{"col":0, "row": 8},
			{"col":0, "row": 9},
			{"col":0, "row": 10},
			{"col":0, "row": 11},
			{"col":0, "row": 12},
			{"col":0, "row": 13},
			{"col":0, "row": 14},
			{"col":0, "row": 15},
			{"col":0, "row": 16},
			{"col":0, "row": 17},
			{"col":1, "row": 0},
			{"col":1, "row": 1},
			{"col":1, "row": 2},
			{"col":1, "row": 3},
			{"col":1, "row": 4},
			{"col":1, "row": 5},
			{"col":1, "row": 6},
			{"col":1, "row": 7},
			{"col":1, "row": 8},
			{"col":1, "row": 9},
			{"col":1, "row": 10},
			{"col":1, "row": 11},
			{"col":1, "row": 12},
			{"col":1, "row": 13},
			{"col":1, "row": 14},
			{"col":1, "row": 15},
			{"col":1, "row": 16},
			{"col":1, "row": 17}
		];
// console.log(nodes);

var Sankey_data = {
	"nodes": nodes,
	"links":[
			{"source":0,"target":18, "percent":0.7},
			{"source":2,"target":18, "percent":0.1},
			{"source":4,"target":19, "percent":0.65},
			{"source":6,"target":20, "percent":0.75},
			{"source":7,"target":21, "percent":0.75},
			{"source":8,"target":21, "percent":0.2},
			{"source":9,"target":22, "percent":0.72},
			{"source":10,"target":22, "percent":0.25},
			{"source":10,"target":23, "percent":0.42},
			{"source":12,"target":24, "percent":0.7},
			{"source":13,"target":25, "percent":0.57},
			{"source":14,"target":25, "percent":0.48},
			{"source":15,"target":26, "percent":0.68},
			{"source":15,"target":27, "percent":0.20},
			{"source":16,"target":28, "percent":0.40},
			{"source":17,"target":28, "percent":0.18}
		]
};


